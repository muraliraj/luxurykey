<?php add_action('init', 'create_sliders', 0);

function create_sliders() {
    $labels = array(
        'name' => _x('sliders', 'post type general name'),
        'singular_name' => _x('sliders', 'post type singular name'),
        'add_new' => _x('Add sliders', 'sliders'),
        'add_new_item' => __('Add sliders'),
        'edit_item' => __('Edit sliders'),
        'new_item' => __('New sliders'),
        'view_item' => __('View sliders'),
        'search_items' => __('Search sliders'),
        'not_found' => __('No sliders found'),
        'not_found_in_trash' => __('No sliders found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'slider','with_front' => FALSE,),
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 7,
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes')
    );

    register_post_type('sliders', $args);
    register_taxonomy("sliders_cat", "sliders", array("hierarchical" => true,
        "label" => "slider Categories",
        "singular_label" => "slider",
        'rewrite' => array('slug' => 'slider','with_front' => FALSE,),
        "query_var" => true,
        "show_ui" => true
            )
    );

}
?>