<?php
/*
* Template Name: Home
*/
?>
<?php
  get_header();

$villargs = array(
    'numberposts' =>-1,
    'post_type' => 'mphb_room_type',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_status'=>"publish"
	);
$villapages = get_posts($villargs);
?>
        <!-- First banner and search -->
         <?php
        $arg_banner = array(
           'numberposts' => -1,
           'orderby' => 'menu_order',
           'order' => 'ASC',
           'post_type' => 'banners'
         );
        $home_banners = get_posts($arg_banner);
        ?>
        <div class="hero-banner dark-theme">
        	<div class="main-banner">
            <?php
            foreach ($home_banners as $home_banner){
              $image_banner = wp_get_attachment_url(get_post_thumbnail_id($home_banner->ID));
              ?>
        		<img src="<?php echo $image_banner; ?>" alt="bannerimage">
            <?php } ?>
       		</div>
        	<div class="hero-banner-desc-center">
        		<div class="container">
					<div class="hero-banner-content w-100">
						<div class="c-heading">
							<h1><?php echo $home_banner->post_title; ?></h1>
							<h3><?php echo $home_banner->post_excerpt; ?></h3>
							<div class="mobile-banner">
					        	<div class="searchbar">
					        		<div class="searchbar-input">
					        			<a class="popup-with-zoom-anim" href="#small-dialog">
						        			<i class="fa fa-search" aria-hidden="true"></i>
						          			<span>Where do you want to go?</span>
					          			</a>
						            </div>
					        	</div>
					        </div>
						</div>
						<div class="search-bar-wrapper">
							<form method="post" action="<?php echo get_bloginfo('url'); ?>/new-listing" id="form-villa" >
								<input type="hidden" name="villa-destination" value="" id="destination"/>
								<input type="hidden" name="villa-purpose" value="" id="purpose"/>
								<input type="hidden" name="villa-check-in" value="" id="check-in"/>
								<input type="hidden" name="villa-check-out" value="" id="check-out"/>
								<input type="hidden" name="villa-price" value="" id="price"/>
								<input type="hidden" name="villa-price-min" value="" id="min-price"/>
								<input type="hidden" name="villa-price-max" value="" id="max-price"/>
							<div class="row">
								<div class="col-2">
									<div class="form-row select-list" id="mapicon">
										<div class="drop-links links-icon">
											<span class="filter">Destination</span>
											<ul class="select destination-sel">
                        <?php

								$place = array();
								foreach($villapages as $villas) {

								$title = $villas->post_title; 
									$tarr= explode('|', $title);
									 $place[] = $tarr[2];
										 } 
										 $place=array_filter(array_unique($place));
										 // var_dump($place[3]);
											foreach ($place as  $placeun) {
												if ($placeun!==	NULL) {
												echo '<li class="select-list-item" data-value="'.$placeun.'" >'.$placeun.'</li>';
												}
											}

							?>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-2">
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Purpose</span>
											<i class="fa fa-map-marker-alt"></i>
											<ul class="select purpose-sel">
					                        <?php
					                        $Proargs = array(
					                          'numberposts' =>-1,
					                          'post_type' => 'purpose',
					                          'orderby' => 'menu_order',
					                          'order' => 'ASC',
					                          'post_status'=>'publish'
					                        );
					                        $villaPros = get_posts($Proargs);
					                        foreach($villaPros as $villaPro) {
					                           $titlePro = $villaPro->post_title;
					                        ?>
											    <li class="select-list-item" data-value="<?php echo $titlePro; ?>"><?php echo $titlePro; ?></li>
                        					<?php } ?>
											</ul>
										</div>
									</div>
								</div>
								<div class="col-2">
									<div class="form-row select-list">
										<span class="input-item home-date" id="date-home">Check in - Check out</span>
										<div class="daterangepicker-options">
											<h3>Flexible Days</h3>  
											<div class="checkbox-group">
												<div class="checkboxradio">
				            						<div class="checkboxradio-row">
				            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" />
				            							<label class="checkboxradio-label radio-label" for="threedays"> +3/-3 Days
				            							</label>
				            						</div>
			            						</div>
			            						<div class="checkboxradio">
				            						<div class="checkboxradio-row">
				            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" />
				            							<label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
				            							</label>
				            						</div>
			            						</div>
			            						<div class="checkboxradio">
				            						<div class="checkboxradio-row">
				            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" />
				            							<label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
				            							</label>
				            						</div>
			            						</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-2">
									<div class="form-row select-list">
										<input type="number" value="1" id="noOfRoom" name="no-of-bedrooms" min="1">
										<span id="defaultText">Bed</span>
										<button type="button" class="homeadds adds">+</button>
										<button type="button" class="homesubs subs" disabled >-</button>
									</div>
								</div>
								<div class="col-2">
									<div class="form-row select-list data-list-input">
										<div class="drop-links">
											<span class="filter">Price</span>
											<i class="fa fa-map-marker-alt"></i>
											<ul class="select data-list-input price-sel">
											    <li class="select-list-item" data-value="500-2999">Silver $500 – 2,999/day</li>
											    <li class="select-list-item" data-value="3000-5999">Gold $3,000 – 5999/day</li>
											    <li class="select-list-item" data-value="6000-9999">Platinum $6,000 – 9.999/day</li>
											    <li class="select-list-item" data-value="10000-16000">Diamond 10.000 – 16.000/day</li>
											</ul>
										</div>
									</div>
								</div>

								<div class="col-2">
									<!-- <div class="button button-primary">
										<a href="#" id="villa-search">Search</a>
									</div> -->
									<input type="submit" class="button button-primary" value="submit" id="villa-search_NULL">
									 
									<!-- <button class="booking-submit button button-primary">
										<a href="#">Search</a>
									</button> -->
								</div>
							</div>
							<div class="topDestinations">
								<?php echo $post->post_content; ?>
							</div>
							</form>
						</div>
					</div>
				</div>
        	</div>
        	<div class="location-now">
				<a href="#"><span>SECRET COVE,</span> KAUAI</a>
			</div>
        	<!-- <div class="goto-section">
	        	<div class="goto-bottom"></div>
	        	<div class="goto-text">more</div>
	        </div> -->
        </div>
      	<!--End of First banner and search -->
		<!--  concirge section slider cards -->
		<!-- End of concirge section slider cards -->
		<!-- Start of Frame 3 slider -->

		    <!-- mobile dialoge banner -->
        <div id="small-dialog" class="homeFilter zoom-anim-dialog mfp-hide">
        	<div class="search-bar-wrapper search-bar-mobile">
				<div class="modalHeader">
	        		<span>Search</span>
	        	</div>
	        	<div class="form-row select-list" id="mapicon">
					<div class="drop-links links-icon">
						<span class="filter">Destination</span>
						<ul class="select">
							<li data-value="Mykonos">Mykonos</li>
						    <li data-value="Tulum">Tulum</li>
						    <li data-value="Ibiza">Ibiza</li>
					      	<li data-value="Santorini">Santorini</li>
							<li data-value="Other Greek Islands">Other Greek Islands</li>
							<li data-value="Private Islands">Private Islands</li>
							<li data-value="Other Global Destinations">Other Global Destinations</li>
						</ul>
					</div>
				</div>
				<div class="form-row select-list">
					<span class="input-item-mob">Check in - Check out</span>
					<div class="daterangepicker-options-mob">
						<h3>Flexible Days</h3>  
						<div class="checkbox-group">
							<div class="checkboxradio">
        						<div class="checkboxradio-row">
        							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedaysmob" type="radio" />
        							<label class="checkboxradio-label radio-label" for="threedaysmob"> +3/-3 Days
        							</label>
        						</div>
    						</div>
    						<div class="checkboxradio">
        						<div class="checkboxradio-row">
        							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendaysmob" type="radio" />
        							<label class="checkboxradio-label radio-label" for="sevendaysmob"> +7/-7 Days
        							</label>
        						</div>
    						</div>
    						<div class="checkboxradio">
        						<div class="checkboxradio-row">
        							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonthmob" type="radio" />
        							<label class="checkboxradio-label radio-label" for="wholemonthmob"> Whole Month
        							</label>
        						</div>
    						</div>
						</div>
					</div>
				</div>
				<div class="form-row select-list">
					<div class="drop-links">
						<span class="filter">Purpose</span>
						<ul class="select ">
						    <li data-value="Villa Rental">Villa Rental</li>
						    <li data-value="Real Estate">Real Estate</li>
						    <li data-value="Concierge Services">Concierge Services</li>
					      	<li data-value="Events">Events</li>
							<li data-value="Weddings">Weddings</li>
							<li data-value="Destination Experiences">Destination Experiences</li>
						</ul>
					</div>
				</div>
				<div class="form-row select-list">
					<input type="number" value="1" id="noOfRoom" min="1"> 
					<span id="defaultText">Bedroom</span>
					<button type="button" class="adds">+</button>
					<button type="button" class="subs" disabled >-</button>
				</div>
				<div class="form-row select-list data-list-input">
					<div class="drop-links">
						<span class="filter">Price</span>
						<ul class="select data-list-input">
						    <li data-value="Silver $500 – 2,999/day">Silver $500 – 2,999/day</li>
						    <li data-value="Gold $3,000 – 5999/day">Gold $3,000 – 5999/day</li>
						    <li data-value="Platinum $6,000 – 9.999/day">Platinum $6,000 – 9.999/day</li>
						    <li data-value="Diamond 10.000 – 16.000/day">Diamond 10.000 – 16.000/day</li>
						</ul>
					</div>
				</div>
				<div class="button button-primary">
					<a href="#">Search</a>
				</div>
			</div>
		</div>
        <!-- mobile dialoge banner -->
        <!-- End of Frame 3 slider -->
		<!-- villa selection section -->
		
		<!--End of villa selection section -->

		<?php
						$subPageArgs = array(
						   'post_parent'   => $post->ID,
						   'post_type'     => 'page',
						   'order'         => 'ASC',
						   'orderby'       => 'menu_order',
						   'post_status'   => 'publish',
						   'numberposts' => -1
						);
						$subPages = get_posts($subPageArgs);
							if (is_array($subPages) && count($subPages)>0) {
						   	foreach ($subPages as $subPage) {
							   $postId = $subPage->ID;
							   $pageTemplate = get_post_meta($postId, '_wp_page_template', true);
							   $getTempPath = TEMPLATEPATH .'/'. $pageTemplate;
							   include TEMPLATEPATH .'/'. $pageTemplate;
						   	}
						}
				?>

		<!--footer start --> 

    <?php	
echo '<button id="villa-search" >test</button>';

    get_footer(); ?>

