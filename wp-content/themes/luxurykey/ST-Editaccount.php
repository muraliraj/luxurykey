<?php 
/***********************
Template Name: Edit account
************************/
get_header();
$current_user = wp_get_current_user();
	$password = sanitize_text_field($_POST['new-password']);     
	$userdata = array(
	    'ID'        =>  $current_user->ID,
	    'user_pass' =>  $password
	);  

	$user_id = wp_update_user($userdata);
	$current_user_id = get_current_user_id();
$user_first= get_user_meta( $current_user_id, 'first_name_reg', $single );

	$user_last = get_user_meta( $current_user_id, last_name_reg, $single );
	$user_phone = get_user_meta( $current_user_id, 'reg_phone', $single );
$user_salut = get_user_meta( $current_user_id, 'reg_salut', $single );
$country_code = get_user_meta( $current_user_id, 'cont_code', $single );
$salut = get_user_meta( $current_user_id, 'salt', $single );
if(count($_POST)>0) {
	
	$key = 'last_name';
	$single = true;
	
	// update_user_meta($current_user->ID,'edi-salut');
	// update_user_meta($current_user->ID,'edit_uname');
	// update_user_meta($current_user->ID,'edit_isd');
	// update_user_meta($current_user->ID,'edit_mobil');
// 	             update_user_meta( $user_id, 'salt', $_POST['salt'] );
       				update_user_meta( $user_id, 'salt', $_POST['edi_salut'] );
				update_user_meta( $user_id, 'reg_phone', $_POST['edit_phone']);
				update_user_meta( $user_id, 'first_name_reg', $_POST['edit_fname']);
                               update_user_meta( $user_id, 'last_name_reg', $_POST['edit_lname']);
						 update_user_meta( $user_id, 'cont_code', $_POST['country_code']);

	if($user_id == $current_user->ID){
		wp_redirect( home_url() );
	} else {
	    echo 'error';
	}   
}

$user = wp_get_current_user();
//$last_name=get_user_meta( $user->ID, 'last_name', true );

?>       <!-- End of header -->
        	
        	<div class="container-fluid">
        		<div class="row accout-tile">
	        		<aside class="col-5">
	        			<div class="accout-tile-left">
		        			<h1><?php echo $user->display_name; ?></h1>
		        			<ul>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/my-account/" class="active no-effect"><i class="fa fa-user" aria-hidden="true"></i>My Account</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/booking/" class="no-effect"><i class="fa fa-bookmark" aria-hidden="true"></i>My Booking</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/history/" class="no-effect"><i class="fa fa-unlock-alt" aria-hidden="true"></i>History</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/change-password" class="no-effect"><i class="fa fa-history" aria-hidden="true"></i>Change Password</a></li>
		        				<li><a href="<?php echo wp_logout_url(home_url()); ?>" class="no-effect"><i class="fa fa-sign-out" aria-hidden="true"></i>Log Out</a></li>
		        			</ul>
		        		</div>
	        		</aside>
	        		<div class="col-7">
	        			<div class="col-6 edit-user-details accout-tile-content">
			        			<h3>Edit User Detail</h3>
			        			<!-- <div class="edit-user"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></div> -->
		        			<form method="post" action="">
		        				<div class="form-row">
									 <select class="select-menu" id="edi_salut"   name="edi_salut" data-placeholder="SALUTATION" required>
										<option value="Mr"<?php selected( $salut, "Mr" ); ?>>Mr.</option>
										<option value="Mrs" <?php selected( $salut, "Mrs" ); ?>>Mrs.</option>
										<option value="Ms" <?php selected( $salut, "Ms" ); ?>>Ms.</option>
									</select> 
									
								<!-- 	<div class="error-message" id="error-salutation">Please select an salutation</div> -->
								</div>
								<div class="form-row">
									<label class="floating-item" data-error="Please enter your Name">
	<input type="text" id="edit_uname" class="floating-item-input input-item" name="edit_fname" value="<?php echo $user_first;?>" required />
										<span class="floating-item-label">First Name</span>
									</label>
									<div class="error-message" id="error-uname">Please enter your Name</div>
								</div>
		        				<div class="form-row">
									<label class="floating-item" data-error="Please enter your Name">
	<input type="text" id="edit_uname" class="floating-item-input input-item" name="edit_lname" value="<?php echo $user_last;?>" required />
										<span class="floating-item-label">Last Name</span>
									</label>
									<div class="error-message" id="error-uname">Please enter your Name</div>
								</div>
						
								<div class="country-mobile">
									<div class="input-row form-row">
										<input type="hidden" id="country_code" class="floating-item-input input-item" name="country_code" value="" />

										<div class="select-list">
							              	<div class="drop-links">
							                  	<span class="filter" id="defaultText"><?php echo $country_code; ?></span>

							                  	<ul class="select overflow-y country-mobile no_beds " >
												    <li data-conty="+213">Algeria (+213)</li>
												    <li data-conty="+376">Andorra (+376)</li>
												    <li data-conty="+244">Angola (+244)</li>
												    <li data-conty="+1264">Anguilla (+1264)</li>
												    <li data-conty="+1268">Antigua Barbuda (+1268)</li>
												    <li data-conty="+54">Argentina (+54)</li>
												    <li data-conty="+374">Armenia (+374)</li>
												    <li data-conty="+297">Aruba (+297)</li>
												    <li data-conty="+61">Australia (+61)</li>
												    <li data-conty="+43">Austria (+43)</li>
												    <li data-conty="+994">Azerbaijan (+994)</li>
												    <li data-conty="+1242">Bahamas (+1242)</li>
												    <li data-conty="+973">Bahrain (+973)</li>
												    <li data-conty="+880">Bangladesh (+880)</li>
												    <li data-conty="+1246">Barbados (+1246)</li>
												    <li data-conty="+375">Belarus (+375)</li>
												    <li data-conty="+32">Belgium (+32)</li>
												    <li data-conty="+501">Belize (+501)</li>
												    <li data-conty="+229">Benin (+229)</li>
												    <li data-conty="+1441">Bermuda (+1441)</li>
												    <li data-conty="+975">Bhutan (+975)</li>
												    <li data-conty="+591">Bolivia (+591)</li>
												    <li data-conty="+387">Bosnia Herzegovina (+387)</li>
												    <li data-conty="+267">Botswana (+267)</li>
												    <li data-conty="+55">Brazil (+55)</li>
												    <li data-conty="+673">Brunei (+673)</li>
												    <li data-conty="+359">Bulgaria (+359)</li>
												    <li data-conty="+226">Burkina Faso (+226)</li>
												    <li data-conty="+257">Burundi (+257)</li>
												    <li data-conty="+855">Cambodia (+855)</li>
												    <li data-conty="+237">Cameroon (+237)</li>
												    <li data-conty="+1">Canada (+1)</li>
												    <li data-conty="+238">Cape Verde Islands (+238)</li>
												    <li data-conty="+1345">Cayman Islands (+1345)</li>
												    <li data-conty="+236">Central African Republic (+236)</li>
												    <li data-conty="+56">Chile (+56)</li>
												    <li data-conty="+86">China (+86)</li>
												    <li data-conty="+57">Colombia (+57)</li>
												    <li data-conty="+269">Comoros (+269)</li>
												    <li data-conty="+242">Congo (+242)</li>
												    <li data-conty="+682">Cook Islands (+682)</li>
												    <li data-conty="+506">Costa Rica (+506)</li>
												    <li data-conty="+385">Croatia (+385)</li>
												    <li data-conty="+53">Cuba (+53)</li>
												    <li data-conty="+90392">Cyprus North (+90392)</li>
												    <li data-conty="+357">Cyprus South (+357)</li>
												    <li data-conty="+42">Czech Republic (+42)</li>
												    <li data-conty="+45">Denmark (+45)</li>
												    <li data-conty="+253">Djibouti (+253)</li>
												    <li data-conty="+1809">Dominica (+1809)</li>
												    <li data-conty="+1809">Dominican Republic (+1809)</li>
												    <li data-conty="+593">Ecuador (+593)</li>
												    <li data-conty="+20">Egypt (+20)</li>
												    <li data-conty="+503">El Salvador (+503)</li>
												    <li data-conty="+240">Equatorial Guinea (+240)</li>
												    <li data-conty="+291">Eritrea (+291)</li>
												    <li data-conty="+372">Estonia (+372)</li>
												    <li data-conty="+251">Ethiopia (+251)</li>
												    <li data-conty="+500">Falkland Islands (+500)</li>
												    <li data-conty="+298">Faroe Islands (+298)</li>
												    <li data-conty="+679">Fiji (+679)</li>
												    <li data-conty="+358">Finland (+358)</li>
												    <li data-conty="+33">France (+33)</li>
												    <li data-conty="+594">French Guiana (+594)</li>
												    <li data-conty="+689">French Polynesia (+689)</li>
												    <li data-conty="+241">Gabon (+241)</li>
												    <li data-conty="+220">Gambia (+220)</li>
												    <li data-conty="+7880">Georgia (+7880)</li>
												    <li data-conty="+49">Germany (+49)</li>
												    <li data-conty="+233">Ghana (+233)</li>
												    <li data-conty="+350">Gibraltar (+350)</li>
												    <li data-conty="+30">Greece (+30)</li>
												    <li data-conty="+299">Greenland (+299)</li>
												    <li data-conty="+1473">Grenada (+1473)</li>
												    <li data-conty="+590">Guadeloupe (+590)</li>
												    <li data-conty="+671">Guam (+671)</li>
												    <li data-conty="+502">Guatemala (+502)</li>
												    <li data-conty="+224">Guinea (+224)</li>
												    <li data-conty="+245">Guinea - Bissau (+245)</li>
												    <li data-conty="+592">Guyana (+592)</li>
												    <li data-conty="+509">Haiti (+509)</li>
												    <li data-conty="+504">Honduras (+504)</li>
												    <li data-conty="+852">Hong Kong (+852)</li>
												    <li data-conty="+36">Hungary (+36)</li>
												    <li data-conty="+354">Iceland (+354)</li>
												    <li data-conty="+91">India (+91)</li>
												    <li data-conty="+62">Indonesia (+62)</li>
												    <li data-conty="+98">Iran (+98)</li>
												    <li data-conty="+964">Iraq (+964)</li>
												    <li data-conty="+353">Ireland (+353)</li>
												    <li data-conty="+972">Israel (+972)</li>
												    <li data-conty="+39">Italy (+39)</li>
												    <li data-conty="+1876">Jamaica (+1876)</li>
												    <li data-conty="+81">Japan (+81)</li>
												    <li data-conty="+962">Jordan (+962)</li>
												    <li data-conty="+7">Kazakhstan (+7)</li>
												    <li data-conty="+254">Kenya (+254)</li>
												    <li data-conty="+686">Kiribati (+686)</li>
												    <li data-conty="+850">Korea North (+850)</li>
												    <li data-conty="+82">Korea South (+82)</li>
												    <li data-conty="+965">Kuwait (+965)</li>
												    <li data-conty="+996">Kyrgyzstan (+996)</li>
												    <li data-conty="+856">Laos (+856)</li>
												    <li data-conty="+371">Latvia (+371)</li>
												    <li data-conty="+961">Lebanon (+961)</li>
												    <li data-conty="+266">Lesotho (+266)</li>
												    <li data-conty="+231">Liberia (+231)</li>
												    <li data-conty="+218">Libya (+218)</li>
												    <li data-conty="+417">Liechtenstein (+417)</li>
												    <li data-conty="+370">Lithuania (+370)</li>
												    <li data-conty="+352">Luxembourg (+352)</li>
												    <li data-conty="+853">Macao (+853)</li>
												    <li data-conty="+389">Macedonia (+389)</li>
												    <li data-conty="+261">Madagascar (+261)</li>
												    <li data-conty="+265">Malawi (+265)</li>
												    <li data-conty="+60">Malaysia (+60)</li>
												    <li data-conty="+960">Maldives (+960)</li>
												    <li data-conty="+223">Mali (+223)</li>
												    <li data-conty="+356">Malta (+356)</li>
												    <li data-conty="+692">Marshall Islands (+692)</li>
												    <li data-conty="+596">Martinique (+596)</li>
												    <li data-conty="+222">Mauritania (+222)</li>
												    <li data-conty="+269">Mayotte (+269)</li>
												    <li data-conty="+52">Mexico (+52)</li>
												    <li data-conty="+691">Micronesia (+691)</li>
												    <li data-conty="+373">Moldova (+373)</li>
												    <li data-conty="+377">Monaco (+377)</li>
												    <li data-conty="+976">Mongolia (+976)</li>
												    <li data-conty="+1664">Montserrat (+1664)</li>
												    <li data-conty="+212">Morocco (+212)</li>
												    <li data-conty="+258">Mozambique (+258)</li>
												    <li data-conty="+95">Myanmar (+95)</li>
												    <li data-conty="+264">Namibia (+264)</li>
												    <li data-conty="+674">Nauru (+674)</li>
												    <li data-conty="+977">Nepal (+977)</li>
												    <li data-conty="+31">Netherlands (+31)</li>
												    <li data-conty="+687">New Caledonia (+687)</li>
												    <li data-conty="+64">New Zealand (+64)</li>
												    <li data-conty="+505">Nicaragua (+505)</li>
												    <li data-conty="+227">Niger (+227)</li>
												    <li data-conty="+234">Nigeria (+234)</li>
												    <li data-conty="+683">Niue (+683)</li>
												    <li data-conty="+672">Norfolk Islands (+672)</li>
												    <li data-conty="+670">Northern Marianas (+670)</li>
												    <li data-conty="+47">Norway (+47)</li>
												    <li data-conty="+968">Oman (+968)</li>
												    <li data-conty="+680">Palau (+680)</li>
												    <li data-conty="+507">Panama (+507)</li>
												    <li data-conty="+675">Papua New Guinea (+675)</li>
												    <li data-conty="+595">Paraguay (+595)</li>
												    <li data-conty="+51">Peru (+51)</li>
												    <li data-conty="+63">Philippines (+63)</li>
												    <li data-conty="+48">Poland (+48)</li>
												    <li data-conty="+351">Portugal (+351)</li>
												    <li data-conty="+1787">Puerto Rico (+1787)</li>
												    <li data-conty="+974">Qatar (+974)</li>
												    <li data-conty="+262">Reunion (+262)</li>
												    <li data-conty="+40">Romania (+40)</li>
												    <li data-conty="+7">Russia (+7)</li>
												    <li data-conty="+250">Rwanda (+250)</li>
												    <li data-conty="+378">San Marino (+378)</li>
												    <li data-conty="+239">Sao Tome &amp; Principe (+239)</li>
												    <li data-conty="+966">Saudi Arabia (+966)</li>
												    <li data-conty="+221">Senegal (+221)</li>
												    <li data-conty="+381">Serbia (+381)</li>
												    <li data-conty="+248">Seychelles (+248)</li>
												    <li data-conty="+232">Sierra Leone (+232)</li>
												    <li data-conty="+65">Singapore (+65)</li>
												    <li data-conty="+421">Slovak Republic (+421)</li>
												    <li data-conty="+386">Slovenia (+386)</li>
												    <li data-conty="+677">Solomon Islands (+677)</li>
												    <li data-conty="+252">Somalia (+252)</li>
												    <li data-conty="+27">South Africa (+27)</li>
												    <li data-conty="+34">Spain (+34)</li>
												    <li data-conty="+94">Sri Lanka (+94)</li>
												    <li data-conty="+290">St. Helena (+290)</li>
												    <li data-conty="+1869">St. Kitts (+1869)</li>
												    <li data-conty="+1758">St. Lucia (+1758)</li>
												    <li data-conty="+249">Sudan (+249)</li>
												    <li data-conty="+597">Suriname (+597)</li>
												    <li data-conty="+268">Swaziland (+268)</li>
												    <li data-conty="+46">Sweden (+46)</li>
												    <li data-conty="+41">Switzerland (+41)</li>
												    <li data-conty="+963">Syria (+963)</li>
												    <li data-conty="+886">Taiwan (+886)</li>
												    <li data-conty="+7">Tajikstan (+7)</li>
												    <li data-conty="+66">Thailand (+66)</li>
												    <li data-conty="+228">Togo (+228)</li>
												    <li data-conty="+676">Tonga (+676)</li>
												    <li data-conty="+1868">Trinidad &amp; Tobago (+1868)</li>
												    <li data-conty="+216">Tunisia (+216)</li>
												    <li data-conty="+90">Turkey (+90)</li>
												    <li data-conty="+7">Turkmenistan (+7)</li>
												    <li data-conty="+993">Turkmenistan (+993)</li>
												    <li data-conty="+1649">Turks &amp; Caicos Islands (+1649)</li>
												    <li data-conty="+688">Tuvalu (+688)</li>
												    <li data-conty="+256">Uganda (+256)</li>
												    <li data-conty="+380">Ukraine (+380)</li>
												    <li data-conty="+971">United Arab Emirates (+971)</li>
												    <li data-conty="+598">Uruguay (+598)</li>
												    <li data-conty="+7">Uzbekistan (+7)</li>
												    <li data-conty="+678">Vanuatu (+678)</li>
												    <li data-conty="+379">Vatican City (+379)</li>
												    <li data-conty="+58">Venezuela (+58)</li>
												    <li data-conty="+84">Vietnam (+84)</li>
												    <li data-conty="+1284">Virgin Islands - British (+1284)</li>
												    <li data-conty="+1340">Virgin Islands - US (+1340)</li>
												    <li data-conty="+681">Wallis &amp; Futuna (+681)</li>
												    <li data-conty="+969">Yemen (North)(+969)</li>
												    <li data-conty="+967">Yemen (South)(+967)</li>
												    <li data-conty="+260">Zambia (+260)</li>
												    <li data-conty="+263">Zimbabwe (+263)</li>
											  	</ul> 
											 
											</div>                                   
										</div>
									</div>
									<div class="form-row">
										<label class="floating-item input-animate" data-error="Please enter mobile number">
<input type="text" id="edit_mobil" class="floating-item-input input-item edit_mobil" name="edit_phone" value="<?php echo $user_phone; ?>" required="">
											<span class="floating-item-label">Mobile number</span>
										</label>
										<div class="error-message" id="err_mobile">Please Enter your Mobile Number</div>
									</div>
								</div>

								

								<div class="button button-primary">
									<button id="edt_acc">Submit</button>
								</div>
</form>
		        			

		        		</div>
	        		</div>
	        	</div>
        	</div>
		</div>	
<?php get_footer(); ?>