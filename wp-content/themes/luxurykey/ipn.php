<?php 
get_header();
$featImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$segments = explode(' ', $post->post_title);

$title = '';

for ($i = 0; $i < count($segments); $i++) {
    if ($i === 1) {
        $title .= ' <strong>' . $segments[$i] . '</strong>';  
        continue;
    } 

    $title .= ' ' . $segments[$i];
}

echo $html;
var_dump($_POST);
?>
<section id="generic-row">
	<div class="hero-banner golden-bg">
		<img src="<?php echo $featImage; ?>" alt="generic-banner" />
		<div class="hero-banner-desc-center">
			<div class="hero-banner-content">
				<div class="slide-top"></div>
					<h1><?php echo $title; ?></h1>
				<div class="slide-bottom"></div>
			</div>
		</div>
	</div>
	<?php echo apply_filters('the_content', $post->post_content); ?>		
</section>
<?php get_footer(); ?>