       
<?php
/**********************
Template Name: Diamond page
***********************/ 
get_header('search');
 $curTerm = get_queried_object(); 
 $curTermId = $curTerm->term_id;
 $args = array(
     'numberposts' => -1,
     'orderby' => 'date',
     'post_status'    => 'publish',
     'order' => 'DESC',
     'tax_query' => array(
       array(
       'taxonomy' => 'mphb_room_type_category',
       'field' => 'term_id',
       'terms' => array($curTermId),
       )
     ),
     'post_type' => 'mphb_room_type'
   );
   $categoryBlogs = get_posts($args);
?>
<div class="filter-top">
    		<div class="container container-type3">
    			<div class="row feature-row">
    				<?php
					$args = array(
						'post_type'=> 'mphb_room_type',
						'post_status' => 'publish',
						'orderby'=>'date',
						'order' => 'ASC',
						'numberposts' => 3
					);
					$banner_posts = get_posts($args);
   ?>
                            <?php   foreach ($banner_posts as $banner_post) {
            // echo $banner_post->ID;
            $gallerymeta  = get_post_meta( $banner_post->ID, 'mphb_gallery', true );
            $galleryimgs = explode(',', $gallerymeta);
            $galleryimgs = array_filter($galleryimgs);
            ?>

				</div>
    		</div>
    	</div>
        <section class="section-feature section-init">
	        <div class="container container-type3 feature-container" >	        	
	        	<div class="row feature-row listRow toggle">
	        		<div class="col-12 listView">
	        			<?php foreach ($categoryBlogs as $categoryBlog) {
	        				?>
		        		<div class="col-4 listItem" id="villa-map2">
					        <div class="feature-item">
								<div class="feature-item-slider">
									<div>
					        			<img src="<?php echo get_bloginfo('template_url'); ?>/img\frame41.jpeg" alt="">
					        		</div>
					        		<div>
					        			<img src="<?php echo get_bloginfo('template_url'); ?>/img\frame42.jpeg" alt="">
					        		</div>
					        		<div>
					        			<img src="<?php echo get_bloginfo('template_url'); ?>/img\frame41.jpeg" alt="">
					        		</div>
					        		<div>
					        			<img src="<?php echo get_bloginfo('template_url'); ?>/img\wide_cottage.jpeg" alt="">
					        		</div>
								</div>
								<div class="feature-item-content">
									<h5>4 BEDROOMS, 8 GUESTS, 3 BATHROOMS</h5>
									<h3><?php echo $categoryBlog->post_title; ?></h3>
									<p><?php echo $categoryBlog->post_villa_price; ?></p>
									<p><a href="#!">Pyrgi</a>, Greece</p>
								</div>
								<div class="feature-item-top">
									<span class="fav-icon"></span>
								</div>
							</div>
						</div>
					<?php } ?>

	        		</div>
	        		<!-- <div class="col-6 mapView hide">
	        			<div style="position: relative;height: 100%;width: 100%;">
	    					<div id="map"></div>
	        			</div>
	        		</div> -->
	        	</div>
			</div>
		</section>

		<!--footer start -->

<?php get_footer(); ?>