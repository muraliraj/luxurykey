
<?php


	get_header('new'); 
	global $post;
		$villa_typeapi=get_post_meta( $post->ID, 'villa_type_api', true );

   /*api villa start*/
   if($villa_typeapi=='api')
   {
      $data_images = get_post_meta( $post->ID, 'mphb_gallery', true );



$data_images_arr = explode(',', $data_images);
$ratevilla = number_format((float)$_POST['rate'], 2, '.', '');

?>



        	<div class="hero-detail">
		    	<div class="detail-banner">
		    		<?php 
		    	foreach ($data_images_arr as  $data_image) {
	 ?>
		    		<a  data-fancybox="test-srcset"
						data-type="image"
						data-srcset="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>"
						data-width="1200"
						data-height="720"
						class="fancy-box" 
						href="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" >
						<img src="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" />
					</a>
	 
	 <?php
}
		    	?>

		    <?php } ?>
		    		
		    		
		   		</div>
		   	</div>



		   	<section class="section-divider section-villa">
		   		<div class="container container-type1">
		   			<div class="breadcrumbs content-page">
			         	<ul>
<?php
$args = array(
'post_type'=> 'mphb_room_type',
'post_status' => 'publish',
'orderby'=>'date',
'order' => 'ASC',
'numberposts' => 3
);
$detail_gallerys = get_posts($args);
?>


         <?php   foreach ($detail_gallerys as $detail_gallery) {


         	?>
         <?php }?>


				            <li><a href="#">Home </a></li>
				            <li><a href="#">Search Results - Mykanos </a></li>
				            <li><a class="active" href="#!"><?php echo $detail_gallery->post_title; ?></a></li>
				        </ul>
				    </div>
				    <div class="row">
	      				<div class="col-9">
	      					<div class="section-intro">

<?php
$args = array(
'post_type'=> 'mphb_room_type',
'post_status' => 'publish',
'orderby'=>'date',
'order' => 'ASC',
'numberposts' => 3
);
$detail_gallerys = get_posts($args);
?>


         <?php   foreach ($detail_gallerys as $detail_gallery) {
        $galleryvalid = get_post_meta( $detail_gallery->ID, 'bedrrommphb_room_type', true );
        $galleryvalid1 = get_post_meta( $detail_gallery->ID, 'mphb_adults_capacity', true );
        $galleryvalid2 = get_post_meta( $detail_gallery->ID, 'bathroommphb_room_type', true );
    
?>
   <?php }?>


	      					 <h5>PLATINUM VILLA</h5>
								<h3><?php echo $detail_gallery->post_title; ?></h3>
								<span><a href="#!">Pyrgi</a>, Greece. From $1,600 / night.</span>
								<h4><?php echo $galleryvalid; ?> BEDROOMS, <?php echo $galleryvalid1; ?> GUESTS, <?php echo $galleryvalid2; ?> BATHROOMS</h4>


	      					</div>
							<?php //content
               
                          echo apply_filters('the_content', $post->post_content); 
                           ?>


<?php //content

  echo apply_filters('the_content', $post->sub_content); 

?>		


	      				</div>
	      				<div class="col-3">
	      					<div class="card booking-card">
	      						<div class="border-heading br-left">
			      					<h3>Book this villa</h3>
			      				</div>
			      				<h5>NUMBER OF GUESTS</h5>
			      				<div class="form-row">
									<select class="select-menu"  name="selectmenu" data-placeholder="Select Guest" required>
										<option value="1">1 Guest</option>
										<option value="2">2 Guests</option>
										<option value="3">3 Guests</option>
										<option value="4">4 Guests</option>
										<option value="8">8 Guests</option>
									</select>
									<div class="error-message">Please select an Guest</div>
								</div>
								<h5>BEDROOMS</h5>
								<div class="form-row">
									<select class="select-menu"  name="selectmenu" data-placeholder="Select Bedroom" required>
										<option value="1">1 Bedroom</option>
										<option value="2">2 Bedrooms</option>
										<option value="3">3 Bedrooms</option>
										<option value="4">4 Bedrooms</option>
									</select>
									<div class="error-message">Please select an Bedroom</div>
								</div>
								<h5>DURATION OF STAY</h5>
								<div class="form-row select-list date-pick">
									<div class="input-item"><span>Check in</span> <i class="la la-arrow-right"></i> <span>Check out</span></div>
								</div>
								<h5>STYLE</h5>
								<div class="form-row">
									<select class="select-menu"  name="selectmenu" data-placeholder="Select Guest" required>
										<option value="diamond">Diamond</option>
										<option value="platinum">Platinum</option>
										<option value="gold">Gold</option>
										<option value="silver">Silver</option>
									</select>
									<div class="error-message">Please select an Guest</div>
								</div>
								<div class="booking-row">
									<div class="booking-total">
										<h6>Total</h6>
										<div class="booking-price">
											<h6>$6,200</h6>
											<p>($1,055 x 2 nights)</p>
											<div class="button button-primary type1 button-small">
												<button>Proceed</button>
											</div>
										</div>
									</div>	
								</div>
								<div class="booking-enq">
									<a href="#!">Send us an enquiry</a>
									<p><strong>Call us <a href="tel:0800 000 0000">0800 000 0000</a></strong></p>
								</div>
	      					</div>
	      				</div>
	      			</div>
		   		</div>
		   	</section>

	   	 	<section class="sandle-bg section-padding section-review">
		   		<div class="container container-type1">
		   			<div class="row">
		   				<div class="col-12">
		   					<h4>From our guests</h4>
		   				</div>
		   				<div class="col-4 review-card">
		   					<div class="card">
		   						<p>It was awesome!</p>
		   						<p>The Villa and location were both fantastic. The property was even better than in the photos. Access to the beach and three great restaurants in walking distance was a big bonus, plus the water taxi was available for a quick trip to Amalfi or Positano.</p>	
		   					</div>
		   					<div class="author">
	   							<h5>Tony D.</h5>
		   						<p>July 31, 2018</p>	
		   					</div>
		   				</div>
		   				<div class="col-4 review-card">
		   					<div class="card">
		   						<p>It was awesome!</p>
		   						<p>The Villa and location were both fantastic. The property was even better than in the photos. Access to the beach and three great restaurants in walking distance was a big bonus, plus the water taxi was available for a quick trip to Amalfi or Positano.</p>	
		   					</div>
		   					<div class="author">
	   							<h5>Tony D.</h5>
		   						<p>July 31, 2018</p>	
		   					</div>
		   				</div>
		   				<div class="col-4 review-card">
		   					<div class="card">
		   						<p>It was awesome!</p>
		   						<p>The Villa and location were both fantastic. The property was even better than in the photos. Access to the beach and three great restaurants in walking distance was a big bonus, plus the water taxi was available for a quick trip to Amalfi or Positano.</p>	
		   					</div>
		   					<div class="author">
	   							<h5>Tony D.</h5>
		   						<p>July 31, 2018</p>	
		   					</div>
		   				</div>
		   			</div>
		   		</div>
		   	</section>
		   	<div class="map-section">
    			<div class="map-section-inner">
					<div id="map"></div>
    			</div>
        	</div>

<?php
$args = array(
'post_type'=> 'mphb_room_type',
'post_status' => 'publish',
'orderby'=>'date',
'order' => 'ASC',
'numberposts' => 3
);
$detail_gallerys = get_posts($args);
?>



			<section class="section-feature section-init">
				<div class="container container-type1">
					<div class="row">
	      				<div class="col-12 text-center home-heading">
	      					<h3>We also recommend</h3>
	      				</div>
	      			</div>
					<div class="row feature-row">

						<?php   foreach ($detail_gallerys as $detail_gallery) {
// echo $banner_post->ID;
$gallerymeta  = get_post_meta( $detail_gallery->ID, 'mphb_gallery', true );
$galleryimgs = explode(',', $gallerymeta);
$galleryimgs = array_filter($galleryimgs);
$gallerymeta = get_post_meta( $detail_gallery->ID, 'bedrrommphb_room_type', true );
$gallerymeta1 = get_post_meta( $detail_gallery->ID, 'mphb_adults_capacity', true );
$gallerymeta2 = get_post_meta( $detail_gallery->ID, 'bathroommphb_room_type', true );
?>
<!-- loop -->
<div class="col-4">
						<div class="feature-item">
							<div class="feature-item-slider">
							<?php 

							if(!empty($galleryimgs))
							{
								$qwe=0;
								foreach ($galleryimgs as $galleryimg) {
									if($qwe == 3){
										break;
									}
									
									if($galleryimg!=''){ ?>



										<div>
										<img src="<?php  echo wp_get_attachment_image_url($galleryimg,'full');  ?>" alt="">
                                   
										</div>
										
										<?php
									}
									$qwe++;
								}
							}
							?>
							</div>
							<div class="feature-item-content">
								<h5><?php echo $gallerymeta; ?> BEDROOMS, <?php echo $gallerymeta1; ?> GUESTS, <?php echo $gallerymeta2; ?> BATHROOMS</h5>
								<h3><?php echo $detail_gallery->post_title; ?></h3>
								
								<p>from $1,164 / night</p>
								<p><a href="#!">Pyrgi</a>, Greece</p>
							</div>
						</div>
					</div>
					<?php }
?>

					</div>
				</div>
				
		</section>

<?php

get_footer();
?>