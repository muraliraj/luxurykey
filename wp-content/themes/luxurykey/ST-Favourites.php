<?php
get_header();
/***********************
Template Name: Favourites
 ************************/
$userDet = wp_get_current_user();
$favourites = get_user_favorites($userDet->ID);
$pageFeatImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
?>
<section class="section-feature section-init listing">
    <div class="mobile">
        <div class="toggleMap">
            MAP <i class="la la-map-marker"></i>
        </div>
    </div>
    <div class="container container-type3 feature-container" >              
        <div class="row feature-row listRow toggle">
            <div class="col-12 listView" id="villa_list">
                <?php 
				
                    if(count($favourites)>0 && $userDet->ID!=''){
                    $villargs_all = array(
                        'numberposts' =>-1,
                        'post_type' => 'mphb_room_type',
                        'orderby' => 'menu_order',
                        'order' => 'ASC',
                        'post_status'=>"publish"
                        );
                    $villapages = get_posts($villargs_all);
                foreach($villapages as $villapage) { 
                    if(!in_array($villapage->ID, $favourites)){
						continue;
					}
                    $title = $villapage->post_title;
                    $galleryimgs = explode(',', $gallerymeta);
                    $galleryimgs = array_filter($galleryimgs);
                    $villa_bath   = get_post_meta( $villapage->ID, 'bathroommphb_room_type', true );
                    $villa_pool  = get_post_meta( $villapage->ID, 'villa_pool', true );
                    $villa_guest  = get_post_meta( $villapage->ID, 'mphb_adults_capacity', true );
                    $villa_address  = get_post_meta( $villapage->ID, 'villa_address', true );
                    $villa_code  = get_post_meta( $villapage->ID, 'villa_code', true );
                    $villa_bed  = get_post_meta( $villapage->ID, 'bedrrommphb_room_type', true );
                    $destination  = get_post_meta( $villapage->ID, 'villa_locations', true );
                    $purpose = get_post_meta( $villapage->ID, 'villa_purpose', true );
                    $featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID));
                    $villa_typeapi=get_post_meta( $villapage->ID, 'villa_type_api', true );
                    $amn_villa = wp_get_object_terms( $villapage->ID, "mphb_room_type_facility");
                    $gallerymeta  = get_post_meta( $villapage->ID, 'mphb_gallery', true );
                    $galleryimgs = explode(',', $gallerymeta);
                    $galleryimgs = array_filter($galleryimgs);
                    $Address  = get_post_meta( $villapage->ID, 'villa_address', true );
                    if($_POST!=''){
                        if($checkin!='' && $checkout!='')
                        {
                            $villa_url .= get_permalink($villapage->ID).'?checkin='.$checkin.'&checkout='.$checkout;
                        }
                        if($villa_bed != ''){
                            $villa_url .= '&beds='.$villa_bed;  
                        }
                        if($villa_guest != ''){
                            $villa_url .= '&guests='.$villa_guest;  
                        }
                        if($villa_guest != ''){
                            $villa_url .= '&price_typ='.$prc_typ;   
                        }
                        if($villa_prc != ''){
                            $villa_url .= '&villa_pc='.$villa_prc;  
                        }
                    }else{
                        $villa_url = get_permalink($villapage->ID);
                    }
                    ?>
                    <div class="col-4 listItem" id="villa-<?php echo $villapage->ID; ?>">

                        <div class="feature-item">
                            <!-- <a href="<?php echo $villa_url; ?>"> -->


                                    
                                    <div class="feature-item-slider">
                                        <?php 
                                        if(!empty($galleryimgs)){
                                            foreach ($galleryimgs as $key => $galleryimg) {
                                                ?>

                                                <div>
                                                    <a href='<?php echo get_permalink($villapage->ID); ?>' class='no-effect' data-url="<?php echo get_permalink($villapage->ID); ?>">
                                                        <img src="<?php echo wp_get_attachment_image_url($galleryimg,'full'); ?>" alt="">
                                                    </a>
                                                </div>
                                                <?php 
                                            }
                                        }else{

                                            ?>
                                            <div>
                                                <a href='<?php echo get_permalink($villapage->ID); ?>' class='no-effect' data-url="<?php echo get_permalink($villapage->ID); ?>" >
                                                    <img src="<?php echo get_bloginfo('template_url'); ?>/banner2.jpeg" alt="">
                                                </a>
                                            </div>
                                        <?php } ?>
                                        
                                    </div>
                                <div class="feature-item-content">
                                    <h5>
                                        <?php
                                        if($villa_bed != ''){
                                            echo '<span class="villa-cont">'.$villa_bed." BEDROOMS </span>";
                                        }
                                        if($villa_guest != ''){
                                            echo '<span class="villa-cont">'.$villa_guest." GUESTS </span>";
                                        }
                                        if($villa_bath != ''){
                                            echo '<span class="villa-cont">'.$villa_bath."BATHROOMS </span>";
                                        }
                                        ?>
                                    </h5>
                                    <h3>
                                        <a href='<?php echo get_permalink($villapage->ID); ?>' class='no-effect' data-url="<?php echo get_permalink($villapage->ID); ?>" >
                                            <?php echo $title; ?>
                                        </a>
                                    </h3>
                                    <?php

                                    if($destination!=''){
                                        ?>
                                        <p>
                                            <a href="#!">
                                                <?php echo ($Address); ?>
                                            </a> 

                                        </p>
                                    <?php } ?>
                                </div>

                                <!--Favorites Button start-->
                                <div class="feature-item-top">
                                    <?php echo do_shortcode('[favorite_button post_id="' . $villapage->ID . '"]'); ?>       
                                </div>
                                <!--Favorites Button End-->

                            </div>

                        </div>
                        <?php  
                        $villa_url = '';
                    }   
                    }

                    ?>
                </div>
                <div class="col-6 mapView hide">
                    <div style="position: relative;height: 100%;width: 100%;">
                        <div id="map"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    get_footer();
    ?>
    
    <script>

   //var beaches =[{"Latitude":19.1237488,"Longitude":74.74072,"DisplayName":"AHMEDNAGAR","MapId":"villa-map2"},{"Latitude":12.9864693,"Longitude":77.55996,"DisplayName":"BANGALORE","MapId":"villa-map1"},{"Latitude":11.3079433,"Longitude":75.75859,"DisplayName":"CALICUT","MapId":"villa-map4"},{"Latitude":28.3890839,"Longitude":79.4095,"DisplayName":"BAREILLY","MapId":"villa-map3"},{"Latitude":30.71387,"Longitude":76.80766,"DisplayName":"CHANDIGARH","MapId":"villa-map6"},{"Latitude":12.93924,"Longitude":80.12999,"DisplayName":"CHENNAI","MapId":"villa-map5"},{"Latitude":11.0051937,"Longitude":76.9749146,"DisplayName":"COIMBATORE","MapId":"villa-map8"},{"Latitude":8.873611,"Longitude":76.62989,"DisplayName":"KOLLAM","MapId":"villa-map9"},{"Latitude":17.7319031,"Longitude":83.3166351,"DisplayName":"VISAKHAPATNAM","MapId":"villa-map10"}];
   var beaches=[<?php echo $villa_map ;?>];

//var beaches=[{'Latitude':37.44529,'Longitude':25.32872,'DisplayName':'Mykonos','MapId':'villa-map2'},{'Latitude':37.44529,'Longitude':25.32872,'DisplayName':'Mykonos','MapId':'villa-map3'},{'Latitude':37.44529,'Longitude':25.32872,'DisplayName':'Mykonos','MapId':'villa-map4'}];

    //var beaches =[{"Latitude":19.1237488,"Longitude":74.74072,"DisplayName":"AHMEDNAGAR","MapId":"villa-map2"},{"Latitude":12.9864693,"Longitude":77.55996,"DisplayName":"BANGALORE","MapId":"villa-map1"},{"Latitude":11.3079433,"Longitude":75.75859,"DisplayName":"CALICUT","MapId":"villa-map4"},{"Latitude":28.3890839,"Longitude":79.4095,"DisplayName":"BAREILLY","MapId":"villa-map3"},{"Latitude":30.71387,"Longitude":76.80766,"DisplayName":"CHANDIGARH","MapId":"villa-map6"},{"Latitude":12.93924,"Longitude":80.12999,"DisplayName":"CHENNAI","MapId":"villa-map5"},{"Latitude":11.0051937,"Longitude":76.9749146,"DisplayName":"COIMBATORE","MapId":"villa-map8"},{"Latitude":8.873611,"Longitude":76.62989,"DisplayName":"KOLLAM","MapId":"villa-map9"},{"Latitude":17.7319031,"Longitude":83.3166351,"DisplayName":"VISAKHAPATNAM","MapId":"villa-map10"}];
    var markers = [];
    function filterSelectedState(selectedLocationName) {
        var filteredLocations = [];

        if(selectedLocationName === null) {
            filteredLocations = beaches;
        } 
        return filteredLocations;
    }
    //initMap(filterSelectedState(null));
    function initMap(locaData) {
        // Data for the markers consisting of a name, a LatLng and a zIndex for the
        // order in which these markers should display on top of each other.
        // var beaches = [
        //   ['Bondi Beach', -33.890542, 151.274856, 4],
        //   ['Coogee Beach', -33.923036, 151.259052, 5],
        //   ['Cronulla Beach', -34.028249, 151.157507, 3],
        //   ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
        //   ['Maroubra Beach', -33.950198, 151.259302, 1]
        // ];   
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom:10,
          //center: new google.maps.LatLng(12.93924,80.12999),
          center: new google.maps.LatLng(37.44529, 25.32872),
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });
        var country = "Mykonos";
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode( {'address' : country}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
            }
        });

        //setMarkers(map);
        Array.prototype.groupBy = function(prop) {
          return this.reduce(function(groups, item) {
            const val = item[prop]
            groups[val] = groups[val] || []
            groups[val].push(item)
            return groups
        }, {})
      };

        // Info window
        var i;
        var infowindow = new google.maps.InfoWindow();
        var infoWindowContent = [];
        for (var i = 0; i < locaData.length; i++) {
          infoWindowContent[i] = getInfoWindowDetails(locaData[i]);
          // Adds markers to the map.
          // Marker sizes are expressed as a Size of X,Y where the origin of the image
          // (0,0) is located in the top left of the image.

          // Origins, anchor positions and coordinates of the marker increase in the X
          // direction to the right and in the Y direction down.
          var image = {
            url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32)
        };
        var  pin = {
          path: "M0-40c-8.28 0-15 6.7-15 15C-15-10 0 0 0 0s15-10 15-25c0-8.28-6.72-15-15-15zm0 17.5c-2.76 0-5-2.25-5-5s2.25-5 5-5c2.76 0 5 2.25 5 5s-2.25 5-5 5z",
          fillColor: "#b49759",
          fillOpacity: .8,
          scale: 1,
          strokeColor: "white",
          strokeWeight: 3,
      };
          // Shapes define the clickable region of the icon. The type defines an HTML
          // <area> element 'poly' which traces out a polygon as a series of X,Y points.
          // The final coordinate closes the poly by connecting to the first coordinate.
          var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
        };

        var beach = locaData[i];
        var bounds = new google.maps.LatLngBounds();
        var latlngM = new google.maps.LatLng(locaData[i].Latitude, locaData[i].Longitude);
        var marker = new google.maps.Marker({
            // position: {lat: beach.Latitude, lng: beach.Longitude},
            position: new latlngM,
            map: map,
            icon: pin,
            shape: shape,
            scaledSize: new google.maps.Size(30, 30),
            title: locaData[i].DisplayName,
            markerID: locaData[i].MapId,
            animation:google.maps.Animation.DROP
        });
        bounds.extend(latlngM);
        google.maps.event.addListener(marker, 'click', (function (marker, i) {
          return function () {               
            infowindow.setContent(infoWindowContent[i]);
            infowindow.open(map, marker);
            google.maps.event.addListenerOnce(infowindow, 'domready', function () {
                $('.mapSlider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: true,
                    dotsClass:'slick-dots slider-dots',
                    fade: true,
                    cssEase: 'linear',
                    prevArrow: "<i clafunction(ess='fa fa-angle-left'></i>",
                    nextArrow: "<i clafunction(ess='fa fa-angle-right'></i>",
                });
            });
                //only for map center,zoomin
                map.setCenter(marker.getPosition());
                //map.setZoom(15);
                
            }
        })(marker, i));
        markers.push(marker); 
        map.fitBounds(bounds);
    }   

    function getInfoWindowDetails(location){
      var contentString = '<div class="mapSlider">';
      if($("#villa-"+locaData[i].MapId+" .feature-item-slider div img").length > 0){
        $("#villa-"+locaData[i].MapId+" .feature-item-slider div").find("img").each(function(){
          var slideImg=$(this).attr('src');
          var featureContent=$("#villa-"+locaData[i].MapId).find(".feature-item-content").html();
          contentString +='<div class="mapSlider-item"><img src="'+slideImg+'"'+' /><div class="mapSlider-content">'+featureContent+'</div></div>';
      });  
    }                         
    contentString +='</div>';
    return contentString;
}
}
initMap(filterSelectedState(null));
    //google.maps.event.addDomListener(window, "load", );
</script>
<script>
 $('a.villadata').on('click', function (e) {
    // var data_url  = $(this).data('url');
    console.log($(this).find('form').length);
    $(this).find('form').submit();

    return false;
});

</script>
<?php get_footer(); ?>
<?php
    if(!is_user_logged_in() ){
    ?>
    <script type="text/javascript">
        $(document).ready(function() {
                $('.sign-nav ul li [data-id="signin"]').trigger('click');
        });
    </script>
<?php } ?>

<!-- <script>
    $(document).ready(function(){

        filter_data();

        function filter_data()
        {
            $('.filter_data').html('<div id="loading" style="" ></div>');
            var action = 'ajax_villa';
            // var minimum_price = $('#hidden_minimum_price').val();
            // var maximum_price = $('#hidden_maximum_price').val();
            var villa_locations = get_filter('villa_locations');
            var villa_purpose = get_filter('villa_purpose');
            var villa_chkin = get_filter('villa_chkin');
            var villa_chkout = get_filter('villa_chkout');
            var villa_bed = get_filter('villa_bed');
            var villa_price = get_filter('villa_price');
            var villa_filter = get_filter('villa_filter');
            $.ajax({
                url:templateUri + '/ajax/ajax_villa.php',
                method:"POST",
                data:{action:action,  villa_locations:villa_locations, villa_purpose:villa_purpose, villa_chkin:villa_chkin,villa_chkout:villa_chkout,villa_bed:villa_bed,villa_price:villa_price,villa_filter:villa_filter,},
                success:function(data){
                    $('.filter_data').html(data);
                }
            });
        }

        function get_filter(class_name)
        {
            var filter = [];
            $('.'+class_name+':checked').each(function(){
                filter.push($(this).val());
            });
            return filter;
        }

        $('.common_selector').click(function(){
            filter_data();
        });

  

    });
    </script> -->