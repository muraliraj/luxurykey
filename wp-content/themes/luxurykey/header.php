<?php

?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Luxury Key | Homepage</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<!-- modernizr included -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment-with-locales.min.js"></script>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
	<script src="https://www.google.com/recaptcha/api.js" async defer></script>

	<style type="text/css">
		body {
			background-color: #fff;
		}

		.render-blk {
			opacity: 1;
		}

		.slick-slide {
			height: unset !important;
		}

		.error-message {
			display: none;
		}
	</style>

	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/app.css">
	<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/style.css">

<?php 
if(is_singular('mphb_room_type')){
?>
	<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOzhN_oBfDzghug_jY9wvzGwPJI5qWs3I&callback=initMap"></script>
<?php } ?>

	<noscript>
		<style type="text/css" media="screen">
			.render-blk {
				opacity: 1;
			}
		</style>
	</noscript>
	<script>
		var templateUri = "<?php echo get_bloginfo('template_url'); ?>";
		var blogUri = "<?php echo get_bloginfo('url'); ?>";
	</script>

	<!-- Auto open popup script-->
	<script>
		$(document).ready(function() {
			$(function() {
				$("#autoopen-popup").click();
			});
		})
	</script>




	<?php wp_head(); ?>
</head>

<body class="">
	<div class="global-message error">
		<div class="container">
			<p>* Please choose all the details to filter </p>
			<a class="global-message-close no-effect" href=""><i class="la la-close"></i></a>
		</div>
	</div>
	<div class="render-blk">
		<!-- header start -->
		<header id="scrolledTop">
			<div class="container container-type2">
<a href="<?php echo get_bloginfo('url'); ?>" class="logo desktop">
					<img src="<?php echo get_bloginfo('template_url'); ?>/img/logo.svg" alt="images">
				</a>
				<a href="<?php echo get_bloginfo('url'); ?>" class="logo mobile">
					<img src="<?php echo get_bloginfo('template_url'); ?>/img/logo-mobile.png" alt="images">
				</a>
				<nav class="menu-open">
					<a href="#" class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></a>
					<?php navmenu_luxurykey('header_menu'); ?>
				</nav>
				<div class="menu-toggle">
					<div class="hamBurger"></div>
					<div class="hamBurger"></div>
					<div class="hamBurger"></div>
				</div>
			</div>
		</header>
		<?php get_template_part('template-parts/signin'); ?>
		<?php get_template_part('template-parts/signup'); ?>
		<?php get_template_part('template-parts/forget_password'); ?>
		<!-- End of header -->
