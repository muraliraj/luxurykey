<?php
/***********************
Template Name: Home villas
************************/
$currentPage = get_post($postId);
$villargs = array(
    'numberposts' =>-1,
    'post_type' => 'mphb_room_type',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_status'=>"publish"
	);
$villapages = get_posts($villargs);
?>
<!-- villa selection section -->
		<div class="villa-wrapper">
			<div class="top-row">
				<h2><?php echo $currentPage->post_title; ?></h2>
			</div>
			<div class="villa-slider">
				<?php foreach($villapages as $villapage) { 
					$villa_bath   = get_post_meta( $villapage->ID, '_villa_bath', true );
					  $villa_pool  = get_post_meta( $villapage->ID, 'villa_pool', true );
					  $villa_guest  = get_post_meta( $villapage->ID, 'mphb_adults_capacity', true );
					  $villa_address  = get_post_meta( $villapage->ID, 'villa_address', true );
					  $villa_bed  = explode('|', $villapage->post_title);
					  $gallerymeta  = get_post_meta( $banner_post->ID, 'mphb_gallery', true );
			          $galleryimgs = explode(',', $gallerymeta);
			          $galleryimgs = array_filter($galleryimgs);
					  $destination  = get_post_meta( $villapage->ID, 'villa_location', true );
					  $purpose = get_post_meta( $villapage->ID, 'villa_purpose', true );
					  $featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID));
					  $amn_villa = wp_get_object_terms( $villapage->ID, "mphb_room_type_facility");
					  
				?>
				<div>
					<div class="flexed-content">
						<div class="flexed-start flexed-content ">
							<div class="column">
								<div class="villa-navslider">
							    	<div class="area-row">
										<h3><?php echo $destination; ?></h3>
									</div>
								</div>
							<div class="flexed-topheading">
								<h2><?php echo $villa_bed[1]; ?></h2>
								<span><?php echo $villa_bed[2].'test'; ?></span>
								<span><?php echo do_shortcode('[favorite_button]'); ?></span>
							</div>
							<div class="flexed-summary">
								<p><?php echo $post->post_excerpt; ?></p>
							</div>
							<div class="d-buttons">
								<div class="button button-primary">
									<a href="<?php echo get_permalink($villapage->ID); ?>">view villa</a>
								</div>
								<div class="button button-secondary">
									<a href="<?php echo get_permalink($villapage->ID); ?>">Book Now</a>
								</div>
							</div>
						</div>
						</div>
						<div class="flexed-image">
							<div class="flexed-image-options">
								<?php 

								if($villapage->post_title[0]!='') { ?>
								<div class="golden-icons">
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.png" alt="bath">
									<span><?php echo $villa_bed[0]; ?></span>
								</div>
								<?php } ?>
								<?php 
								
									
									foreach ($amn_villa as $key => $amn_villa_fac) {
											
									if($amn_villa_fac->slug == 'jacuzzi') { 
										?>
								<div class="golden-icons">
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.png" alt="bath">
									<span>6</span>
								</div>
								<?php }
								if($amn_villa_fac->slug =='swimming-pool') { ?>
								<div class="golden-icons">
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.png" alt="pool">
									<span>1</span>
								</div>
								<?php 
							}
							
							 ?>
							<?php } 
							?>
							 <?php 
							 if($villa_guest!=''){ 
							 	?>
							 	<div class="golden-icons">
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="person">
									<span><?php echo $villa_guest; ?></span>
								</div>

							 <?php } ?>

							</div>
							<div class="imageClip">
								<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/default.svg" class="placeholder">
								<div>
									<img src="<?php echo $featImage; ?>" alt="frame4-image">
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
			</div>
		</div>
		<!--End of villa selection section -->