<?php 
get_header();
$featImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$segments = explode(' ', $post->post_title);

$title = '';

for ($i = 0; $i < count($segments); $i++) {
    if ($i === 1) {
        $title .= ' <strong>' . $segments[$i] . '</strong>';  
        continue;
    } 

    $title .= ' ' . $segments[$i];
}

echo $html;

?>
<section id="generic-row">
	<div class="hero-banner golden-bg">
		<img src="<?php echo $featImage; ?>" alt="generic-banner" />
	</div>

		<div class="hero-banner-desc-center">
			<div class="hero-banner-content">
				<div class="slide-top"></div>
					<h1><?php echo $title; ?></h1>
				<div class="slide-bottom"></div>
			</div>
		</div>
	
	<?php echo apply_filters('the_content', $post->post_content); ?>		
</section>
<?php 
if($post->post_name == 'real-estate'||$post->post_name == 'concierge'||$post->post_name == 'experiences'||$post->post_name == 'events' ||$post->post_name == 'weddings'){
?>

<?php

global $wpdb;
global $_POST;      
// exit();
if(isset($_POST['contactpage'])){
if ($_POST['contactpage'] =="submit" ) {
$table = $wpdb->prefix ."contact_list";
$data['fullname']        = sanitize_text_field($_POST['fullname']);
$data['email']            = sanitize_text_field($_POST['email']);
$data['country_code']            = sanitize_text_field($_POST['country_code']);
$data['mobile']        = sanitize_text_field($_POST['mobile']);
$data['other_services']            = sanitize_text_field($_POST['other_services']);
$data['message']          = nl2br($_POST['message']);
$data['formdate']      = date('Y-m-d H:i:s');
$data['fullname'] = ucwords($data['fullname']);
$format = array('%s','%s','%s','%s','%s','%s');

$err = 0;
if (empty($data['fullname'])) {
$error['fullname'] = "Please enter your Full Name";
$err++;
}

if (empty($data['email'])) {
$error['email'] = "Please enter your Email";
$err++;
}
if (empty($data['country_code'])) {
$error['country_code'] = "Please enter your country_code";
$err++;
}
if (empty($data['mobile'])) {
$error['mobile'] = "Please enter your Telephone";
$err++;
}
if (empty($data['other_services'])) {
$error['other_services'] = "Please enter your Telephone";
$err++;
}
if (empty($data['message'])) {
$error['message'] = "Please enter your Message";
$err++;
}

// var_dump($table);
// exit();
  if (empty($err)) {

      $insert_contactpage = $wpdb->insert($table, $data, $format);

      if ($wpdb->insert_id) { 
      $message = ' 
		<html>
		 <body>
		<div style="max-width:560px;font-size:14px">
		    <p>Thank you for contacting us. We will get back to you soon!</p>
		    
               <p>Luxurykey Team</p>
    	</div>
		</body>
		</html> ';
		  $admin_message = ' 
		<html>
		 <body>
		<div style="max-width:560px;font-size:14px">
		    <p>Thank you for contacting us. We will get back to you soon!</p>
		    
               <p>Luxurykey Team</p>
    	</div>
		</body>
		</html> ';
		  $admin_message = ' 
		<html>
		 <body>
		<div style="max-width:560px;font-size:14px">
		<p>Hi</p>
        <p> '. $data['fullname'] .' has sent an enquiry from Luxury Key website. Please find the details below,<br />
			Name:       '. $data['fullname'] .' <br />
			Email:     ' . $data['email'] . ' <br />
		    mobile:    ' . $data['country_code'] . ' ' . $data['mobile'] . '  <br />
			other services:     ' . $data['other_services'] . ' <br />
			message:     ' . $data['message'] . ' <br />
    	</div>
		</body>
		</html> ';
        $subject = "We got your message!";
        $to = $_POST["email"];
        $from = "LuxuryKey <mathivanan@madebyfire.com>";
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=iso-8859-1" . "\r\n";
        $headers .= "From: " . $from . "\r\n";
        wp_mail($to, $subject, $message, $headers);
		  $to = 'mathivanan@madebyfire.com';
		wp_mail($to, $subject, $admin_message, $headers);
echo 1;
  }

    }
    else
    {
    	echo 2;
    }

  }
}

?>


<div class="contact">

<div class="detail-intro">
<div class="text-center top-row no-padding">
<h2><strong>REQUEST </strong>FORM</h2>                
</div>
<?php wp_nonce_field('conactus_nonce','contact_list'); ?>
<form name="contact_list" id="contact_list" method="post" action="" >
   
<div class="container">
<div class="row">
<div class="col-6">
<div class="form-row">
<label class="floating-item" data-error="Please enter your Full Name">
<input type="text" id="fullname" class="floating-item-input input-item" name="fullname" value="" />
<span class="floating-item-label">Full Name</span>
</label>
<div class="error-message" id="error-fullname">Please enter your Full Name</div>
</div>
</div>
<div class="col-6">
<div class="form-row">
<label class="floating-item" data-error="Please enter your Email ID">

<input type="text" id="email-contact" class="floating-item-input input-item" name="email" value="" />
<span class="floating-item-label">Email</span>
</label>
<div class="error-message" id="error-email">Please enter your Email ID</div>
</div>
</div>

<div class="col-6">
	<div class="country-mobile">
		<div class="input-row form-row">
			<input type="hidden" id="country_code" class="floating-item-input input-item" name="country_code" value="" />
			<div class="select-list">
              	<div class="drop-links">
                  	<span class="filter" id="defaultText">Country Code</span>
                  	 	<ul class="select overflow-y country-mobile no_beds " >
					    <li data-conty="+213">Algeria (+213)</li>
					    <li data-conty="+376">Andorra (+376)</li>
					    <li data-conty="+244">Angola (+244)</li>
					    <li data-conty="+1264">Anguilla (+1264)</li>
					    <li data-conty="+1268">Antigua Barbuda (+1268)</li>
					    <li data-conty="+54">Argentina (+54)</li>
					    <li data-conty="+374">Armenia (+374)</li>
					    <li data-conty="+297">Aruba (+297)</li>
					    <li data-conty="+61">Australia (+61)</li>
					    <li data-conty="+43">Austria (+43)</li>
					    <li data-conty="+994">Azerbaijan (+994)</li>
					    <li data-conty="+1242">Bahamas (+1242)</li>
					    <li data-conty="+973">Bahrain (+973)</li>
					    <li data-conty="+880">Bangladesh (+880)</li>
					    <li data-conty="+1246">Barbados (+1246)</li>
					    <li data-conty="+375">Belarus (+375)</li>
					    <li data-conty="+32">Belgium (+32)</li>
					    <li data-conty="+501">Belize (+501)</li>
					    <li data-conty="+229">Benin (+229)</li>
					    <li data-conty="+1441">Bermuda (+1441)</li>
					    <li data-conty="+975">Bhutan (+975)</li>
					    <li data-conty="+591">Bolivia (+591)</li>
					    <li data-conty="+387">Bosnia Herzegovina (+387)</li>
					    <li data-conty="+267">Botswana (+267)</li>
					    <li data-conty="+55">Brazil (+55)</li>
					    <li data-conty="+673">Brunei (+673)</li>
					    <li data-conty="+359">Bulgaria (+359)</li>
					    <li data-conty="+226">Burkina Faso (+226)</li>
					    <li data-conty="+257">Burundi (+257)</li>
					    <li data-conty="+855">Cambodia (+855)</li>
					    <li data-conty="+237">Cameroon (+237)</li>
					    <li data-conty="+1">Canada (+1)</li>
					    <li data-conty="+238">Cape Verde Islands (+238)</li>
					    <li data-conty="+1345">Cayman Islands (+1345)</li>
					    <li data-conty="+236">Central African Republic (+236)</li>
					    <li data-conty="+56">Chile (+56)</li>
					    <li data-conty="+86">China (+86)</li>
					    <li data-conty="+57">Colombia (+57)</li>
					    <li data-conty="+269">Comoros (+269)</li>
					    <li data-conty="+242">Congo (+242)</li>
					    <li data-conty="+682">Cook Islands (+682)</li>
					    <li data-conty="+506">Costa Rica (+506)</li>
					    <li data-conty="+385">Croatia (+385)</li>
					    <li data-conty="+53">Cuba (+53)</li>
					    <li data-conty="+90392">Cyprus North (+90392)</li>
					    <li data-conty="+357">Cyprus South (+357)</li>
					    <li data-conty="+42">Czech Republic (+42)</li>
					    <li data-conty="+45">Denmark (+45)</li>
					    <li data-conty="+253">Djibouti (+253)</li>
					    <li data-conty="+1809">Dominica (+1809)</li>
					    <li data-conty="+1809">Dominican Republic (+1809)</li>
					    <li data-conty="+593">Ecuador (+593)</li>
					    <li data-conty="+20">Egypt (+20)</li>
					    <li data-conty="+503">El Salvador (+503)</li>
					    <li data-conty="+240">Equatorial Guinea (+240)</li>
					    <li data-conty="+291">Eritrea (+291)</li>
					    <li data-conty="+372">Estonia (+372)</li>
					    <li data-conty="+251">Ethiopia (+251)</li>
					    <li data-conty="+500">Falkland Islands (+500)</li>
					    <li data-conty="+298">Faroe Islands (+298)</li>
					    <li data-conty="+679">Fiji (+679)</li>
					    <li data-conty="+358">Finland (+358)</li>
					    <li data-conty="+33">France (+33)</li>
					    <li data-conty="+594">French Guiana (+594)</li>
					    <li data-conty="+689">French Polynesia (+689)</li>
					    <li data-conty="+241">Gabon (+241)</li>
					    <li data-conty="+220">Gambia (+220)</li>
					    <li data-conty="+7880">Georgia (+7880)</li>
					    <li data-conty="+49">Germany (+49)</li>
					    <li data-conty="+233">Ghana (+233)</li>
					    <li data-conty="+350">Gibraltar (+350)</li>
					    <li data-conty="+30">Greece (+30)</li>
					    <li data-conty="+299">Greenland (+299)</li>
					    <li data-conty="+1473">Grenada (+1473)</li>
					    <li data-conty="+590">Guadeloupe (+590)</li>
					    <li data-conty="+671">Guam (+671)</li>
					    <li data-conty="+502">Guatemala (+502)</li>
					    <li data-conty="+224">Guinea (+224)</li>
					    <li data-conty="+245">Guinea - Bissau (+245)</li>
					    <li data-conty="+592">Guyana (+592)</li>
					    <li data-conty="+509">Haiti (+509)</li>
					    <li data-conty="+504">Honduras (+504)</li>
					    <li data-conty="+852">Hong Kong (+852)</li>
					    <li data-conty="+36">Hungary (+36)</li>
					    <li data-conty="+354">Iceland (+354)</li>
					    <li data-conty="+91">India (+91)</li>
					    <li data-conty="+62">Indonesia (+62)</li>
					    <li data-conty="+98">Iran (+98)</li>
					    <li data-conty="+964">Iraq (+964)</li>
					    <li data-conty="+353">Ireland (+353)</li>
					    <li data-conty="+972">Israel (+972)</li>
					    <li data-conty="+39">Italy (+39)</li>
					    <li data-conty="+1876">Jamaica (+1876)</li>
					    <li data-conty="+81">Japan (+81)</li>
					    <li data-conty="+962">Jordan (+962)</li>
					    <li data-conty="+7">Kazakhstan (+7)</li>
					    <li data-conty="+254">Kenya (+254)</li>
					    <li data-conty="+686">Kiribati (+686)</li>
					    <li data-conty="+850">Korea North (+850)</li>
					    <li data-conty="+82">Korea South (+82)</li>
					    <li data-conty="+965">Kuwait (+965)</li>
					    <li data-conty="+996">Kyrgyzstan (+996)</li>
					    <li data-conty="+856">Laos (+856)</li>
					    <li data-conty="+371">Latvia (+371)</li>
					    <li data-conty="+961">Lebanon (+961)</li>
					    <li data-conty="+266">Lesotho (+266)</li>
					    <li data-conty="+231">Liberia (+231)</li>
					    <li data-conty="+218">Libya (+218)</li>
					    <li data-conty="+417">Liechtenstein (+417)</li>
					    <li data-conty="+370">Lithuania (+370)</li>
					    <li data-conty="+352">Luxembourg (+352)</li>
					    <li data-conty="+853">Macao (+853)</li>
					    <li data-conty="+389">Macedonia (+389)</li>
					    <li data-conty="+261">Madagascar (+261)</li>
					    <li data-conty="+265">Malawi (+265)</li>
					    <li data-conty="+60">Malaysia (+60)</li>
					    <li data-conty="+960">Maldives (+960)</li>
					    <li data-conty="+223">Mali (+223)</li>
					    <li data-conty="+356">Malta (+356)</li>
					    <li data-conty="+692">Marshall Islands (+692)</li>
					    <li data-conty="+596">Martinique (+596)</li>
					    <li data-conty="+222">Mauritania (+222)</li>
					    <li data-conty="+269">Mayotte (+269)</li>
					    <li data-conty="+52">Mexico (+52)</li>
					    <li data-conty="+691">Micronesia (+691)</li>
					    <li data-conty="+373">Moldova (+373)</li>
					    <li data-conty="+377">Monaco (+377)</li>
					    <li data-conty="+976">Mongolia (+976)</li>
					    <li data-conty="+1664">Montserrat (+1664)</li>
					    <li data-conty="+212">Morocco (+212)</li>
					    <li data-conty="+258">Mozambique (+258)</li>
					    <li data-conty="+95">Myanmar (+95)</li>
					    <li data-conty="+264">Namibia (+264)</li>
					    <li data-conty="+674">Nauru (+674)</li>
					    <li data-conty="+977">Nepal (+977)</li>
					    <li data-conty="+31">Netherlands (+31)</li>
					    <li data-conty="+687">New Caledonia (+687)</li>
					    <li data-conty="+64">New Zealand (+64)</li>
					    <li data-conty="+505">Nicaragua (+505)</li>
					    <li data-conty="+227">Niger (+227)</li>
					    <li data-conty="+234">Nigeria (+234)</li>
					    <li data-conty="+683">Niue (+683)</li>
					    <li data-conty="+672">Norfolk Islands (+672)</li>
					    <li data-conty="+670">Northern Marianas (+670)</li>
					    <li data-conty="+47">Norway (+47)</li>
					    <li data-conty="+968">Oman (+968)</li>
					    <li data-conty="+680">Palau (+680)</li>
					    <li data-conty="+507">Panama (+507)</li>
					    <li data-conty="+675">Papua New Guinea (+675)</li>
					    <li data-conty="+595">Paraguay (+595)</li>
					    <li data-conty="+51">Peru (+51)</li>
					    <li data-conty="+63">Philippines (+63)</li>
					    <li data-conty="+48">Poland (+48)</li>
					    <li data-conty="+351">Portugal (+351)</li>
					    <li data-conty="+1787">Puerto Rico (+1787)</li>
					    <li data-conty="+974">Qatar (+974)</li>
					    <li data-conty="+262">Reunion (+262)</li>
					    <li data-conty="+40">Romania (+40)</li>
					    <li data-conty="+7">Russia (+7)</li>
					    <li data-conty="+250">Rwanda (+250)</li>
					    <li data-conty="+378">San Marino (+378)</li>
					    <li data-conty="+239">Sao Tome &amp; Principe (+239)</li>
					    <li data-conty="+966">Saudi Arabia (+966)</li>
					    <li data-conty="+221">Senegal (+221)</li>
					    <li data-conty="+381">Serbia (+381)</li>
					    <li data-conty="+248">Seychelles (+248)</li>
					    <li data-conty="+232">Sierra Leone (+232)</li>
					    <li data-conty="+65">Singapore (+65)</li>
					    <li data-conty="+421">Slovak Republic (+421)</li>
					    <li data-conty="+386">Slovenia (+386)</li>
					    <li data-conty="+677">Solomon Islands (+677)</li>
					    <li data-conty="+252">Somalia (+252)</li>
					    <li data-conty="+27">South Africa (+27)</li>
					    <li data-conty="+34">Spain (+34)</li>
					    <li data-conty="+94">Sri Lanka (+94)</li>
					    <li data-conty="+290">St. Helena (+290)</li>
					    <li data-conty="+1869">St. Kitts (+1869)</li>
					    <li data-conty="+1758">St. Lucia (+1758)</li>
					    <li data-conty="+249">Sudan (+249)</li>
					    <li data-conty="+597">Suriname (+597)</li>
					    <li data-conty="+268">Swaziland (+268)</li>
					    <li data-conty="+46">Sweden (+46)</li>
					    <li data-conty="+41">Switzerland (+41)</li>
					    <li data-conty="+963">Syria (+963)</li>
					    <li data-conty="+886">Taiwan (+886)</li>
					    <li data-conty="+7">Tajikstan (+7)</li>
					    <li data-conty="+66">Thailand (+66)</li>
					    <li data-conty="+228">Togo (+228)</li>
					    <li data-conty="+676">Tonga (+676)</li>
					    <li data-conty="+1868">Trinidad &amp; Tobago (+1868)</li>
					    <li data-conty="+216">Tunisia (+216)</li>
					    <li data-conty="+90">Turkey (+90)</li>
					    <li data-conty="+7">Turkmenistan (+7)</li>
					    <li data-conty="+993">Turkmenistan (+993)</li>
					    <li data-conty="+1649">Turks &amp; Caicos Islands (+1649)</li>
					    <li data-conty="+688">Tuvalu (+688)</li>
					    <li data-conty="+256">Uganda (+256)</li>
					    <li data-conty="+380">Ukraine (+380)</li>
					    <li data-conty="+971">United Arab Emirates (+971)</li>
					    <li data-conty="+598">Uruguay (+598)</li>
					    <li data-conty="+7">Uzbekistan (+7)</li>
					    <li data-conty="+678">Vanuatu (+678)</li>
					    <li data-conty="+379">Vatican City (+379)</li>
					    <li data-conty="+58">Venezuela (+58)</li>
					    <li data-conty="+84">Vietnam (+84)</li>
					    <li data-conty="+1284">Virgin Islands - British (+1284)</li>
					    <li data-conty="+1340">Virgin Islands - US (+1340)</li>
					    <li data-conty="+681">Wallis &amp; Futuna (+681)</li>
					    <li data-conty="+969">Yemen (North)(+969)</li>
					    <li data-conty="+967">Yemen (South)(+967)</li>
					    <li data-conty="+260">Zambia (+260)</li>
					    <li data-conty="+263">Zimbabwe (+263)</li>
				  	</ul> 
				  	
				</div>                                   
			</div>
		</div>
		<div class="form-row">
			<label class="floating-item input-animate" data-error="Please enter mobile number">
				<input type="text" id="edit_mobil" class="floating-item-input input-item edit_mobil" name="mobile" value="" required="">
				<span class="floating-item-label">Mobile Number</span>
			</label>
			<div class="error-message" id="err_mobile">Please Enter your Mobile Number</div>
		</div>
	</div>
</div>
 <?php     
 $msg_name = get_post_meta($post->ID, 'msg_name', true);
 $realEsts = explode("|", $msg_name);
 ?>
	<div class="col-6">
		<div class="form-row">
			<label class="floating-item input-animate" data-error="Please Choose your salutation">
			<select class="select-menu" id="edit_salu" class="floating-item-input input-item edit_salu" name="other_services" value="" required="">
                <?php foreach ($realEsts as $key => $realEst) {?>
                	<option value="<?php echo $realEst ?>"><?php echo $realEst ?></option>
                <?php } ?>
				</select>
			</label>
		</div>
	</div>
<div class="col-12">
<div class="form-row">
<label class="floating-item" data-error="Please enter atleast six characters">
<textarea class="floating-item-input input-item" rows="3" name="message" id="message"></textarea>
<span class="floating-item-label">Message</span>
</label>
<div class="error-message" id="error-message">Please enter your Messages</div>
</div>
</div>
<div class="col-12 text-center">	
<div class="button button-primary">
<div class="g-recaptcha" data-sitekey="6LcDWaoUAAAAAEvrqSX7jVZ0IA4yzdVACZ6iPxAi"></div>
<div class="button"><button id="contact_submit" name="contactpage" value="submit">Submit</button></div>
</div>
</div>
</div>
</div>
</form>
</div>
</div>




<?php } ?>
<?php get_footer(); ?>