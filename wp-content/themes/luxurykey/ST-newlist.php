<?php 
/***********************
Template Name: Villas list11
************************/
/*


Array
(
    [villa-destination] => Mykonos
    [villa-check-in] => 2019-06-19
    [villa-check-out] => 2019-06-26
    [villa-price] => 3000-5999
    [villa-price-min] => 3000
    [villa-price-max] => 5999
    [no-of-bedrooms] => 2
    [flexible] => 
)
function searchmodule($route,$location,$checkin,$checkout,$numerofbeds,$minprice,$maxprice,$flexible,$package,$villatype='',$eventtype='',$villalocality=''){

*/
/*echo "<pre>";
print_r($_POST);
exit();*/
// $var=searchmodule('filter','Mykonos','2019-08-15','2019-08-25',5,200,5000,3,NULL);
get_header('search');
?>
<?php
$route='search';

$location	=$_POST['villa-destination'];
$checkin 	=$_POST['villa-check-in'];
$checkout	=$_POST['villa-check-out'];
$numerofbeds=$_POST['no-of-bedrooms'];
$minprice	=$_POST['villa-price-min'];
$maxprice	=$_POST['villa-price-max'];
$flexible	=$_POST['flexible'];
$package= implode( ',' , $_POST['package']) ;
$available = searchmodule($route,$location,$checkin,$checkout,$numerofbeds,$minprice,$maxprice,$flexible,$package,$villatype='',$eventtype='',$villalocality='');
/*echo "<pre>";
print_r($available);
exit();*/
$available_id= array_keys($available);
 $villargs = array( 
       'post__in' => $available_id,
    'post_type' => 'mphb_room_type',
    'post_status' => 'published',
    // 'include'=>$manualAvailable,
    'posts_per_page' => -1
	);

// echo "<pre>";
// print_r($villapages);
// exit();
?>


 <div class="filter-top">
    		<div class="container container-type1">
    			<div class="row feature-row">
					<div class="col-12">
						<div class="section-intro section-icons flex-sb">
							<h4>We've found <?php echo count($available_id); ?> luxury villas for you in Mykanos</h4>	
							<ul>
								<li><span>view</span></li>
								<li><i class="la la-table list-icon on"></i></li>
								<li><i class="la la-map-marker map-icon"></i></li>
							</ul>
						</div>						
					</div>
				</div>
    		</div>
    	</div>
        <section class="section-feature section-init">
	        <div class="container container-type3 feature-container" >	        	
	        	<div class="row listRow toggle">
	        		<div class="col-12 listView">
	        		<?php 
$villapages = get_posts($villargs);

$iter=1;
	foreach($villapages as $villapage) {
$title = $villapage->post_title; 
$tarr=array();
$tarr= explode('|', $title);
$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
// if($tarr[2]==$villa_destination){
$villa_bath   					= 		get_post_meta( $villapage->ID, '_villa_bath', true );
$villa_pool  					= 		get_post_meta( $villapage->ID, 'villa_pool', true );
$bathroommphb_room_type  					= 		get_post_meta( $villapage->ID, 'bathroommphb_room_type', true );

$villa_guest  					= 		get_post_meta( $villapage->ID, 'mphb_adults_capacity', true );
$villa_address  				= 		get_post_meta( $villapage->ID, 'villa_address', true );
$villa_code  					= 		get_post_meta( $villapage->ID, 'villa_code', true );
$villa_bed  					= 		explode('|', $villapage->post_title);
$destination  					= 		get_post_meta( $villapage->ID, 'villa_location', true );
$purpose 						= 		get_post_meta( $villapage->ID, 'villa_purpose', true );
$featImage 						= 		wp_get_attachment_url(get_post_thumbnail_id($villapage->ID));
$villa_typeapi 					= 		get_post_meta( $villapage->ID, 'villa_type_api', true );
$amn_villa 						= 		wp_get_object_terms( $villapage->ID, "mphb_room_type_facility");
$gallerymeta  					= 		get_post_meta( $villapage->ID, 'mphb_gallery', true );
$villa_latitude  				= 		get_post_meta( $villapage->ID, 'villa_latitude', true );
$villa_longtitude  				= 		get_post_meta( $villapage->ID, 'villa_longtitude', true );
$villa_locations  				= 		get_post_meta( $villapage->ID, 'villa_locations', true );

?>
<!-- <a  href="<?php echo get_permalink($villapage->ID); ?>?checkin=&checkout&rate" class="listingform-submit"> -->
<a href="javascript:void(0)" class="listingform-submit">
<form  action="<?php echo get_permalink($villapage->ID); ?>"  method="post" class="listingform col-4 listItem" id="villa-map<?php echo $iter;  ?>" >
								<input type="hidden" name="ckin"  value="<?php if(isset($_POST)){ echo $checkin; } ?>" >
								<input type="hidden" name="ckout" value="<?php if(isset($_POST)){ echo $checkout; } ?>"  >
						<!-- <div class="col-4 listItem" id="villa-map<?php echo $iter;  ?>"> -->
					        <div class="feature-item">
								<div class="feature-item-slider">
<?php

					  $galleryimgs=explode(',', $gallerymeta);
					  $galleryimgs=array_filter($galleryimgs);
					  if(!empty($galleryimgs))
					  {
					  	$iter0=0;
				foreach ($galleryimgs as $galleryimg) {
					if($galleryimg!='' && $iter0<4){ ?>
<div>
	<img src="<?php  echo wp_get_attachment_image_url($galleryimg,'full');  ?>" alt="">
</div>
<?php
					}
					$iter0++;
				}
					}else{
						?>
						<div>
							<img src="<?php echo get_the_post_thumbnail_url($villapage->ID,'full'); ?>" alt="">
						</div>
						<?php

					}
		
?>
								</div>
								<div class="feature-item-content">
									<h5><?php   echo $villa_guest.' BEDROOMS,'.$villa_guest.' GUESTS,'.$bathroommphb_room_type.' BATHS';?></h5>
									<h3><?php echo $tarr[1]; ?></h3>
									<p>from <?php echo $available[$villapage->ID]; ?> / night</p>
									<p><a href="#!"><?php echo $villa_locations ; ?></a>, Greece</p>
								</div>
								<div class="feature-item-top">
									<!-- <i class="la la-heart-o"></i> -->
									<span class="fav-icon"></span>
								</div>
							</div>
						<!-- </div> -->
</form>
						</a>
<?php 
$iter++;
} ?>
	        		</div>
	        		<div class="col-6 mapView hide">
	        			<div style="position: relative;height: 100%;width: 100%;">
	    					<div id="map"></div>
	        			</div>
	        		</div>
	        	</div>
			</div>
		</section>


<?php

get_footer();

?>
<script type="text/javascript">
/*	jQuery(document).ready(function(){
		jQuery('.listingform-submit').click(function(){
			jQuery(this).find('.listingform').submit();
			alert(jQuery(this).find('.listingform').length);
		});
	});*/
	jQuery( document ).on('click',".listingform-submit",function(event) {

			/*alert($(this).element);*/
});
</script>