<?php 
get_header();
/***********************
Template Name: My Account
************************/
$user = wp_get_current_user();

$first_name = get_user_meta( $user->ID, 'first_name_reg', true);
$last_name = get_user_meta( $user->ID, 'last_name_reg', true);
$phone_number = get_user_meta($user->ID, 'reg_phone', true);
$country_code = get_user_meta($user->ID, 'cont_code', true);
$salut = get_user_meta($user->ID, 'salt', true);
?>       <!-- End of header -->
        	
        	<div class="container-fluid">
        		<div class="row accout-tile">
	        		<aside class="col-5">
	        			<div class="accout-tile-left">
		        			<h1><?php echo $first_name; ?></h1>
		        			<ul>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/my-account/" class="active no-effect"><i class="fa fa-user" aria-hidden="true"></i>My Account</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/booking" class="no-effect"><i class="fa fa-bookmark" aria-hidden="true"></i>My Next Booking</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/history" class="no-effect"><i class="fa fa-unlock-alt" aria-hidden="true"></i>History</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/change-password" class="no-effect"><i class="fa fa-history" aria-hidden="true"></i>Change Password</a></li>
		        				<li><a href="<?php echo wp_logout_url(home_url()); ?>" class="no-effect"><i class="fa fa-sign-out" aria-hidden="true"></i>Log Out</a></li>
		        			</ul>
		        		</div>
	        		</aside>

	        		<div class="col-7">
	        			<div class="accout-tile-content user-detail">
	        				<div class="d-flex justify-content-between align-items-center">
			        			<h3>User Detail</h3>
								<div class="edit-user"><a href="<?php echo get_bloginfo('url'); ?>/edit-account" class="no-effect"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></div>
			        		</div>
		        			<ul class="split-list">
		        				<?php if(first_name!=''){ ?>
		        				<li>Name:</li>
		        				<li><?php echo $salut; ?>. <?php echo $first_name; ?> <?php echo $last_name; ?></li>
		        				<?php } if($user->user_email!=''){ ?>
		        				<li>Mail-Id:</li>
		        				<li><?php echo $user->user_email; ?></li>
		        				<?php } if($phone_number!=''){ ?>
		        				<li>Phone no:</li>
		        				<li><?php echo $country_code; ?>-<?php echo $phone_number; ?></li>
		        			<?php } ?>
		        			</ul>
		        		</div>
	        		</div>
	        	</div>
        	</div>
		</div>	
		  <?php get_footer(); ?>