<?php

global $wpdb;
global $_POST;

if (isset($_POST['footersub'])) {

  if ($_POST['footersub'] == "signup") {
    $table = $wpdb->prefix . "signup_list";
    $data['firstname']        = sanitize_text_field($_POST['firstname']);
    $data['email']            = sanitize_text_field($_POST['email']);
    $data['posted_date']      = date('Y-m-d H:i:s');
    $data['firstname'] = ucwords($data['firstname']);
    $format = array('%s', '%s', '%s');

    $err = 0;
    if (empty($data['firstname'])) {
      $error['firstname'] = "Please enter your FIRST NAME";
      $err++;
    }
    if (empty($data['email'])) {
      $error['email'] = "Please enter your EMAIL ADDRESS";
      $err++;
    }

    if (empty($err)) {

      $insert_footersub = $wpdb->insert($table, $data, $format);

      $lastid = $wpdb->insert_id;

      if ($lastid == "") { 
      }
    }
  }
}

?>


<div class="bg-formwrapper">
  <img src="<?php echo get_bloginfo('template_url'); ?>\img\banner5.jpeg" alt="form-bg">

  <?php wp_nonce_field('conactus_nonce', 'signup_list'); ?>

    <div class="container">
      <div class="row" id="footer_news">
        <div class="col-3">
          <h3>Sign up to the LUXURY KEY to get exclusive offers.</h3>
        </div>

        <div class="col-9">
            <div class="horizontal-form" >

            <label class="floating-item" data-error="Please enter your First Name">
             <input type="text" id="firstname1" class="floating-item-input input-item" name="firstname" value="" placeholder="First Name" />
            <div class="error-message" id="error-firstname1">Please enter your First Name</div>
           </label>     
            <label class="floating-item" data-error="Please enter Email Address">
              <input type="email" id="email-contact" class="floating-item-input input-item" name="email" value="" placeholder="Email Address" />   
                <div class="error-message" id="error-emailid1">Please Enter Your Email Address</div>
                
              </label>
           
              
              <div class="button button-primary">
                <div class="button"><button id="signfit" name="footersub" value="signup">sign up</button></div>

              </div>
          
            </div>
        </div>
      </div>
		<div class="msg_sub error-message" style="display: none"><h3>Thanks for subscribing successfully!!</h3></div>
    </div>
</div>


<!--footer start -->
<footer>
     <div class="container footer-container">
        <?php 
if ( $post_footer = get_page_by_path( 'footer', OBJECT, 'page' ) ){
        $footerCont =  $post_footer->post_content;
        ?>
        <?php echo do_shortcode($footerCont); ?>

      <?php } ?>
    </div>
    </footer>
    </div>  
    <!--end of footer start -->
    <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/app.js"></script>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/wow.js"></script>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/moment.min.js"></script>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/custom.js"></script>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/validation.js"></script>
        <script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/search_condt.js"></script>
        
    <?php

     if($post->post_name == "vacation-rental" && isset($_POST['villa-check-in']) && isset($_POST['villa-check-out'])){
   ?>
  <script>
    $(function() {
      /*function format (inputString) {
          if (!inputString) {
              inputString = this.isUtc() ? hooks.defaultFormatUtc : hooks.defaultFormat;
          }
          var output = formatMoment(this, inputString);
          return this.localeData().postformat(output);
      }*/
      var checkin = '<?php echo $_POST['villa-check-in']; ?>'
      var checkout = '<?php echo $_POST['villa-check-out']; ?>'
      var checkin_date = new Date(checkin);
      var checkin_newDate = checkin_date.formatDate("DD MMM YY");
      var checkout_date = new Date(checkout);
      var checkout_newDate = checkout_date.formatDate("DD MMM YY");
      var value_date = checkin +' to '+checkout;
      var applyDater = $(".select-list .input-item");
        applyDater.daterangepicker({
          startDate: checkin_newDate,
          endDate: checkout_newDate,
        });
      $(applyDater).text('<span>'+checkin_date+'</span><i class="la la-arrow-right"></i> <span>'+checkout_newDate+'</span>');
      $(applyDater).addClass('active');
    });

  </script>


<?php } ?>
<?php if ($post->post_name != "vacation-rentals") { 
wp_footer();
  ?>

  
<?php
}

     if(is_singular( 'mphb_room_type' ) && isset($_POST['checkin']) && isset($_POST['checkout'])){
      $villa_typeapi=get_post_meta( $post->ID, 'villa_type_api', true );
      $villa_typ_id = $villa_typeapi=='api' || $villa_typeapi=='api_manu'?'detail-dateselect':'detail-dateselect-manual';
   ?>
  <script>
      
    $(function() {
      var checkin = '<?php echo $_POST['checkin']; ?>';
      var checkout = '<?php echo $_POST['checkout']; ?>';
      
      // var checkin_newDate = checkin_date.formatDate("DD MMM YY");
      // console.log(checkin_newDate);
      var checkout_date = new Date(checkout);
      // var checkout_newDate = checkout_date.formatDate("DD MMM YY");
      // var value_date = checkin +' to '+checkout;
      var applyDate_single = $("#<?php echo $villa_typ_id; ?> .input-item");
        /*applyDate_single.daterangepicker({
          startDate: checkin_newDate,
          endDate: checkout_newDate,
        });*/
      $(applyDate_single).html('<span>'+checkin+'</span><i class="la la-arrow-right"></i> <span>'+checkout+'</span>');
      $(applyDate_single).addClass('active');



    });
</script>
<?php 
}else{
  # code...
  ?>
  <script>
      $(function() {
  $('.show-availability').html('Kindly select checkin and checkout')
  $('#continue-booking-detail').attr('disabled','disabled');
  $('#continue-booking-detail').addClass('btn-custom-disabled');
   });
    </script>
<?php  

}
?>
     
</body>
</html>
    
