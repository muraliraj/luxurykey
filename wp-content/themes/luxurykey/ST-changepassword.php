<?php 
/***********************
Template Name: Change Password
************************/
get_header();
if(isset($_POST)) {
	$current_user = wp_get_current_user();
	$password = sanitize_text_field($_POST['new-password']);     
	$userdata = array(
	    'ID'        =>  $current_user->ID,
	    'user_pass' =>  $password
	);  
	$user_id = wp_update_user($userdata);
	if($user_id == $current_user->ID){
		wp_redirect( home_url() );
	} else {
	    echo 'error';
	}   
}
?>	
<div class="container-fluid">
	<div class="row accout-tile">
		<aside class="col-5">
			<div class="accout-tile-left">
    			<h1>User Name</h1>
    			<ul>
    				<li><a href="<?php echo get_bloginfo('url'); ?>/my-account/"  class="no-effect"><i class="fa fa-user" aria-hidden="true"></i>My Account</a></li>
    				<li><a href="<?php echo get_bloginfo('url'); ?>/booking"  class="no-effect"><i class="fa fa-bookmark" aria-hidden="true"></i>My Booking</a></li>
    				<li><a href="<?php echo get_bloginfo('url'); ?>/history"  class="no-effect"><i class="fa fa-unlock-alt" aria-hidden="true"></i>History</a></li>
    				<li><a href="<?php echo get_bloginfo('url'); ?>/change-password" class="active no-effect"><i class="fa fa-history" aria-hidden="true"></i>Change Password</a></li>
    				<li><a href="<?php echo wp_logout_url(home_url()); ?>" class="no-effect"><i class="fa fa-sign-out" aria-hidden="true"></i>Log Out</a></li>
    			</ul>
    		</div>
		</aside>
		<div class="col-7">
			<div class="col-6 accout-tile-content">
    			<h3>Change password</h3>
    			<p>Please fill the below fields to change your password.</p>
    			<form method="post" action="">
    				<div class="form-row">
						<label class="floating-item" data-error="Please enter old password">
							<input type="text" class="floating-item-input input-item" name="old-password" id="old-password" value="" placeholder="Old password" />
						</label>
						<div class="error-message" id="err_old_password">Please enter your name</div>
					</div>
					<div class="form-row">
						<label class="floating-item" data-error="Please enter new password">
							<input type="text" class="floating-item-input input-item" name="new-password" id="new-password" value="" placeholder="New password" />
						</label>
						<div class="error-message" id="err_new_password">Please enter your name</div>
					</div>
					<div class="form-row">
						<label class="floating-item" data-error="Please enter new password">
							<input type="text" class="floating-item-input input-item" name="confirm-password" id="confirm-password" value="" placeholder="Confirm password" />
						</label>
						<div class="error-message" id="err_confirm_password">Please enter your name</div>
					</div>
					<div class="button button-primary">
						<button id="change-password">submit</button>
					</div>
    			</form>
    		</div>
		</div>
	</div>
</div>
</div>	
<?php get_footer();  ?>