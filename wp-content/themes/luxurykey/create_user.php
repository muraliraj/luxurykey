<?php 
get_header();
/***********************
Template Name: Villas list
************************/
$villargs = array(
    'numberposts' =>-1,
    'post_type' => 'villas',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_status'=>"publish"
	);
$villapages = get_posts($villargs);
// var_dump($_POST);
if (!isset($_POST)|| empty($_POST)) {
?>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOzhN_oBfDzghug_jY9wvzGwPJI5qWs3I&callback=initMap"></script>
    <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: {lat: -33.9, lng: 151.2}
        });
        setMarkers(map);
      }
      var beaches = [
      	<?php 
      	foreach($villapages as $villapage) {
      		$price = get_post_meta( $villapage->ID, 'villa_price', true );
			$villa_latitude  = get_post_meta( $villapage->ID, 'villa_latitude', true );
      		$villa_longtitude  = get_post_meta( $villapage->ID, 'villa_longtitude', true );
      		?>
        [<?php echo $villapge->post_title; ?>, <?php echo $villa_latitude; ?>, <?php echo $villa_longtitude; ?>, <?php echo $price; ?>],
    	<?php } ?>
        

      ];
	  function numberWithCommas(x) {
		 var parts = x.toString().split(".");
		 parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		 return parts.join(".");
	  }
      function setMarkers(map) {
        var markerIcon = {
	        url: 'http://image.flaticon.com/icons/svg/252/252025.svg',
	        scaledSize: new google.maps.Size(65, 65),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(32,65),
	        labelOrigin:  new google.maps.Point(33,25),
	      };
        for (var i = 0; i < beaches.length; i++) {
          var beach = beaches[i],
          	  price = numberWithCommas(beach[3]).toString() + "$";
          var marker = new google.maps.Marker({
            position: {lat: beach[1], lng: beach[2]},
            map: map,
            icon: markerIcon,
            label: {
		      text: price,
		      color: 'black',
		      fontSize: '12px',
		      fontWeight: 'bold'
		    }
          });
        }
      }
    </script>

<!-- villa list -->
		<div class="container container-type1 flexed-listing">
        	<div class="row listRow toggle">
        		<div class="col-6 listView">
        			<?php  
	        			
	                  	foreach($villapages as $villapage) { 
	                  		$featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID) );
	                  		$price = get_post_meta( $villapage->ID, 'villa_price', true );
	                  		$villaDestination = get_post_meta( $villapage->ID, 'villa_locations', true );
	                  		$villaPurpose = get_post_meta( $villapage->ID, 'villa_purpose', true );
	                  		$villaBed = get_post_meta( $villapage->ID, 'villa_bed', true );
	                  		$villaGuest = get_post_meta( $villapage->ID, 'villa_guest', true );
	                  		$villaBath = get_post_meta( $villapage->ID, 'villa_bath', true );
	                  		$villaPool = get_post_meta( $villapage->ID, 'villa_pool', true );
	                  		?>

	        		<div class="col-12 listItem <?php echo $villaDestination; ?> " data-destination="<?php echo $villaDestination; ?>" data-bed="<?php echo $villaBed; ?>" data-guest="<?php echo $villaGuest; ?>" data-bath="<?php echo $villaBath; ?>" data-pool="<?php echo $villaPool; ?>" data-purpose="<?php echo $villaPurpose; ?>" data-price="<?php echo $price; ?>">
				        <div class="xlist-item">
		        			<img src="<?php echo $featImage; ?>" alt="">
		        			<?php if($villaBed!='' || $villaGuest!='' || $villaBath!='' || $villaPool!='') { ?>
		        			<div class="xlist-item-feature">
		        				<?php if($villaBed!='') { ?>
			        				<div>
										<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.svg" alt="images">
										<p><?php echo $villaBed; ?></p>
									</div>
								<?php } ?>
								<?php if($villaGuest!='') { ?>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="images">
									<p><?php echo $villaGuest; ?>2</p>
								</div>
								<?php } ?>
								<?php if($villaBath!='') { ?>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.svg" alt="images">
									<p><?php echo $villaBath; ?></p>
								</div>
								<?php } ?>
								<?php if($villaPool!='') { ?>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.svg" alt="images">
									<p><?php echo $villaPool; ?></p>
								</div>
								<?php } ?>
		        			</div>
		        		<?php } ?>
		        			<div class="xlist-item-desc">
		        				<div>
			        				<h4><?php echo $villapage->post_title; ?></h4>
									<p><?php echo $villapage->post_excerpt; ?></p>
									<?php if($price!='') { ?>
									<p>From: €<?php echo $price; ?> / per night</p>
									<?php } ?>
								</div>
								<div class="button button-primary button-small">
									<a href="<?php echo get_permalink($villapage->ID);?>" tabindex="0">view villa</a>
								</div>
		        			</div>
		        		</div>
					</div>
				    <?php } ?>
        		</div>
        		<div class="col-6 mapView">
		        	<div id="map"></div>
        		</div>
        	</div>
		</div>
		
<?php } else {

$destination = $_POST['villa-destination'];
$purpose = $_POST['villa-purpose'];
$checkin = $_POST['villa-check-in'];
$checkout = $_POST['villa-check-out'];
$bedrooms = $_POST['no-of-bedrooms'];

$checkin = date('Y-m-d', strtotime($checkin));
$checkout = date('Y-m-d', strtotime($checkout));
$price = $_POST['villa-price'];
$splitPrice = explode('-',$price);
$des =array();
if($destination!='') {
	$des[] = array(
            'key' => 'villa_locations',
            'value' => $destination,
        );
}
if($purpose!='') {
	$des[] = array(
            'key' => 'villa_purpose',
            'value' => $purpose,
        );
}
if($bedrooms!='') {
	$des[] = array(
            'key' => 'villa_bed',
            'value' => $bedrooms,
        );
}
if($checkin!='' && $checkin!='1970-01-01') {
	$des[] = array(
            'key' => 'villa_chkin',
            'value' => $checkin,
        );
}
if($checkout!='' && $checkout!='1970-01-01') {
	$des[] = array(
            'key' => 'villa_chkout',
            'value' => $checkout,
        );
}
if($price!='' && $price!='€[object Object] - €[object Object]') {
	$des[] = array(
	            'key' => 'villa_price',
	           'value' => array(floatval($splitPrice[0]),floatval($splitPrice[1])),
	            'compare' => 'BETWEEN',
	            'type' => 'NUMERIC'
	        );
}

$villargs = array(
	'numberposts' =>-1,
	'post_type' => 'villas',
	'orderby' => 'title',
	'order' => 'DESC',
	'post_status'=>"publish",
	'meta_query' => $des,
);
$villapages = get_posts($villargs);
?>
<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOzhN_oBfDzghug_jY9wvzGwPJI5qWs3I&callback=initMap"></script>
    <script>
      function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 13,
          center: {lat: -33.9, lng: 151.2}
        });
        setMarkers(map);
      }
      var beaches = [
       <?php 
      	foreach($villapages as $villapage) {
      		$price = get_post_meta( $villapage->ID, 'villa_price', true );
			$villa_latitude  = get_post_meta( $villapage->ID, 'villa_latitude', true );
      		$villa_longtitude  = get_post_meta( $villapage->ID, 'villa_longtitude', true );
      		?>
        [<?php echo $villapge->post_title; ?>, <?php echo $villa_latitude; ?>, <?php echo $villa_longtitude; ?>, <?php echo $price; ?>],
    	<?php } ?>
      ];
	  function numberWithCommas(x) {
		 var parts = x.toString().split(".");
		 parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		 return parts.join(".");
	  }
      function setMarkers(map) {
        var markerIcon = {
	        url: 'http://image.flaticon.com/icons/svg/252/252025.svg',
	        scaledSize: new google.maps.Size(65, 65),
	        origin: new google.maps.Point(0, 0),
	        anchor: new google.maps.Point(32,65),
	        labelOrigin:  new google.maps.Point(33,25),
	      };
        for (var i = 0; i < beaches.length; i++) {
          var beach = beaches[i],
          	  price = numberWithCommas(beach[3]).toString() + "$";
          var marker = new google.maps.Marker({
            position: {lat: beach[1], lng: beach[2]},
            map: map,
            icon: markerIcon,
            label: {
		      text: price,
		      color: 'black',
		      fontSize: '12px',
		      fontWeight: 'bold'
		    }
          });
        }
      }
    </script>

<div class="container container-type1 flexed-listing" id="villa_listing">
        	<div class="row listRow toggle">
        		<div class="col-6 listView">
        			<?php  
	        			
	                  	foreach($villapages as $villapage) { 
	                  		$featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID) );
	                  		$price = get_post_meta( $villapage->ID, 'villa_price', true );
	                  		$villaBed = get_post_meta( $villapage->ID, 'villa_bed', true );
	                  		$villaGuest = get_post_meta( $villapage->ID, 'villa_guest', true );
	                  		$villaBath = get_post_meta( $villapage->ID, 'villa_bath', true );
	                  		$villaPool = get_post_meta( $villapage->ID, 'villa_pool', true );
	                  		?>

	        		<div class="col-12 listItem">
				        <div class="xlist-item">
		        			<img src="<?php echo $featImage; ?>" alt="">
		        			<?php if($villaBed!='' || $villaGuest!='' || $villaBath!='' || $villaPool!='') { ?>
		        			<div class="xlist-item-feature">
		        				<?php if($villaBed!='') { ?>
			        				<div>
										<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.svg" alt="images">
										<p><?php echo $villaBed; ?></p>
									</div>
								<?php } ?>
								<?php if($villaGuest!='') { ?>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="images">
									<p><?php echo $villaGuest; ?>2</p>
								</div>
								<?php } ?>
								<?php if($villaBath!='') { ?>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.svg" alt="images">
									<p><?php echo $villaBath; ?></p>
								</div>
								<?php } ?>
								<?php if($villaPool!='') { ?>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.svg" alt="images">
									<p><?php echo $villaPool; ?></p>
								</div>
								<?php } ?>
		        			</div>
		        		<?php } ?>
		        			<div class="xlist-item-desc filter_data">
		        				<div>
			        				<h4><?php echo $villapage->post_title; ?></h4>
			        				<p><?php echo $villapage->post_excerpt; ?></p>
									<?php //echo apply_filters('the_content', $villapage->post_content); ?>
									<?php if($price!='') { ?>
									<p>From: €<?php echo $price; ?> / per night</p>
									<?php } ?>
								</div>
								<div class="button button-primary button-small">
									<a href="<?php echo get_permalink($villapage->ID);?>" tabindex="0">view villa</a>
								</div>
		        			</div>
		        		</div>
					</div>
				    <?php } ?>
        		</div>
        		<div class="col-6 mapView">
		        	<div id="map"></div>
        		</div>
        	</div>
		</div>

<?php } ?>
<?php get_footer(); ?>

<!-- <script>
$(document).ready(function(){

    filter_data();

    function filter_data()
    {
        $('.filter_data').html('<div id="loading" style="" ></div>');
        var action = 'ajax_villa';
        // var minimum_price = $('#hidden_minimum_price').val();
        // var maximum_price = $('#hidden_maximum_price').val();
        var villa_locations = get_filter('villa_locations');
        var villa_purpose = get_filter('villa_purpose');
        var villa_chkin = get_filter('villa_chkin');
         var villa_chkout = get_filter('villa_chkout');
        var villa_bed = get_filter('villa_bed');
        var villa_price = get_filter('villa_price');
        var villa_filter = get_filter('villa_filter');
        $.ajax({
            url:templateUri + '/ajax/ajax_villa.php',
            method:"POST",
            data:{action:action,  villa_locations:villa_locations, villa_purpose:villa_purpose, villa_chkin:villa_chkin,villa_chkout:villa_chkout,villa_bed:villa_bed,villa_price:villa_price,villa_filter:villa_filter,},
            success:function(data){
                $('.filter_data').html(data);
            }
        });
    }

    function get_filter(class_name)
    {
        var filter = [];
        $('.'+class_name+':checked').each(function(){
            filter.push($(this).val());
        });
        return filter;
    }

    $('.common_selector').click(function(){
        filter_data();
    });

  

});
</script> -->