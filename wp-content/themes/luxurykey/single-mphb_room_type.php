<?php
get_header(); 
$data_images = get_post_meta( $post->ID, 'mphb_gallery', true );
$data_images_arr = explode(',', $data_images);
$roomType = MPHB()->getRoomTypeRepository()->findById($post->ID) ;
$ratevilla= $roomType->getDefaultPrice();
$villa_typeapi=get_post_meta( $post->ID, 'villa_type_api', true );
$data_images = get_post_meta( $post->ID, 'mphb_gallery', true );
$villa_code = get_post_meta( $post->ID, 'villa_code', true );
$data_images_arr = explode(',', $data_images);
$checkin = $_POST['checkin'];
$checkout = $_POST['checkout'];
$beds = $_POST['beds'];
$price_typ = $_POST['prc_typ'];
$guests = $_POST['guests'];
$prc_villa = $_POST['villa_prc'];

if($prc_villa!='nan'&&$prc_villa!=''){

$prc_villa = str_replace(',', '', $prc_villa);
}else{
	$prc_villa = '';
}
$term_args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'term_id');
$destination  = get_post_meta( $post->ID, 'villa_locations', true );
// $prc_tup = wp_get_post_terms( $post->ID, 'mphb_room_type_prc_typ', array("fields" => "ids"));
/*$term_list = wp_get_post_terms($post->ID, 'mphb_room_type_prc_typ', array("fields" => "ids"));
if(count($term_list)>0){
	$term_lnk = get_term( $term_list[0], 'mphb_room_type_prc_typ' );
	$term_link = get_term_link( $term_list[0] );	
	$term_name = $term_lnk->name;
}
$term_loc = wp_get_post_terms($post->ID, 'mphb_room_type_prc_typ', array("fields" => "ids"));
if(count($term_list)>0){
	$term_lnk = get_term( $term_list[0], 'mphb_room_type_prc_typ' );
	$term_link = get_term_link( $term_list[0] );	
	$term_name = $term_lnk->name;
}
*/
$villa_cat = get_post_meta($post->ID,'villa_type',true);
$date1 = new DateTime($checkin); //inclusive
$date2 = new DateTime($checkout); //exclusive
$checkin_date = date_create($_POST['checkin']);
$checkin = date_format($checkin_date,"d M y");
$checkout_date = date_create($_POST['checkout']);
$checkout = date_format($checkout_date,"d M y");
$tot_nights = $date2->diff($date1);
$tot_nights = $tot_nights->format("%a"); //3
$villa_typeapi=get_post_meta( $post->ID, 'villa_type_api', true );
$latitude = get_post_meta( $post->ID, 'villa_latitude', true );
$longtitude = get_post_meta( $post->ID, 'villa_longtitude', true );
$title = $post->post_title;
$villa_map = "{'Latitude':".$latitude.",'Longitude':".$longtitude.",'DisplayName':'".$title."','MapId':'".$post->ID."'}";

$villa_typ_id = $villa_typeapi == 'api'?'detail-dateselect':'detail-dateselect-manual';
/*api villa start*/

	$data_images = get_post_meta( $post->ID, 'mphb_gallery', true );



	$data_images_arr = explode(',', $data_images);
	$ratevilla = number_format((float)$_POST['rate'], 2, '.', '');

	?>

<style>
#qlwapp
{
	display: block!important;
}

</style>

	<div class="hero-detail">
		<div class="detail-banner">
			<?php 
			foreach ($data_images_arr as  $data_image) {
				?>
				<a  data-fancybox="test-srcset"
				data-type="image"
				data-srcset="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>"
				data-width="1200"
				data-height="720"
				class="fancy-box" 
				href="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" >
				<img src="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" />
			</a>

			<?php
		}
		?>

</div>
</div>



<section class="section-divider section-villa <?php echo $villa_typ_id.''.$villa_typeapi; ?> ">
	<div class="container ">
		<div class="breadcrumbs content-page">
			
				<ul>
				 <li><a href="<?php echo get_bloginfo('url'); ?>">Luxurykey </a></li>
				 <li><a href="<?php echo get_bloginfo('url'); ?>/vacation-rentals/?dest=<?php echo $destination; ?>">Vacation Rentals - <?php echo $destination; ?></a></li>
			     
			  <li><a class="active" href="#!"><?php echo $post->post_title; ?></a></li>
			
			</ul>
		</div>

		<div class="row">
			<div class="col-8 tick-list">
				<div class="section-intro">
					<?php if($villa_cat!=''){ ?>
						<h5><a href="<?php echo get_permalink($villa_cat); ?>"><?php echo get_the_title($villa_cat).' Villa';  ?></a></h5>
					<?php } ?>
					<h1><?php echo $post->post_title; ?></h1>
					<?php 
						$villa_bed  = get_post_meta( $post->ID, 'bedrrommphb_room_type', true );
						$villa_guest  = get_post_meta( $post->ID, 'mphb_adults_capacity', true );
						$villa_bath   = get_post_meta( $post->ID, 'bathroommphb_room_type', true );
						$Address  = get_post_meta( $post->ID, 'villa_address', true );

					?>

					<span><a href=javascript:void()><?php echo ($Address); ?></a></span>

					<h4>
						<?php
					      if($villa_bed!=''){
					          echo '<span class="villa-cont">'.$villa_bed." BEDROOMS</span>";
					      }
					      if($villa_guest!=''){
					        echo '<span class="villa-cont">'.$villa_guest." GUESTS</span>";
					      }
					      if($villa_bath!=''){
					        echo '<span class="villa-cont">'.$villa_bath." BATHROOMS</span>";
					      } 
					      ?>
					</h4>

				</div>
				<?php 
				$accord_1 = get_post_meta($post->ID, 'accord_1', true);
                $Addcontent1 = get_post_meta($post->ID, 'add_content_1', true);?>


      <div class="accordion-row">
					<?php 
						if($accord_1!=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent1; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord_1;?></p>
						</div>
					</div>
					<?php } ?>
				</div>
				 <div class="accordion-row">
                 <div class="accordion-row-blk">
	            <h4>Facilities</h4>
				<div class="accordion-content text-golden">

				

                <div class="content-feature">
					<?php 
					$amn_villas = wp_get_object_terms( $post->ID, "mphb_room_type_facility");
					foreach ($amn_villas as $key => $amn_villa) {
						$img_url = z_taxonomy_image_url($amn_villa->term_id,NULL, TRUE);
						?>
                           <div class="feature-icon">
							<img src="<?php echo $img_url; ?>" alt="showers" />
							<h6><?php echo $amn_villa->name; ?></h6>
							</div>
					     <?php } ?>	
						
					</div>
				</div>
				
				</div>
			 </div><br>	
				

				<?php 
				    echo apply_filters('the_content', $post->post_content); 
                   ?>
				





				<?php 
				
				$accord_2 = get_post_meta($post->ID, 'accord_2', true);
				$accord_3 = get_post_meta($post->ID, 'accord_3', true);
				$accord_4 = get_post_meta($post->ID, 'accord_4', true);
				$accord_5 = get_post_meta($post->ID, 'accord_5', true);
				$accord_6 = get_post_meta($post->ID, 'accord_6', true);
				$accord_7 = get_post_meta($post->ID, 'accord_7', true);
				$accord_8 = get_post_meta($post->ID, 'accord_8', true);
				$Addcontent2 = get_post_meta($post->ID, 'add_content_2', true);
				$Addcontent3 = get_post_meta($post->ID, 'add_content_3', true);
				$Addcontent4 = get_post_meta($post->ID, 'add_content_4', true);
				$Addcontent5 = get_post_meta($post->ID, 'add_content_5', true);
				$Addcontent6 = get_post_meta($post->ID, 'add_content_6', true);
				$Addcontent7 = get_post_meta($post->ID, 'add_content_7', true);
				$Addcontent8 = get_post_meta($post->ID, 'add_content_8', true);




				?>




				<div class="accordion-row">
					
					<?php 
						if($accord_2!=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent2; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord_2;?></p>
						</div>
					</div>
					<?php } ?>
					<?php 
						if($accord_3!=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent3; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord_3;?></p>
						</div>
					</div>
					<?php } ?>
					<?php 
						if($accord_4!=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent4; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord_4;?></p>
						</div>
					</div>
					<?php } ?>
					<?php 
						if($accord_5!=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent5; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord_5;?></p>
						</div>
					</div>
					<?php } ?>
					<?php 
						if($accord_6!=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent6; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord_6;?></p>
						</div>
					</div>
					<?php } ?>
					<?php 
						if($accord_7!=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent7; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord_7;?></p>
						</div>
					</div>
					<?php } ?>

					<?php 
						if($accord_8=''){
					?>
					<div class="accordion-row-blk">
						<h4><?php echo $Addcontent8; ?></h4>
						<div class="accordion-content">
							<p><?php echo $accord8;?></p>
						</div>
					</div>
					<?php } ?>

				</div>
			</div>
			<?php 
			if($villa_typeapi == 'api' || $villa_typeapi == 'api_manu'){
				?>
				<div class="col-4">
					<div class="card booking-card">
						<div class="border-heading br-left">
							<h3>Book this villa</h3>
						</div>
						
						<!-- <h5>STYLE</h5>
						<div class="form-row">
							<select class="select-menu"  name="selectmenu" data-placeholder="Select Guest" required>
								<option value="<?php echo $beds; ?>" selected><?php echo $price_typ; ?></option>
							</select>
						</div> -->
						<form action="https://luxurykey.reserve-online.net/" method="post" >
							<!-- <h5>BEDROOMS</h5>
							<div class="form-row select-list select-list-flex text-center">
								<button type="button" class="subs" disabled >-</button>
								<div class="select-list-data">
									<input type="hidden" value="1" class="no_room_det" id="noOfRoom" min="1" disabled="disabled"> 
									<span class="noOfRoom">1</span>		
									<span id="defaultText">Bedroom</span>		
								</div>										
								<button type="button" class="adds">+</button>										
							</div -->
							<h5>DURATION OF STAY</h5>
							<div class="form-row select-list date-pick" id="<?php echo $villa_typ_id; ?>" >
								<div class="input-item">
									<?php 

									if($_POST['checkin']!=''&& $_POST['checkout']!=''){
										?>
									<span><?php echo $checkin; ?></span> <i class="la la-arrow-right"></i> <span><?php echo $checkout; ?></span>
									<?php 
									}else{
										?>
										<span>Checkin</span> <i class="la la-arrow-right"></i> <span>Checkout</span>
									<?php } ?>
								</div>
							</div>
							<?php 
							if($villa_typeapi == 'api_manu'){
							?>
							<input type="hidden" name="room_vill" id="property_id" value="<?php echo $post->ID; ?>" >
							<input type="hidden" name="room" value="<?php echo $villa_code; ?>" >
							<?php }else{ ?>
							<input type="hidden" name="room" id="property_id" value="<?php echo $villa_code; ?>" >
							<?php } ?>
							<input type="hidden" name="checkin" value="<?php echo $checkin; ?>" id="check-in" >
							<input type="hidden" name="checkout" value="<?php echo $checkout; ?>" id="check-out" >
							<input type="hidden" id="rate_manual_villa" value="<?php echo $prc_villa; ?>" > 
							<input type="hidden" id="direct" name="direct" value="void" > 
							<input type=hidden name=currency value=EUR>
							<div class="show-availability" ></div>
							<div class="booking-row " >
								<div class="booking-total">
									<?php
										if(count($_POST) > 0){
									?>	
										<h6>Total</h6>
									<?php } ?>
									<div class="booking-price">
										<?php 
										$totprc = number_format((float)$prc_villa * (int)$tot_nights,2);
                                        $disable_text = $_POST['checkin']!='' && $_POST['checkout']!='' && ($prc_villa!='nan' && $prc_villa!='' && $prc_villa!=0 && !is_nan($prc_villa))?'':'disabled="disabled" class="btn-custom-disabled"';
                                                                                
										if(count($_POST) > 0 && ($prc_villa!='nan' && $prc_villa!='' && $prc_villa!=0 && !is_nan($prc_villa) )) {
										?>
										<div id="book_calc">
											<h6>€<?php echo $totprc; ?></h6>
											<p>(<?php echo $prc_villa; ?> x <?php echo $tot_nights ?> nights)</p>
										</div>	
										<?php } ?> 
										<div class="button button-primary type1 button-small" >
											<button id="continue-booking-detail" <?php echo $disable_text; ?>>Proceed</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<?php 
						$wlcm_msg_arr = get_option('qlwapp');
						$wlcm_msg = $wlcm_msg['user']['message'];
						?>
						<div class="booking-enq">
							<a href="<?php echo get_bloginfo('template_url'); ?>/contact/" target="_blank">Send us an enquiry</a>
<!-- 							<p><strong> <a href="https://web.whatsapp.com/send?phone%3D00306983896398%26text%3D<?php echo $wlcm_msg; ?>" target="_blank">Text us</a></strong></p> -->
						</div>
					</div>
					<?php 
					
					if($latitude!='' && $longtitude!=''){
						?>
				<div class="map-section" style="margin-top:20px">
					<div class="map-section-inner">
						<div id="map"></div>
					</div>
        		</div>
				<?php } ?>
				</div>
			<?php }else{ 
				    				      $data_images = get_post_meta( $post->ID, 'mphb_gallery', true );
					$data_images_arr = explode(',', $data_images);

					$roomType = MPHB()->getRoomTypeRepository()->findById($post->ID) ;
					$ratevilla= $roomType->getDefaultPrice();



				?>
				<div class="col-4">
					<div class="card booking-card">
						<div class="border-heading br-left">
							<h3>Book this villa</h3>
						</div>
						<!-- <h5>BEDROOMS</h5> -->
						<form action="<?php echo MPHB()->settings()->pages()->getCheckoutPageUrl(); ?>"
						 method="POST"
						 id="mphb-recommendation"
						 class="mphb-recommendation">
						 <?php wp_nonce_field( \MPHB\Shortcodes\CheckoutShortcode::NONCE_ACTION_CHECKOUT, \MPHB\Shortcodes\CheckoutShortcode::RECOMMENDATION_NONCE_NAME, true ); ?>
						
							<h5>DURATION OF STAY</h5>
							<div class="form-row select-list date-pick" id="<?php echo $villa_typ_id; ?>" >
								<div class="input-item">
									<?php 

									if($_POST['checkin']!=''&& $checkout!=''){
										?>
									<span><?php echo $checkin; ?></span> <i class="la la-arrow-right"></i> <span><?php echo $checkout; ?></span>
									<?php 
									}else{
										?>
										<span>Checkin</span> <i class="la la-arrow-right"></i> <span>Checkout</span>
									<?php } ?>
								</div>
							</div>
						<!-- <h5>STYLE</h5>
						<div class="form-row">
							<select class="select-menu"  name="selectmenu" data-placeholder="Select Guest" required>
								<option value="<?php echo $beds; ?>" selected><?php echo $price_typ; ?></option>
							</select>
						</div> -->
						
							<input type="hidden" name="room" id="property_id" value="<?php echo $post->ID; ?>" >
							<input type="hidden" name="mphb_check_in_date" value="<?php echo $checkin; ?>" id="check-in" >
							<input type="hidden" name="mphb_check_out_date" value="<?php echo $checkout; ?>" id="check-out" >
							<input type="hidden" name="mphb_rooms_details[<?php echo  $post->ID; ?>]"  id="property_id" value="1" >
							<input type="hidden" id="rate_manual_villa" value="<?php echo $ratevilla; ?>" > 
							<input type="hidden" id="mphb_adults" value="8" > 
							
							<input type=hidden name=currency value=EUR>
							<div class="show-availability" ></div>
							<div class="booking-row " >
								<div class="booking-total">
									<?php
										if(count($_POST) > 0){
									?>	
										<h6>Total</h6>
									<?php } ?>
									<div class="booking-price">
										<?php 
										$totprc = number_format((float)$ratevilla * (int)$tot_nights,2);
                                        $disable_text = $_POST['checkin']!='' && $_POST['checkout']!='' && ($ratevilla!='nan' && $ratevilla!='' && $ratevilla!=0 && !is_nan($ratevilla))?'':'disabled="disabled" class="btn-custom-disabled"';
                                                                                
										if(count($_POST) > 0 && ($ratevilla!='nan' && $ratevilla!='' && $ratevilla!=0 && !is_nan($ratevilla) )) {
										?>
										<div id="book_calc">
											<h6>€<?php echo $totprc; ?></h6>
											<p>(<?php echo $ratevilla; ?> x <?php echo $tot_nights ?> nights)</p>
										</div>
										<?php } ?> 
										<div class="button button-primary type1 button-small" >
											<button id="continue-booking-detail" <?php echo $disable_text; ?>>Proceed</button>
										</div>
									</div>
								</div>
							</div>
						</form>
						<?php 
						$wlcm_msg_arr = get_option('qlwapp');
						$wlcm_msg = $wlcm_msg['user']['message'];
						?>
						<div class="booking-enq">
							<a href="http://luxurykey.madebyfire.com/contact/" target="_blank">Send us an enquiry</a>
<!-- 							<p><strong> <a href="https://web.whatsapp.com/send?phone%3D00306983896398%26text%3D<?php echo $wlcm_msg; ?>" target="_blank">Text us</a></strong></p> -->
						</div>
					</div>
					
					<?php 
					
					if($latitude!='' && $longtitude!=''){
						?>
				<div class="map-section" style="margin-top:20px">
					<div class="map-section-inner">
						<div id="map"></div>
					</div>
        		</div>
				<?php } ?>
				</div>

			
			<?php } ?>
		</div>
	</div>
</section>

			<?php 

				$comments = get_comments(array( 'post_id' => $post->ID ));
					
				if(count($comments)>1){

?>
<section class="sandle-bg section-padding section-review">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<h4>From our guests</h4>
			</div>
			<?php 

				
				
				foreach ($comments as $key => $customer) {
					# code...
				
			?>
			<div class="col-4 review-card">
				<div class="card">
						<p><?php echo $customer->comment_content; ?></p>
				</div>
				<div class="author">
					<h5><?php echo $customer->comment_author; ?></h5>
				    	<?php $post_date = date('M d, Y' ,strtotime($customer->comment_date)); ?>	
						<p><?php  echo $post_date; ?></p>



				</div>
				
			</div>
		<?php } ?> 	
		</div>
	</div>
</section>

<?php 
}

	 ?>
<?php
$destination =  get_post_meta( $post->ID, 'villa_locations', true );
$args = array(
	'post_type'=> 'mphb_room_type',
	'post_status' => 'publish',
	'orderby'=>'rand',
	'order' => 'ASC',
	'meta_key' => 'villa_locations',
	'meta_value' => $destination,
	'numberposts' => 3,
        'meta_query'  => array(
						    array(
						        'key'       => 'villa_type_api',
						        'value'     => 'private',
						        'compare'   => '!=',
						    )
						)

);
$detail_gallerys = get_posts($args);
?>



<section class="section-feature section-init">
	<div class="container container-type3">
		<div class="row feature-row">
			<div class="col-12 text-center home-heading">
				<h3>We also recommend</h3>
			</div>
		</div>

		<div class="row feature-row">

			<?php   foreach ($detail_gallerys as $detail_gallery) {
// echo $banner_post->ID;
				$gallerymeta  = get_post_meta( $detail_gallery->ID, 'mphb_gallery', true );
				$galleryimgs = explode(',', $gallerymeta);
				$galleryimgs = array_filter($galleryimgs);
				$gallerymeta = get_post_meta( $detail_gallery->ID, 'bedrrommphb_room_type', true );
				$gallerymeta1 = get_post_meta( $detail_gallery->ID, 'mphb_adults_capacity', true );
				$gallerymeta2 = get_post_meta( $detail_gallery->ID, 'bathroommphb_room_type', true );
				$villa_bath   = get_post_meta( $detail_gallery->ID, 'bathroommphb_room_type', true );
					 	$villa_pool  = get_post_meta( $detail_gallery->ID, 'villa_pool', true );
					  	$villa_guest  = get_post_meta( $detail_gallery->ID, 'mphb_adults_capacity', true );
					  	$villa_address  = get_post_meta( $detail_gallery->ID, 'villa_address', true );
					  	$villa_code  = get_post_meta( $detail_gallery->ID, 'villa_code', true );
					  	$villa_bed  = get_post_meta( $detail_gallery->ID, 'bedrrommphb_room_type', true );
					  	$destination  = get_post_meta( $detail_gallery->ID, 'villa_locations', true );
					  	$purpose = get_post_meta( $detail_gallery->ID, 'villa_purpose', true );
					  	$featImage = wp_get_attachment_url(get_post_thumbnail_id($detail_gallery->ID));
					  	$villa_typeapi=get_post_meta( $detail_gallery->ID, 'villa_type_api', true );
					  	$amn_villa = wp_get_object_terms( $detail_gallery->ID, "mphb_room_type_facility");
					  	$Address=get_post_meta( $detail_gallery->ID, 'villa_address', true );

					
				?>
				<!-- loop -->

				<div class="col-4">
					<div class="feature-item">
						<div class="feature-item-slider">
							<?php 
								foreach ($galleryimgs as $key => $galleryimg) {
								?>

							<div>
								<a href="<?php echo get_permalink($detail_gallery->ID);?> " target='_blank'>
			        				<img src="<?php echo wp_get_attachment_image_url($galleryimg,'medium'); ?>" alt="">
								</a>
			        		</div>
			        		<?php 
							}
							?>
						</div>
					</div>
					<div class="feature-item-content">
							<h5>
							<?php
					      if($villa_bed!=''){
					          echo '<span class="villa-cont">'.$villa_bed." BEDROOMS</span>";
					      }
					      if($villa_guest!=''){
					        echo '<span class="villa-cont">'.$villa_guest." GUESTS</span>";
					      }
					      if($villa_bath!=''){
					        echo '<span class="villa-cont">'.$villa_bath." BATHROOMS</span>";
					      } 
					      ?>
							</h5>
							<a href="<?php echo get_permalink($detail_gallery->ID);?> ">
								<h3><?php echo $detail_gallery->post_title; ?></h3>
							</a>
							<?php
								if($Address!=''){
							?>
							<p>
								
									<?php echo $Address; ?>
								
							</p>
							<?php } ?>
						</div>
				</div>
				<?php }
				?>
		</div>
		
	</section>
	<script type="text/javascript">
		var beaches=[<?php echo $villa_map ;?>];

    var markers = [];
      function filterSelectedState(selectedLocationName) {
        var filteredLocations = [];

        if(selectedLocationName === null) {
            filteredLocations = beaches;
        } 
        return filteredLocations;
      }
    //initMap(filterSelectedState(null));
    function initMap(locaData) {
        // Data for the markers consisting of a name, a LatLng and a zIndex for the
        // order in which these markers should display on top of each other.
        // var beaches = [
        //   ['Bondi Beach', -33.890542, 151.274856, 4],
        //   ['Coogee Beach', -33.923036, 151.259052, 5],
        //   ['Cronulla Beach', -34.028249, 151.157507, 3],
        //   ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
        //   ['Maroubra Beach', -33.950198, 151.259302, 1]
        // ];   
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom:10,
          //center: new google.maps.LatLng(12.93924,80.12999),
          center: new google.maps.LatLng(37.44529, 25.32872),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var country = "Mykonos";
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode( {'address' : country}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
            }
        });

        //setMarkers(map);
        Array.prototype.groupBy = function(prop) {
          return this.reduce(function(groups, item) {
            const val = item[prop]
            groups[val] = groups[val] || []
            groups[val].push(item)
            return groups
          }, {})
        };

        // Info window
        var i;
        var infowindow = new google.maps.InfoWindow();
        var infoWindowContent = [];
        for (var i = 0; i < locaData.length; i++) {
          infoWindowContent[i] = getInfoWindowDetails(locaData[i]);
          // Adds markers to the map.
          // Marker sizes are expressed as a Size of X,Y where the origin of the image
          // (0,0) is located in the top left of the image.

          // Origins, anchor positions and coordinates of the marker increase in the X
          // direction to the right and in the Y direction down.
          var image = {
            url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32)
          };
          var  pin = {
              path: "M0-40c-8.28 0-15 6.7-15 15C-15-10 0 0 0 0s15-10 15-25c0-8.28-6.72-15-15-15zm0 17.5c-2.76 0-5-2.25-5-5s2.25-5 5-5c2.76 0 5 2.25 5 5s-2.25 5-5 5z",
              fillColor: "#b49759",
              fillOpacity: .8,
              scale: 1,
              strokeColor: "white",
              strokeWeight: 3,
          };
          // Shapes define the clickable region of the icon. The type defines an HTML
          // <area> element 'poly' which traces out a polygon as a series of X,Y points.
          // The final coordinate closes the poly by connecting to the first coordinate.
          var shape = {
            coords: [1, 1, 1, 20, 18, 20, 18, 1],
            type: 'poly'
          };

          var beach = locaData[i];
          var bounds = new google.maps.LatLngBounds();
          var latlngM = new google.maps.LatLng(locaData[i].Latitude, locaData[i].Longitude);
          var marker = new google.maps.Marker({
            // position: {lat: beach.Latitude, lng: beach.Longitude},
            position: latlngM,
            map: map,
            icon: pin,
            shape: shape,
            scaledSize: new google.maps.Size(30, 30),
            title: locaData[i].DisplayName,
            markerID: locaData[i].MapId,
            animation:google.maps.Animation.DROP
          });
          bounds.extend(latlngM);
          google.maps.event.addListener(marker, 'hover', (function (marker, i) {
              return function () {               
                infowindow.setContent(infoWindowContent[i]);
                infowindow.open(map, marker);
                google.maps.event.addListenerOnce(infowindow, 'domready', function () {
                    $('.mapSlider').slick({
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        dots: true,
                        dotsClass:'slick-dots slider-dots',
                        fade: true,
                        cssEase: 'linear',
                        prevArrow: "<i clafunction(ess='fa fa-angle-left'></i>",
                        nextArrow: "<i clafunction(ess='fa fa-angle-right'></i>",
                    });
                });
                //only for map center,zoomin
                map.setCenter(marker.getPosition());
                //map.setZoom(15);
                
              }
          })(marker, i));
          if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
		       var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.001, bounds.getNorthEast().lng() + 0.001);
		       var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.001, bounds.getNorthEast().lng() - 0.001);
		       bounds.extend(extendPoint1);
		       bounds.extend(extendPoint2);
		    }
			map.fitBounds(bounds);
			map.panToBounds(bounds); 
			var listener = google.maps.event.addListener(map, "idle", function() {
			  if (map.getZoom() >= 10){
			  	map.setZoom(15); 
			  }
			  google.maps.event.removeListener(listener); 
			}); 
          markers.push(marker); 
        }   

        function getInfoWindowDetails(location){
		  var contentString = '<div class="mapSlider">';
              if($("#villa-"+locaData[i].MapId+" .feature-item-slider div img").length > 0){
                $("#villa-"+locaData[i].MapId+" .feature-item-slider div").find("img").each(function(){
				  var slideImg=$(this).attr('src');
                  var featureContent=$("#villa-"+locaData[i].MapId).find(".feature-item-content").html();
                  contentString +='<div class="mapSlider-item"><img src="'+slideImg+'"'+' /><div class="mapSlider-content">'+featureContent+'</div></div>';
                });  
              }                         
              contentString +='</div>';
          return contentString;
        }
	}
	initMap(filterSelectedState(null));
	</script>
	<?php

	get_footer();
	?>
	
	