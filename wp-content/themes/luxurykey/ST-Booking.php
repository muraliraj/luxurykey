<?php 
/***********************
Template Name: Booking
************************/
get_header();

?> 


<div class="container-fluid">
        		<div class="row accout-tile">
	        		<aside class="col-5">
	        			<div class="accout-tile-left">
		        			<h1>User Name</h1>
		        				<ul>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/my-account/" class="no-effect"><i class="fa fa-user" aria-hidden="true"></i>My Account</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/booking/" class="active no-effect"><i class="fa fa-bookmark" aria-hidden="true"></i>My Next Booking</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/history/" class="no-effect"><i class="fa fa-unlock-alt" aria-hidden="true"></i>History</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/change-password"  class="no-effect"><i class="fa fa-history" aria-hidden="true"></i>Change Password</a></li>
		        				<li><a href="<?php echo wp_logout_url(home_url()); ?>" class="no-effect"><i class="fa fa-sign-out" aria-hidden="true"></i>Log Out</a></li>
		        			</ul>
		        		</div>
	        		</aside>
	        		<div class="col-7">
	        			<div class="accout-tile-content type1">
		        			<h3>My Next Booking</h3>
		        			<br>
		        		    <p>There are no new bookings in your list. <br>Guess it’s time for you to start exploring your next holiday destination.</p>
		        		</div>
	        		</div>
	        	</div>
        	</div>

		</div>	

	  	<?php get_footer(); ?>