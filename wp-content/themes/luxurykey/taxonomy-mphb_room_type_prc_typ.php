<?php 
get_header();
   $curTerm = get_queried_object(); 
   $curTermId = $curTerm->term_id;
 $args = array(
     'numberposts' => -1,
     'orderby' => 'date',
     'post_status'    => 'publish',
     'order' => 'DESC',
     'tax_query' => array(
       array(
       'taxonomy' => 'mphb_room_type_category',
       'field' => 'term_id',
       'terms' => array($curTermId),
       )
     ),
     'post_type' => 'mphb_room_type'
   );
   $categoryBlogs = get_posts($args);
?>
<div class="filter-top">
    		<div class="container container-type3">
    			<div class="row feature-row">
					<div class="col-12">
						<div class="section-intro section-icons flex-sb">
							<h3><?php echo $curTermName = $curTerm->name; ?></h3>	
							<ul>
								<li><span>view</span></li>
								<li><i class="la la-table list-icon on"></i></li>
								<li><i class="la la-map-marker map-icon"></i></li>
							</ul>
						</div>						
					</div>
				</div>
    		</div>
    	</div>
        <section class="section-feature section-init">
	        <div class="container container-type3 feature-container" >	        	
	        	<div class="row feature-row listRow toggle">

                 <?php
                 $args = array(
                 'post_type'=> 'mphb_room_type',
                 'post_status' => 'publish',				
                 'orderby'=>'date',
                 'order' => 'ASC',
                 'numberposts' => 1
                 );
                 $banner_posts = get_posts($args);
                  ?>
             <?php   foreach ($banner_posts as $banner_post) {
            // echo $banner_post->ID;
            $gallerymeta  = get_post_meta( $banner_post->ID, 'mphb_gallery', true );
            $galleryimgs = explode(',', $gallerymeta);
            $galleryimgs = array_filter($galleryimgs);
            ?>
             <?php } ?>


	        		
		        		<div class="col-4 listItem" id="villa-map2">
					        <div class="feature-item">
								<div class="feature-item-slider">
									                   <?php 
                     if(!empty($galleryimgs))
                     {
                        $qwe=0;
                        foreach ($galleryimgs as $galleryimg) {
                           if($qwe == 4){
                              break;
                           }

                           if($galleryimg!=''){ ?>
                        <div>
                        <img src="<?php  echo wp_get_attachment_image_url($galleryimg,'full');  ?>" alt="">
                        </div>
                       <?php
                          }
                          $qwe++;
                          }
                          }
                       ?>
									
					   </div>
						        <div class="feature-item-content">
									<h5>4 BEDROOMS, 8 GUESTS, 3 BATHROOMS</h5>
									<h3><?php echo $categoryBlog->post_title; ?></h3>
									<p><?php echo $categoryBlog->post_villa_price; ?></p>
									<p><a href="#!">Pyrgi</a>, Greece</p>
								</div>
								<div class="feature-item-top">
									<span class="fav-icon"></span>
								</div>
							</div>
						</div>
	        		</div>
	        		<!-- <div class="col-6 mapView hide">
	        			<div style="position: relative;height: 100%;width: 100%;">
	    					<div id="map"></div>
	        			</div>
	        		</div> -->
	        	</div>
			
		</section>

		<!--footer start -->

<?php get_footer(); ?>