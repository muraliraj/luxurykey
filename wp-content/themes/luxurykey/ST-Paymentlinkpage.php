<?php
/***********************
Template Name: Payment link
************************/
get_header();
$featImage = wp_get_attachment_url(get_post_thumbnail_id($post->ID));
$segments = explode(' ', $post->post_title);

$title = '';

for ($i = 0; $i < count($segments); $i++) {
    if ($i === 1) {
        $title .= ' <strong>' . $segments[$i] . '</strong>';  
        continue;
    } 

    $title .= ' ' . $segments[$i];
}

echo $html;
?>
<section id="generic-row">
	<div class="hero-banner golden-bg">
		<img src="<?php echo $featImage; ?>" alt="generic-banner" />
		<div class="hero-banner-desc-center">
			<div class="hero-banner-content">
				<div class="slide-top"></div>
					<h1><?php echo $title; ?></h1>
				<div class="slide-bottom"></div>
			</div>
		</div>
	</div>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">

  <!-- Identify your business so that you can collect the payments. -->
  <input type="hidden" name="business" value="muralilarumkd@gmail.com">

  <!-- Specify a Buy Now button. -->
  <input type="hidden" name="cmd" value="_xclick">

  <!-- Specify details about the item that buyers will purchase. -->
  <input type="hidden" name="item_name" value="Hot Sauce-12oz. Bottle">
  <input type="hidden" name="amount" value="5.95">
  <input type="hidden" name="currency_code" value="USD">

  <!-- Display the payment button. -->
  <input type="image" name="submit" border="0"
  src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
  alt="Buy Now">
  <img alt="" border="0" width="1" height="1"
  src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" >

</form>

</section>
<?php get_footer(); ?>