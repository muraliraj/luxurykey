
<?php 
/***********************
Template Name: Terms and condition
************************/
get_header();
?>
<div class="subpage"">
	<div class="wrapper">
		<div class="container">
			<div class="row">
				<div class="col-10 center-block ">
					<ul class="tick-list">	
						<?php echo apply_filters('the_content', $post->post_content); ?>	
					</ul>	
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
get_footer();  
?>