<?php 
get_header();
   global $post;
		$villa_typeapi=get_post_meta( $post->ID, 'villa_type_api', true );

   /*api villa start*/
   if($villa_typeapi=='api')
   {
      $data_images = get_post_meta( $post->ID, 'mphb_gallery', true );
$data_images_arr = explode(',', $data_images);
$ratevilla = number_format((float)$_POST['rate'], 2, '.', '');
 ?>
<div class="hero-detail">
		    	<div class="detail-banner">
		    	<?php 
		    	foreach ($data_images_arr as  $data_image) {
	 ?>
		    		<a  data-fancybox="test-srcset"
						data-type="image"
						data-srcset="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>"
						data-width="1200"
						data-height="720"
						class="fancy-box" 
						href="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" >
						<img width="240" src="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" />
					</a>
	 
	 <?php
}
		    	?>
		   		</div>
		   	</div>
	        <!-- villa detail short -->
	        <div class="detail-intro">
	        	        		<?php $title = $post->post_title; 
									$tarr= explode('|', $title);
									$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
									?>
	        	<p><?php echo $tarr[1].', '.$tarr[2]; ?></p>	
	        	<div class="amenities">
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/bed-black.svg" alt="bedroom"><span>
									 <?php	echo $int;
									 ?></span>Bed</div>
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/person-black.svg" alt="guest"><span>
<?php echo get_post_meta( $post->ID, 'mphb_adults_capacity')[0];  ?>	
	        		</span>Guests</div>
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/tub.svg" alt="bath"><span>1</span>Bath</div>
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/poolblack.svg" alt="pool"><span>1</span>Pool</div>
	        	</div>	
	        	<?php if(isset($_POST['rate'])){ ?>
	        	<div class="price">From<span>€<?php echo $ratevilla; ?></span>per/night</div>
	        	<?php } ?>
	        	<div class="review-shell d-flex">
		        	<div class="col-6">
		        		<h6>Platinum</h6>
		        	</div>
	        	</div>
	        </div>

	        <!-- villa detail tile -->
	        <div class="detail-tile">
	        	<div class="detail-tile-col">
	        		<div class="title-line">
	        			<h3>Villa Overview</h3>
	        		</div>
	<?php echo apply_filters('the_content', $post->post_content); ?>		

					<div class="title-line">
	        			<h3>SERVICE FACILITIES AND 	</h3>
	        		</div>
	        		<div class="two-col-list clearfix">
	        			<ul>
	        		<!--    <li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\Wifi.png" alt=""> Wi-Fi</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\dryer.png" alt="">Hair Dryer</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\ac.png" alt=""> A/C</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\parking.png" alt="">Private Parking</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\pool.png" alt="">Pool towels</li> -->
	        				<?php
	                        $arrr =  wp_get_object_terms( $post->ID ,'mphb_room_type_facility');
	                        // print_r($arrr);
	                        foreach ($arrr as $arr) {
								
	                        	$img_url = z_taxonomy_image_url($arr->term_id,NULL, TRUE);
						
	                        	if($img_url!=''){
	                        		$imgsrc = '<img src="'.$img_url.'" alt="bath">';
	                        	}
	                        	// var_dump($arr->term_id);
	                        	echo '<li>'.$imgsrc.$arr->name.'</li>';
	                        }
	        				?>
	        			</ul>
	        		</div>
	        		<br>
	        		<div class="detail-row">
		        		<div class="title-line">
		        			<h3>What Guests are Saying</h3>
		        		</div>
<!-- 		        		<div class="listFilter">
	        				<p>Sort by</p>
	        				<span class="filter-placeholder">Most Relavant</span>
		        			<ul>
		        				<li>Most Relavant</li>
		        				<li>Most Relavant</li>
		        			</ul>
		        		</div> -->
	        		</div>
	        		<div class="review-block">
	        			<div class="user">
	        				<img src="<?php echo get_bloginfo('template_url'); ?>/img/thumb-temp.png" alt="">
	        				<div class="user-name">
		        				<h6>Nicky</h6>
		        				<p>December 2018</p>
		        			</div>
	        			</div>
	        			<p class="more">We fell in love with the apartment the moment we entered. The location is amazing and the apartment is beautiful. The rooms are very spacious and provide a beautiful picturesque view. The apartment is so airy and the sunlight beams in, which I love the most. Feels lively. Sit with a cup of coffee in the evening, you can feel the calmness there. I canâ€™t describe how beautiful and cozy it was. The kitchen has a modern touch to it and the counter is so cute. He also provided us with.</p>
	        		</div>
	        		<div class="review-block">
	        			<div class="user">
	        				<img src="<?php echo get_bloginfo('template_url'); ?>/img/thumb-temp.png" alt="">
	        				<div class="user-name">
		        				<h6>Nicky</h6>
		        				<p>December 2018</p>
		        			</div>
	        			</div>
	        			<p class="more">We fell in love with the apartment the moment we entered. The location is amazing and the apartment is beautiful. The rooms are very spacious and provide a beautiful picturesque view. The apartment is so airy and the sunlight beams in, which I love the most. Feels lively. Sit with a cup of coffee in the evening, you can feel the calmness there. I canâ€™t describe how beautiful and cozy it was. The kitchen has a modern touch to it and the counter is so cute. He also provided us with.</p>
	        		</div>
	        	</div>
	        	<div class="detail-tile-col fixed">
	        		<div class="detail-form">
		        		<div class="title-line">
		        			<h3>Booking Details</h3>
		        		</div>
		        		<div class="form-row">
						<label class="floating-item">
							<input type="text" id="first-name" class="floating-item-input input-item" value="1 Bedroom" readonly />
						</label>
						</div>
						<div class="form-row select-list" id="detail-dateselect" >
							<label class="floating-item" style="height: 45px">
							<!-- if(isset()) -->
							<?php  if (isset($_POST['ckout'])) {
								echo '<span class="input-item">'.$_POST['ckin'].'<i class="fa fa-arrow-right"></i>'.$_POST['ckout'].'</span>';
							}else{ ?>
								<span class="input-item">Check In <i class="fa fa-arrow-right"></i> Check Out</span>
								<?php } ?>
							</label>
						</div>
						<span class="err">There is a 7 night minimum for these dates. Extend your stay or inquire below.</span>
							<div class="show-availability" ></div>
							<?php if(isset($_POST['totaldays'])){ ?>
						<div class="detail-row price-detail">
							<h6>€<?php echo $ratevilla." x ".$_POST['totaldays']; ?> Nights</h6>
							<h6>€<?php echo $ratevilla*$_POST['totaldays']; ?></h6>
						</div>
						<div class="detail-row price-detail">
							<h6>Payable Amount</h6>
							<h6>€<?php echo $ratevilla*$_POST['totaldays']; ?></h6>
						</div>
							<?php } ?>
						<div class="detail-row">
							<div class="button button-secondary">
								<!-- <button>Inquire</button> -->
								<a href="http://luxurykey.madebyfire.com/contact/" target="_blank" >Inquire</a>
							</div>
							<form action="https://luxurykey.reserve-online.net/" method="post" >
								<input type="hidden" name="room" id="property_id" value="<?php echo  get_post_meta( $post->ID, 'villa_code', true ); ?>" >
							<?php // if(isset($_POST['ckin']) ){ ?>
								<input type="hidden" name="checkin" value="<?php echo $_POST['ckin']; ?>" id="check-in" >
								<input type="hidden" name="checkout" value="<?php echo $_POST['ckout']; ?>" id="check-out" >
							<?php 	 //} ?>
								<input type=hidden name=currency value=EUR>

							<div class="button button-primary">
								<button type="submit" id="continue-booking-detail">Continue to Booking</button>
							</div>
							</form>
						</div>
					</div>
	        	</div>
	        </div>
	        <!-- villas card section -->
			<div class="container container-type1 flexed-listing no-padding" id="flexed-listing">
				<div class="row">
					<div class="col-6">
						<div class="xlist-item">
		        			<img src="<?php echo get_bloginfo('template_url'); ?>/img/frame4.jpeg" alt="">
		        			<div class="xlist-item-feature">
		        				<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.svg" alt="images">
									<p>1
									 </p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.svg" alt="images">
									<p>2</p>
								</div>
		        			</div>
		        			<div class="xlist-item-desc">
		        				<div>
			        				<h4>Villa <strong> Cottage </strong></h4>
									<p>Panormos, Mykonos, Greece</p>
									<p>From: â‚¬450 / per night</p>
								</div>
								<div class="button button-primary button-small">
									<a href="#" tabindex="0">view villa</a>
								</div>
		        			</div>
		        		</div>
					</div>
					<div class="col-6">
						<div class="xlist-item">
		        			<img src="<?php echo get_bloginfo('template_url'); ?>/img/frame41.jpeg" alt="">
		        			<div class="xlist-item-feature">
		        				<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.svg" alt="images">
									<p>1</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.svg" alt="images">
									<p>2</p>
								</div>
		        			</div>
		        			<div class="xlist-item-desc">
		        				<div>
			        				<h4>Villa <strong> Cottage </strong></h4>
									<p>Panormos, Mykonos, Greece</p>
									<p>From: â‚¬450 / per night</p>
								</div>
								<div class="button button-primary button-small">
									<a href="#" tabindex="0">view villa</a>
								</div>
		        			</div>
		        		</div>
					</div>
				</div>
			</div>

			<?php 
			/*api villa ends*/
		}else{

			/*manual villa start*/


      $data_images = get_post_meta( $post->ID, 'mphb_gallery', true );
$data_images_arr = explode(',', $data_images);

$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID) ;
$ratevilla= $roomType->getDefaultPrice();
// $ratevilla = number_format((float)$_POST['rate'], 2, '.', '');

 ?>
<div class="hero-detail">
		    	<div class="detail-banner">
		    	<?php 
		    	foreach ($data_images_arr as  $data_image) {
	 ?>
		    		<a  data-fancybox="test-srcset"
						data-type="image"
						data-srcset="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>"
						data-width="1200"
						data-height="720"
						class="fancy-box" 
						href="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" >
						<img width="240" src="<?php  echo wp_get_attachment_image_url($data_image,'full'); ?>" />
					</a>
	 
	 <?php
}
		    	?>
		   		</div>
		   	</div>
	        <!-- villa detail short -->
	        <div class="detail-intro">
	        	        		<?php $title = $post->post_title; 
									$tarr= explode('|', $title);
									$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
									?>
	        	<p><?php echo $tarr[1].', '.$tarr[2]; ?></p>	
	        	<div class="amenities">
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/bed-black.svg" alt="bedroom"><span>
									 <?php	echo $int;
									 ?></span>Bed</div>
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/person-black.svg" alt="guest"><span>
<?php echo get_post_meta( $post->ID, 'mphb_adults_capacity')[0];  ?>	
	        		</span>Guests</div>
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/tub.svg" alt="bath"><span>1</span>Bath</div>
	        		<div><img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/poolblack.svg" alt="pool"><span>1</span>Pool</div>
	        	</div>	
	        	<div class="price">From<span>€<?php echo $ratevilla; ?></span>per/night</div>
	        	<div class="review-shell d-flex">
		        	<div class="col-6">
		        		<h6>Platinum</h6>
		        	</div>
	        	</div>
	        </div>

	        <!-- villa detail tile -->
	        <div class="detail-tile">
	        	<div class="detail-tile-col">
	        		<div class="title-line">
	        			<h3>Villa Overview</h3>
	        		</div>
	<?php echo apply_filters('the_content', $post->post_content); ?>		


					<div class="title-line">
	        			<h3>SERVICE FACILITIES AND 	</h3>
	        		</div>
	        		<div class="two-col-list clearfix">
	        			<ul>
	        		<!--    <li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\Wifi.png" alt=""> Wi-Fi</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\dryer.png" alt="">Hair Dryer</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\ac.png" alt=""> A/C</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\parking.png" alt="">Private Parking</li>
	        				<li><img src="<?php echo get_bloginfo('template_url'); ?>/img/facilities\pool.png" alt="">Pool towels</li> -->
	        				<?php
	                        $arrr=  wp_get_object_terms( $post->ID ,'mphb_room_type_facility');
	                        // print_r($arrr);
	                        foreach ($arrr as $arr) {
	                        	$img_url = z_taxonomy_image_url($arr->term_id,NULL, TRUE);
	                        	if($img_url!=''){
	                        		$imgsrc = '<img src="'.$img_url.'" alt="bath">';
	                        	}
	                        	// var_dump($arr->term_id);
	                        	echo '<li>'.$arr->name.'</li>';
	                        }
	        				?>
	        			</ul>
	        		</div>
	        		<br>
	        		<div class="detail-row">
		        		<div class="title-line">
		        			<h3>What Guests are Saying</h3>
		        		</div>
		        		<div class="listFilter">
	        				<p>Sort by</p>
	        				<span class="filter-placeholder">Most Relavant</span>
		        			<ul>
		        				<li>Most Relavant</li>
		        				<li>Most Relavant</li>
		        			</ul>
		        		</div>
	        		</div>
	        		<div class="review-block">
	        			<div class="user">
	        				<img src="<?php echo get_bloginfo('template_url'); ?>/img/thumb-temp.png" alt="">
	        				<div class="user-name">
		        				<h6>Nicky</h6>
		        				<p>December 2018</p>
		        			</div>
	        			</div>
	        			<p class="more">We fell in love with the apartment the moment we entered. The location is amazing and the apartment is beautiful. The rooms are very spacious and provide a beautiful picturesque view. The apartment is so airy and the sunlight beams in, which I love the most. Feels lively. Sit with a cup of coffee in the evening, you can feel the calmness there. I canâ€™t describe how beautiful and cozy it was. The kitchen has a modern touch to it and the counter is so cute. He also provided us with.</p>
	        		</div>
	        		<div class="review-block">
	        			<div class="user">
	        				<img src="<?php echo get_bloginfo('template_url'); ?>/img/thumb-temp.png" alt="">
	        				<div class="user-name">
		        				<h6>Nicky</h6>
		        				<p>December 2018</p>
		        			</div>
	        			</div>
	        			<p class="more">We fell in love with the apartment the moment we entered. The location is amazing and the apartment is beautiful. The rooms are very spacious and provide a beautiful picturesque view. The apartment is so airy and the sunlight beams in, which I love the most. Feels lively. Sit with a cup of coffee in the evening, you can feel the calmness there. I canâ€™t describe how beautiful and cozy it was. The kitchen has a modern touch to it and the counter is so cute. He also provided us with.</p>
	        		</div>
	        	</div>
	        	<div class="detail-tile-col fixed">
	        		<div class="detail-form">
		        		<div class="title-line">
		        			<h3>Booking Details</h3>
		        		</div>
		        		<div class="form-row">
						<label class="floating-item">
							<input type="text" id="first-name" class="floating-item-input input-item" value="1 Bedroom" readonly />
						</label>
						</div>
						<div class="form-row select-list" id="detail-dateselect-manual" >
							<label class="floating-item" style="height: 45px">
							<!-- if(isset()) -->
							<?php  if (isset($_POST['ckout'])) {
								echo '<span class="input-item">'.$_POST['ckin'].'<i class="fa fa-arrow-right"></i>'.$_POST['ckout'].'</span>';
							}else{ ?>
								<span class="input-item">Check In <i class="fa fa-arrow-right"></i> Check Out</span>
								<?php } ?>
							</label>
						</div>
						<span class="err">There is a 7 night minimum for these dates. Extend your stay or inquire below.</span>
							<div class="show-availability" ></div>
							<?php if($_POST['totaldays']){ ?>
						<div class="detail-row price-detail">
							<h6>€<?php echo $ratevilla." x ".$_POST['totaldays']; ?> Nights</h6>
							<h6>€<?php echo $ratevilla*$_POST['totaldays']; ?></h6>
						</div>
						<div class="detail-row price-detail">
							<h6>Payable Amount</h6>
							<h6>€<?php echo $ratevilla*$_POST['totaldays']; ?></h6>
						</div>
								<?php } ?>
						<div class="detail-row">
							<div class="button button-secondary">
								<!-- <button>Inquire</button> -->
							<a href="http://luxurykey.madebyfire.com/contact/" target="_blank" >Inquire</a>

							</div>
		<form action="<?php echo MPHB()->settings()->pages()->getCheckoutPageUrl(); ?>"
			  method="POST"
			  id="mphb-recommendation"
			  class="mphb-recommendation">						<!-- 	mphb_check_in_date: 2019-05-01
mphb_check_out_date: 2019-05-16
mphb_rooms_details[21]: 1 -->
<?php wp_nonce_field( \MPHB\Shortcodes\CheckoutShortcode::NONCE_ACTION_CHECKOUT, \MPHB\Shortcodes\CheckoutShortcode::RECOMMENDATION_NONCE_NAME, true ); ?>
								<input type="hidden" id="rate_manual_villa" value="<?php echo $ratevilla; ?>" > 
								<input type="hidden" id="property_id" value="<?php echo  $post->ID; ?>" >
								<input type="hidden" name="mphb_rooms_details[<?php echo  $post->ID; ?>]"  id="property_id" value="1" >

							<?php // if(isset($_POST['ckin']) ){ ?>
								<input type="hidden" name="mphb_check_in_date" value="<?php echo $_POST['ckin']; ?>" id="check-in" >
								<input type="hidden" name="mphb_check_out_date" value="<?php echo $_POST['ckout']; ?>" id="check-out" >
							<?php 	 //} ?>

							<div class="button button-primary">
								<button type="submit" id="continue-booking-detail" >Continue to Booking</button>
							</div>
							</form>
						</div>
					</div>
	        	</div>
	        </div>
	        <!-- villas card section -->
			<div class="container container-type1 flexed-listing no-padding" id="flexed-listing">
				<div class="row">
					<div class="col-6">
						<div class="xlist-item">
		        			<img src="<?php echo get_bloginfo('template_url'); ?>/img/frame4.jpeg" alt="">
		        			<div class="xlist-item-feature">
		        				<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.svg" alt="images">
									<p>1
									 </p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.svg" alt="images">
									<p>2</p>
								</div>
		        			</div>
		        			<div class="xlist-item-desc">
		        				<div>
			        				<h4>Villa <strong> Cottage </strong></h4>
									<p>Panormos, Mykonos, Greece</p>
									<p>From: â‚¬450 / per night</p>
								</div>
								<div class="button button-primary button-small">
									<a href="#" tabindex="0">view villa</a>
								</div>
		        			</div>
		        		</div>
					</div>
					<div class="col-6">
						<div class="xlist-item">
		        			<img src="<?php echo get_bloginfo('template_url'); ?>/img/frame41.jpeg" alt="">
		        			<div class="xlist-item-feature">
		        				<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.svg" alt="images">
									<p>1</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.svg" alt="images">
									<p>2</p>
								</div>
								<div>
									<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.svg" alt="images">
									<p>2</p>
								</div>
		        			</div>
		        			<div class="xlist-item-desc">
		        				<div>
			        				<h4>Villa <strong> Cottage </strong></h4>
									<p>Panormos, Mykonos, Greece</p>
									<p>From: â‚¬450 / per night</p>
								</div>
								<div class="button button-primary button-small">
									<a href="#" tabindex="0">view villa</a>
								</div>
		        			</div>
		        		</div>
					</div>
				</div>
			</div>

			<?php 
			/*manual villa ends*/
		} ?>
			<?php
		get_footer();
			?>
