/*
* Search js condition check 
*/

$(document).ready(function() {
	$('#search_villa').on('click', function (e) {
		var dest = $('#destination').val();
		var prc_typ = $('#prc-typ-sel').val();
		var chk_in = $('#check-in').val();
		var chk_out = $('#check-out').val();
		var villa_prc = $('#vill-price').val();
		var villa_occs = $('#villa-occs').val();
		var no_beds = $('#no-of-bedrooms').val();
		if((dest == 'Private Islands' || dest == 'Other Global Destinations' || dest == 'Other Greek Islands') && (villa_occs == 'Villa Rentals')){
			console.log(blogUri+"/private-villas");
			window.location = blogUri+"/private-villas";  	
			return false;
		}else
		if(dest!='' && villa_occs=='Real Estate')
		{	window.location = blogUri+"/real-estate";
			return false;
		}else
		if(dest!='' && villa_occs=='Concierge')
		{	window.location = blogUri+"/concierge";
			return false;
		}else
		if(dest!='' && villa_occs=='Events Planning')
		{	window.location = blogUri+"/events";
			return false;
		}else
		if(dest!='' && villa_occs=='Wedding')
		{	window.location = blogUri+"/weddings";
			return false;
		}else{
			return true;
		}
	});
});