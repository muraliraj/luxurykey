<?php
/*
* Template Name: Home pageold
*/
?>

<?php
get_header();

?>
<!-- popup start -->
<!-- popup end -->
<!-- First banner and search -->



<div class="hero-banner dark-theme">
<div class="main-banner">



<?php
$args = array(
'post_type'=> 'banners',
'post_status' => 'publish',
'order' => 'DESC',
'numberposts' => -1
);
$banner_posts = get_posts($args);
?>

<?php   foreach ($banner_posts as $banner_post) { ?>

<a data-fancybox="test-srcset"
data-type="image"
data-srcset="<?php echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>"
data-width="1200"
data-height="720"
class="fancy-box" 
href="<?php  echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>" >
<img width="240" src="<?php  echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>" />
</a>


<?php } ?>

</div>
<div class="hero-banner-desc-center">
<div class="container">
<div class="hero-banner-content w-100">
<div class="c-heading">
<h1>Let's plan the holiday of your dreams</h1>
<div class="mobile-banner">
<div class="searchbar">
<div class="searchbar-input">
<a class="popup-with-zoom-anim" href="#small-dialog">
<i class="fa fa-search" aria-hidden="true"></i>
<span>Where do you want to go?</span>
</a>
</div>
</div>
</div>
</div>
<div class="search-bar-wrapper">
<div class="row">
<div class="col-3">
<div class="form-row select-list" id="mapicon">
<div class="drop-links links-icon">
<span class="filter">Choose a destination</span>
<ul class="select">
<li class="select-list-item" data-value="Mykonos">Mykonos</li>
<li class="select-list-item" data-value="Tulum">Tulum</li>
<li class="select-list-item" data-value="Ibiza">Ibiza</li>
<li class="select-list-item" data-value="Santorini">Santorini</li>
<li class="select-list-item" data-value="Other Greek Islands">Other Greek Islands</li>
<li class="select-list-item" data-value="Private Islands">Private Islands</li>
<li class="select-list-item" data-value="Other Global Destinations">Other Global Destinations</li>
</ul>
</div>
</div>
</div>
<div class="col-3">
<div class="form-row select-list">
<div class="input-item"><span>Check in</span> <i class="la la-arrow-right"></i> <span>Check out</span></div>
<div class="daterangepicker-options">
<h4>Flexible Days</h4>  
<div class="checkbox-group">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" />
<label class="checkboxradio-label radio-label" for="threedays"> +3/-3 Days
</label>
</div>
</div>
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" />
<label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
</label>
</div>
</div>
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" />
<label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
</label>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="col-3">
<div class="form-row select-list select-list-flex bedroom-search" >
<button type="button" class="subs" disabled >-</button>
<div class="select-list-data">
<input type="hidden" value="1" class="noOfRoom" id="noOfRoom" min="1" disabled="disabled"> 
<span class="noOfRoom">1</span>		
<span id="defaultText">Bedroom</span>		
</div>										
<button type="button" class="adds">+</button>										
</div>
</div>
<div class="col-2">
<div class="form-row select-list data-list-input">
<div class="drop-links">
<span class="filter">Price</span>
<i class="fa fa-map-marker-alt"></i>
<ul class="select data-list-input">
<li class="select-list-item" data-value=" $500 – 2,999/day">$500 – 2,999/day</li>
<li class="select-list-item" data-value="$3,000 – 5999/day">$3,000 – 5999/day</li>
<li class="select-list-item" data-value="$6,000 – 9.999/day"> $6,000 – 9.999/day</li>
<li class="select-list-item" data-value="10.000 – 16.000/day"> 10.000 – 16.000/day</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="diamond" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="diamond">Diamond
</label>
</div>
</div>
</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="platinum" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="platinum">Platinum
</label>
</div>
</div>
</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="gold" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="gold">Gold
</label>
</div>
</div>
</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="silver" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="silver">Silver
</label>
</div>
</div>
</li>
</ul>
</div>
</div>
</div>
<div class="col-1">
<div class="button button-primary">
<a href="<?php echo get_bloginfo('url'); ?>/accomodation" >Search</a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="location-now">
<a href="javaScript:void(0);" id="villa-search" ><span>SECRET COVE,</span> KAUAI</a>
</div>
</div>

<!-- mobile dialoge banner -->
<div id="small-dialog" class="homeFilter zoom-anim-dialog mfp-hide">
<div class="search-bar-wrapper search-bar-mobile">
<div class="modalHeader">
<span>Search</span>
</div>
<div class="form-row select-list" id="mapicon">
<div class="drop-links links-icon">
<span class="filter">Destination</span>
<ul class="select">
<li class="select-list-item" data-value="Mykonos">Mykonos</li>
<li class="select-list-item" data-value="Tulum">Tulum</li>
<li class="select-list-item" data-value="Ibiza">Ibiza</li>
<li class="select-list-item" data-value="Santorini">Santorini</li>
<li class="select-list-item" data-value="Other Greek Islands">Other Greek Islands</li>
<li class="select-list-item" data-value="Private Islands">Private Islands</li>
<li class="select-list-item" data-value="Other Global Destinations">Other Global Destinations</li>
</ul>
</div>
</div>
<div class="form-row select-list">
<span class="input-item-mob">Check in - Check out</span>
<div class="daterangepicker-options-mob">
<h4>Flexible Days</h4>  
<div class="checkbox-group">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedaysmob" type="radio" />
<label class="checkboxradio-label radio-label" for="threedaysmob"> +3/-3 Days
</label>
</div>
</div>
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendaysmob" type="radio" />
<label class="checkboxradio-label radio-label" for="sevendaysmob"> +7/-7 Days
</label>
</div>
</div>
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonthmob" type="radio" />
<label class="checkboxradio-label radio-label" for="wholemonthmob"> Whole Month
</label>
</div>
</div>
</div>
</div>
</div>
<div class="form-row select-list">
<div class="drop-links">
<span class="filter">Purpose</span>
<ul class="select ">
<li class="select-list-item" data-value="Villa Rental">Villa Rental</li>
<li class="select-list-item" data-value="Real Estate">Real Estate</li>
<li class="select-list-item" data-value="Concierge Services">Concierge Services</li>
<li class="select-list-item" data-value="Events">Events</li>
<li class="select-list-item" data-value="Weddings">Weddings</li>
<li class="select-list-item" data-value="Destination Experiences">Destination Experiences</li>
</ul>
</div>
</div>
<div class="form-row select-list">
<input type="number" value="1" class="noOfRoom" min="1"> 
<span id="defaultText">Bedroom</span>
<button type="button" class="adds">+</button>
<button type="button" class="subs" disabled >-</button>
</div>
<div class="form-row select-list data-list-input">
<div class="drop-links">
<span class="filter">Price</span>
<ul class="select data-list-input">
<!-- <li data-value="Silver $500 – 2,999/day">Silver $500 – 2,999/day</li>
<li data-value="Gold $3,000 – 5999/day">Gold $3,000 – 5999/day</li>
<li data-value="Platinum $6,000 – 9.999/day">Platinum $6,000 – 9.999/day</li>
<li data-value="Diamond 10.000 – 16.000/day">Diamond 10.000 – 16.000/day</li> -->
<li class="select-list-item" data-value=" $500 – 2,999/day">$500 – 2,999/day</li>
<li class="select-list-item" data-value="$3,000 – 5999/day">$3,000 – 5999/day</li>
<li class="select-list-item" data-value="$6,000 – 9.999/day"> $6,000 – 9.999/day</li>
<li class="select-list-item" data-value="10.000 – 16.000/day"> 10.000 – 16.000/day</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="diamond" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="diamond">Diamond
</label>
</div>
</div>
</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="platinum" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="platinum">Platinum
</label>
</div>
</div>
</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="gold" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="gold">Gold
</label>
</div>
</div>
</li>
<li class="checkbox-select">
<div class="checkboxradio">
<div class="checkboxradio-row">
<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="silver" type="checkbox" />
<label class="checkboxradio-label checkbox-label" for="silver">Silver
</label>
</div>
</div>
</li>
</ul>
</div>
</div>
<div class="button button-primary">
<a href="<?php echo get_bloginfo('url'); ?>/accomodation" >Search</a>
</div>
</div>
</div>
<!-- mobile dialoge banner -->
<div class="popup-overlay">
<div class="popup small transparent border-red" id="confirmVilla">
<div class="popup-closebtn"  data-id="confirmVilla">
×
</div>
<div class="popup-bgwrapper">
<img src="<?php echo get_bloginfo('template_url'); ?>/img/popup-bg.jpeg" alt="popup-bg" />
</div>
<div class="popup-body">
<div class="flexed-start full-width">
<h2>Lorem ipsum !</h2>								
</div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
<form>
<div class="form-row">
<label class="floating-item" data-error="Please enter Name">
<input type="text" id="popup-username" class="floating-item-input input-item" name="popup-username" value="" placeholder="Name" required />
</label>
</div>
<div class="form-row">
<label class="floating-item" data-error="Please enter Name">
<input type="email" id="popup-email" class="floating-item-input input-item" name="popup-email" value=""  placeholder="Email" required />
</label>
</div>
<div class="form-row">
<div class="button button-primary">
<button>Submit</button>
</div>
</div>
</form>
</div>
</div>
</div>
<!--End of First banner and search -->
<section class="section-services section-home">
<div class="container">
<div class="row">
<div class="col-10 center-block home-heading intro-block">
<h2>Destination experts and luxury villa mavens</h2>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
</div>
</div>
<div class="row text-center">
<div class="col-4">
<div class="border-heading">
<h4>Curated Villas</h4>
</div>		
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
</div>
<div class="col-4">
<div class="border-heading">
<h4>24/7 Concierge Services</h4>
</div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
</div>
<div class="col-4">
<div class="border-heading">
<h4>Best Rate Guarantee</h4>
</div>
<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
</div>
</div>
</div>
</section>



<section class="section-experience section-small" id="scrollnext">
<div class="container container-type1">
<div class="row">
<div class="col-12 text-center home-heading">
<h3>A world of experiences from Luxury Key</h3>
</div>
</div>
<div class="card-box type-1 services-slider-right">
<?php
$args = array(
'post_type'=> 'concierge',
'post_status' => 'publish',
'order' => 'DESC',
'numberposts' => -1
);
$banner_posts = get_posts($args);
?>

<?php   foreach ($banner_posts as $banner_post) { 

$excerpt = $banner_post->post_excerpt;

?>

<div class="col-4">
<a href="javascript:void(0)">
<figure> 
<img src="<?php  echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>" alt="services_1" />
<div class="button button-secondary figure-middle">
<button><?php echo $excerpt; ?></button>
</div>
<figcaption>
<div class="border-heading white">
<h4><?php echo $banner_post->post_title; ?></h4>
</div>
<p><?php echo apply_filters('the_content', $banner_post->post_content);?></p>
</figcaption>
</figure>
</a>					
</div>

<?php } ?>

</div>

<div class="card-box type-1 services-slider-left">
<?php
$args = array(
'post_type'=> 'concierge',
'post_status' => 'publish',
'order' => 'DESC',
'numberposts' => -1
);
$banner_posts = get_posts($args);
?>

<?php   foreach ($banner_posts as $banner_post) { 

$excerpt = $banner_post->post_excerpt;

?>

<div class="col-4">
<a href="javascript:void(0)">
<figure> 
<img src="<?php  echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>" alt="services_1" />
<div class="button button-secondary figure-middle">
<button><?php echo $excerpt; ?></button>
</div>
<figcaption>
<div class="border-heading white">
<h4><?php echo $banner_post->post_title; ?></h4>
</div>
<p><?php echo apply_filters('the_content', $banner_post->post_content);?></p>
</figcaption>
</figure>
</a>					
</div>

<?php } ?>

</div>

</div>
</section>
<?php
$args = array(
'post_type'=> 'mphb_room_type',
'post_status' => 'publish',
'orderby'=>'date',
'order' => 'ASC',
'numberposts' => 3
);
$banner_posts = get_posts($args);
?>


<!--  -->
		<section class="section-feature section-init">
			<div class="container container-type1">
				<div class="row">
      				<div class="col-12 text-center home-heading">
      					<h3>Featured Villas</h3>
      				</div>
      			</div>
				<div class="row feature-row">

<?php   foreach ($banner_posts as $banner_post) {
// echo $banner_post->ID;
$gallerymeta  = get_post_meta( $banner_post->ID, 'mphb_gallery', true );
$galleryimgs = explode(',', $gallerymeta);
$galleryimgs = array_filter($galleryimgs);
$gallerymeta = get_post_meta( $banner_post->ID, 'bedrrommphb_room_type', true );
$gallerymeta1 = get_post_meta( $banner_post->ID, 'mphb_adults_capacity', true );
$gallerymeta2 = get_post_meta( $banner_post->ID, 'bathroommphb_room_type', true );
?>
<!-- loop -->
<div class="col-4">
						<div class="feature-item">
							<div class="feature-item-slider">
							<?php 

							if(!empty($galleryimgs))
							{
								$qwe=0;
								foreach ($galleryimgs as $galleryimg) {
									if($qwe == 3){
										break;
									}
									
									if($galleryimg!=''){ ?>



										<div>
										<img src="<?php  echo wp_get_attachment_image_url($galleryimg,'full');  ?>" alt="">
                                   
										</div>
										
										<?php
									}
									$qwe++;
								}
							}else{
								?>
								<div>
									<img src="<?php echo wp_get_attachment_url( get_post_thumbnail_id( $banner_post->ID ) );  ?>" alt="">
								
								</div>
								<?php
							}
							?>
							</div>
							<div class="feature-item-content">
								<h5><?php echo $gallerymeta; ?> BEDROOMS, <?php echo $gallerymeta1; ?> GUESTS, <?php echo $gallerymeta2; ?> BATHS</h5>
								<h3><?php echo $banner_post->post_title; ?></h3>
								<p>from $1,164 / night</p>
								<p><a href="#!">Pyrgi</a>, Greece</p>
							</div>
						</div>
					</div>	
<!-- loop ends -->

<?php }
?>
</div>
			</div>
		</section>



<?php	
get_footer(); 
?>