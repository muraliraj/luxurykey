<?php 
get_header();
/***********************
Template Name: My History
************************/ ?>
	        <!-- End of header -->
        	<div class="container-fluid">
        		<div class="row accout-tile">
	        		<aside class="col-5">
	        			<div class="accout-tile-left">
		        			<h1>User Name</h1>
		        			<ul>
		        					<li><a href="<?php echo get_bloginfo('url'); ?>/my-account/" ><i class="fa fa-user" aria-hidden="true"></i>My Account</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/my-booking" ><i class="fa fa-bookmark" aria-hidden="true"></i>My Booking</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/my-history" class="active"><i class="fa fa-unlock-alt" aria-hidden="true"></i>History</a></li>
		        				<li><a href="<?php echo get_bloginfo('url'); ?>/change-password"><i class="fa fa-history" aria-hidden="true"></i>Change Password</a></li>
		        				<li><a href="<?php echo wp_logout_url(home_url()); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i>Log Out</a></li>
		        			</ul>
		        		</div>
	        		</aside>
	        		<div class="col-7">
	        			<div class="accout-tile-content">
		        			<h3>My Booking</h3>
		        			<div class="row section-padding">
			        			<div class="col-6 listItem">
							        <div class="xlist-item">
					        			<img src="<?php echo get_bloginfo('template_url'); ?>/img\wide_cottage.jpeg" alt="">
					        			<div class="xlist-item-desc">
					        				<div>
					        					<p>$450 x 3 Nights = <strong>$1,350</strong></p>
						        				<h4>Villa <strong> Cottage </strong></h4>
												<p>Panormos, Mykonos, Greece</p>
											</div>
											<div>
												<div class="booking-date">24 Jan 2019 - 27 Jan 2019</div>
												<div class="button">
													<a href="#">Cancelled</a>
												</div>
											</div>
					        			</div>
					        		</div>
								</div>
								<div class="col-6 listItem">
							        <div class="xlist-item">
					        			<img src="<?php echo get_bloginfo('template_url'); ?>/img\frame4.jpeg" alt="">
					        			<div class="xlist-item-desc">
					        				<div>
					        					<p>$450 x 3 Nights = <strong>$1,350</strong></p>
						        				<h4>Villa <strong> Cottage </strong></h4>
												<p>Panormos, Mykonos, Greece</p>
											</div>
											<div>
												<div class="booking-date">24 Jan 2019 - 27 Jan 2019</div>
												<div class="button button-secondary">
													<a href="#">Write review</a>
												</div>
											</div>
					        			</div>
					        		</div>
								</div>
							</div>
		        		</div>
	        		</div>
	        	</div>
        	</div>

		</div>	
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/app.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/wow.js"></script>
<!-- 	  	<script type="text/javascript">
		  	var jsArr = ['js/wow.js','js/app.js'];
			for(var j = 0; j < jsArr.length; j++) {
		        var script = document.createElement('script')
		        script.setAttribute('type', 'text/javascript')
		        script.setAttribute('src', jsArr[j])
		        document.getElementsByTagName('head')[0].appendChild(script)
		    }
	    </script> -->
	</body>
</html>