function isValidEmailAddress(r) {
  var e = RegExp(/^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i);
  return e.test(r)
}
function nameValidate($this){
 input_value = $this.value;
 if (input_value == undefined){
  input_value = $this.val();
}
input_value = input_value.replace(/\s\s+/g, ' ').trim();
if(checkEmpty($this)){
  $(this).closest('.form-field').find('.error-message').slideUp();
  return 1;
}
}
function featureSlickSlider(){
  return  {            
    autoplay:false,
    slidesToShow: 1,
    slidesToScroll: 1,
    fade: true,
    cssEase: 'linear',
    arrows: true,
    dots: false,
    dotsClass:'slick-dots slider-dots',
    prevArrow: "<i class='la la-angle-left slick-arrow-small'></i>",
    nextArrow: "<i class='la la-angle-right slick-arrow-small'></i>",

  }  
}

function ValidatePAN()
{
 var pan_no = document.getElementById("dealer_pan");

 if (pan_no.value != "") {
  PanNo = pan_no.value;
  var panPattern = /^([a-zA-Z]{5})(\d{4})([a-zA-Z]{1})$/;
  if (PanNo.search(panPattern) == -1) {
              //  alert("Invalid Pan No");

              pan_no.value='';
              return false;
            }

          }
        }

        /*Telephone validation*/
        function isNumber(elementRef) {
          keyCode=elementRef.charCode;
          if ((keyCode >= 48) && (keyCode <= 57) || (keyCode <= 32)) {
            return true;
          }  else if (keyCode == 43) {
            if (jQuery('#'+elementRef.target.id).val().trim().length == 0){
              return true;
            } else {
              return false;
            }
          }
          return false;
        }

        /*Name validation*/
        function onlyAlphabets(e) {
          try {
            if (window.event) {
              var charCode = window.event.keyCode;
            }else if (e) {
              var charCode = e.which;
            } else {
              return true;
            }
            if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 32 || charCode==0 || charCode==8){
              return true;
            }else{
              return false;
            }
          }
          catch (err) {
            alert(err.Description);
          }
        }


        /*validate email with charCode*/
        jQuery(document).on('keypress','#email,#applicantemail',function(e){
          $(this).attr('maxlength','100');
          try {
            if (window.event) {
              var charCode = window.event.keyCode;
            } else if (e) {
              var charCode = e.which;
            } else { return true; }
            if ((charCode > 63 && charCode < 91) || (charCode > 96 && charCode < 123) || (charCode > 47 && charCode < 58) || charCode==0 || charCode==8 || charCode==46 || charCode==45 || charCode==95){
              return true;
            } else {
              return false;
            }
          }
          catch (err) {
            alert(err.Description);
          }
        });

//     $(document).on({
//     "contextmenu": function(e) {
//         console.log("ctx menu button:", e.which); 

//         // Stop the context menu
//         e.preventDefault();
//     },
//     "mousedown": function(e) { 
//         console.log("normal mouse down:", e.which); 
//     },
//     "mouseup": function(e) { 
//         console.log("normal mouse up:", e.which); 
//     }
// });

//  jQuery(document).on('keypress','#telephone,#applicanttelephone',function(e){
//   if (event.keyCode == 32) {
//         return false;
//     }
// });

// jQuery(document).on('keypress','.input-item',function(e){
//   if (event.keyCode == 32) {
//         return false;
//     }
// });

// jQuery(document).on('keypress','.alu-spdes,.alu-sp,#club_name,#dealer_phone,#dealer_contact,#description,#albumname,#telephone,#firstname,#city,#state,#country,#message,#reg_name,#dealer_full_name,#dealer_ship_name,#dealer_address1,#dealer_address2,#dealer_state,#dealer_city,#departure_from,#arrived_at,#story_title,#your_story',function(e){
//   try {
//   if (window.event) {
//     var charCode = window.event.keyCode;
//   }
//   else if (e) {
//     var charCode = e.which;
//   }
//   else { return true; }
//   if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode==0 || charCode==8)
//   return true;
//   else if ((charCode === 32 && !this.value.length))
//   return false;
//   else if (charCode == 32)
//   return true;
//   else
//     return false;
//   }
//   catch (err) {
//    // alert(err.Description);
//   }
// });

/*allow only one space*/
var lastkey;
var ignoreChars = ' '+String.fromCharCode(0);

jQuery(document).on('keypress','#reg_name,#last_name',function(e){
 e = e || window.event;
 var char = String.fromCharCode(e.charCode);
 if (ignoreChars.indexOf(char) == 0 && ignoreChars.indexOf(lastkey) == 0) {
   lastkey = char;
   return false;
 } else {
   lastkey = char;
   return true;
 }
});


/*********Mobile Validation*********/
var ua = navigator.userAgent.toLowerCase();
if (ua.indexOf("android") > -1 && !(ua.indexOf('chrome firefox') > -1)) {
  $(document).on('keyup keypress','#dealer_ship_name,#applicantname,#message,#address,#applicantmessage',function(e) {
    var regex = /^[a-zA-Z]$/;
    var regexSpace = /^[a-zA-Z\s]$/;
    var str = $(this).val();
    var subStr = str.substr(str.length - 1);
    if (!regex.test(subStr)) {
      if (str.length == 1) {
        $(this).val(str.substr(0, (str.length - 1)));
      }
      else if (str.length > 1) {
        if (!regexSpace.test(subStr)) {
          $(this).val(str.substr(0, (str.length - 1)));
        }
      }
      else {
        $(this).val();
      }
    }
  });
}
var ua = navigator.userAgent.toLowerCase();
if (ua.indexOf("android") > -1 && !(ua.indexOf('chrome firefox') > -1)) {
  $(document).on('keyup keypress','#email','#applicantemail',function(e) {
    var regex = /^[a-zA-Z0-9@_-]$/;
    var regexSpace = /^[a-zA-Z0-9.@_!#$%^&()=,[]|{}]$/;
    var str = $(this).val();
    var subStr = str.substr(str.length - 1);
    if (!regex.test(subStr)) {
      if (str.length == 1) {
        $(this).val(str.substr(0, (str.length - 1)));
      }
      else if (str.length > 1) {
        if (!regexSpace.test(subStr)) {
          $(this).val(str.substr(0, (str.length - 1)));
        }
      }
      else {
        $(this).val();
      }
    }
  });
}
var ua = navigator.userAgent.toLowerCase();
if (ua.indexOf("android") > -1 && !(ua.indexOf('chrome firefox') > -1)) {
  jQuery('#telephone,#applicanttelephone,#dealer_phone','#reg_phone_number','#dealer_contact').prop('type','tel');
  $('#telephone','#dealer_phone','#reg_phone_number','#dealer_contact').bind('input keyup keypress', function(e) {
    var regex = /^[0-9]*$/;
    var regexSpace = /^[+0-9]*$/;
    var str = $(this).val();
    var subStr = str.substr(str.length - 1);
    if (regex.test(subStr)) {
     $(this).val();
   }
   else {
    if (str.length == 1) {
     $(this).val(str.substr(0, (str.length - 1)));
   } 
   else if (str.length > 1) {
    if (!regexSpace.test(subStr)) {
     $(this).val(str.substr(0, (str.length - 1)));
   }
 }
 else {
  $(this).val();
}
}
}); 
}

function trim (el) {
  el.value = el.value.
       replace (/(^\s*)|(\s*$)/gi, ""). // removes leading and trailing spaces
       replace (/[ ]{2,}/gi," ").       // replaces multiple spaces with one space 
       replace (/\n +/,"\n");           // Removes spaces after newlines
       return;
     }

     /* #end jawasonroad form */
     var err_email = "Enter your email address";
     var err_name = "Enter your name";
     var err_emailvalid = "Enter a valid email address";

     /********* Newsletter subscription validation ***********/
     $('#subscribe-button').on('click',function (e) {
      e.preventDefault();
      $('.msg_sub').hide();
      $('.err-msg').hide();
      var $this = $(this);
      var sub_email = $('#subscribe-email').val();
      var sub_name = $('#subscribe-name').val();
      var x=0;
      sub_email = jQuery.trim(sub_email);
      if (sub_name == '' || sub_name == null) {
        $('#err_sub_name').text(err_name);
        $('#err_sub_name').css('display', 'block'); 
        x++;           
      } else {
        $('#err_sub_name').css('display', 'none');
      }


      if (sub_email == '' || sub_email == null) {
        $('#err_sub_email').text(err_email);
        $('#err_sub_email').css('display', 'block');            
        x++;
      } else if (!isValidEmailAddress(sub_email)) {
        $('#err_sub_email').text(err_emailvalid);
        $('#err_sub_email').css('display', 'block');
        x++;
      } else {
        $('#err_sub_email').css('display', 'none');
      }
      if (x==0) {
        $.ajax({
          type: "POST",
          url: templateUri+"/ajax/ajax.php",
          data: {email: sub_email,name: sub_name, action: 'subscribe'}
        }).done(function (msg) {
          
          if(msg==1)
          {
            $('#subscribe-email').val('');
            $('#subscribe-name').val('');
            $('.err-msg').hide();
            $('.msg_sub').show();

            setTimeout(function(){ 
              $('#preloader_sub').css('display','none');
              $('#subscribe-row').hide();
              $('#submitBtn').css('display','block');
            }, 500); 
          } else {
            $('#subscribe-email').val('');
            $('#subscribe-name').val('');
            $('.err-msg').show();
            $('.msg_sub').hide();
            
            setTimeout(function(){ 
              $('#preloader_sub').css('display','none');
              $('#submitBtn').css('display','block');
            }, 800); 
          }
        });
      }
      return false;
    });
     function formatDate(date) {
       var d = new Date(date),
       month = '' + (d.getMonth() + 1),
       day = '' + d.getDate(),
       year = d.getFullYear();

       if (month.length < 2) month = '0' + month;
       if (day.length < 2) day = '0' + day;

       return [year, month, day].join('-');
     }
     function calculateNights(date) {
      var dateSplit = date.split('-');
      var arrivalday = parseInt(dateSplit[1]);
      var departureday = parseInt(dateSplit[0]);
      var numberNights = departureday - arrivalday;
      numberNights = (numberNights < 1) ? numberNights + 7 : numberNights;
    }
    function showDays(date){
      if(date.length) {
        var dateSplit = date.split('-');
        var startDay = new Date(dateSplit[1]);
        var endDay = new Date(dateSplit[0]);
        var millisecondsPerDay = 1000 * 60 * 60 * 24;

        var millisBetween = startDay.getTime() - endDay.getTime();
        var days = millisBetween / millisecondsPerDay;

      // Round down.
      noOfDays = Math.floor(days);
      noOfNights = noOfDays;
      var priceVal = $('.pricepernight').text();
      $('.nights').text(noOfNights);
      $('#payment-nights').val(noOfNights);
      $('.total').text(priceVal*noOfNights);
      $('#amount').val(priceVal*noOfNights);
    }


  }
  
  $( document ).ready(function() {
    $('.global-message-close').on('click', function (e) {
      e.preventDefault();
      $('.global-message').slideUp();
    });
    jQuery( document ).on('click',"#sign-up",function(event) {
     var reg_phone_number=jQuery('#reg_phone_number').val();
     var reg_name=jQuery('#reg_name').val();
     var last_name=jQuery('#last_name').val();
     var email=jQuery('#email').val();
     var upass=jQuery('#reg_pwd').val();
     var regex = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
     var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/i;
     var x=0;


     if (reg_name=='' || reg_name == undefined) {
       jQuery('#err_name').parents('.form-row').addClass('error-row');
       jQuery('#err_name').text("Enter your first name").show();

       x++;

     } else {
       jQuery('#err_name').parents('.form-row').removeClass('error-row');
       jQuery('#err_name').hide();
     }

     if (last_name=='' || last_name == undefined) {
       jQuery('#errl_name').parents('.form-row').addClass('error-row');
       jQuery('#errl_name').text("Enter your last name").show();

       x++;

     } else {
       jQuery('#errl_name').parents('.form-row').removeClass('error-row');
       jQuery('#errl_name').hide();
     }



     if (email!='') {
      if (!regex.test(email)) {
        jQuery('#err_email').hide();
        jQuery('#err_email').parents('.form-row').addClass('error-row');
        jQuery('#err_email').text("Enter a valid email address").show();
        x++;
      } else {
        jQuery('#err_email').parents('.form-row').removeClass('error-row');
        jQuery('#err_email').hide();
      }
    } else {
     jQuery('#err_email').hide();
     jQuery('#err_email').text("Enter your email address").show();
     jQuery('#err_email').parents('.form-row').addClass('error-row');
     x++;
   }



   if (reg_phone_number=='' || reg_phone_number == undefined) {
    jQuery('#err_phone_number').parents('.form-row').addClass('error-row');
    jQuery('#err_phone_number').text("Enter your phone number").show();
    x++;
  } else {
    if(reg_phone_number.length<10) {
      jQuery('#err_phone_number').parents('.form-row').addClass('error-row');
      jQuery('#err_phone_number').text("Enter a valid phone number").show();
      x++;
    } else {
      jQuery('#err_phone_number').parents('.form-row').removeClass('error-row');
      jQuery('#err_phone_number').hide();
    }
  }



  if (upass=='' || upass == undefined) {
   jQuery('#err_register_password').parents('.form-row').addClass('error-row');
   jQuery('#err_register_password').text("Enter your password").show();

   x++;

 } else {
   jQuery('#err_register_password').parents('.form-row').removeClass('error-row');
   jQuery('#err_register_password').hide();
 }


 if (x==0) {
   return true;
 }
 return false;

});



   
    $(".select-list #date-home").on("apply.daterangepicker",function(t,e){
      $('#check-in').val(e.startDate.format("YYYY-MM-DD"));
      $('#check-out').val(e.endDate.format("YYYY-MM-DD"));
      console.log(e.startDate);

    });


    jQuery( document ).on('click',"#change-password",function(event) {

     var old_password=jQuery('#old-password').val();
     var new_password=jQuery('#new-password').val();
     var confirm_password=jQuery('#confirm-password').val();
     var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/i;
     var x=0;


     if (old_password=='' || old_password == undefined) {
       jQuery('#err_old_password').parents('.form-row').addClass('error-row');
       jQuery('#err_old_password').text("Enter your old password").show();

       x++;

     } else {
       jQuery('#err_old_password').parents('.form-row').removeClass('error-row');
       jQuery('#err_old_password').hide();
     }


     if (new_password=='' || new_password == undefined) {
       jQuery('#err_new_password').parents('.form-row').addClass('error-row');
       jQuery('#err_new_password').text("Enter your new password").show();

       x++;

     } else {
       jQuery('#err_new_password').parents('.form-row').removeClass('error-row');
       jQuery('#err_new_password').hide();
     }

     if (confirm_password=='' || confirm_password == undefined) {
       jQuery('#err_confirm_password').parents('.form-row').addClass('error-row');
       jQuery('#err_confirm_password').text("Re enter your new password").show();

       x++;

     } else {
       jQuery('#err_confirm_password').parents('.form-row').removeClass('error-row');
       jQuery('#err_confirm_password').hide();
     }


     if (x==0) {
       return true;
     }
     return false;
     
   });
    $('.adds').click(function() {
      var $rooms = $("#noOfRoom");
      $roomlength = $(document).find('#no-of-bedrooms').length
      var a = $rooms.val();
      console.log(a);
      if($roomlength >0){
        $(document).find('#no-of-bedrooms').val(a);
      } 

    });

    $('.subs').click(function() {
      var $rooms = $("#noOfRoom");
      var b = $rooms.val();
      $roomlength = $(document).find('#no-of-bedrooms').length
      if($roomlength >0){
        $(document).find('#no-of-bedrooms').val(b);

      }
    });
    $('.destination-sel li').on('click',function(){
      var value = $(this).attr('data-locs');
      $('#destination').val(value);
    });
    $('.no_beds li').on('click',function(){
      var beds = $(this).attr('data-beds');
      $('#no-of-bedrooms').val(beds);
      $(this).parents('.drop-links').find('span').html(beds + ' ' + ' Bedrooms');
      $(this).parent().removeClass('selected');
      $(this).parent().slideUp();

      $(this).parents('.form-row').removeClass('active');
    });
    $('.villa-occs li').on('click',function(){
      var value = $(this).attr('data-occs');
		var dest_val = $(document).find('#destination').val();
    $('#villa-occs').val(value)
		
    });
    $('#search_villa').on('click',function(e){
      // e.preventDefault();
      var x = 0;
      var dest = $('#destination').val()
      if(dest == ''){
        $(".global-message").slideDown();
        x++;
      }else{
        $(".global-message").slideUp();
        
      }

      if(x == 0){
        return true;
      }else{
        return false;
      }
      alert(x);
    });
    
    $('.prc-typ-sel li').on('click',function(){
      var value = $(this).text();
      $('#prc-typ-sel').val(value);

    });
    $('#detail-dateselect').daterangepicker({
      autoUpdateInput: false,
      autoApply:true,
      locale: {
        cancelLabel: 'Clear',
        direction: 'ltr banner-daterangepicker'
      }                  
    });
    $('#detail-dateselect').on('apply.daterangepicker', function(ev, picker) {
      $(this).html('<div class="input-item"><span>'+picker.startDate.format('DD MM YYYY')+'</span><i class="la la-arrow-right"></i></span><span>'+picker.endDate.format('DD MM YYYY')+'</span></div>');
      $('#check-in').val( picker.startDate.format('YYYY-MM-DD'));
      $('#check-out').val(picker.endDate.format('YYYY-MM-DD'));
                    var diff = picker.endDate.diff(picker.startDate, 'days'); // returns correct number
                    $.ajax({
                      type: "POST",        
                      dataType: "json",
                      url: blogUri+"/wp-admin/admin-ajax.php",
                      data: {action:"searchvilla_detail",room:$('#property_id').val(), checkin:$('#check-in').val(), checkout:$('#check-out').val()},
                      success :function(data){
                        if(data==-1)
                        {
                          $('.show-availability').html('This villa is not available for the selected criteria. Please change date to check availability.')
                          $('#continue-booking-detail').attr('disabled','disabled');
                          $('#continue-booking-detail').addClass('btn-custom-disabled');
                        }else{
                          $('.show-availability').html('')
                          $('#continue-booking-detail').removeAttr('disabled','disabled');
                          $('#continue-booking-detail').removeClass('btn-custom-disabled');
                          $('.detail-row.price-detail').each(function(){
                            $(this).remove();
                          });
                          $('.show-availability').after('<div class="detail-row price-detail "><h6>€'+data/diff+' x '+diff+' Nights</h6><h6>€'+data+'</h6></div><div class="detail-row price-detail"><h6>Payable Amount</h6><h6>€'+data+'</h6></div>')

                        }
                      }

                    }).done(function (data) {
                     console.log(data);
                   });

                  });
    
    $('#detail-dateselect').on('cancel.daterangepicker', function(ev, picker) {
      $(this).text('Check in - Check out');
    });
    /*api villa ends*/
    /*manual villa starts*/
    $('#detail-dateselect-manual').daterangepicker({
      autoUpdateInput: false,
      autoApply:true,
      locale: {
        cancelLabel: 'Clear',
        direction: 'ltr banner-daterangepicker'
      }                  
    });
    $('#detail-dateselect-manual').on('apply.daterangepicker', function(ev, picker) {

      $(this).html('<div class="input-item"><span>'+picker.startDate.format('DD MM YYYY')+'</span><i class="la la-arrow-right"></i></span><span>'+picker.endDate.format('DD MM YYYY')+'</span></div>');
      $(this).parent(".form-row").addClass("active");
      // $(this).html(picker.startDate.format('MM/DD/YYYY') + ' <i class="fa fa-arrow-right"></i> ' + picker.endDate.format('MM/DD/YYYY'));
      $('#check-in').val( picker.startDate.format('YYYY-MM-DD') );
      $('#check-out').val(picker.endDate.format('YYYY-MM-DD'));
                    var diff = picker.endDate.diff(picker.startDate, 'days'); // returns correct number
                    $.ajax({
                      type: "POST",        
                      dataType: "json",
                      url: blogUri+"/wp-admin/admin-ajax.php",
                      data: {action:"searchvilla_detail_manual",room:$('#property_id').val(), checkin:$('#check-in').val(), checkout:$('#check-out').val()},
                      success :function(data){
                        if(data==-1)
                        {
                          $('.show-availability').html('This villa is not available for the selected criteria. Please change date to check availability.')
                          $('#continue-booking-detail').attr('disabled','disabled');
                          $('#continue-booking-detail').addClass('btn-custom-disabled');
                        }else{
                          var rate_manual_villa = $('#rate_manual_villa').val();
                          $('.show-availability').html('');
                          $('.booking-price h6').html('');
                          $('.booking-price p').html('');
                          $('#continue-booking-detail').removeAttr('disabled','disabled');
                          $('#continue-booking-detail').removeClass('btn-custom-disabled');
                          $('.detail-row.price-detail').each(function(){
                            $(this).remove();
                          });
                          $('.booking-price h6').html(rate_manual_villa*diff);
                          $('.booking-price p').html('('+rate_manual_villa+'X'+diff+' nights)');

                        }
                      }

                    }).done(function (data) {
                     console.log(data);
                   });

                  });

    $('#detail-dateselect-manual').on('cancel.daterangepicker', function(ev, picker) {
      $(this).text('Check in - Check out');
    });
    /*manual villa ends*/
    $('.filter-options label').on('click',function(){
      var value = $(this).text();



      $(".listItem").each(function() {
        var labels = $('.listItem').attr('data-destination');
        if($('.listItem').attr('data-destination')==value.trim()) {
          $('.'+$('.listItem').attr('data-destination')).show();
        } else {
          $('.'+$('.listItem').attr('data-destination')).hide();
        }
      });

  
    });
      $('.sign-nav a, .account-link a, .forget-text a').on('click', function(){
            $(this).parents('.popup-overlay').removeClass('active');
            $('body,html').removeClass('hidden-body');
            var id = $(this).attr('data-id');
            $('#'+id).removeClass("open");
            var main = $(this).attr('data-id');

            $('#' + main).addClass('active').fadeIn(200); 
            $('body').addClass('y-hidden'); 
        });


    $('#sign-in').on('click',function(e){
      e.preventDefault();
      var username = $('#user_name').val();
      var password = $('#pass_word').val();
      var vill_dest = $(document).find('#villa-destination').val();
      var vill_prc_typ = $(document).find('#villa-price-typ').val();
      var vill_chk_in = $(document).find('#villa-check-in').val();
      var vill_chk_out = $(document).find('#villa-check-out').val();
      
      

      var regpan = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/i;
     var x=0;


     if (user_name=='' || user_name == undefined) {
       jQuery('#err_name1').parents('.form-row').addClass('error-row');
       jQuery('#err_name1').text("Enter your email address").show();

       x++;

     } else {
       jQuery('#err_name1').parents('.form-field').removeClass('error-row');
       jQuery('#err_name1').hide();
     }


     if (pass_word=='' || pass_word == undefined) {
       jQuery('#err_register_password1').parents('.form-row').addClass('error-row');
       jQuery('#err_register_password1').text("Enter your password").show();

       x++;

     } else {
       jQuery('#err_register_password1').parents('.form-field').removeClass('error-row');
       jQuery('#err_register_password1').hide();
     }


     if (x==0) {
      $.ajax({
        type:'POST',
     // data:{"action":"Attending","data": form },
     url: blogUri+"/wp-admin/admin-ajax.php",
     data: {action:"custom_login",username:username,password:password},
     success: function(data) {
        var resp = data.trim();
        resp = resp.split('~');
        
         if(resp[0] == 1){
            window.location.href='/my-account';
        }else{
          $('#wp_login_form').html(data)
        } 
            // 
          }

        }); 
       
     }else{
      return false;
      
        }

    });




    $('#sign-up').on('click',function(e){
      e.preventDefault();
      console.log('jere');
      var reg_name = $('#reg_name').val();
      var lname = $('#last_name').val();
      var email = $('#email').val();
      var reg_phone_number = $('#reg_phone_number').val();
      var reg_pwd = $('#reg_pwd').val();
    //alert(lname);
    $.ajax({
      type:'POST',
     // data:{"action":"Attending","data": form },
     url: blogUri+"/wp-admin/admin-ajax.php",
     data: {action:"create_account",reg_name:reg_name,lname:lname,email:email,reg_phone_number:reg_phone_number,reg_pwd:reg_pwd},
     success: function(data) {
      
     var resp = data.trim();
        resp = resp.split('~')
        
         if(resp[0] == 1){
            window.location.href='/my-account';
        }else{
          $('#wp_reg_form').html(data)
        } 
            // 
          }

        }); 

  });

    $('.price-sel li').on('click',function(){
      var value = $(this).attr('data-prc-type');
      $('#vill-price').val(value);
      $('#min-price').val(value.split('-')[0]);
      $('#max-price').val(value.split('-')[1]);

    });
    var html = $('#laneconfigdisplay').html()
    setInterval(function(){ 
      if($('#laneconfigdisplay').html() != html){ 
        html = $('#laneconfigdisplay').html();
        showDays(html);
        var dateSplit = html.split('-');
        var startDay = new Date(dateSplit[1]);
        var endDay = new Date(dateSplit[0]);
        $('#checkin').val(formatDate(dateSplit[0]));
        $('#checkout').val(formatDate(dateSplit[1]));

      } 
  }, 1000) //checks your content box all 10 seconds and triggers alert when content has changed..

    $("ul#villa_dest").on("click","li", function(){
      var vill_dest_set = $(this).attr('data-locs');
      $(document).find('#villa-destination').val(vill_dest_set);
      var vill_dest = $(document).find('#villa-destination').val();
      var vill_prc_typ = $(document).find('#villa-price-typ').val();
      var vill_chk_in = $(document).find('#villa-check-in').val();
      var vill_chk_out = $(document).find('#villa-check-out').val();
      var vill_prc = $(document).find('#villa-price').val();
      var vill_prc_min = $(document).find('#villa-price-min').val();
      var vill_prc_max = $(document).find('#villa-price-max').val();
      var vill_bed = $(document).find('#no-of-bedrooms').val();
      if(vill_chk_in == '' && vill_chk_out==''){
        $(".global-message").slideDown();

        $('.more-filter-villa .drop-links span').addClass('curser-disabled pointer-none ');
        return false;
      }else{
        if($(document).find('.global-message').length > 0){
          $(".global-message").slideUp();
          $('.more-filter-villa .drop-links span').removeClass('curser-disabled .pointer-none');
        }
        return true;
      }
      $.ajax({
        type: "POST",
        dataType: "html",
        url: blogUri+"/wp-admin/admin-ajax.php",
        data: {action:"filter_villa",vill_dest:vill_dest, vill_prc_typ:vill_prc_typ, vill_prc:vill_prc, vill_chk_in:vill_chk_in, vill_chk_out:vill_chk_out,vill_prc_min:vill_prc_min,vill_prc_max:vill_prc_max, vill_bed:vill_bed},
        success :function(data){
          console.log($(document).find('#villa-destination').val());
          var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          // $('.feature-item-slider').removeClass("slick-initialized slick-slider");
         // $(".feature-item-slider").slick(featureSlickSlider());
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }

          var strobj = resp_data[2];
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            loCntry = $(document).find('#villa-destination').val();

            console.log('beforesend->'+loCntry);
            console.log('aftersend->'+vill_dest_set);
            
            initMap(objArray,loCntry);

          }else{
            $('.list-icon').click();
          }
          if(vill_dest_set != "Mykonos" || resp_data[1] == 0){
            $('.map-icon').click();
          }
          $(".feature-item-slider").slick(featureSlickSlider());

        }
      }).done(function (data) {
        var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          // $('.feature-item-slider').removeClass("slick-initialized slick-slider");
         // $(".feature-item-slider").slick(featureSlickSlider());
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }
          console.log('ajax success');
          $(".feature-item-slider").slick(featureSlickSlider());
        
      });

    });
     $('.map-icon').click();
    $(document).on('click','.villadata', function (e) {
      console.log($(this).find('form').length);
      $(this).find('form').submit();


      return false;
    });
    $('input:radio[name="checkgroup"]').change(
    function(){
        if ($(this).is(':checked')) {
          var vill_dest = $(document).find('#villa-destination').val();
          var vill_prc_typ = $(document).find('#villa-price-typ').val();
          var vill_chk_in = $(document).find('#villa-check-in').val();
          var vill_chk_out = $(document).find('#villa-check-out').val();
          var vill_prc = $(document).find('#villa-price').val();
          var vill_prc_min = $(document).find('#villa-price-min').val();
          var vill_prc_max = $(document).find('#villa-price-max').val();
          var vill_bed = $(document).find('#no-of-bedrooms').val();
          var villaXmlData_flex = $(this).val();
          if(vill_chk_in == '' && vill_chk_out==''){
            $(".global-message").slideDown();
            $('.drop-links.span').addClass('curser-disabled');
            return false;
          }else{
            if($(document).find('.global-message').length > 0){
              $(".global-message").slideUp();
              $('.drop-links.span').removeClass('curser-disabled');
            }
          }
          $.ajax({
            type: "POST",
            dataType: "html",
            url: blogUri+"/wp-admin/admin-ajax.php",
            data: {action:"filter_villa",vill_dest:vill_dest, vill_prc_typ:vill_prc_typ, vill_prc:vill_prc, vill_chk_in:vill_chk_in, vill_chk_out:vill_chk_out,vill_prc_min:vill_prc_min,vill_prc_max:vill_prc_max, vill_bed:vill_bed,villaXmlData_flex:villaXmlData_flex},
            success :function(data){

              var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }
              $(".feature-item-slider").slick(featureSlickSlider());

            }
          }).done(function (data) {
           var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }
          $(".feature-item-slider").slick(featureSlickSlider());
            
          });
        }
    });

    $("ul#villa_prc").on("click","li", function(){
      var data_prc_val = $(this).attr('data-prc');
      var data_prc = data_prc_val.split('-');
      var data_prc_min = data_prc[0];
      var data_prc_max = data_prc[1];
      $(document).find('#villa-price-min').val(data_prc_min);
      $(document).find('#villa-price-max').val(data_prc_max);
      $(document).find('#villa-price').val(data_prc_val);
      var vill_dest = $(document).find('#villa-destination').val();
      var vill_prc_typ = $(document).find('#villa-price-typ').val();
      var vill_chk_in = $(document).find('#villa-check-in').val();
      var vill_chk_out = $(document).find('#villa-check-out').val();
      var vill_prc = $(document).find('#villa-price').val();
      var vill_prc_min = $(document).find('#villa-price-min').val();
      var vill_prc_max = $(document).find('#villa-price-max').val();
      var vill_bed = $(document).find('#no-of-bedrooms').val();
      if(vill_chk_in == '' && vill_chk_out==''){
        $(".global-message").slideDown();
        $('.more-filter-villa .drop-links span').addClass('curser-disabled pointer-none ');
        return false;
      }else{
        if($(document).find('.global-message').length > 0){
          $(".global-message").slideUp();
          $('.more-filter-villa .drop-links span').removeClass('curser-disabled .pointer-none');
        }
        return true;
      }
      $.ajax({
        type: "POST",
        dataType: "html",
        url: blogUri+"/wp-admin/admin-ajax.php",
        data: {action:"filter_villa",vill_dest:vill_dest, vill_prc_typ:vill_prc_typ, vill_prc:vill_prc, vill_chk_in:vill_chk_in, vill_chk_out:vill_chk_out,vill_prc_min:vill_prc_min,vill_prc_max:vill_prc_max, vill_bed:vill_bed},
        success :function(data){

          var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          // $('.feature-item-slider').removeClass("slick-initialized slick-slider");
         // $(".feature-item-slider").slick(featureSlickSlider());
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }
          console.log('ajax success');
          $(".feature-item-slider").slick(featureSlickSlider());
        }
      }).done(function (data) {
        
        
        var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          // $('.feature-item-slider').removeClass("slick-initialized slick-slider");
          //$(".feature-item-slider").slick(featureSlickSlider());
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }    
          console.log('ajax done');
          $(".feature-item-slider").slick(featureSlickSlider());

        // console.log(typeof resp_data[2]);
        // var strobj = resp_data[2].toString();
        // var obj = $.parseJSON(strobj);
        // console.log(["'"+obj+"'"]);
        // initMap(["'"+obj+"'"]);
        
      });

    });
    $('input[name=checkgroup]').on('change',function(){
     $(document).find('#villa-flexible').val($(this).value);

   });

     $(".clear-filters").click(function() {
       $(this).parents('.more-filter-villa').find('.checkboxradio-item:checked').each(function() {
         $(this).prop("checked", false);;  
       });
       $('.select.mega-dropdown').removeClass('selected').slideUp('fast');
       $('.mega-dropdown').parents('.select-list').find('.drop-links .select').removeClass('selected');
       $('.mega-dropdown').parents('.select-list').removeClass('active');    
      });
    $('.apply-filters').click(function(){
      var villa_locality = [];
      var villa_style = [];
      var villa_occasion = [];

      $('.villa_loc').each(function(i){
        if(this.checked){
          villa_locality[i] = $(this).val();
        }

      });
      $('.villa_style').each(function(i){
        if(this.checked){
          villa_style[i] = $(this).val();
        }

      });
      $('.villa_occs').each(function(i){
        if(this.checked){
          villa_occasion[i] = $(this).val();
        }

      });
      var villa_locality = villa_locality.filter(function(v){return v!==''});
      var villa_style = villa_style.filter(function(v){return v!==''});
      var villa_occasion = villa_occasion.filter(function(v){return v!==''});


      var vill_dest = $(document).find('#villa-destination').val();
      var vill_prc_typ = $(document).find('#villa-price-typ').val();
      var vill_chk_in = $(document).find('#villa-check-in').val();
      var vill_chk_out = $(document).find('#villa-check-out').val();
      var vill_prc = $(document).find('#villa-price').val();
      var vill_prc_min = $(document).find('#villa-price-min').val();
      var vill_prc_max = $(document).find('#villa-price-max').val();
      var vill_bed = $(document).find('#no-of-bedrooms').val();
      if(vill_chk_in == '' && vill_chk_out==''){
        $(".global-message").slideDown();
        $('.drop-links.span').addClass('curser-disabled');
        return false;
      }else{
        if($(document).find('.global-message').length > 0){
          $(".global-message").slideUp();
          $('.drop-links.span').removeClass('curser-disabled');
        }
      }
      $.ajax({
        type: "POST",
        dataType: "html",
        url: blogUri+"/wp-admin/admin-ajax.php",
        data: {action:"filter_villa",villa_locality:villa_locality,villa_occasion:villa_occasion,villa_style:villa_style,vill_dest:vill_dest, vill_prc_typ:vill_prc_typ, vill_prc:vill_prc, vill_chk_in:vill_chk_in, vill_chk_out:vill_chk_out,vill_prc_min:vill_prc_min,vill_prc_max:vill_prc_max, vill_bed:vill_bed},
        success :function(data){
          var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);
          }else{
            $('.list-icon').click();
          }  
          $(".feature-item-slider").slick(featureSlickSlider());
          $('.select.mega-dropdown').removeClass('selected').slideUp('fast');
          $('.mega-dropdown').parents('.select-list').find('.drop-links .select').removeClass('selected');
          $('.mega-dropdown').parents('.select-list').removeClass('active'); 
         
        }
      }).done(function (data) {
        var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }  
          // $('.feature-item-slider').slick('unslick').slick('reinit');
          $(".feature-item-slider").slick(featureSlickSlider());

      });

    });
    $('ul.no_beds li').click(function() {
      $(document).find('#no-of-bedrooms').val($(this).attr('data-beds'));
      var vill_dest = $(document).find('#villa-destination').val();
      var vill_prc_typ = $(document).find('#villa-price-typ').val();
      var vill_chk_in = $(document).find('#villa-check-in').val();
      var vill_chk_out = $(document).find('#villa-check-out').val();
      var vill_prc = $(document).find('#villa-price').val();
      var vill_prc_min = $(document).find('#villa-price-min').val();
      var vill_prc_max = $(document).find('#villa-price-max').val();
      var vill_bed = $(document).find('#no-of-bedrooms').val();
      if(vill_chk_in == '' && vill_chk_out==''){
        $(".global-message").slideDown();
        $('.drop-links.span').addClass('curser-disabled');
        return false;
      }else{
        if($(document).find('.global-message').length > 0){
          $(".global-message").slideUp();
          $('.drop-links.span').removeClass('curser-disabled');
        }
      }
      $.ajax({
        type: "POST",
        dataType: "html",
        url: blogUri+"/wp-admin/admin-ajax.php",
        data: {action:"filter_villa",vill_dest:vill_dest, vill_prc_typ:vill_prc_typ, vill_prc:vill_prc, vill_chk_in:vill_chk_in, vill_chk_out:vill_chk_out,vill_prc_min:vill_prc_min,vill_prc_max:vill_prc_max, vill_bed:vill_bed},
        success :function(data){
          var resp_data = data.split('~');     
          console.log(vill_dest);   
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }  
          $(".feature-item-slider").slick(featureSlickSlider());
        }
      }).done(function (data) {
        // $(".feature-item-slider").slick("reinit");
        var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);

          }else{
            $('.list-icon').click();
          }  
          $(".feature-item-slider").slick(featureSlickSlider());
      });
    });

    $('.select-list #serch_date').on('apply.daterangepicker', function(ev, picker) {
      var vill_dest = $(document).find('#villa-destination').val();
      var vill_prc_typ = $(document).find('#villa-price-typ').val();
      var vill_chk_in = picker.startDate.format('YYYY-MM-DD');
      var vill_chk_out = picker.endDate.format('YYYY-MM-DD');
      $(document).find('#villa-check-in').val(vill_chk_in);
      $(document).find('#villa-check-out').val(vill_chk_out);
      var vill_prc = $(document).find('#villa-price').val();
      var vill_prc_min = $(document).find('#villa-price-min').val();
      var vill_prc_max = $(document).find('#villa-price-max').val();
      var vill_bed = $(document).find('#no-of-bedrooms').val();
      var villaXmlData_flex = $(document).find('#villa-flexible').val();
      if(vill_chk_in == '' && vill_chk_out==''){
        $(".global-message").slideDown();
        $('.drop-links.span').addClass('curser-disabled');
        return false;
      }else{
        if($(document).find('.global-message').length > 0){
          $(".global-message").slideUp();
          $('.drop-links.span').removeClass('curser-disabled');
        }
      }
      $.ajax({
        type: "POST",
        dataType: "html",
        url: blogUri+"/wp-admin/admin-ajax.php",
        data: {action:"filter_villa",villaXmlData_flex:villaXmlData_flex, vill_dest:vill_dest, vill_prc_typ:vill_prc_typ, vill_prc:vill_prc, vill_chk_in:vill_chk_in, vill_chk_out:vill_chk_out,vill_prc_min:vill_prc_min,vill_prc_max:vill_prc_max, vill_bed:vill_bed},
        success :function(data){
          var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);
          }else{
            $('.list-icon').click();
          }  
          $(".feature-item-slider").slick(featureSlickSlider());

        }
      }).done(function (data) {

        var resp_data = data.split('~');        
          $(document).find('#villa_list').html(resp_data[0]);
          $(document).find('.count-villa span').text(resp_data[1]);
          $(document).find('#count_loc').text(vill_dest);
          if($(document).find('.map-icon').hasClass('on')){
          $(document).find('.villadata').removeClass('col-4');
          $(document).find('.villadata').addClass('col-12');
          }
          else{
             $(document).find('.villadata').removeClass('col-12');
            $(document).find('.villadata').addClass('col-4');
          }
          var strobj = resp_data[2]
          var resp_dataNew = strobj.split('|');
          var objArray =[];
          if(resp_dataNew.length != 0){
            resp_dataNew.forEach(function(value){
              
              objArray.push($.parseJSON(value))
            });
            initMap(objArray);
          }else{
            $('.list-icon').click();
          }  
        $(".feature-item-slider").slick(featureSlickSlider());
      });
    });

    $('#villa-search').on('click', function (e) {
      e.preventDefault();

      var destination = $('#destination').val();
      var purpose = $('#purpose').val();
      var price = $('#price').val();
      var checkin = $('#check-in').val();
      var checkout = $('#check-out').val();
      var noofbedrooms = $('#noOfRoom').val();

      $.ajax({
        type: "POST",
        dataType: "json",
        url: blogUri+"/wp-admin/admin-ajax.php",
        data: {action:"searchvilla",destination:destination, purpose:purpose, price:price, checkin:checkin, checkout:checkout, bedrooms:noofbedrooms},
      }).done(function (data) {
        if(data==1) {
          setInterval(function(){
            if($('#date-home').html() != html){ 
              html = $('#date-home').html();
              var arr = html.split('-');
              $('#check-in').val(arr[0]);
              $('#check-out').val(arr[1]);
            } 
          }, 1000);
          setTimeout(function(){
            $('#form-villa').submit();
          },1000);


        } else {

        }
      });
      return false;


    });

  });
// Script Added by Binu: Fuction to get the Homepage popup & submit form data to ajax-popup.php
  $('#searchpop').on('click', function(e) {
    e.preventDefault();
    var name = $("#popup-name").val();
    var email = $("#popup-email").val();
    var prc_typ = $('#prc_typ').val();
    var chk_in = $('#checkin').val();
    var chk_out = $('#checkout').val();
    var villa_prc = $('#villa_prc').val();
    var villa_occs = $('#villa-occs').val();
    var no_beds = $('#no-of-bedrooms').val();
    $.ajax({
      type: "POST",
      url: templateUri + "/ajax/ajax-popup.php",
      data: {
        email: email,
        name: name,
        prc_typ:prc_typ,
        chk_in:chk_in,
        chk_out:chk_out,
        villa_prc:villa_prc,
        villa_occs:villa_occs,
        no_beds:no_beds
      },
      success: function(data) {
          console.log(data);  
      }
    });
  });


  $('.popup-closebtn').on('click', function(e) {
    e.preventDefault();
    var cname = 'lightbox_cookie';
    var cvalue = 'true'
    document.cookie = cname + "=" + cvalue + ";path=/";
  });
$( document ).ready(function() {
  $('.select-list #serch_date').on('cancel.daterangepicker', function(ev, picker) {
  alert(6);
      $(this).val('');
  });  
  console.log('daterangepicker');
  $('.select-list .input-item').click(function(){
    if($(this).hasClass('date-clear')){
      console.log($(".banner-daterangepicker").find('table td').length);
      $(".banner-daterangepicker").find('table td').removeClass('active available in-range start-date end-date');
      $(this).removeClass('date-clear');  
    }
    
    
  });
  $('.select-list .input-item').on('show.daterangepicker', function(ev, picker) {
    //do something, like clearing an input
     $('body,html').addClass('body-hidden');
   });
   $('.select-list .input-item').on('hide.daterangepicker', function(ev, picker) {
     //do something, like clearing an input
     $('body,html').removeClass('body-hidden');
   });

  $('input[name="flexible-days"]').on('change', function() {
    var days=$(this).val()-1;        
    $('.select-list .input-item').data('daterangepicker').setStartDate(moment());
    $('.select-list .input-item').data('daterangepicker').setEndDate(moment().add(days, 'days'));
    $(".daterangepicker-options").insertBefore($(".banner-daterangepicker .drp-buttons"));
  });
  $('.select-list .input-item').daterangepicker({
      minDate: moment().subtract('days', 0),
      autoUpdateInput: true,
      autoApply:false,
      locale: {
        cancelLabel: 'Clear',
        direction: 'ltr banner-daterangepicker'
      },
      linkedCalendars: false,
  });

  $('.select-list .input-item').on('apply.daterangepicker', function(ev, picker) {
      $(this).text(picker.startDate.format('DD MMMM') + ' - ' + picker.endDate.format('DD MMMM'));
      $(this).parent(".form-row").addClass("active");
  });
  $(document).on("click",".datepicker-clear",function(){
    var mNow = moment();
    var mToday = moment().subtract(24, 'hours');
    var mLastWeek = moment().add(6, 'days');
    var mLastMonth =  moment().subtract(29, 'days');
    var mLastYear = moment().subtract(365, 'days');
    var mStart = mLastMonth;
    var mEnd = mNow;
    $(".banner-daterangepicker").find('table td').removeClass('active in-range start-date end-date');
    $('.select-list .input-item').html('<span>Check in</span> <i class="la la-arrow-right"></i> <span>Check out</span>');
    $('.select-list .input-item').parent(".form-row").removeClass("active");
    $('.select-list .input-item').addClass('date-clear');
    $('.select-list .input-item').data('daterangepicker').setStartDate(mToday);
    $('.select-list .input-item').data('daterangepicker').setEndDate(mToday);
  });

  $('.select-list .input-item').on('cancel.daterangepicker', function(ev, picker) {
      $(document).find('.banner-daterangepicker table td').each(function(){
        $(this).removeClass('active in-range start-date end-date');  
      });  
      $(this).html('<span>Check in</span> <i class="la la-arrow-right"></i> <span>Check out</span>');
      $(this).parent(".form-row").removeClass("active");
  });
  $(".daterangepicker-options").insertBefore($(".banner-daterangepicker .drp-buttons"));
  $(".datepicker-clear").insertBefore($(".banner-daterangepicker .drp-buttons .cancelBtn"));
});
   

