	<?php
/***********************
Template Name: Home slider
************************/
$currentPage = get_post($postId);
?>

    <!-- Start of Frame 3 slider -->

		<section class="bannerSlider">
			<div class="banners">
				<?php $sliderArgs = array(
						'post_type'     => 'sbanners',
						'numberposts' => -1,
						'order'         => 'ASC',
						'orderby'       => 'menu_order',
						'post_status'   => 'publish',
				);
				$sliderPages = get_posts($sliderArgs);
				$j=0;
				foreach($sliderPages as $sliderPage){
								$featImage = wp_get_attachment_url(get_post_thumbnail_id($sliderPage->ID) );
								?>
                <div class="banners-fluid">
                	<img src="<?php echo $featImage; ?>" alt="<?php echo $sliderPage->post_title; ?>" />
                	<div class="heading">
                		<h3><?php echo $sliderPage->post_content; ?></h3>
                	</div>
                </div>
			<?php $j++; 
					}
			 ?>
			 <div class="banners-show">
			 		<?php 
			 			$featImage = wp_get_attachment_url(get_post_thumbnail_id($subPage->ID) );
			 		?>
                	<img src="<?php echo $featImage; ?>" alt="frame3" />
                	<div class="heading">
                		<h3><?php echo $subPage->post_title; ?></h3>
                	</div>
                </div>
            </div>
            <div class="navigation">
							<?php
							foreach($sliderPages as $sliderPage){
											$featImage = wp_get_attachment_url(get_post_thumbnail_id($sliderPage->ID) );?>
        		<div class="navigation-item active" data-increment="<?php echo $sliderPage->post_title; ?>">
        			<img src="<?php echo $featImage ?>" alt="<?php echo $sliderPage->post_title; ?>" />
        			<span><?php echo $sliderPage->post_title; ?></span>
        		</div>
					<?php } ?>
        	</div>
        </section>
        <!-- End of Frame 3 slider -->
