	<?php
	/********
	Shortcodes
	*********/
	// echo "string";

	function span( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<span>'.do_shortcode($content).'</span>';
	}
	add_shortcode('span', 'span');

	function empty_line($atts, $content = null) {
    $content = preg_replace('#^<\/p>|<p>$#', '', $content);
    $content = shortcode_empty_paragraph_fix($content);
    return '<div style="">&nbsp;</div>';
	}
	add_shortcode('empty_line', 'empty_line');

	function break_tag( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<br/>';
	}
	add_shortcode('break_tag', 'break_tag');

	function div_tag( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div>'.do_shortcode($content).'</div>';
	}
	add_shortcode('div_tag', 'div_tag');

	function container( $atts, $content = null ) {
		if(isset($atts['type'])) { 
		if($atts['type']) {
			$type = $atts['type'];
			if($type="flex") {
				$class = 'flexed-listing';
			} else {
				$class = 'section-divider';
			}
		} else {
			$type = '';
		}
		}
	   	$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   	$content=shortcode_empty_paragraph_fix_tag($content);
	   	return '<div class="container container-type1 '.$class.'">'.do_shortcode($content).'</div>';
	}
	add_shortcode('container', 'container');

	function block( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="para-block">'.do_shortcode($content).'</div>';
	}
	add_shortcode('block', 'block');


	function figure( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<figure>'.do_shortcode($content).'</figure>';
	}
	add_shortcode('figure', 'figure');

	function left_border( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="para-border-left"></div>';
	}
	add_shortcode('left_border', 'left_border');

	function right_border( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="para-border-right"></div>';
	}
	add_shortcode('right_border', 'right_border');

	function list_block( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="list-hero">'.do_shortcode($content).'</div>';
	}
	add_shortcode('list_block', 'list_block');

	function list_entry( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="list-Entry">'.do_shortcode($content).'</div>';
	}
	add_shortcode('list_entry', 'list_entry');

	function golden_background( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="list-Image golden-bg">'.do_shortcode($content).'</div>';
	}
	add_shortcode('golden_background', 'golden_background');

	function list_content( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="list-Block">'.do_shortcode($content).'</div>';
	}
	add_shortcode('list_content', 'list_content');

	function list_middle_content( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="list-middle">'.do_shortcode($content).'</div>';
	}
	add_shortcode('list_middle_content', 'list_middle_content');

	function title_line( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="title-line">'.do_shortcode($content).'</div>';
	}
	add_shortcode('title_line', 'title_line');

	function tick_list( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="tick-list">'.do_shortcode($content).'</div>';
	}
	add_shortcode('tick_list', 'tick_list');

	function icon_list( $atts, $content = null ) {
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="two-col-list clearfix">'.do_shortcode($content).'</div>';
	}
	add_shortcode('icon_list', 'icon_list');

	function hero_banner($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="hero-banner dark-theme">'.do_shortcode($content).'</div>';
	}
	add_shortcode('hero_banner', 'hero_banner');

	
	function main_banner($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="main-banner">'.do_shortcode($content).'</div>';
	}
	add_shortcode('main_banner', 'main_banner');

	
	function hero_banner_desc($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="hero-banner-desc-center">'.do_shortcode($content).'</div>';
	}
	add_shortcode('hero_banner_desc', 'hero_banner_desc');


	function container_div($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="container">'.do_shortcode($content).'</div>';
	}
	add_shortcode('container_div', 'container_div');

	
	function hero_banner_content($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="hero-banner-content w-100">'.do_shortcode($content).'</div>';
	}
	add_shortcode('hero_banner_content', 'hero_banner_content');

	
	function heading($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="heading">'.do_shortcode($content).'</div>';
	}
	add_shortcode('heading', 'heading');

	
	function c_heading($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="c-heading">'.do_shortcode($content).'</div>';
	}
	add_shortcode('c_heading', 'c_heading');


	function section_common($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<section class="section-common" id="scrollnext">'.do_shortcode($content).'</section>';
	}
	add_shortcode('section_common', 'section_common');

	
	function services_slider_right($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="card-box type-1 services-slider-right">'.do_shortcode($content).'</div>';
	}
	add_shortcode('services_slider_right', 'services_slider_right');

	
	function services_slider_left($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="card-box type-1 services-slider-left">'.do_shortcode($content).'</div>';
	}
	add_shortcode('services_slider_left', 'services_slider_left');


	function three_column($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="col-4">'.do_shortcode($content).'</div>';
	}
	add_shortcode('three_column', 'three_column');

	
	function figcaption($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<figcaption>'.do_shortcode($content).'</figcaption>';
	}
	add_shortcode('figcaption', 'figcaption');

	
	function btn_text($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="button button-primary"><button>'.do_shortcode($content).'</button></div>';
	}
	add_shortcode('btn_text', 'btn_text');

	
	function bannerslider($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<section class="bannerSlider">'.do_shortcode($content).'</section>';
	}
	add_shortcode('bannerslider', 'bannerslider');

	
	function banners($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="banners">'.do_shortcode($content).'</div>';
	}
	add_shortcode('banners', 'banners');
	
	function banners_content($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="banners-fluid">'.do_shortcode($content).'</div>';
	}
	add_shortcode('banners_content', 'banners_content');

	
	function navigation($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="navigation">'.do_shortcode($content).'</div>';
	}
	add_shortcode('navigation', 'navigation');

	
	function navigation_item($atts, $content = null)
	{
		if(isset($atts['increment'])) { 
			if($atts['increment']) { $inc=$atts['increment']; }
		}
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="navigation-item" data-increment="'.$inc.'">'.do_shortcode($content).'</div>';
	}
	add_shortcode('navigation_item', 'navigation_item');

	
	function villa_wrap($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="villa-wrapper">'.do_shortcode($content).'</div>';
	}
	add_shortcode('villa_wrap', 'villa_wrap');

	
	function villa_title($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="top-row">'.do_shortcode($content).'</div>';
	}
	add_shortcode('villa_title', 'villa_title');

	
	function villa_slider($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="villa-slider">'.do_shortcode($content).'</div>';
	}
	add_shortcode('villa_slider', 'villa_slider');

	
	function flexed_content($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div><div class="flexed-content">'.do_shortcode($content).'</div><div>';
	}
	add_shortcode('flexed_content', 'flexed_content');


	function flexed_content_start($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="flexed-start flexed-content">'.do_shortcode($content).'</div>';
	}
	add_shortcode('flexed_content_start', 'flexed_content_start');


	function column($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="column">'.do_shortcode($content).'</div>';
	}
	add_shortcode('column', 'column');

	function villa_nav_slider($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="villa-navslider"><div class="area-row">'.do_shortcode($content).'</div></div>';
	}
	add_shortcode('villa_nav_slider', 'villa_nav_slider');


	function flex_top_heading($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="flexed-topheading">'.do_shortcode($content).'</div>';
	}
	add_shortcode('flex_top_heading', 'flex_top_heading');


	function flex_summary($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="flexed-summary">'.do_shortcode($content).'</div>';
	}
	add_shortcode('flex_summary', 'flex_summary');


	function d_buttons($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="d-buttons">'.do_shortcode($content).'</div>';
	}
	add_shortcode('d_buttons', 'd_buttons');


	function primary_btn_link($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="button button-primary">'.do_shortcode($content).'</div>';
	}
	add_shortcode('primary_btn_link', 'primary_btn_link');


	function secondary_btn_link($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="button button-secondary">'.do_shortcode($content).'</div>';
	}
	add_shortcode('secondary_btn_link', 'secondary_btn_link');


	function flexed_image($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="flexed-image">'.do_shortcode($content).'</div>';
	}
	add_shortcode('flexed_image', 'flexed_image');


	function flexed_image_options($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="flexed-image-options">'.do_shortcode($content).'</div>';
	}
	add_shortcode('flexed_image_options', 'flexed_image_options');


	function golden_icon($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="golden-icons">'.do_shortcode($content).'</div>';
	}
	add_shortcode('golden_icon', 'golden_icon');


	function imageclip($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="imageClip">'.do_shortcode($content).'</div>';
	}
	add_shortcode('imageclip', 'imageclip');




	function gotoTop($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="gotoTop"><span>Top</span></div>';
	}
	add_shortcode('gotoTop', 'gotoTop');

function searchform_home($atts, $content = null){
$return = ' <div id="small-dialog" class="homeFilter zoom-anim-dialog mfp-hide">
        	<div class="search-bar-wrapper search-bar-mobile">
							<form method="post" action="'.get_bloginfo('url').'/villas" id="form-villa" >
								<input type="hidden" name="villa-destination" value="" id="destination"/>
								<input type="hidden" name="villa-purpose" value="" id="purpose"/>
								<input type="hidden" name="villa-check-in" value="" id="check-in"/>
								<input type="hidden" name="villa-check-out" value="" id="check-out"/>
								<input type="hidden" name="villa-price" value="" id="price"/>
							<div class="row">
								<div class="col-2">
									<div class="form-row select-list" id="mapicon">
										<div class="drop-links links-icon">
											<span class="filter">Destination</span>
											<ul class="select destination-sel">';
                        $villargs = array(
                          'numberposts' =>-1,
                          'post_type' => 'destination',
                          'orderby' => 'menu_order',
                          'order' => 'ASC',
                        );
                        $villaLocs = get_posts($villargs);
                        foreach($villaLocs as $villaLoc) {
                           $title = $villaLoc->post_title;
                        
						$return .= 	'<li data-value="'.$title.'">'.$title.'</li>';
                       } 
							$return .= 				'</ul>
										</div>
									</div>
								</div>
								<div class="col-2">
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Purpose</span>
											<i class="fa fa-map-marker-alt"></i>
											<ul class="select purpose-sel">';

                        $Proargs = array(
                          'numberposts' =>-1,
                          'post_type' => 'purpose',
                          'orderby' => 'menu_order',
                          'order' => 'ASC',
                          'post_status'=>'publish'
                        );
                        $villaPros = get_posts($Proargs);
                        foreach($villaPros as $villaPro) {
                           $titlePro = $villaPro->post_title;
                       $return .= ' <li data-value="'.$titlePro.'">'.$titlePro.'</li>';
                       }
									$return .='</ul>
										</div>
									</div>
								</div>
								<div class="col-2">
									<div class="form-row select-list">
										<span class="input-item" id="date-home">Check in - Check out</span>
									</div>
								</div>
								<div class="col-2">
									<div class="form-row select-list">
										<input type="number" value="1" id="noOfRoom" name="no-of-bedrooms" min="1">
										<span id="defaultText">Bedroom</span>
										<button type="button" class="adds">+</button>
										<button type="button" class="subs" disabled >-</button>
									</div>
								</div>';
								$priceArray = array();
								foreach($villapages as $villas) { 
										$price = get_post_meta( $villas->ID, '_villa_price', true );
										$priceArray[] = $price;
									?>

								<?php } 
	$largest  = $priceArray[0];
		$smallest = $priceArray[0];
								foreach ($priceArray as $priceArrr) {
									if($priceArrr > $largest){
										$largest = $priceArrr;
									}
									else if($priceArrr < $smallest){
										$smallest = $priceArrr;
									}
								}


								
								$return .= '<div class="col-2">
									<div class="form-row select-list data-list-input">
										<div class="drop-links">
											<span class="filter">Price</span>
											<i class="fa fa-map-marker-alt"></i>
											<ul class="select data-list-input price-sel">
											    <li data-value="500-2999">Silver $500 – 2,999/day</li>
											    <li data-value="3000-5999">Gold $3,000 – 5999/day</li>
											    <li data-value="6000-9999">Platinum $6,000 – 9.999/day</li>
											    <li data-value="10000-16000">Diamond 10.000 – 16.000/day</li>
											</ul>
										</div>
									</div>
								</div>

								<div class="col-2">
									<input type="submit" class="button button-primary" value="submit" id="villa-search">
									<!-- <button class="booking-submit button button-primary">
										<a href="#">Search</a>
									</button> -->
								</div>
							</div>
							<div class="topDestinations">'.$post->post_content.'
							</div>
							</form>
						</div>
						</div>';
						return $return;

					}
add_shortcode('searchform_home', 'searchform_home');



	function mobile_banner($atts, $content = null)
	{

	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="mobile-banner">
					        	<div class="searchbar">
					        		<div class="searchbar-input">
					        			<a class="popup-with-zoom-anim" href="#small-dialog">
						        			<i class="fa fa-search" aria-hidden="true"></i>'.$content.'</a>
						            </div>
					        	</div>
					        </div>';
	}
	add_shortcode('mobile_banner', 'mobile_banner');
	

	function location_private($atts, $content = null)
	{

	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '<div class="location-now">'.$content.'</div>';
	}
	add_shortcode('location_private', 'location_private');
