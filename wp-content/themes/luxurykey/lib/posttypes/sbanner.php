<?php add_action('init', 'create_sbanners', 0);

function create_sbanners() {
    $labels = array(
        'name' => _x('Slider Banners', 'post type general name'),
        'singular_name' => _x('Slider Banners', 'post type singular name'),
        'add_new' => _x('Add Slider Banners', 'Slider Banners'),
        'add_new_item' => __('Add Slider Banners'),
        'edit_item' => __('Edit Slider Banners'),
        'new_item' => __('New Slider Banners'),
        'view_item' => __('View Slider Banners'),
        'search_items' => __('Search Slider Banners'),
        'not_found' => __('No Slider Banners found'),
        'not_found_in_trash' => __('No Slider Banners found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'sbanner','with_front' => FALSE,),
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 7,
        'menu_icon'   => 'dashicons-images-alt2',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes')
    );

    register_post_type('sbanners', $args);
}
?>
