<?php add_action('init', 'create_banners', 0);

function create_banners() {
    $labels = array(
        'name' => _x('Home Banners', 'post type general name'),
        'singular_name' => _x('Home Banners', 'post type singular name'),
        'add_new' => _x('Add Home Banners', 'Home Banners'),
        'add_new_item' => __('Add Home Banners'),
        'edit_item' => __('Edit Home Banners'),
        'new_item' => __('New Home Banners'),
        'view_item' => __('View Home Banners'),
        'search_items' => __('Search Home Banners'),
        'not_found' => __('No Home Banners found'),
        'not_found_in_trash' => __('No Home Banners found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'banner','with_front' => FALSE,),
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 7,
        'menu_icon'   => 'dashicons-format-image',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes')
    );

    register_post_type('banners', $args);

}
?>
