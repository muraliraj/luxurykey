<?php add_action('init', 'create_concierge', 0);


function create_concierge() {
    $labels = array(
        'name' => _x('Concierge', 'post type general name'),
        'singular_name' => _x('concierge', 'post type singular name'),
        'add_new' => _x('Add Concierge', 'Home Concierge'),
        'add_new_item' => __('Add Concierge'),
        'edit_item' => __('Edit Concierge'),
        'new_item' => __('New Concierge'),
        'view_item' => __('View Concierge'),
        'search_items' => __('Search Concierge'),
        'not_found' => __('No Concierge found'),
        'not_found_in_trash' => __('No Concierge found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'concierge','with_front' => FALSE,),
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 7,
        'menu_icon'   => 'dashicons-businessman',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes')
    );

    register_post_type('concierge', $args);

}

?>