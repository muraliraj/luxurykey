<?php add_action('init', 'create_purpose', 0);

function create_purpose() {
    $labels = array(
        'name' => _x('Occasion', 'post type general name'),
        'singular_name' => _x('Purpose', 'post type singular name'),
        'add_new' => _x('Add Purpose', 'Home Purpose'),
        'add_new_item' => __('Add Purpose'),
        'edit_item' => __('Edit Purpose'),
        'new_item' => __('New Purpose'),
        'view_item' => __('View Purpose'),
        'search_items' => __('Search Purpose'),
        'not_found' => __('No Purpose found'),
        'not_found_in_trash' => __('No Purpose found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'purpose','with_front' => FALSE,),
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 7,
        'menu_icon'   => 'dashicons-store',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes')
    );

    register_post_type('purpose', $args);

}
