<?php add_action('init', 'create_price', 0);

function create_price() {
    $labels = array(
        'name' => _x('Price', 'post type general name'),
        'singular_name' => _x('Price', 'post type singular name'),
        'add_new' => _x('Add Price', 'Home Price'),
        'add_new_item' => __('Add Price'),
        'edit_item' => __('Edit Price'),
        'new_item' => __('New Price'),
        'view_item' => __('View Price'),
        'search_items' => __('Search Price'),
        'not_found' => __('No Price found'),
        'not_found_in_trash' => __('No Price found in Trash'),
        'parent_item_colon' => ''
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'price','with_front' => FALSE,),
        'capability_type' => 'post',
        'hierarchical' => true,
        'menu_position' => 7,
        'menu_icon'   => 'dashicons-cart',
        'supports' => array('title', 'editor', 'thumbnail', 'excerpt', 'page-attributes')
    );

    register_post_type('price', $args);

}
