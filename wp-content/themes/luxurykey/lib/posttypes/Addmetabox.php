<?php
add_action('admin_menu', 'mphb_room_type_accord_options');

function mphb_room_type_accord_options() {

    /*Give your custom posttypes name*/
    
    
        add_meta_box('mphb_room_type_accord_options', 'Accordion Section', 'mphb_room_type_callback_options_design', 'mphb_room_type');
    
}

function mphb_room_type_callback_options_design($post_id) {
    global $post;
    $accord_1 = get_post_meta($post->ID, 'accord_1', true);
    $accord_2 = get_post_meta($post->ID, 'accord_2', true);
    $accord_3 = get_post_meta($post->ID, 'accord_3', true);
    $accord_4 = get_post_meta($post->ID, 'accord_4', true);
    $accord_5 = get_post_meta($post->ID, 'accord_5', true);
    $accord_6 = get_post_meta($post->ID, 'accord_6', true);
    $accord_7 = get_post_meta($post->ID, 'accord_7', true);
    $accord_8 = get_post_meta($post->ID, 'accord_8', true);
    $Addcontent1 = get_post_meta($post->ID, 'add_content_1', true);
    $Addcontent2 = get_post_meta($post->ID, 'add_content_2', true);
    $Addcontent3 = get_post_meta($post->ID, 'add_content_3', true);
    $Addcontent4 = get_post_meta($post->ID, 'add_content_4', true);
    $Addcontent5 = get_post_meta($post->ID, 'add_content_5', true);
    $Addcontent6 = get_post_meta($post->ID, 'add_content_6', true);
    $Addcontent7 = get_post_meta($post->ID, 'add_content_7', true);
    $Addcontent8 = get_post_meta($post->ID, 'add_content_8', true);
?>


    

     <?php
     echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_1"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_1" name="add_content_1" value="' . esc_attr( $Addcontent1) . '" style="width: 100%;" /></td>';
            echo '</table>'; 
     wp_editor( htmlspecialchars_decode($accord_1), 'accord_1', $accord_1 = array('textarea_name'=>'accord_1') );
     ?>


     <?php 
            echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_2"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_2" name="add_content_2" value="' . esc_attr( $Addcontent2) . '" style="width: 100%;" /></td>';
            echo '</table>';
     wp_editor( htmlspecialchars_decode($accord_2 ), 'accord_2', $accord_2  = array('textarea_name'=>'accord_2') );
     ?>

    <?php 
            echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_3"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_3" name="add_content_3" value="' . esc_attr( $Addcontent3) . '" style="width: 100%;" /></td>';
            echo '</table>';
    wp_editor( htmlspecialchars_decode($accord_3 ), 'accord_3', $accord_3  = array('textarea_name'=>'accord_3') );
    ?>

    <?php 
            echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_4"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_4" name="add_content_4" value="' . esc_attr( $Addcontent4) . '" style="width: 100%;" /></td>';
            echo '</table>';
    wp_editor( htmlspecialchars_decode($accord_4 ), 'accord_4', $accord_4  = array('textarea_name'=>'accord_4') );
    ?>


    <?php 
            echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_5"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_5" name="add_content_5" value="' . esc_attr( $Addcontent5) . '" style="width: 100%;" /></td>';
            echo '</table>';
    wp_editor( htmlspecialchars_decode($accord_5 ), 'accord_5', $accord_5  = array('textarea_name'=>'accord_5') );
    ?>


    <?php 
            echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_6"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_6" name="add_content_6" value="' . esc_attr( $Addcontent6) . '" style="width: 100%;" /></td>';
            echo '</table>';
    wp_editor( htmlspecialchars_decode($accord_6 ), 'accord_6', $accord_6  = array('textarea_name'=>'accord_6') );
    ?>


    <?php 
    echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_7"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_7" name="add_content_7" value="' . esc_attr( $Addcontent7) . '" style="width: 100%;" /></td>';
            echo '</table>';
 
    wp_editor( htmlspecialchars_decode($accord_7 ), 'accord_7', $accord_7  = array('textarea_name'=>'accord_7') );
    ?>

        <?php 
    echo '<table>';
            echo '<tr>';
            echo '<td><label for="add_content_8"><strong>Accordion Title</strong></label></td>';
            echo '<td>:</td>';
            echo '<td><input type="text" id="add_content_8" name="add_content_7" value="' . esc_attr( $Addcontent8) . '" style="width: 100%;" /></td>';
            echo '</table>';
 
    wp_editor( htmlspecialchars_decode($accord_8 ), 'accord_8', $accord_8  = array('textarea_name'=>'accord_8') );
    ?>
    
  

<?php
}

add_action('save_post', 'save_mphb_room_accord_type');

function save_mphb_room_accord_type($post_id) {
    global $post;
    get_post_type($post_id);
        // do not save if this is an auto save routine
    if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
        return $post->ID;
        update_post_meta($post_id, 'accord_1', $_REQUEST['accord_1']);
        update_post_meta($post_id, 'accord_2', $_REQUEST['accord_2']);
        update_post_meta($post_id, 'accord_3', $_REQUEST['accord_3']);
        update_post_meta($post_id, 'accord_4', $_REQUEST['accord_4']);
        update_post_meta($post_id, 'accord_5', $_REQUEST['accord_5']);
        update_post_meta($post_id, 'accord_6', $_REQUEST['accord_6']);
        update_post_meta($post_id, 'accord_7', $_REQUEST['accord_7']);
        update_post_meta($post_id, 'accord_8', $_REQUEST['accord_8']);

        update_post_meta($post_id, 'add_content_1', $_REQUEST['add_content_1']);
        update_post_meta($post_id, 'add_content_2', $_REQUEST['add_content_2']);
        update_post_meta($post_id, 'add_content_3', $_REQUEST['add_content_3']);
        update_post_meta($post_id, 'add_content_4', $_REQUEST['add_content_4']);
        update_post_meta($post_id, 'add_content_5', $_REQUEST['add_content_5']);
        update_post_meta($post_id, 'add_content_6', $_REQUEST['add_content_6']);
        update_post_meta($post_id, 'add_content_7', $_REQUEST['add_content_7']);
        update_post_meta($post_id, 'add_content_8', $_REQUEST['add_content_8']);



}
?>
