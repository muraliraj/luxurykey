<?php
add_action('admin_menu', 'template_metabox_options');

function template_metabox_options() {
    $types = array('page');
foreach( $types as $type ) {
      add_meta_box('template_metabox_options', 'Message Options', 'template_metabox_options_design', $type);
}

}
function template_metabox_options_design($post_id) {
    global $post;
        $msg_name = get_post_meta($post->ID, 'msg_name', true);
    ?>
    <div id="groundDiv" <?php echo $firstCls; ?> >       
        <table cellpadding="3" cellspacing="18" border="0" id='ground_floor' class='fontsize10'   style='font-size: 10px;'>
           
                <tr>
                    <td class="left"><label for="tax-order"><h3>Product Message Text</h3></label></td>
                    <td  class="left">
                        <input type="textbox" name="msg_name" id="msg_name" value="<?php echo $msg_name; ?>" style="width: 210%;"></td> 
                    </td>
                </tr>
        </table>
    </div>   
    <?php
}

add_action('save_post', 'save_template_metabox');

function save_template_metabox($post_id) {
    global $post;

    get_post_type($post_id);

    //if (get_post_type($post_id) == 'project_post') {
        // do not save if this is an auto save routine
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
            return $post->ID;  
            
             if(array_key_exists('msg_name', $_REQUEST))
            {
                update_post_meta($post_id, 'msg_name', $_REQUEST['msg_name']);
            }
            

    //}
}
?>