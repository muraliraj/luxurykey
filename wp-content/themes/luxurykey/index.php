<?php
   /*
   * Template Name: Home page
   */
   ?>

   <?php
   get_header();
   
   ?>
   <div class="hero-banner">
     <div class="main-banner">
      <?php
      $args = array(
       'post_type'=> 'banners',
       'post_status' => 'publish',
       'order' => 'DESC',
       'numberposts' => -1
     );
       $home_ban = get_post_meta( $banner_post->ID, 'banners', true );
      $banner_posts = get_posts($args);
      ?>
      <?php   foreach ($banner_posts as $banner_post) { ?>

        <img width="240" src="<?php  echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>" />

      <?php } ?>
    </div>

    <div class="hero-banner-desc-center">
      <div class="container">
        <div class="hero-banner-content w-100">
          <div class="c-heading">
            <h1><?php echo $banner_post->post_title; ?></h1>
          </div>
          <form method="post" action="<?php echo get_bloginfo('url'); ?>/vacation-rental"  id="form-villa">
            <input type="hidden" name="villa-destination" value="" id="destination"/>
            <input type="hidden" name="villa-price-typ" value="" id="prc-typ-sel"/>
            <input type="hidden" name="villa-check-in" value="" id="check-in"/>
            <input type="hidden" name="villa-check-out" value="" id="check-out"/>
            <input type="hidden" name="villa-price" value="" id="vill-price"/>
            <input type="hidden" name="villa-price-min" value="" id="min-price"/>
            <input type="hidden" name="villa-price-max" value="" id="max-price"/>
            <input type="hidden" name="villa-occassion" value="" id="villa-occs"/>
            <input type="hidden" name="no-of-bedrooms" value="" id="no-of-bedrooms"/>
            <div class="search-bar-wrapper">
              <div class="row">
                <div class="col">
                  <div class="form-row select-list">
                    <div class="drop-links">
                      <?php
                      $args = array(
                       'post_type'=> 'destination',
                       'post_status' => 'publish',
                       'order' => 'DESC',
                       'numberposts' => -1
                     );
                      $destination_posts = get_posts($args);
                      ?>
                      <span class="filter">Destination</span>
                      <ul class="select destination-sel">
                       <?php   foreach ($destination_posts as $destination_post) { ?>

                        <li class="select-list-item" data-value="<?php echo $destination_post->post_title; ?>" data-locs="<?php echo $destination_post->post_title  ; ?>" dest="<?php echo $destination_post->post_title; ?>"><?php echo $destination_post->post_title; ?></li>



                      <?php }?>
                    </ul>
                  </div>
                </div>
              </div>
              <div class="col">
                <div class="form-row select-list">
                  <div class="drop-links">
                   <span class="filter">Occasion</span>
                   <ul class="select villa-occs">
                    <?php
                    $villargs_homesearch = array(
                      'numberposts' =>-1,
                      'post_type' => 'purpose',
                      'orderby' => 'menu_order',
                      'order' => 'ASC',
                    );
                    $villaLocs = get_posts($villargs_homesearch);
                    foreach($villaLocs as $villaLoc) {
                      $title = $villaLoc->post_title;
                      $data_attr = $title!='Villa Rentals'?'data-non-field="true"':'';
                      ?>
                      <li class="select-list-item" data-value="<?php echo $title; ?>" data-occs="<?php echo $title; ?>" <?php echo $data_attr; ?>><?php echo $title; ?></li>
                    <?php } ?>
                  </ul>
                </div>
              </div>
            </div>
            <div class="col non-field">
              <div class="form-row select-list home_chk_dt">
                <div class="input-item" id="date-home"><span>Check in</span> <i class="la la-arrow-right"></i> <span>Check out</span></div>
                <div class="daterangepicker-options">
                  <h4>Flexible Days</h4>  
                  <div class="checkbox-group">
                    <div class="checkboxradio">
                      <div class="checkboxradio-row">
                        <input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" />
                        <label class="checkboxradio-label radio-label" for="threedays"> +3/-3 Days
                        </label>
                      </div>
                    </div>
                    <div class="checkboxradio">
                      <div class="checkboxradio-row">
                        <input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" />
                        <label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
                        </label>
                      </div>
                    </div>
                    <div class="checkboxradio">
                      <div class="checkboxradio-row">
                        <input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" />
                        <label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
                        </label>
                      </div>
                    </div>
                  </div>
                 <button type="button" class="datepicker-clear btn btn-sm btn-default">Clear</button>
                </div>
              </div>
            </div>
            <div class="col non-field">
              <div class="form-row select-list">
                  <div class="drop-links">
                  <span class="filter" id="defaultText">Bedroom</span>
                 
                  <ul class="select overflow-y no_bedss">
                      <li data-beds="1">1</li>
                      <li data-beds="2">2</li>
                      <li data-beds="3">3</li>
                      <li data-beds="4">4</li>
                      <li data-beds="5">5</li>
                      <li data-beds="6">6</li>
                      <li data-beds="7">7</li>
                      <li data-beds="8">8</li>
                      <li data-beds="9">9</li>
                      <li data-beds="10">10</li>
                      <li data-beds="11">11</li>
                      <li data-beds="12">12</li>
                      <li data-beds="13">13</li>
                      <li data-beds="14">14</li>
                      <li data-beds="15">15</li>
                      <li data-beds="16">16</li>
                      <li data-beds="17">17</li>
                      <li data-beds="18">18</li>
                      <li data-beds="19">19</li>
                      <li data-beds="20">20</li>
                      <li data-beds="21">21</li>
                      <li data-beds="22">22</li>
                      <li data-beds="23">23</li>
                      <li data-beds="24">24</li>
                      <li data-beds="25">25</li>
                    </ul> 
                </div>                    
                                 
              </div>
             
            </div>

            <div class="col non-field">
              <div class="combine-dropdown type1">
                <div class="form-row select-list">
                  <div class="drop-links">
                    <span class="filter">Price</span>
                    <ul class="select price-sel">
                      <li class="select-list-item" data-prc-type="500-2999" data-value="€500 – €2,999/day">€500 – €2,999/day</li>
                      <li class="select-list-item" data-prc-type="3000-5999" data-value="€3,000 – €5999/day">€3,000 – €5999/day</li>
                      <li class="select-list-item" data-prc-type="6000-9999" data-value="€6,000 – €9.999/day"> €6,000 – €9.999/day</li>
                      <li class="select-list-item" data-prc-type="10000-16000" data-value="€10.000 – €16.000/day"> €10.000 – €16.000/day</li>
                    </ul>
                  </div>
                </div>
                <div class="form-row select-list">
                  <div class="drop-links">
                    <span class="filter">Style</span>



                    <ul class="select prc-typ-sel">
                      <a href="http://127.0.0.1/luxurykey/private-villas/"></a>
                      <?php
                      $term_prcstyles = get_terms( array(
                        'taxonomy' => 'mphb_room_type_prc_typ',
                        'hide_empty' => false,
                      ) );
                      foreach ($term_prcstyles as $key => $term_prcstyle) {
                        ?>
                        <li class="select-list-item" data-value="<?php echo $term_prcstyle->name; ?>" data-villa-style="<?php echo $term_prcstyle->slug; ?>"><?php echo $term_prcstyle->name; ?></li>

                      <?php }?>

                     <!-- <li><a href="#!">What is this?</a></li>-->
                    </ul>

                  </div>
                </div>
              </div>
            </div>
            <div class="col-1">
              <div class="button button-primary">
                <button  data-id="confirmVilla" id="search_villa">Search</button>
              </div>
            </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
<!--Popup section-->

<?php

if ((!isset($_COOKIE['lightbox_cookie'])) || ($_COOKIE['lightbox_cookie'] != 'true')) {
   echo '<button type="button" id="autoopen-popup" class="popup-trigger no-effect" data-id="confirmVilla" style="display:none;">popupdisplay</button>';
}
?>
<div class="popup-overlay">
 <div class="popup medium white-bg no-padding border-red" id="confirmVilla">
  <div class="popup-closebtn" data-id="confirmVilla">
   ×
 </div>
 <div class="popup-body flex-body">
   <div class="popup-image">
    <img src="<?php echo get_bloginfo('template_url'); ?>/img/banner2.jpeg" alt="popup-bg" />
  </div>
  <div class="popup-content">
    <p>Register with Luxury Key so we can share with you a world of exclusive experiences</p>
    <?php wp_nonce_field('conactus_nonce', 'search_list'); ?>
    <form name="search_list" id="search_list" method="post" action="">

     <div class="form-row">
      <label class="floating-item" data-error="Please enter Name">
       <input type="text" id="popup-name" class="floating-item-input input-item" name="Name" value="" placeholder="Name" />
       <span class="floating-item-label">Name</span>
     <div class="error-message" id="error-firstname">Please enter your First Name</div>
     </label>

   </div>

   <div class="form-row">
    <label class="floating-item" data-error="Please enter Email">
     <input type="email" id="popup-email" class="floating-item-input input-item" name="Email" value="" placeholder="Email" />
     <span class="floating-item-label">Email</span>
<div class="error-message" id="error-emailid">Please Enter Your Email Address</div>
   </label>


 </div>
 <div class="form-row">
  <div class="button button-primary">
   <div class="button"><button id="searchpop" name="booking" value="Submit">Submit</button>
   </div>

 </div>
</div>
</form>
</div>
</div>
</div>

</div>


<?php //content

echo apply_filters('the_content', $post->post_content); 
?>


<section class="section-experience section-small" id="scrollnext">
 <div class="container container-type3">
  <div class="row">
   <div class="col-12 text-center home-heading">
    <h3>A world of experiences from Luxury Key</h3>
  </div>
</div>
</div>
<div class="container container-type3 container-plr-6 services-slider-container">   
  <div class="card-box type-1 services-slider-left">
   <?php
   $args = array(
    'post_type'=> 'concierge',
    'post_status' => 'publish',
    'order' => 'DESC',
    'numberposts' => -1
  );
   $banner_posts = get_posts($args);
   $conciertee  = get_post_meta( $banner_post->ID, 'concierge', true );	  
   ?>
   <?php   foreach ($banner_posts as $banner_post) { 
    $excerpt = $banner_post->post_excerpt;
	$wedd= $banner_post->post_title;
    $wed="WEDDING";

    if($wed == $wedd ) {
    ?>
    <div class="card-item">
      <a href="<?php echo get_permalink(); ?>/weddings">
       <figure>
        <img src="<?php  echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>" alt="services_1" />
        <div class="button button-secondary figure-middle">
         <button><?php echo $excerpt; ?></button>
       </div>
       <figcaption>
         <div class="border-heading white">
          <h4><?php echo $banner_post->post_title; ?></h4>

        </div>
        <p><?php echo apply_filters('the_content', $banner_post->post_content);?></p>
      </figcaption>
    </figure>
    <span></span>
  </a>
</div>
<?php 
} else {?>
<div class="card-item">
      <a href="<?php echo get_permalink(); ?>/concierge">
       <figure>
        <img src="<?php  echo wp_get_attachment_url(get_post_thumbnail_id($banner_post->ID)); ?>" alt="services_1" />
        <div class="button button-secondary figure-middle">
         <button><?php echo $excerpt; ?></button>
       </div>
       <figcaption>
         <div class="border-heading white">
          <h4><?php echo $banner_post->post_title; ?></h4>

        </div>
        <p><?php echo apply_filters('the_content', $banner_post->post_content);?></p>
      </figcaption>
    </figure>
    <span></span>
  </a>
</div>
<?php } ?>


<?php } ?>
</div>   
</div>
</section>


<!--  -->
<section class="section-feature section-init">
 <div class="container container-type3">
  <div class="row">
   <div class="col-12 text-center home-heading">
    <h3>Featured Villas</h3>
  </div>
</div>
</div>
<div class="container container-type3 feature-container">
  <div class="row feature-row">

   <?php
   $args = array(
     'post_type'=> 'mphb_room_type',
     'post_status' => 'publish',
     'orderby'=>'rand',
     'order' => 'ASC',

     'numberposts' => 3,
     'meta_query'  => array(
						    array(
						        'key'       => 'villa_type_api',
						        'value'     => 'private',
						        'compare'   => '!=',
						    )
						)
   );
   $banner_posts = get_posts($args);
   ?>
   <?php   foreach ($banner_posts as $banner_post) {
            // echo $banner_post->ID;
    $gallerymeta  = get_post_meta( $banner_post->ID, 'mphb_gallery', true );
    $galleryimgs = explode(',', $gallerymeta);
    $galleryimgs = array_filter($galleryimgs);
    $gallerymeta = get_post_meta( $banner_post->ID, 'bedrrommphb_room_type', true );
    $gallerymeta1 = get_post_meta( $banner_post->ID, 'mphb_adults_capacity', true );
    $villabaths = get_post_meta( $banner_post->ID, 'bathroommphb_room_type', true );
    $Address = get_post_meta( $banner_post->ID, 'villa_address', true );
    
    ?>
    <!-- loop -->
    <div class="col-4">
      <div class="feature-item">
         <a href='<?php echo get_permalink($banner_post->ID); ?>' class='villadata' data-url="<?php echo get_permalink($banner_post->ID); ?>">

       <div class="feature-item-slider">

        
        <?php 
        if(!empty($galleryimgs))
        {
          $qwe=0;
          foreach ($galleryimgs as $galleryimg) {
           if($qwe == 3){
            break;
          }

          if($galleryimg!=''){ ?>
            <div>
             <img src="<?php  echo wp_get_attachment_image_url($galleryimg,'medium');  ?>" alt="">
           </div>
           <?php
         }
         $qwe++;
       }
     }
     ?>
   </div>
   <div class="slider-nav-wrapper">
      <ul class="feature-item-slider-nav slider-nav">
        <?php 
            foreach ($galleryimgs as $galleryimg) {
        ?>
        <li>
          <span></span>
        </li>
      <?php } ?>
      </ul>
    </div>
   <div class="feature-item-content">
      <h5>
      <?php
      if($gallerymeta!=''){
          echo '<span class="villa-cont">'.$gallerymeta." BEDROOMS</span>";
      }
      if($gallerymeta1!=''){
        echo '<span class="villa-cont">'.$gallerymeta1." GUESTS</span>";
      }
      if($villabaths!=''){
        echo '<span class="villa-cont">'.$villabaths." BATHROOMS</span>";
      } 
      ?>
     <h3><a href="<?php echo get_permalink($banner_post->ID); ?>" target="_blank"><?php echo $banner_post->post_title; ?></a></h3>
 
    <?php if($Address!=''){ ?>
     <p><a href=javascript:void()><?php echo ($Address); ?></a></p>
    <?php } ?>
  </div>
  <div class="feature-item-top">
    <span><?php echo do_shortcode('[favorite_button post_id="'.$banner_post->ID.'"]'); ?></span>

  </div>
</div>
</div>
<!-- loop ends -->
<?php }
?>
</div>
</div>

</section>
<!--Features Section-->
<?php 
get_footer(); 
?>