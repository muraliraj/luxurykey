<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Luxury Key | Villa Detail</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<!-- modernizr included -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	

	
	<script>
		function initMap() {
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 13,
				center: {lat: -37, lng: 25}
			});
			setMarkers(map);
		}
		var beaches = [
		['Bondi Beach', -33.890542, 151.274856, 4500],
		['Coogee Beach', -33.923036, 151.259052, 2300],
		['Cronulla Beach', -34.028249, 151.157507, 3150],
		['Manly Beach', -33.80010128657071, 151.28747820854187, 3200],
		['Maroubra Beach', -33.950198, 151.259302, 1500]
		];
		function numberWithCommas(x) {
			var parts = x.toString().split(".");
			parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			return parts.join(".");
		}
		function setMarkers(map) {
			var markerIcon = {
				url: 'http://image.flaticon.com/icons/svg/252/252025.svg',
				scaledSize: new google.maps.Size(65, 65),
				origin: new google.maps.Point(0, 0),
				anchor: new google.maps.Point(32,65),
				labelOrigin:  new google.maps.Point(33,25),
			};
			for (var i = 0; i < beaches.length; i++) {
				var beach = beaches[i],
				price = numberWithCommas(beach[3]).toString() + "$";
				var marker = new google.maps.Marker({
					position: {lat: beach[1], lng: beach[2]},
					map: map,
					icon: markerIcon,
					label: {
						text: price,
						color: 'black',
						fontSize: '12px',
						fontWeight: 'bold'
					}
				});
			}
		}
	</script>
	<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAj2VsL2Hjul-_ZhlOtYSpdhiyZSUTkTUE&callback=initMap"></script>
	<style type="text/css">
	body{
		background-color: #fff;
	}  
	.error-message{
		display: none;
	}
	.select-list.date-pick{
		height: 51px;
	}
	.slick-slide{
		height: unset!important;
	}
	.feature-item-slider{
		height: fit-content;
	}
	.render-blk{ opacity:1; }
</style>
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/app.css">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/style.css">




<noscript>
	<style type="text/css" media="screen">
	.render-blk{ opacity: 1; }	
</style>
</noscript>
<script>
	var templateUri = "<?php  echo get_bloginfo('template_url'); ?>";
	var blogUri 	= "<?php echo get_bloginfo('url'); ?>";
</script>
</head>
<body  class="subpage internal-js date-js">

	<div class="render-blk">
		<!-- header start -->
		<header class="nonav" id="scrolledTop">
			<a href="<?php echo get_bloginfo('url'); ?>" class="logo">
				<img src="<?php echo get_bloginfo('template_url'); ?>/img/logo.svg" alt="images">
			</a>
			<div class="container">
				<a class="popup-with-zoom-anim list-filter" href="#small-dialog">
					<i class="fa fa-list-ul" aria-hidden="true"></i>
				</a>
				<a class="mag-popup-close">
					<i class="la la-close" aria-hidden="true"></i>
				</a>
				<div id="small-dialog" class="zoom-anim-dialog search-bar-container filter-villa  mfp-hide">
					<div class="row">
						<?php 
						if(is_singular( 'mphb_room_type' )){
							?>
							<div class="col">
								<div class="form-row select-list" id="mapicon">
									<div class="drop-links">
										<span class="filter">Choose a destination</span>
										<ul class="select">
											<li class="select-list-item" data-value="Mykonos" selected>Mykonos</li>
											<li class="select-list-item" data-value="Tulum">Tulum</li>
											<li class="select-list-item" data-value="Ibiza">Ibiza</li>
											<li class="select-list-item" data-value="Santorini">Santorini</li>
											<li class="select-list-item" data-value="Other Greek Islands">Other Greek Islands</li>
											<li class="select-list-item" data-value="Private Islands">Private Islands</li>
											<li class="select-list-item" data-value="Other Global Destinations">Other Global Destinations</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col check-io-column">
								<div class="form-row select-list">
									<div class="input-item"><span><?php echo $_POST['villa-check-in']; ?></span> <i class="la la-arrow-right"></i> <span><?php echo $_POST['villa-check-out']; ?></span></div>
									<div class="daterangepicker-options">
										<h4>Flexible Days</h4>  
										<div class="checkbox-group">
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" value="3" />
													<label class="checkboxradio-label radio-label" for="threedays" > +3/-3 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" value="7" />
													<label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" value="30" />
													<label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="form-row select-list">
									<div class="drop-links">
										<span class="filter">No of Bedroom</span>
										<ul class="select">
											<li class="select-list-item" data-value="1 Bedroom">1 Bedroom</li>
											<li class="select-list-item" data-value="2 Bedrooms">2 Bedrooms</li>
											<li class="select-list-item" data-value="3 Bedrooms">3 Bedrooms</li>
											<li class="select-list-item" data-value="4 Bedrooms">4 Bedrooms</li>
											<li class="select-list-item" data-value="8 Bedrooms">8 Bedrooms</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-4">
								<div class="combine-dropdown">
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Price</span>
											<ul class="select">
												<li class="select-list-item" data-value="500-2999">€500 – €2,999/day</li>
												<li class="select-list-item" data-value="3000-5999">€3,000 – €5999/day</li>
												<li class="select-list-item" data-value="6000-9999"> €6,000 – €9.999/day</li>
												<li class="select-list-item" data-value="10000-16000"> €10.000 – €16.000/day</li>
											</ul>
										</div>
									</div>
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Style</span>
											<ul class="select">
												<li class="select-list-item" data-value="Diamond">Diamond</li>
												<li class="select-list-item" data-value="Platinum">Platinum</li>
												<li class="select-list-item" data-value="Gold">Gold</li>
												<li class="select-list-item" data-value="Silver">Silver</li>
												<li><a href="#!">What is this?</a></li>
											</ul>

										</div>
									</div>
								</div>
							</div>
							<?php 
						}else{
							$checkin_date = date_create($_POST['villa-check-in']);
							$checkin = date_format($checkin_date,"d M");
							$checkout_date = date_create($_POST['villa-check-out']);
							$checkout = date_format($checkout_date,"d M");
							?>
							<input type="hidden" name="villa-destination" id="villa-destination" value="<?php echo $_POST['villa-destination'];	?>">
							<input type="hidden" name="villa-price-typ" id="villa-price-typ" value="<?php echo $_POST['villa-price-typ']; ?>">
							<input type="hidden" name="villa-check-in" id="villa-check-in" value="<?php echo $_POST['villa-check-in']; ?>">
							<input type="hidden" name="villa-check-out" id="villa-check-out" value="<?php echo $_POST['villa-check-out']; ?>">
							<input type="hidden" name="villa-price" id="villa-price" value="<?php echo $_POST['villa-price']; ?>">
							<input type="hidden" name="villa-price-min" id="villa-price-min" value="<?php echo $_POST['villa-price-min']; ?>">
							<input type="hidden" name="villa-price-max" id="villa-price-max" value="<?php echo $_POST['villa-price-max']; ?>">
							<input type="hidden" name="no-of-bedrooms" id="no-of-bedrooms" value="<?php echo $_POST['no-of-bedrooms']; ?>">
							<input type="hidden" name="villa-flexible" id="villa-flexible" value="<?php echo $_POST['villa-flexible']; ?>">
							<div class="col">
								<div class="form-row select-list" id="mapicon">
									<div class="drop-links">
									<?php 
										if($_POST["villa-destination"] != ''){
									?>
										<span class="filter"><?php echo $_POST["villa-destination"]; ?></span>
									<?php }else{ ?>
										<span class="filter" >Choose a destination</span>
									<?php } ?>
										<ul class="select" id="villa_dest">
											<?php 
											$args = array(
						                       'post_type'=> 'destination',
						                       'post_status' => 'publish',
						                       'order' => 'DESC',
						                       'numberposts' => -1
						                     );
						                      $destination_posts = get_posts($args);  
											foreach ($destination_posts as $destination_post) { 
												?>

					                        <li class="select-list-item" data-value="<?php echo $destination_post->post_title; ?>" data-locs="<?php echo $destination_post->post_title; ?>" dest="Mykonos"<?php echo $destination_post->post_title; ?>"><?php echo $destination_post->post_title; ?></li>



					                      <?php }?>
										</ul>
									</div>
								</div>
							</div>
							<div class="col check-io-column">
								<div class="form-row select-list">
									<?php 
										if($_POST['villa-check-in']!='' && $_POST['villa-check-in']!=''){
									?>
									<div class="input-item sel-loc" id="serch_date"><span><?php echo $checkin; ?></span> <i class="la la-arrow-right"></i> <span><?php echo $checkout; ?></span></div>
									<?php }else{ ?>
										<div class="input-item sel-loc" id="serch_date"><span>Checkin</span> <i class="la la-arrow-right"></i> <span>Checkout</span></div>
									<?php } ?>
									<div class="daterangepicker-options">
										<h4>Flexible Days</h4>  
										<div class="checkbox-group">
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" value="3" />
													<label class="checkboxradio-label radio-label" for="threedays" > +3/-3 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" value="7" />
													<label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" value="30" />
													<label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="form-row select-list select-list-flex">
									<button type="button" class="subs nobeds" disabled >-</button>
									<div class="select-list-data">
										<?php 
											if($_POST["no-of-bedrooms"] != ''){
										?>
											<input type="hidden" value="<?php echo $_POST["no-of-bedrooms"]; ?>" class="noOfRoom" id="noOfRoom" min="1" disabled="disabled"> 
											<span class="noOfRoom" id="villa_no_bed"><?php echo $_POST["no-of-bedrooms"]; ?></span>
											<span class="filter" id="defaultText">Bedroom</span>
										<?php }else{ ?>
											<input type="hidden" value="1" class="noOfRoom" id="noOfRoom" min="1" disabled="disabled"> 
											<span class="noOfRoom">1</span>	
											<span class="filter" id="defaultText">Bedroom</span>
										<?php } ?>		
									</div>										
									<button type="button" class="adds nobeds">+</button>										
								</div>
							</div>
							<div class="col-4">
								<div class="combine-dropdown">
									<div class="form-row select-list">
										<div class="drop-links">
											<?php 
											if($_POST["villa-price"] != ''){
											?>
												<span class="filter"><?php echo $_POST["villa-price"]; ?></span>
											<?php }else{ ?>
												<span class="filter">Price</span>
											<?php } ?>
											<ul class="select" id="villa_prc">
											    <li class="select-list-item" data-value="$500 – 2,999/day" data-prc="500-2999">$500 – $2,999/day</li>
											    <li class="select-list-item" data-value="$3,000 – 5999/day" data-prc="3000-5999">$3,000 – $5999/day</li>
											    <li class="select-list-item" data-value="$6,000 – 9.999/day" data-prc="6000-9999"> $6,000 – $9.999/day</li>
											    <li class="select-list-item" data-value="$10.000 – 16.000/day" data-prc="10000-16000"> $10.000 – $16.000/day</li>
											</ul>
										</div>
									</div>
									<div class="form-row select-list">
										<div class="drop-links">
											<?php 
											if($_POST["villa-price"] != ''){
											?>
												<span class="filter" id="villa_prc_typ"><?php echo $_POST["villa-price-typ"]; ?></span>
											<?php }else{ ?>
												<span class="filter" id="villa_prc">Price Type</span>
											<?php } ?>
											<ul class="select">
												<li class="select-list-item" data-value="Diamond">Diamond</li>
											    <li class="select-list-item" data-value="Platinum">Platinum</li>
											    <li class="select-list-item" data-value="Gold">Gold</li>
										      	<li class="select-list-item" data-value="Silver">Silver</li>
										      	<li><a href="#!">What is this?</a></li>
											</ul>
											
										</div>
									</div>
								</div>
							</div>
							<div class="col filter-btn">
								<div class="form-row select-list data-list-input">
									<div class="drop-links">
										<span class="filter">More Filters</span>
										<ul class="select data-list-input mega-dropdown">
											<li class="checkbox-select">
												<h4>Areas Location</h4>
												<div class="checkboxradio-group">
													<?php 
													$term_locs = get_terms( array(
														'taxonomy' => 'mphb_room_type_loc',
														'hide_empty' => false,
													) );
													foreach ($term_locs as $key => $term_loc) {
														# code...

														?>
														<div class="checkboxradio">
															<div class="checkboxradio-row">
																<input class="checkboxradio-item checkboxradio-invisible villa_loc" name="<?php echo $term_loc->slug; ?>" type="checkbox" id="<?php echo $term_loc->slug; ?>" value="<?php echo $term_loc->slug; ?>" />
																<label class="checkboxradio-label checkbox-label" for="<?php echo $term_loc->slug; ?>"><?php echo $term_loc->name; ?>
															</label>
														</div>
													</div>
													<?php 
												}
												?>
											</div>
										</li>
										<li class="checkbox-select">
											<h4>Villa Style</h4>
											<div class="checkboxradio-group">
												<?php 
												$term_styles = get_terms( array(
													'taxonomy' => 'mphb_room_type_style',
													'hide_empty' => false,
												) );
												foreach ($term_styles as $key => $term_style) {
														# code...
													
													?>
													<div class="checkboxradio">
														<div class="checkboxradio-row">
															<input class="checkboxradio-item checkboxradio-invisible villa_style" name="<?php echo $term_style->slug; ?>" type="checkbox" id="<?php echo $term_style->slug; ?>" value="<?php echo $term_style->slug; ?>" />
															<label class="checkboxradio-label checkbox-label" for="<?php echo $term_style->slug; ?>"><?php echo $term_style->name; ?>
														</label>
													</div>
												</div>
												<?php 
											}
											?>
										</div>
									</li>
									<li class="checkbox-select">
										<h4>Holiday Occasion</h4>
										<div class="checkboxradio-group">
											<?php 
											$term_occs = get_terms( array(
												'taxonomy' => 'mphb_room_type_occ',
												'hide_empty' => false,
											) );
											foreach ($term_occs as $key => $term_occ) {
														# code...

												?>
												<div class="checkboxradio">
													<div class="checkboxradio-row">
														<input class="checkboxradio-item checkboxradio-invisible villa_occs" name="<?php echo $term_occ->slug; ?>" type="checkbox" id="<?php echo $term_occ->slug; ?>" value="<?php echo $term_occ->slug; ?>" />
														<label class="checkboxradio-label checkbox-label" for="<?php echo $term_occ->slug; ?>"><?php echo $term_occ->name; ?>
													</label>
												</div>
											</div>
											<?php 
										}
										?>
									</div>
								</li>
								<li class="checkbox-select flex-jsb">
									<div class="button button-secondary">
										<button type="button" class="apply-filters button">Clear</button>
									</div>
									<div class="button button-primary">
										<button type="button " class="apply-filters button">Apply Filter</button>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
</header>