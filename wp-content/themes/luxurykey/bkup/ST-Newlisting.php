<?php 
/***********************
Template Name: new Villas list
************************/
get_header('search');

$apiUrl = get_option('api_endpoint');
$username = get_option('api_username');
$password = get_option('api_password');
$villa_destination		=	$_POST['villa-destination'];

$purpose				=	$_POST['villa-purpose'];
$checkin				=	$_POST['villa-check-in'];
$checkout				=	$_POST['villa-check-out'];
$numbeds				=	(int)$_POST['no-of-bedrooms'];
$prc_typ  				=	$_POST['villa-price-typ'];
$checkin1=date_create($checkin);
$checkout1 = date_create($checkout);
$diff=date_diff($checkin1,$checkout1);
$totaldays=$diff->days;
$price					=	$_POST['villa-price'];
if ($_POST['villa-price-min']!='' && $_POST['villa-price-max']!=''  ) {
	$min_price				=	$_POST['villa-price-min'];
	$max_price				=	$_POST['villa-price-max'];
}
else
{
	$min_price=0;
	$max_price=999999;
}
$noofbed  =	$_POST['no-of-bedrooms'];
$authorization = 'Basic '. base64_encode($username.":".$password);
$feedUrl = $apiUrl.'/availability/LUXURYKEYS?checkin='.$checkin.'&checkout='.$checkout;

$ch = curl_init();
$headers = array(
	'Content-Type: application/x-www-form-urlencoded',
	'charset: utf-8',
	'Authorization:'. $authorization,
);
curl_setopt($ch, CURLOPT_URL, $feedUrl);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
$output = curl_exec($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$body = substr($output, $header_size);
$villaXmlData = new SimpleXMLElement($body);
$villaData = $villaXmlData->rates;
$villaXmlData=xml2array($villaXmlData);
$villa_rates=$villaXmlData['data']['rates']['rate'];
foreach ($villa_rates as  $villa_rate) {
	$villa_rate=xml2array($villa_rate);
	$title = $villa_rate['room']; 
	$tarr= explode('|', $title);
	$place = $tarr[2];
	$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
	$villapricerange[$villa_rate['type']][]=$villa_rate['pricing']['stay'];
	if($villa_rate['pricing']['stay']/$totaldays >= $min_price && $villa_rate['pricing']['stay']/$totaldays <= $max_price ){
		


		if("Mykonos" == $villa_destination){	
			if($int>=$numbeds)
			{
			 $vr[]=$villa_rate['type'];
			}
		}	
	}
}
$vr = array_unique($vr);
if (!empty($_POST) && ($_POST['villa-check-out'] || $_POST['villa-check-in'] )) {
$roomsAtts = array(
'availability'	 => 'locked',
'from_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkin ),
'to_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkout )
		);
$result =  getAvailableRoomTypes_custom($roomsAtts);
// echo "<pre>";
 // print_r($result);
 foreach ($result as $value) {
$manualAvailable[]=$value['id'];
 }
}
/*manual villa search integration*/
$villargs_all = array(
    'numberposts' =>-1,
    'post_type' => 'mphb_room_type',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_status'=>"publish"
	);
$villapages_all = get_posts($villargs_all);
$place = array();
foreach($villapages_all as $villas_all)
{
	$title = $villas_all->post_title; 
	$place[] = $title;
} 
$place=array_filter(array_unique($place));
if (!empty($_POST)) {

	 $villargs = array(
	    'meta_key' => 'villa_code',
	    'meta_value' => $vr,
	    'post_type' => 'mphb_room_type',
	    'post_status' => 'published',
	    'posts_per_page' => -1
		);
	}else {
		 $villargs = array(
	    'post_type' => 'mphb_room_type',
	    'post_status' => 'published',
	    'posts_per_page' => -1
		);
	}

	$villapages = get_posts($villargs);
	
	if(count($manualAvailable)>0){
		$menu_villa_args = array(
			'post_type' => 'mphb_room_type',
			'post_status' => 'published',
		    'post__in' => $manualAvailable
		);
		$villa_array = get_posts($menu_villa_args);
		$villapages = array_merge($villapages,$villa_array);
	}
?>
<div class="filter-top">
	<div class="container container-type3">
		<div class="row feature-row">
			<div class="col-12">
				<div class="section-intro section-icons flex-sb count-villa">
					<h4>We've found <span><?php echo count($villapages) ?></span> luxury villas for you in Mykanos</h4>	
					<ul>
						<li><span>view</span></li>
						<li><i class="la la-table list-icon on"></i></li>
						<li><i class="la la-map-marker map-icon"></i></li>
					</ul>
				</div>						
			</div>
		</div>
	</div>
</div>
<section class="section-feature section-init listing">
	<div class="mobile">
		<div class="toggleMap">
			MAP <i class="la la-map-marker"></i>
		</div>
	</div>
    <div class="container container-type3 feature-container" >	        	
    	<div class="row feature-row listRow toggle">
    		<div class="col-12 listView" id="villa_list">
    			<?php 
				

    			
					foreach($villapages as $villapage) { 
						$title = $villapage->post_title; 
						$tarr=array();
						$tarr= explode('|', $title);
						$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
					 	$villa_bath   = get_post_meta( $villapage->ID, 'bathroommphb_room_type', true );
					 	$villa_pool  = get_post_meta( $villapage->ID, 'villa_pool', true );
					  	$villa_guest  = get_post_meta( $villapage->ID, 'mphb_adults_capacity', true );
					  	$villa_address  = get_post_meta( $villapage->ID, 'villa_address', true );
					  	$villa_code  = get_post_meta( $villapage->ID, 'villa_code', true );
					  	$villa_bed  = get_post_meta( $villapage->ID, 'bedrrommphb_room_type', true );
					  	$destination  = get_post_meta( $villapage->ID, 'villa_locations', true );
					  	$purpose = get_post_meta( $villapage->ID, 'villa_purpose', true );
					  	$featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID));
					  	$villa_typeapi=get_post_meta( $villapage->ID, 'villa_type_api', true );
					  	$amn_villa = wp_get_object_terms( $villapage->ID, "mphb_room_type_facility");
					  	$gallerymeta  = get_post_meta( $villapage->ID, 'mphb_gallery', true );
					  	$galleryimgs = explode(',', $gallerymeta);
				        $galleryimgs = array_filter($galleryimgs);
						if($villa_typeapi=='api'){
							$villa_prc = number_format((float)min($villapricerange[$villa_code])/$totaldays, 2 );
							/*if($villa_prc == 0 && !count($_POST)>1){
								continue;
							}*/
						}else{
				        	$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID) ;
							$villa_prc = number_format($roomType->getDefaultPrice(),2);
						}

				        if($_POST!=''){
				        	if($checkin!='' && $checkout!='')
				        	{
				        		$villa_url .= get_permalink($villapage->ID).'?checkin='.$checkin.'&checkout='.$checkout;
				        	}
				        	if($villa_bed != ''){
				        		$villa_url .= '&beds='.$villa_bed;	
				        	}
				        	if($villa_guest != ''){
				        		$villa_url .= '&guests='.$villa_guest;	
				        	}
				        	if($villa_guest != ''){
				        		$villa_url .= '&price_typ='.$prc_typ;	
				        	}
				        	if($villa_prc != ''){
				        		$villa_url .= '&villa_pc='.$villa_prc;	
				        	}
				        }else{
				        	$villa_url = get_permalink($villapage->ID);
				        }
				?>
        		<div class="col-4 listItem" id="villa-<?php echo $villapage->ID; ?>">
        		
			        <div class="feature-item">
					<!-- <a href="<?php echo $villa_url; ?>"> -->
			
					
					<a href='javascript:void(0)' class='villadata' data-url="<?php echo get_permalink($villapage->ID); ?>">
					<form method="post" enctype="multipart/form-data" action="<?php echo get_permalink($villapage->ID); ?>">
					<input type='hidden' name="villa_prc" value="<?php echo $villa_prc; ?>" id="villa_prc" >
					<input type='hidden' name="beds" value="<?php echo $villa_bed; ?>" id="beds" >
					<input type='hidden' name="guests" value="<?php echo $villa_guest; ?>" id="guests" >
					<input type='hidden' name="prc_typ" value="<?php echo $prc_typ; ?>" id="prc_typ" >
					<input type='hidden' name="checkin" value="<?php echo $checkin; ?>" id="checkin" >
					<input type='hidden' name="checkout" value="<?php echo $checkout; ?>" id="checkout" >
				
						<div class="feature-item-slider">
							<?php 
							if(!empty($galleryimgs)){
							foreach ($galleryimgs as $key => $galleryimg) {
								?>

							<div>
			        			<img src="<?php echo wp_get_attachment_image_url($galleryimg,'full'); ?>" alt="">
			        		</div>
			        		<?php 
							}
							}else{
							
							?>
							<div>
			        			<img src="https://luxurykey.madebyfire.com/wp-content/uploads/2019/02/banner2.jpeg" alt="">
			        		</div>
							<?php } ?>
						</div>
						</form>
						</a>
						<div class="feature-item-content">
							<h5>
								<?php
									if($villa_bed != ''){
										echo $villa_bed.' BEDROOMS ';
									}
									if($villa_guest != ''){
										echo $villa_guest.' GUESTS ';
									}
									if($villa_bath != ''){
										echo $villa_bath.' BATHS ';
									}
								  ?>
							</h5>
							<h3><?php echo $title; ?></h3>
							<?php
								if($villa_typeapi=='api'){
									 if(!empty($villa_rates) && min($villapricerange[$villa_code])/$totaldays!=0){ 
									 	?>
										<p>
											From: €<?php echo number_format((float)min($villapricerange[$villa_code])/$totaldays, 2, '.', ''); ?> / per night
										</p>
									<?php 
									}
								}else{
							?>
									<p>
										From: €
										<?php  
											$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID) ;
											echo $roomType->getDefaultPrice();
										?> 
										/ per-night
									</p>	
							<?php
								} 
								if($destination!=''){
							?>
							<p>
								<a href="#!">
									<?php echo $destination.','; ?>
								</a> 
								Greece
							</p>
							<?php } ?>
						</div>
						
						<div class="feature-item-top">
							<span class="fav-icon"></span>
						</div>
					</div>
			
				</div>
						<?php  
						$villa_url = '';
					}	
				
				?>
    		</div>
    		<div class="col-6 mapView hide">
    			<div style="position: relative;height: 100%;width: 100%;">
					<div id="map"></div>
    			</div>
    		</div>
    	</div>
	</div>
</section>
<?php
get_footer();
?>