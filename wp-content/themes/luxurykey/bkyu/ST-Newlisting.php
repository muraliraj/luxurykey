<?php 
/***********************
Template Name: new Villas list
************************/
get_header('search');
?>
<?php
$apiUrl = get_option('api_endpoint');
$username = get_option('api_username');
$password = get_option('api_password');
if($_POST['villa-destination'] != ''){
	$villa_destination = $_POST['villa-destination'];
}elseif($_GET['locat']!='') {
	$villa_destination = $_GET['locat'];
}else{
	$villa_destination = 'Mykonos';
}
$purpose				=	$_POST['villa-purpose'];
$checkin				=	$_POST['villa-check-in'];
$checkout				=	$_POST['villa-check-out'];
$numbeds				=	(int)$_POST['no-of-bedrooms'];
$prc_typ  				=	$_POST['villa-price-typ'];
$checkin1=date_create($checkin);
$checkout1 = date_create($checkout);
$diff=date_diff($checkin1,$checkout1);
$totaldays=$diff->days;
$price					=	$_POST['villa-price'];
if ($_POST['villa-price-min']!='' && $_POST['villa-price-max']!=''  ) {
	$min_price				=	$_POST['villa-price-min'];
	$max_price				=	$_POST['villa-price-max'];
}
else
{
	$min_price=0;
	$max_price=999999;
}
$noofbed  =	$_POST['no-of-bedrooms'];
$authorization = 'Basic '. base64_encode($username.":".$password);
$feedUrl = $apiUrl.'/availability/LUXURYKEYS?checkin='.$checkin.'&checkout='.$checkout;

$ch = curl_init();
$headers = array(
	'Content-Type: application/x-www-form-urlencoded',
	'charset: utf-8',
	'Authorization:'. $authorization,
);
curl_setopt($ch, CURLOPT_URL, $feedUrl);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
$output = curl_exec($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$body = substr($output, $header_size);
$villaXmlData = new SimpleXMLElement($body);
$villaData = $villaXmlData->rates;
$villaXmlData=xml2array($villaXmlData);
$villa_rates=$villaXmlData['data']['rates']['rate'];
foreach ($villa_rates as  $villa_rate) {
	$villa_rate=xml2array($villa_rate);
	$title = $villa_rate['room']; 
	$tarr= explode('|', $title);
	$place = $tarr[2];
	$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
	$villapricerange[$villa_rate['type']][]=$villa_rate['pricing']['stay'];
	if($villa_rate['pricing']['stay']/$totaldays >= $min_price && $villa_rate['pricing']['stay']/$totaldays <= $max_price ){
		


		
		if($int>=$numbeds)
		{
			$vr[]=$villa_rate['type'];
		}

	}
}
$vr = array_unique($vr);
if (!empty($_POST) && ($_POST['villa-check-out'] || $_POST['villa-check-in'] )) {
	$roomsAtts = array(
		'availability'	 => 'locked',
		'from_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkin ),
		'to_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkout )
	);
	$result =  getAvailableRoomTypes_custom($roomsAtts);
// echo "<pre>";
 // print_r($result);
	foreach ($result as $value) {
		$manualAvailable[]=$value['id'];
	}
}
/*manual villa search integration*/
$villargs_all = array(
	'numberposts' =>-1,
	'post_type' => 'mphb_room_type',
	'orderby' => 'menu_order',
	'order' => 'ASC',
	'post_status'=>"publish"
);
$villapages_all = get_posts($villargs_all);
$place = array();
foreach($villapages_all as $villas_all)
{
	$title = $villas_all->post_title; 
	$place[] = $title;
} 
$place=array_filter(array_unique($place));
if (!empty($_POST)) {

	$villargs = array(
		'meta_key' => 'villa_code',
		'meta_value' => $vr,
		'post_type' => 'mphb_room_type',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'meta_query'    => array(
						    array(
						        'key'       => 'villa_type_api',
						        'value'     => 'private',
						        'compare'   => '!=',
						    )
						)

	);
}else {
	$villargs = array(
		'post_type' => 'mphb_room_type',
		'post_status' => 'publish',
		'posts_per_page' => -1,
		'meta_query'  => array(
						    array(
						        'key'       => 'villa_type_api',
						        'value'     => 'private',
						        'compare'   => '!=',
						    )
						)
	);
}


$villapages = get_posts($villargs);

if(count($manualAvailable)>0){
	$menu_villa_args = array(
		'post_type' => 'mphb_room_type',
		'post_status' => 'publish',
		'post__in' => $manualAvailable
	);
	$villa_array = get_posts($menu_villa_args);
	$villapages = array_merge($villapages,$villa_array);
}
$locs_map = array();
$villa_ids = array();
foreach($villapages as $villapage){
	$latitude = get_post_meta( $villapage->ID, 'villa_latitude', true );
	$longtitude = get_post_meta( $villapage->ID, 'villa_longtitude', true );
	if($latitude=='' || $longtitude=='' ){
		continue;
	}
	$destination =  get_post_meta( $villapage->ID, 'villa_locations', true );

	if($villa_destination != $destination ){
		continue;
	}
	$title = $villapage->post_title; 
	$villa_ids[] =$villapage->ID;
	//	$villa_location[] =  get_post_meta( $villapage->ID, 'villa_locations', true );
	$locs_map[] = "{'Latitude':".$latitude.",'Longitude':".$longtitude.",'DisplayName':'".$title."','MapId':'".$villapage->ID."'}";
}
$villa_map=implode(',',$locs_map);
?>
<div class="filter-top">
	<div class="container container-type3">
		<div class="row feature-row">
			<div class="col-12">
				<div class="top-bar text-right">
					<div class="button button-territory popup-trigger no-effect" data-id="confirmVilla" >
						<button>receive email offer</button>
					</div>
				</div>
				<div class="section-intro section-icons flex-sb count-villa">
					<h4>We've found <span><?php echo count($villa_ids) ?></span> luxury villas for you in <span id="count_loc"><?php echo $villa_destination; ?></span></h4>	
					<ul>
						<li><span>view</span></li>
						<li><i class="la la-table list-icon on"></i></li>
						<li><i class="la la-map-marker map-icon"></i></li>
					</ul>
				</div>						
			</div>
		</div>
	</div>
</div>
<button type="button" id="privat_villa_popup" class="popup-trigger no-effect" data-id="confirmVilla" style="display:none;">
	popupdisplay
</button>
<div class="popup-overlay">
	<div class="popup medium white-bg no-padding border-red" id="confirmVilla">
		<div class="popup-closebtn" data-id="confirmVilla">
			×
		</div>
		<div class="popup-body flex-body">
			<div class="popup-image">
				<img src="<?php echo get_bloginfo('template_url'); ?>/img/banner2.jpeg" alt="popup-bg" />
			</div>
			<div class="popup-content">
				<p>If you would also like to receive exclusive offers of non-listed properties, please share your contact details with us. </p>
				<?php wp_nonce_field('conactus_nonce', 'search_list'); ?>
				<form name="search_list" id="wp_login_form" method="post" action="">
					<div class="form-row">
						<label class="floating-item" data-error="Please enter your first name">
							<input type="text" id="user_name" class="floating-item-input input-item" name="username" value="" />
							<span class="floating-item-label">Email address</span>
						</label>
						<div class="error-message" id="err_name1">Please enter your email address</div>
					</div>
					<div class="form-row">
						<label class="floating-item" data-error="Please enter your first name">
							<input type="Password" id="pass_word" class="floating-item-input input-item" name="password" value="" />

							<span class="floating-item-label">Password</span>
						</label>
						<div class="error-message" id="err_register_password1">Please enter your password</div>
					</div>
					<div class="forget">
						<div class="form-row checkboxradio">
							<div class="checkboxradio-row">
								<input class="checkboxradio-item checkboxradio-invisible" name="rememberme"  id="checkbox1" type="checkbox" />
								<label class="checkboxradio-label checkbox-label"  for="checkbox1">Remeber me</label>
							</div>
						</div>
						<div class="forget-text"><a href="javascript:void(0);" data-id="forget">Forgot password?</a></div>
					</div>
					<div class="clearfix">
						<!-- <input type="submit" id="submitbtn" name="submit" class="button button-primary fR" value="Login">  -->
						<div class="button button-primary fR">
							<button  type="submit" id="sign-in" name="submit" >Login</button>
						</div>
					</div>

					</form>
					<div class="account-link">Don’t have an account? <a href="#" data-id="signup">Sign up</a></div>
					</div>
				</div>
			</div>
		</div>

	</div>
	<?php 
	if($_POST['villa-destination'] != ''){
		$villa_destination = $_POST['villa-destination'];
	}elseif($_GET['locat']!='') {
		$villa_destination = $_GET['locat'];
	}else{
		$villa_destination = 'Mykonos';
	}
	$purpose				=	$_POST['villa-purpose'];
	$checkin				=	$_POST['villa-check-in'];
	$checkout 				=   $_POST['villa-check-out'];
	$numbeds				=	(int)$_POST['no-of-bedrooms'];
	$prc_typ  				=	$_POST['villa-price-typ'];

	?>
	<input type="hidden" name="prc_typ" id="prc_typ" value="<?php echo $prc_typ;  ?>">
	<input type="hidden" name="checkin" id="checkin" value="<?php echo $checkin; ?>">
	<input type="hidden" name="checkout" id="checkout" value="<?php echo $checkout; ?>">
	<input type="hidden" name="villa_prc" id="villa_prc" value="">
	<input type="hidden" name="villa-occs" id="villa-occs" value="">
	<input type="hidden" name="no-of-bedrooms" id="no-of-bedrooms" value="<?php echo $numbeds; ?>">
	<section class="section-feature section-init listing">
		<div class="mobile">
			<div class="toggleMap">
				MAP <i class="la la-map-marker"></i>
			</div>
		</div>
		<div class="container container-type3 feature-container" >	        	
			<div class="row feature-row listRow toggle">
				<div class="col-12 listView" id="villa_list">

					<?php 
					foreach($villapages as $villapage) { 

						$upload_dir = wp_upload_dir(); 
						$gall_villa_dir = ( $upload_dir['basedir'] .'/'. $villapage->post_name);
						$gall_villa_url = ( $upload_dir['baseurl'] .'/'. $villapage->post_name);
						$files = scandir($gall_villa_dir); 
						$total = count($files); 
						$galleryimgs = explode(',', $gallerymeta);
						$galleryimgs = array_filter($galleryimgs);

						$title = $villapage->post_title; 
						$tarr=array();
						$tarr= explode('|', $title);
						$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
						$villa_bath   = get_post_meta( $villapage->ID, 'bathroommphb_room_type', true );
						$villa_pool  = get_post_meta( $villapage->ID, 'villa_pool', true );
						$villa_guest  = get_post_meta( $villapage->ID, 'mphb_adults_capacity', true );
						$villa_address  = get_post_meta( $villapage->ID, 'villa_address', true );
						$villa_code  = get_post_meta( $villapage->ID, 'villa_code', true );
						$villa_bed  = get_post_meta( $villapage->ID, 'bedrrommphb_room_type', true );
						$destination  = get_post_meta( $villapage->ID, 'villa_locations', true );
						$purpose = get_post_meta( $villapage->ID, 'villa_purpose', true );
						$featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID));
						$villa_typeapi=get_post_meta( $villapage->ID, 'villa_type_api', true );
						$amn_villa = wp_get_object_terms( $villapage->ID, "mphb_room_type_facility");
						$gallerymeta  = get_post_meta( $villapage->ID, 'mphb_gallery', true );
						$galleryimgs = explode(',', $gallerymeta);
						$galleryimgs = array_filter($galleryimgs);
						$Address  = get_post_meta( $villapage->ID, 'villa_address', true );
						if($villa_typeapi=='private'){
							continue;
						}
						if($villa_destination != $destination ){
							continue;
						}
						if($villa_typeapi=='api' && $villa_typeapi=='api_manu'){
							$villa_prc = number_format((float)min($villapricerange[$villa_code])/$totaldays, 2 );
							/*if($villa_prc == 0 && !count($_POST)>1){
								continue;
							}*/
						}else{
							$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID) ;
							$villa_prc = number_format($roomType->getDefaultPrice(),2);
						}

						if($_POST!=''){
							if($checkin!='' && $checkout!='')
							{
								$villa_url .= get_permalink($villapage->ID).'?checkin='.$checkin.'&checkout='.$checkout;
							}
							if($villa_bed != ''){
								$villa_url .= '&beds='.$villa_bed;	
							}
							if($villa_guest != ''){
								$villa_url .= '&guests='.$villa_guest;	
							}
							if($villa_guest != ''){
								$villa_url .= '&price_typ='.$prc_typ;	
							}
							if($villa_prc != ''){
								$villa_url .= '&villa_pc='.$villa_prc;	
							}
						}else{
							$villa_url = get_permalink($villapage->ID);
						}
						?>
						<div class="col-4 listItem" id="villa-<?php echo $villapage->ID; ?>">

							<div class="feature-item">
								<!-- <a href="<?php echo $villa_url; ?>"> -->


									
										<div>
											<div class="feature-item-slider">
												<?php 
												if(!empty($galleryimgs)){
													foreach ($galleryimgs as $key => $galleryimg) {
														?>

														<div>
															<a href='javascript:void(0)' class='villadata'  data-url="<?php echo get_permalink($villapage->ID); ?>">
																<form method="post" enctype="multipart/form-data" action="<?php echo get_permalink($villapage->ID); ?>" target="_blank">
																	<input type='hidden' name="villa_prc" value="<?php echo $villa_prc; ?>" id="villa_prc" >
																	<input type='hidden' name="beds" value="<?php echo $villa_bed; ?>" id="beds" >
																	<input type='hidden' name="guests" value="<?php echo $villa_guest; ?>" id="guests" >
																	<input type='hidden' name="prc_typ" value="<?php echo $prc_typ; ?>" id="prc_typ" >
																	<input type='hidden' name="checkin" value="<?php echo $checkin; ?>" id="checkin" >
																	<input type='hidden' name="checkout" value="<?php echo $checkout; ?>" id="checkout" >
																<img src="<?php echo wp_get_attachment_image_url($galleryimg,'medium'); ?>" alt="">
															</form></a>
														</div>
														<?php 
													}
												}else{

													?>
													<div>
														<a href='javascript:void(0)' class='villadata'  data-url="<?php echo get_permalink($villapage->ID); ?>">
														<form method="post" enctype="multipart/form-data" action="<?php echo get_permalink($villapage->ID); ?>" target="_blank">
															<input type='hidden' name="villa_prc" value="<?php echo $villa_prc; ?>" id="villa_prc" >
															<input type='hidden' name="beds" value="<?php echo $villa_bed; ?>" id="beds" >
															<input type='hidden' name="guests" value="<?php echo $villa_guest; ?>" id="guests" >
															<input type='hidden' name="prc_typ" value="<?php echo $prc_typ; ?>" id="prc_typ" >
															<input type='hidden' name="checkin" value="<?php echo $checkin; ?>" id="checkin" >
															<input type='hidden' name="checkout" value="<?php echo $checkout; ?>" id="checkout" >
															<img src="https://luxurykey.madebyfire.com/wp-content/uploads/2019/02/banner2.jpeg" alt="">
														</form>
														</a>
													</div>
												<?php } ?>
											</div>
											<div class="slider-nav-wrapper">
												<ul class="feature-item-slider-nav slider-nav">
													<?php 
													if(!empty($galleryimgs))
													{
														$qwe=0;
														foreach ($galleryimgs as $galleryimg) {
															if($qwe == 5){
																break;
															}
															if($galleryimg!=''){ ?>
																<li>
																	<span></span>
																</li>
																<?php
															}
															$qwe++;
														}
													}
													?>
												</ul>
											</div>

										</div>
									<div class="feature-item-content">
										<h5>
											<?php
											if($villa_bed != ''){
												echo '<span class="villa-cont">'.$villa_bed." BEDROOMS</span>";
											}
											if($villa_guest != ''){
												echo '<span class="villa-cont">'.$villa_guest." GUESTS</span>";
											}
											if($villa_bath != ''){
												echo '<span class="villa-cont">'.$villa_bath." BATHROOMS</span>";
											}
											?>
										</h5>
										<h3><a href="<?php echo get_permalink($villapage->ID); ?>" target="_blank"><?php echo $title; ?></a></h3>
										<?php
										if($villa_typeapi=='api'){
											if(!empty($villa_rates) && min($villapricerange[$villa_code])/$totaldays!=0){ 
												?>
												<p>
													From: €<?php echo number_format((float)min($villapricerange[$villa_code])/$totaldays, 2, '.', ''); ?> / per night
												</p>
												<?php 
											}
										}else{
											$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID);
											
											if($roomType->getDefaultPrice() != 0){
											?>
											<p>
												From: €
												<?php  
												$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID) ;
												echo $roomType->getDefaultPrice();
												?> 
												/ per-night
											</p>	
											<?php
											} 
										}
										if($destination!=''){
											?>
											<p>
												<a href="#!">
													<?php echo ($Address); ?>
												</a> 

											</p>
										<?php } ?>
									</div>

									<!--Favorites Button start-->
									<div class="feature-item-top">
										<?php echo do_shortcode('[favorite_button post_id="' . $villapage->ID . '"]'); ?> 		
									</div>
									<!--Favorites Button End-->

								</div>

							</div>
							<?php  
							$villa_url = '';
						}	

						?>
					</div>
					<div class="col-6 mapView hide">
						<div style="position: relative;height: 100%;width: 100%;">
							<div id="map"></div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<input type="hidden" name="map_id" id="map_id" value="<?php echo $villa_map; ?>" >
		<?php
		get_footer();
		$postid_dest = get_post_by_title($villa_destination);
		$latid = get_post_meta($postid_dest->ID, 'msg_name', true);
        $longt = get_post_meta($postid_dest->ID, 'msg_sub', true);
		?>
		<script>

			$(document).ready(function() {
				setTimeout(function(){
					$('#privat_villa_popup').click();
				},5000);	
			});
			var map_id = $(document).find('#map_id').val();

			var beaches=[<?php echo $villa_map; ?>];


			var markers = [];
			function filterSelectedState(selectedLocationName) {
				var filteredLocations = [];

				if(selectedLocationName === null) {
					filteredLocations = beaches;
				} 
				return filteredLocations;
			}
    //initMap(filterSelectedState(null));
    function initMap(locaData) {
    	console.log(locaData);
    	var map = new google.maps.Map(document.getElementById('map'), {
    		zoom:12.2,
          //center: new google.maps.LatLng(12.93924,80.12999),
          center: new google.maps.LatLng(<?php echo $latid; ?>, <?php echo $longt; ?>),
          mapTypeId: google.maps.MapTypeId.ROADMAP
      });
    	var country = "<?php echo $villa_destination; ?>";
    	var geocoder = new google.maps.Geocoder();
    	
    	geocoder.geocode( {'address' : country}, function(results, status) {
    		if (status == google.maps.GeocoderStatus.OK) {
    			map.setCenter(results[0].geometry.location);
    		}
    	});

        //setMarkers(map);
        Array.prototype.groupBy = function(prop) {
        	return this.reduce(function(groups, item) {
        		const val = item[prop]
        		groups[val] = groups[val] || []
        		groups[val].push(item)
        		return groups
        	}, {})
        };

        // Info window
        var i;
        var infowindow = new google.maps.InfoWindow();
        var infoWindowContent = [];
        for (var i = 0; i < locaData.length; i++) {
        	infoWindowContent[i] = getInfoWindowDetails(locaData[i]);
          // Adds markers to the map.
          // Marker sizes are expressed as a Size of X,Y where the origin of the image
          // (0,0) is located in the top left of the image.

          // Origins, anchor positions and coordinates of the marker increase in the X
          // direction to the right and in the Y direction down.
          var image = {
          	url: 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
            // This marker is 20 pixels wide by 32 pixels high.
            size: new google.maps.Size(20, 32),
            // The origin for this image is (0, 0).
            origin: new google.maps.Point(0, 0),
            // The anchor for this image is the base of the flagpole at (0, 32).
            anchor: new google.maps.Point(0, 32)
        };
        var  pin = {
        	path: "M0-40c-8.28 0-15 6.7-15 15C-15-10 0 0 0 0s15-10 15-25c0-8.28-6.72-15-15-15zm0 17.5c-2.76 0-5-2.25-5-5s2.25-5 5-5c2.76 0 5 2.25 5 5s-2.25 5-5 5z",
        	fillColor: "#b49759",
        	fillOpacity: .8,
        	scale: 1,
        	strokeColor: "white",
        	strokeWeight: 3,
        };
          // Shapes define the clickable region of the icon. The type defines an HTML
          // <area> element 'poly' which traces out a polygon as a series of X,Y points.
          // The final coordinate closes the poly by connecting to the first coordinate.
          var shape = {
          	coords: [1, 1, 1, 20, 18, 20, 18, 1],
          	type: 'poly'
          };

          var beach = locaData[i];
          var latlngM = new google.maps.LatLng(locaData[i].Latitude, locaData[i].Longitude);
          var marker = new google.maps.Marker({
            // position: {lat: beach.Latitude, lng: beach.Longitude},
            position: latlngM,
            map: map,
            icon: pin,
            shape: shape,
            scaledSize: new google.maps.Size(30, 30),
            title: locaData[i].DisplayName,
            markerID: locaData[i].MapId,
            animation:google.maps.Animation.DROP
        });
          google.maps.event.addListener(marker, 'click', (function (marker, i) {
          	return function () {               
          		infowindow.setContent(infoWindowContent[i]);
          		infowindow.open(map, marker);
          		google.maps.event.addListenerOnce(infowindow, 'domready', function () {
          			$('.mapSlider').slick({
          				autoplay:false,
			             slidesToShow: 1,
			             slidesToScroll: 1,
			             fade: true,
			             cssEase: 'linear',
			             arrows: true,
			             dots: false,
			             // dotsClass:'slick-dots slider-dots',
			             prevArrow: "<i class='la la-angle-left slick-arrow-small'></i>",
			             nextArrow: "<i class='la la-angle-right slick-arrow-small'></i>",
			 
          			});

          		});
                //only for map center,zoomin
                map.setCenter(marker.getPosition());
                //map.setZoom(15);
                
            }
        })(marker, i));

// 			if (bounds.getNorthEast().equals(bounds.getSouthWest())) {
// 		       var extendPoint1 = new google.maps.LatLng(bounds.getNorthEast().lat() + 0.001, bounds.getNorthEast().lng() + 0.001);
// 		       var extendPoint2 = new google.maps.LatLng(bounds.getNorthEast().lat() - 0.001, bounds.getNorthEast().lng() - 0.001);
// 		       bounds.extend(extendPoint1);
// 		       bounds.extend(extendPoint2);
// 		    }
//            map.fitBounds(bounds);
// 			map.panToBounds(bounds);
markers.push(marker); 
}   

function getInfoWindowDetails(location){
	var contentString = '<div class="mapSlider ">';
	if($("#villa-"+locaData[i].MapId+" .feature-item-slider div img").length > 0){
		$("#villa-"+locaData[i].MapId+" .feature-item-slider div").find("img").each(function(){
			var slideImg=$(this).attr('src');
			var featureContent=$("#villa-"+locaData[i].MapId).find(".feature-item-content").html();
			contentString +='<div class="mapSlider-item"><img src="'+slideImg+'"'+' /><div class="mapSlider-content">'+featureContent+'</div></div>';
		});  
	}                         
	contentString +='</div>';
	return contentString;
}
}
initMap(filterSelectedState(null));
	//google.maps.event.addDomListener(window, "load", );
</script>
<script>
	$('a.villadata').on('click', function (e) {
	// var data_url  = $(this).data('url');
	
	$(this).find('form').submit();

	return false;
});
</script>