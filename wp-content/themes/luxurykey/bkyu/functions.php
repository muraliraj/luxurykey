	<?php
	
	@ini_set( 'upload_max_size' , '64M' );
	@ini_set( 'post_max_size', '64M');
	@ini_set( 'max_execution_time', '300' );
	add_action('init', 'init_custom_load');
	if (!defined('TMPL_URL')) {
		define('TMPL_URL', get_template_directory_uri());
	}

	if($_COOKIE['cook_pol']!='no' || $_COOKIE['cook_pol']==''){
		setcookie('cook_pol', 'yes', time() + (86400 * 30), "/");
	}
	function init_custom_load(){

		if(is_admin()) {
			wp_enqueue_style('font-awesome.min', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css');
			wp_enqueue_style('jquery-ui-css', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
			wp_enqueue_script('jquery-ui-datepicker');
		    wp_register_style('jquery-ui', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css');
		    wp_enqueue_style('jquery-ui');
		}
	}
	remove_action('wp_head', 'wp_generator');
	show_admin_bar(false);
	remove_image_size('large');
	remove_image_size('medium');
	remove_image_size('thumbnail');
	require_once(ABSPATH . 'wp-admin/includes/user.php');

	function remove_img_attributes( $html ) {
		$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
		return $html;
	}
	add_filter( 'post_thumbnail_html', 'remove_img_attributes', 10 );
	add_filter( 'image_send_to_editor', 'remove_img_attributes', 10 );
	/* For post types and metabox */
	require_once(TEMPLATEPATH . "/lib/admin-config.php");

	/* Featured Image */
	add_theme_support('post-thumbnails');
	/*title search*/
function wpse_11826_search_by_title( $search, $wp_query ) {
    if ( ! empty( $search ) && ! empty( $wp_query->query_vars['search_terms'] ) ) {
        global $wpdb;

        $q = $wp_query->query_vars;
        $n = ! empty( $q['exact'] ) ? '' : '%';

        $search = array();

        foreach ( ( array ) $q['search_terms'] as $term )
            $search[] = $wpdb->prepare( "$wpdb->posts.post_title LIKE %s", $n . $wpdb->esc_like( $term ) . $n );

        if ( ! is_user_logged_in() )
            $search[] = "$wpdb->posts.post_password = ''";

        $search = ' AND ' . implode( ' AND ', $search );
    }

    return $search;
}

add_filter( 'posts_search', 'wpse_11826_search_by_title', 10, 2 );

	/*	Menu Backend */	
	add_theme_support( 'menus' );
	add_theme_support( 'hotel-theme' );

	/*	Multipost Thumbnail Image */
	if (class_exists('MultiPostThumbnails')) {
		new MultiPostThumbnails(array(
			'label' => 'mobile image Dim:760X440',
			'id' => 'mobileimage',
			'post_type' => 'banners'
		)
	);
		new MultiPostThumbnails(array(
			'label' => 'Home Tile Image Dim:760X440',
			'id' => 'iconimage',
			'post_type' => 'beauty'
		)
	);
		new MultiPostThumbnails(array(
			'label' => 'Page Banner Image Dim:760X440',
			'id' => 'page_banner',
			'post_type' => 'page'
		)
	);
		new MultiPostThumbnails(array(
			'label' => 'company logo',
			'id' => 'banner_art',
			'post_type' => 'articles'
		)
	);
		new MultiPostThumbnails(array(
			'label' => 'Home Tile Image Dim:760X440',
			'id' => 'recpiconimg',
			'post_type' => 'recipe'
		)
	);
		new MultiPostThumbnails(array(
			'label' => 'Home Tile Image Dim:760X440',
			'id' => 'fiticonimg',
			'post_type' => 'fitness'
		)
	);
	}
	/*	For Excerpt */
	add_post_type_support('page', 'excerpt');

	/* Format the content */
	function content_formatter($content) {
		$bad_content = array('<p></div></p>', '<p><div class="full', '_width"></p>', '</div></p>', '<p><ul', '</ul></p>', '<p><div', '<p><block', 'quote></p>', '<p><hr /></p>', '<p><table>', '<td></p>', '<p></td>', '</table></p>', '<p></div>', 'nosidebar"></p>', '<p><p>', '<p><a', '</a></p>', '_half"></p>', '_third"></p>', '_fourth"></p>', '<p><p', '</p></p>', 'child"></p>', '<p></p>');
		$good_content = array('</div>', '<div class="full', '_width">', '</div>', '<ul', '</ul>', '<div', '<block', 'quote>', '<hr />', '<table>', '<td>', '</td>', '</table>', '</div>', 'nosidebar">', '<p>', '<a', '</a>', '_half">', '_third">', '_fourth">', '<p', '</p>', 'child">', '');
		$new_content = str_replace($bad_content, $good_content, $content);
		return $new_content;
	}
	remove_filter('the_content', 'wpautop');
	add_filter('the_content', 'wpautop', 10);
	add_filter('the_content', 'content_formatter', 11);
	function xml2array ( $xmlObject, $out = array () )
	{
		foreach ( (array) $xmlObject as $index => $node )
			$out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
		return $out;
	}  
	add_action( 'init', 'gp_register_taxonomy_for_object_type' );
	function gp_register_taxonomy_for_object_type() {
		register_taxonomy_for_object_type( 'post_tag', 'villas' );
	};

	/* For empty paragraph */
	function shortcode_empty_paragraph_fix_tag($content) {
		$array = array(
			'<p>[' => '[',
			']</p>' => ']',
			'<p></p>' => '',
			']<br />' => ']'
		);
		$content = strtr($content, $array);
		return $content;
	}
		/****************************
		Get Menus In Header And Footer
		*****************************/
		function navmenu_luxurykey($menu_type,$mob_menu="") {
			$menu_args = array(
				'order' => 'ASC',
				'orderby' => 'menu_order',
				'post_type' => 'nav_menu_item',
				'post_status' => 'publish',
				'output' => ARRAY_A,
				'output_key' => 'menu_order',
				'nopaging' => true,
				'update_post_term_cache' => false
			);
			$main_menu_array = wp_get_nav_menu_items($menu_type, $menu_args);
			switch ($menu_type) {
				case 'header_menu':
				if($mob_menu==''){
					echo '<ul>';
					foreach ($main_menu_array as $key => $left_array_val) {
						$menu_title = $left_array_val->title;
						$menu_villa_name = $left_array_val->object_id;
						$menu_post_name = get_post($menu_villa_name);
						
						$menu_id = $left_array_val->ID;
						if ( $left_array_val->menu_item_parent == 0 ) {
							$url_list = $menu_post_name->post_name == 'new-listing'?'javascript:void(0)':$left_array_val->url;
							echo '<li><a href="'.$url_list.'">'.$left_array_val->title.'</a>';
						}
						if($menu_post_name->post_name == 'new-listing'){
							echo '<div class="sub-nav"><ul>';
							$dest_args = array(
				                       'post_type'=> 'destination',
				                       'post_status' => 'publish',
				                       'order' => 'ASC',
				                       'numberposts' => -1
				                     );
							$dest_posts = get_posts($dest_args);

							foreach ($dest_posts as $key => $dest_post) {
								
								if($dest_post->post_title =='Other Greek Islands' || $dest_post->post_title == 'Other Global Destinations' || $dest_post->post_title =='Private Islands'){
									continue;
								}else{
									
									echo '<li><a href="'.get_permalink($menu_post_name->ID).'?locat='.$dest_post->post_title.'">'.$dest_post->post_title.'</a></li>';
								}
							}
							echo '</ul></div>';
						}else{

							customSubmenu($menu_id, $main_menu_array, $menu_title);
						}
						if ( $left_array_val->menu_item_parent == 0 ) :
							echo '</li>';
						endif; 
					}

					if(is_user_logged_in()) {
						$dashboard = '<li>
						<a href="'.get_bloginfo('url').'/my-account/">My account</a>
						</li>
						';
						$heart = '<li><a href="'.get_bloginfo('url').'/favorites/"><i class="fa fa-heart"></i></a></li>';
					} else {
						$dashboard = '<li>
						<a href="javascript:void(0);" data-id="signin">Sign in</a>
						</li>
						<li>
						<a href="javascript:void(0);" data-id="signup">Sign up</a>
						</li>';
						$heart = '<li><a href="'.get_bloginfo('url').'/favorites/"><i class="fa fa-heart"></i></a></li>';
					}
					echo ''.$heart.'<li><a href="javascript:void(0);"><i class="fa fa-user"></i></a><div class="sub-nav sign-nav"><ul>'.$dashboard.'</ul></div></li></ul>';
					echo '</ul>';
				}else if($mob_menu=='yes'){
					foreach ($main_menu_array as $key => $main_menu_mob) {
						echo '<li><a href="'.$main_menu_mob->url.'">'.$main_menu_mob->title.'</a></li>';
					}
				}
				break;
				case 'footer_menu':
				echo '<ul>';
				foreach ($main_menu_array as $key => $main_menu_mob) {
					echo '<li><a href="'.$main_menu_mob->url.'">'.$main_menu_mob->title.'</a></li>';
				}
				echo '</ul>';
				break;
				default:
	            # code...
				break;
			}
		}

		function customSubmenu($menuVal, $menu_items, $current_menu_title) {
			global $post;
			$submenu_count = 0;
			foreach ($menu_items as $item) {

				if ($item->menu_item_parent == $menuVal) {
					$submenu_count++;
				}

			}
			if ($submenu_count != 0) {
				print '<div class="sub-nav">
				<ul>';

				foreach ($menu_items as $subitem) {
					$subtarget = "";
					if ($subitem->menu_item_parent == $menuVal) {
						if ($subitem->target == "_blank") {
							$subtarget = "target='_blank'";
						}
						$menu_url = $subitem->url;
						if($menu_url=="#"){
							$menu_url ="javascript:void(0);";
						} 			
						?>
						<li>
							<a href="<?php echo $menu_url; ?>"><?php echo $subitem->title; ?></a>
						</li>                                    
						<?php
					}
				}
				print "</ul></div>";
			}
		}
		/*********
	    Remove Image Height and Width
	    **********/
	    add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10,3 );
	    add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

	    function remove_width_attribute( $html ) {
	    	$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
	    	return $html;
	    }

	    add_filter( 'the_content', 'remove_thumbnail_dimensions', 10 );
	    function remove_thumbnail_dimensions( $html ) {
	    	$html = preg_replace( '/(width|height)=\"\d*\"\s/', "", $html );
	    	return $html;
	    }
		/*********
	    Remove P tag from images
	    **********/
	    function filter_ptags_on_images($content) {
	    	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	    	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
	    }
	    add_filter('acf_the_content', 'filter_ptags_on_images');
	    add_filter('the_content', 'filter_ptags_on_images');

		/********
		Shortcodes
		*********/

		function span( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<span>'.do_shortcode($content).'</span>';
		}
		add_shortcode('span', 'span');

		function break_tag( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<br/>';
		}
		add_shortcode('break_tag', 'break_tag');

		function div_tag( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div>'.do_shortcode($content).'</div>';
		}
		add_shortcode('div_tag', 'div_tag');

		function container( $atts, $content = null ) {
			if(isset($atts['type'])) {
				if($atts['type']) {
					$type = $atts['type'];
					if($type="flex") {
						$class = 'flexed-listing';
					} else {
						$class = 'section-divider';
					}
				} else {
					$type = '';
				}
			}
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="container container-type1 '.$class.'">'.do_shortcode($content).'</div>';
		}
		add_shortcode('container', 'container');


		function anchor( $atts, $content = null ) {
			if(isset($atts['type'])) {
				if($atts['link']) {
					$link = $atts['link'];
				} else {
					$link = '';
				}
			}
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<a href="'.$link.'">'.do_shortcode($content).'</a>';
		}
		add_shortcode('anchor', 'anchor');

		function block( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="para-block">'.do_shortcode($content).'</div>';
		}
		add_shortcode('block', 'block');

		function left_border( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="para-border-left"></div>';
		}
		add_shortcode('left_border', 'left_border');

		function right_border( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="para-border-right"></div>';
		}
		add_shortcode('right_border', 'right_border');

		function list_block( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="list-hero">'.do_shortcode($content).'</div>';
		}
		add_shortcode('list_block', 'list_block');

		function list_entry( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="list-Entry">'.do_shortcode($content).'</div>';
		}
		add_shortcode('list_entry', 'list_entry');

		function golden_background( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="list-Image golden-bg">'.do_shortcode($content).'</div>';
		}
		add_shortcode('golden_background', 'golden_background');

		function list_content( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="list-Block">'.do_shortcode($content).'</div>';
		}
		add_shortcode('list_content', 'list_content');

		function list_middle_content( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="list-middle">'.do_shortcode($content).'</div>';
		}
		add_shortcode('list_middle_content', 'list_middle_content');

		function title_line( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="title-line">'.do_shortcode($content).'</div>';
		}
		add_shortcode('title_line', 'title_line');

		function tick_list( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="tick-list">'.do_shortcode($content).'</div>';
		}
		add_shortcode('tick_list', 'tick_list');

		function icon_list( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="two-col-list clearfix">'.do_shortcode($content).'</div>';
		}
		add_shortcode('icon_list', 'icon_list');


		function accordion_block($atts, $content = null)
		{
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="accordion-row-blk">'.do_shortcode($content).'</div>';
		}
		add_shortcode('accordion_block', 'accordion_block');


		function accordion_row($atts, $content = null)
		{

			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="accordion-row">'.do_shortcode($content).'</div>';
		}
		add_shortcode('accordion_row', 'accordion_row');

		function accordion_content($atts, $content = null)
		{

			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '	<div class="accordion-content dotlist">'.do_shortcode($content).'</div>';
		}
		add_shortcode('accordion_content', 'accordion_content');


		function content_feature($atts, $content = null)
		{

			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '	<div class="content-feature">'.do_shortcode($content).'</div>';
		}
		add_shortcode('content_feature', 'content_feature');

		function feature_icon($atts, $content = null)
		{

			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '	<div class="feature-icon">'.do_shortcode($content).'</div>';
		}
		add_shortcode('feature_icon', 'feature_icon');


       function footer_container( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="container footer-container">'.do_shortcode($content).'</div>';
		}
		add_shortcode('footer_container', 'footer_container');


		function footer_section( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="row footer-flex">'.do_shortcode($content).'</div>';
		}
		add_shortcode('footer_section', 'footer_section');

		function footer_content( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="col-6 footer-content footer-content-flex">'.do_shortcode($content).'</div>';
		}
		add_shortcode('footer_content', 'footer_content');


		function footer_address( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="">'.do_shortcode($content).'</div>';
		}
		add_shortcode('footer_address', 'footer_address');

		function footer_links( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="col-3 footer-content">'.do_shortcode($content).'</div>';
		}
		add_shortcode('footer_links', 'footer_links');

		function social_links( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="d-flex social-links">'.do_shortcode($content).'</div>';
		}
		add_shortcode('social_links', 'social_links');

		function facebook_links( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			$link = $atts['link'];
			return '<a class="no-effect" href="'.$link.'"><i class="fa fa-facebook-f"></i></a>';
		}
		add_shortcode('facebook_links', 'facebook_links');

		function twitter_links( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			$link = $atts['link'];
			return '<a class="no-effect" href="'.$link.'"><i class="fa fa-twitter"></i></a>';
		}
		add_shortcode('twitter_links', 'twitter_links');

		function instagram_links( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			$link = $atts['link'];
			return '<a class="no-effect" href="'.$link.'"><i class="fa fa-instagram"></i></a>';
		}
		add_shortcode('instagram_links', 'instagram_links');

		function pinterest_links( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			$link = $atts['link'];
			return '<a class="no-effect" href="'.$link.'"><i class="fa fa-pinterest-p"></i></a>';
		}
		add_shortcode('pinterest_links', 'pinterest_links');


		function footer_copy( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="container footer-copy">'.do_shortcode($content).'</div>';
		}
		add_shortcode('footer_copy', 'footer_copy');
        function golden_heading( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="text-golden">'.do_shortcode($content).'</div>';
		}
		add_shortcode('golden_heading', 'golden_heading');



		/**
		 * Implement custom functions for the site.
		 */
		require get_parent_theme_file_path('/inc/additional_functions.php');
		require get_parent_theme_file_path('/inc/meta-box.php');
		/**
		 * Implement Site Settings
		 */
		require get_parent_theme_file_path('/inc/site_settings.php');
		require get_parent_theme_file_path('/inc/whatsapp/src/whatsprot.class.php');
		/*
		* API feeds
		*/
		function blog_instagram_api_curl_connect( $api_url ){
			$connection_c = curl_init(); // initializing
			curl_setopt( $connection_c, CURLOPT_URL, $api_url ); // API URL to connect
			curl_setopt( $connection_c, CURLOPT_RETURNTRANSFER, 1 ); // return the result, do not print
			curl_setopt( $connection_c, CURLOPT_TIMEOUT, 20 );
			$json_return = curl_exec( $connection_c ); // connect and get json data

			curl_close( $connection_c ); // close connection
			return json_decode( $json_return ); // decode and return
		}

	// sign up
		function create_account(){
	    //You may need some data validation here
			$user = ( isset($_POST['reg_name']) ? $_POST['reg_name'] : '' );
			$crt_salut = ( isset($_POST['crt_salut']) ? $_POST['crt_salut'] : '' );
			$crt_isd = ( isset($_POST['crt_isd']) ? $_POST['crt_isd'] : '' );
			$display_name = ( isset($_POST['lname']) ? $_POST['lname'] : '' );
			$pass = ( isset($_POST['reg_pwd']) ? $_POST['reg_pwd'] : '' );
			$reg_email = ( isset($_POST['email']) ? $_POST['email'] : '' );
			$phone = ( isset($_POST['reg_phone_number']) ? $_POST['reg_phone_number'] : '' );
			//print_r($_POST); 

			if ( !email_exists( $reg_email ) ) {
				$uname = explode('@', $reg_email);
				$userdata = array(
			        'user_login' => $uname[0],
			        'user_pass'  => $pass,
			        'user_email' => $reg_email,
			        'first_name' => $display_name,
			    );
			 
			    $user_id = wp_insert_user( $userdata ) ;
			    if( !is_wp_error($user_id) ) {
        			echo '1';
			    } else {
			        echo $user_id->get_error_message();
			    }
			    exit();
				/*if( !is_wp_error($user_id) ) {
	           //user has been created
					$user = new WP_User( $user_id );
					$user->set_role( 'subscriber' );
					$test = update_user_meta($user_id,'MobileNo',$_POST['reg_phone_number']);
					var_dump($test);
					update_user_meta($user_id,'l_name',$_POST['lname']);
					update_user_meta($user_id,'crt_salut',$_POST['crt_salut']);
					update_user_meta($user_id,'crt_isd',$_POST['crt_isd']);
			   //Redirect
					
			   echo '1~'.get_permalink($post->ID);
					// wp_redirect("'.get_bloginfo('url').'/my-account/");
					
					 exit;

				} else {
					echo '2';

	           //$user_id is a WP_Error object. Manage the error
				}*/
			}else{
				echo "<p>Your email id is already subscribed</p>";
			}
			exit();
		}
		add_action('wp_ajax_create_account', 'create_account');
	 	add_action('wp_ajax_nopriv_create_account', 'create_account');	
	// add_action('init','create_account');

		/***** Generate payment link **********/

		add_action('wp_ajax_generate_link', 'generate_link');
		add_action('wp_ajax_nopriv_generate_link', 'generate_link');
		function generate_link(){
			global $wpdb;
		/*$tableName = $wpdb->prefix."services_booking";
		$description = $_POST['description'];
		$date = $_POST['date'];
		$guests = $_POST['guests'];
		$currency = $_POST['currency'];
		$email = $_POST['email'];
		$price = $_POST['price'];
		$phone = $_POST['phone'];
		$wpdb->insert($tableName, array(
		    'description' => $description,
		    'date' => $date,
		    'guests' => $guests,
		    'currency' => $currency,
		    'email' => $email,
		    'price' => $price,
		    'phone' => $phone,
		    'generated_date' => date( 'Y-m-d H:i:s' )
		));*/

	$username = "8760623462"; // Username
	$password = ""; // Password

	$wa = new WhatsProt($username, "WhatsApp", true);

	try {
		$wa->connect();
	//$wa->loginWithPassword($password);
	} catch(Exception $e) {
		echo "ERROR : Login Failed";
		exit(0);
	}
	$no="8760623462"; // Number
	$msg="Karuvaya"; // Message

	try {
		$wa->sendMessage($no, $msg);
		echo 'Text Message Sent';
	} catch(Exception $e) {
		echo "ERROR : Text Message Sending Failed";
	}
	die();
}

add_filter('dfi_post_types', 'filter_post_types');
function filter_post_types() {
	    return array('villas', 'page'); //will display DFI in post and page
	}

function button_prim( $atts, $content = null ) {
			$content = preg_replace('#^<\/p>|<p>$#', '', $content);
			$content=shortcode_empty_paragraph_fix_tag($content);
			return '<div class="button button-primary">'.do_shortcode($content).'</div>';
		}
		add_shortcode('button_prim', 'button_prim');
function section_home($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '	<section class="section-services section-home">'.do_shortcode($content).'</div>';
	}
	add_shortcode('section_home', 'section_home');
	function contain($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '	<div class="container">'.do_shortcode($content).'</div>';
	}
	add_shortcode('contain', 'contain');
    function row($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '	<div class="row">'.do_shortcode($content).'</div>';
	}
	add_shortcode('row', 'row');
	
	function center_block($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '	<div class="col-8 center-block home-heading intro-block">'.do_shortcode($content).'</div>';
	}
	add_shortcode('center_block', 'center_block');
	 
    function content_block($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '	<div class="row content-block text-center">'.do_shortcode($content).'</div>';
	}
	add_shortcode('content_block', 'content_block');
	function col4($atts, $content = null)
	{
	   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
	   $content=shortcode_empty_paragraph_fix_tag($content);
	   return '	<div class="col-4">'.do_shortcode($content).'</div>';
	}
	add_shortcode('col4', 'col4');
	function border_heading($atts, $content = null) {
   $content = preg_replace('#^<\/p>|<p>$#', '', $content);
   $content = shortcode_empty_paragraph_fix_tag($content);
   // $link = $atts['link'];
   return '<a href="'.$link.'" class="border-heading">' . do_shortcode($content) . '</a>';
  }
  add_shortcode('border_heading', 'border_heading');


	/********** Search API integration and updating Villas from API *********/

	add_action('wp_ajax_searchvilla', 'searchvilla');
	add_action('wp_ajax_nopriv_searchvilla', 'searchvilla');
	function searchvilla(){

		$villargs = array(
			'numberposts' =>-1,
			'post_type' => 'mphb_room_type',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_status'=>"publish"
		);
		$villapages = get_posts($villargs);
		$allCodes = array();
		foreach ($villapages as $key => $villapage) {

			$villaUniqueCode = get_post_meta( $villapage->ID, 'villa_code', true );
			$aaaa = get_post_meta( $villapage->ID, 'mphb_gallery_acc', true );
			$allCodes[] = $villaUniqueCode;
		}
		// $destination = $_POST['destination'];
		// $purpose = $_POST['purpose'];
		// $checkin = $_POST['checkin'];
		// $checkout = $_POST['checkout'];
		// $bedrooms = $_POST['bedrooms'];
		// $price = $_POST['price'];
		$apiUrl = get_option('api_endpoint');
		$username = get_option('api_username');
		$password = get_option('api_password');
		$authorization = 'Basic '. base64_encode($username.":".$password);
		$feedUrl = $apiUrl.'/room/LUXURYKEYS';
		$ch = curl_init();
		$headers = array(
			'Content-Type: application/x-www-form-urlencoded',
			'charset: utf-8',
			'Authorization:'. $authorization,
		);
		curl_setopt($ch, CURLOPT_URL, $feedUrl);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		$output = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($output, $header_size);
		$villaXmlData = new SimpleXMLElement($body);
		curl_close($ch);
		$villaData = $villaXmlData->data->rooms->room;

		foreach ($villaData as $key => $villa) {
			$villa_ameintities =$villa->amenities->amenity;
			$villa_ameintities = xml2array($villa_ameintities );

			$villaCode 			= $villa->code;
			$villaName_1 			= $villa->name;
			$tarr= explode('|', $villaName_1);
			$villaName=$tarr[1]; 
			$noofbed = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
			$villaDescription 	= $villa->description;
			$minPersons 		= $villa->capacity->min_pers;
			$maxPersons 		= $villa->capacity->max_pers;
			$maxAdults  		= $villa->capacity->max_adults;
			$isChildrenAllowed 	= $villa->capacity->children_allowed;
			$photos 			= $villa->photos->photo;
			$gall = array();
			foreach ($photos as $photo) {
				$xsmallImage = $photo->xsmall;
				$smallImage = $photo->small;
				$mediumImage = $photo->medium;
				$largeImage = $photo->large;
				$gall[] = $largeImage;
				// var_dump($largeImage);
				// echo "<img src='".$largeImage."' />";
			}
			$gallery = implode(",",$gall);

			if(!in_array($villaCode, $allCodes)) {
				$postarr = array(
					'post_title' => $villaName,
					'post_date' => date( 'Y-m-d H:i:s' ),
					'post_content' => $villaDescription,
					'post_status' => 'publish',
					'post_type' => 'mphb_room_type',
				);
				$villaId = wp_insert_post($postarr);
				
				if($villaId) {
					update_post_meta( $villaId, 'villa_code', (string)$villaCode);
					update_post_meta( $villaId, 'mphb_adults_capacity', (string)$maxAdults );
					update_post_meta( $villaId, 'mphb_gallery_acc', (string)$gallery );
					update_post_meta( $villaId, 'villa_type_api','api');
					update_post_meta( $villaId, 'bedrrommphb_room_type',$noofbed);
					
					$image_id = array();
					foreach ($gall as $images) {

						$image_id[]= customuploadRemoteImageAndAttach($images);
					}
					update_post_meta( $villaId, 'mphb_gallery',implode( ',', $image_id ));


					// update_post_meta( $villaId, 'mphb_gallery', (string)$gallery );
					// var_dump($gallery);
					// wp_set_post_terms($villaId ,$variii,'mphb_room_type_facility',true);

					uploadRemoteImageAndSetFeaturedImage($largeImage,$villaId);
					wp_set_object_terms( $villaId ,$villa_ameintities ,'mphb_room_type_facility',true);
				}
			} else {
				continue;
				foreach ($villapages as $key => $villapage) {
					$villaUniqueCode = get_post_meta( $villapage->ID, 'villa_code', true );
					if($villaUniqueCode == $villaCode) {
						$postarr = array(
							'ID' => $villapage->ID,
							'post_title' => $villaName,
							'post_date' => date( 'Y-m-d H:i:s' ),
						    // 'post_content' => $villaDescription,
							'post_status' => 'publish',
							'post_type' => 'mphb_room_type',
						);
						wp_update_post($postarr);
						update_post_meta( $villapage->ID, 'villa_type_api','api');
						update_post_meta( $villapage->ID, 'villa_code', 			(string)$villaCode);
						update_post_meta( $villapage->ID, 'mphb_adults_capacity', 	(string)$maxAdults );
						update_post_meta( $villapage->ID, 'mphb_gallery_acc', 	(string)$gallery );
						update_post_meta(  $villapage->ID, 'bedrrommphb_room_type',$noofbed);
					/*$image_id = array();
					foreach ($gall as $images) {

					$image_id[]= customuploadRemoteImageAndAttach($images);

				}*/
									// update_post_meta( $villapage->Id, 'mphb_gallery', implode( ',', $image_id )  );
					 // wp_set_post_terms($villapage->ID ,$variii,'mphb_room_type_facility',true);

				uploadRemoteImageAndSetFeaturedImage($largeImage,$villapage->ID);
				wp_set_object_terms($villapage->ID ,$villa_ameintities,'mphb_room_type_facility',true);

			}

		}
	}
	echo '12';
}
echo 1;
die();
}


/********** Search API integration and updating Villas from API *********/
/********** Availability in details page (api) *********/

add_action('wp_ajax_searchvilla_detail', 'searchvilla_detail');
add_action('wp_ajax_nopriv_searchvilla_detail', 'searchvilla_detail');

function searchvilla_detail(){

	$apiUrl = get_option('api_endpoint');
	$username = get_option('api_username');
	$password = get_option('api_password');
	$authorization = 'Basic '. base64_encode($username.":".$password);
	$feedUrl = $apiUrl.'/availability/LUXURYKEYS';
	$ch = curl_init();
	$headers = array(
		'Content-Type: application/x-www-form-urlencoded',
		'charset: utf-8',
		'Authorization:'. $authorization,
	);
	curl_setopt($ch, CURLOPT_URL, $feedUrl."?checkin=".$_POST['checkin']."&checkout=".$_POST['checkout']."&room=".$_POST['room']);
	curl_setopt($ch, CURLOPT_HEADER, true);
	curl_setopt($ch, CURLOPT_VERBOSE, 1);
	curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch, CURLOPT_POST, 1);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	$output = curl_exec($ch);
	$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	$body = substr($output, $header_size);
	$villaXmlData = new SimpleXMLElement($body);
	curl_close($ch);
	$villaData = $villaXmlData->rates;
	$villaXmlData=xml2array($villaXmlData);
	$villa_rates=$villaXmlData['data']['rates']['rate'];
	foreach ($villa_rates as $villa_rate) {
		$villa_rate=xml2array($villa_rate);
		$villapricerange[$villa_rate['type']][]=$villa_rate['pricing']['stay'];
		$vr[]=$villa_rate['type'];
	}
	if($vr[0]==$_POST['room']){
    	// echo 1;
		echo min($villapricerange[$villa_rate['type']]);
	}else{
		echo -1;
	}
	die();


}

/********** Availability in details page *********/

/********** Availability in details page (manual) *********/

add_action('wp_ajax_searchvilla_detail_manual', 'searchvilla_detail_manual');
add_action('wp_ajax_nopriv_searchvilla_detail_manual', 'searchvilla_detail_manual');

function searchvilla_detail_manual(){
	if(!empty($_POST['checkin'])){
		$checkin   = $_POST['checkin'];
		$checkout   = $_POST['checkout'];
		$roomsAtts = array(
			'availability'	 => 'locked',
			'from_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkin ),
			'to_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkout )
		);
		$result =  getAvailableRoomTypes_custom($roomsAtts);
// echo "<pre>";
 // print_r($result);
		foreach ($result as $value) {
			$manualAvailable[]=$value['id'];
		}
	}
	if(in_array($_POST['room'],$manualAvailable,TRUE)){
		echo 1;
	}else{
		echo -1;
	}
	die();


}

/********** Availability in details page (manual)*********/

	/*add_action('wp_ajax_searchvilla', 'searchvilla');
	add_action('wp_ajax_nopriv_searchvilla', 'searchvilla');
	function searchvilla(){
		$villargs = array(
		    'numberposts' =>-1,
		    'post_type' => 'villas',
		    'orderby' => 'menu_order',
		    'order' => 'ASC',
		    'post_status'=>"publish"
		);
		$villapages = get_posts($villargs);
		$allCodes = array();
		foreach ($villapages as $key => $villapage) {
			$villaUniqueCode = get_post_meta( $villapage->ID, 'villa_code', true );
			$allCodes[] = $villaUniqueCode;
		}
		$destination = $_POST['destination'];
		$purpose = $_POST['purpose'];
		$checkin = $_POST['checkin'];
		$checkout = $_POST['checkout'];
		$bedrooms = $_POST['bedrooms'];
		$price = $_POST['price'];
		$apiUrl = get_option('api_endpoint');
		$username = get_option('api_username');
		$password = get_option('api_password');
		$authorization = 'Basic '. base64_encode($username.":".$password);
		$feedUrl = $apiUrl.'/availability/LUXURYKEYS?checkin=2019-05-01&checkout=2019-05-14';
		$ch = curl_init();
	    $headers = array(
			'Content-Type: application/x-www-form-urlencoded',
			'charset: utf-8',
			'Authorization:'. $authorization,
	    );
	    curl_setopt($ch, CURLOPT_URL, $feedUrl);
	    curl_setopt($ch, CURLOPT_HEADER, true);
	    curl_setopt($ch, CURLOPT_VERBOSE, 1);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($ch, CURLOPT_POST, 1);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
	    $output = curl_exec($ch);
	    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
	    $body = substr($output, $header_size);
	    $villaXmlData = new SimpleXMLElement($body);
	    curl_close($ch);
	    $villaData = $villaXmlData->data->rooms->room;
	    
	    foreach ($villaData as $key => $villa) {
	    	$villaCode 			= $villa->code;
			$villaName 			= $villa->name;
			$villaDescription 	= $villa->description;
			$minPersons 		= $villa->capacity->min_pers;
			$maxPersons 		= $villa->capacity->max_pers;
			$maxAdults 			= $villa->capacity->max_adults;
			$isChildrenAllowed 	= $villa->capacity->children_allowed;
			$photos = $villa->photos->photo;
			foreach ($photos as $photo) {
				$xsmallImage = $photo->xsmall;
				$smallImage = $photo->small;
				$mediumImage = $photo->medium;
				$largeImage = $photo->large;
			}
	    	if(!in_array($villaCode, $allCodes)) {
	    		$postarr = array(
				    'post_title' => $villaName,
				    'post_date' => date( 'Y-m-d H:i:s' ),
				    'post_content' => $villaDescription,
				    'post_status' => 'publish',
				    'post_type' => 'villas',
				);
				$villaId = wp_insert_post($postarr);
				
				if($villaId) {
					update_post_meta( $villaId, 'villa_code', (string)$villaCode);
					update_post_meta( $villaId, 'villa_purpose', (string)$purpose );
					update_post_meta( $villaId, 'villa_location', (string)$destination );
					update_post_meta( $villaId, 'villa_bed', (string)$bedrooms );
				}
	    	} else {
	    		foreach ($villapages as $key => $villapage) {
	    			$villaUniqueCode = get_post_meta( $villapage->ID, 'villa_code', true );
	    			if($villaUniqueCode == $villaCode) {
	    				$postarr = array(
			    			'ID' => $villapage->ID,
						    'post_title' => $villaName,
						    'post_date' => date( 'Y-m-d H:i:s' ),
						    'post_content' => $villaDescription,
						    'post_status' => 'publish',
						    'post_type' => 'villas',
						);
						wp_update_post($postarr);
						
						update_post_meta( $villapage->ID, 'villa_code', (string)$villaCode);
						update_post_meta( $villapage->ID, 'villa_purpose', (string)$purpose );
						update_post_meta( $villapage->ID, 'villa_location', (string)$destination );
						update_post_meta( $villapage->ID, 'villa_bed', (string)$bedrooms );
	    			}
		    		
				}
	    	}
	    }
	    echo 1;
	    die();
	}*/

	function uploadRemoteImageAndSetFeaturedImage($image_url, $parent_id){
		$image = $image_url;
		$get = wp_remote_get( $image );
		$type = wp_remote_retrieve_header( $get, 'content-type' );
		if (!$type)
			return false;
		$mirror = wp_upload_bits( basename( $image ), '', wp_remote_retrieve_body( $get ) );
		$attachment = array(
			'post_title'=> basename( $image ),
			'post_mime_type' => $type
		);
		$attach_id = wp_insert_attachment( $attachment, $mirror['file'], $parent_id );
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		$attach_data = wp_generate_attachment_metadata( $attach_id, $mirror['file'] );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		set_post_thumbnail( $parent_id, $attach_id );
		return $attach_id;
	}

	function uploadRemoteImageAndAttach($image_url, $parent_id){
		$image = $image_url;
		$get = wp_remote_get( $image );
		$type = wp_remote_retrieve_header( $get, 'content-type' );
		if (!$type)
			return false;
		$mirror = wp_upload_bits( basename( $image ), '', wp_remote_retrieve_body( $get ) );
		$attachment = array(
			'post_title'=> basename( $image ),
			'post_mime_type' => $type
		);
		$attach_id = wp_insert_attachment( $attachment, $mirror['file'], $parent_id );
		require_once(ABSPATH . 'wp-admin/includes/image.php');
		$attach_data = wp_generate_attachment_metadata( $attach_id, $mirror['file'] );
		wp_update_attachment_metadata( $attach_id, $attach_data );
		$imageUrl = wp_get_attachment_url($attach_id);
		return $attach_id;
	}
	function customuploadRemoteImageAndAttach($image_url){
		$image = $image_url;
		$get = wp_remote_get( $image );
		$type = wp_remote_retrieve_header( $get, 'content-type' );
		if (!$type)
			return false;
		$mirror = wp_upload_bits( basename( $image ), '', wp_remote_retrieve_body( $get ) );
		$attachment = array(
			'post_title'=> basename( $image ),
			'post_mime_type' => $type
		);
		$attach_id = wp_insert_attachment( $attachment, $mirror['file'], $parent_id );
		return $attach_id ;
	}
	/*
	* Image upload to corresponding folder to specific media 
	*/
// 	add_filter( 'wp_handle_upload_prefilter', 'my_pre_upload' );
// 	function my_pre_upload( $file ) {
// 	    add_filter( 'upload_dir', 'my_custom_upload_dir' );
// 	    return $file;
// 	}

// 	function my_custom_upload_dir( $param ) {
// 	    $id = $_REQUEST['post_id'];
// 	    $post_inf  = get_post( $id ); 
// 	    $parent = $post_inf->post_parent;
// 	    $foldname_gall = $post_inf->post_name;
// 	    if( "mphb_room_type" == get_post_type( $id ) || "mphb_room_type" == get_post_type( $parent ) ) {
// 	        $mydir         = $foldname_gall;
// 	        $param['path'] = $param['basedir'] .'/'. $mydir;
// 	        $param['url']  = $param['baseurl'] .'/'. $mydir;
// 	    }
// 	    return $param;
// 	}
	/*function customuploadRemoteImageAndAttach($image_url){
		$image = $image_url;
		$get = wp_remote_get( $image );
		$type = wp_remote_retrieve_header( $get, 'content-type' );
		if (!$type)
			return false;
		$mirror = wp_upload_bits( basename( $image ), '', wp_remote_retrieve_body( $get ) );
		$attachment = array(
			'post_title'=> basename( $image ),
			'post_mime_type' => $type
		);
		$attach_id = wp_insert_attachment( $attachment, $mirror['file'], $parent_id );
		return $attach_id ;
	}
*/
	add_filter('manage_mphb_room_type_posts_columns','filter_cpt_columns');

	function filter_cpt_columns( $columns ) {
	    // this will add the column to the end of the array
	    $columns['villa_type'] = 'Villa Type';
	    //add more columns as needed

	    // as with all filters, we need to return the passed content/variable
	    return $columns;
	}
	add_action( 'manage_posts_custom_column','action_custom_columns_content', 10, 2 );
	function action_custom_columns_content ( $column_id, $post_id ) {
	    //run a switch statement for all of the custom columns created
	    switch( $column_id ) { 
	        case 'villa_type':
	            echo ($value = get_post_meta($post_id, 'villa_type_api', true ) ) ? $value : 'No First Name Given';
	        break;

	        //add more items here as needed, just make sure to use the column_id in the filter for each new item.

	   }
	}
	//Sorting columns 
	// make columns sortable
    add_filter( 'manage_edit-mphb_room_type_sortable_columns', 'my_set_sortable_columns' );
    function my_set_sortable_columns( $columns )
	{
	    $columns['villa_type'] = 'villa_type';
	    return $columns;
	}

	add_action( 'pre_get_posts', 'my_sort_custom_column_query' );
	function my_sort_custom_column_query( $query )
	{
	    $orderby = $query->get( 'orderby' );

	    if ( 'villa_type' == $orderby ) {

	        $meta_query = array(
	            'relation' => 'OR',
	            array(
	                'key' => 'villa_type_api',
	                'compare' => 'NOT EXISTS', // see note above
	            ),
	            array(
	                'key' => 'villa_type_api',
	            ),
	        );

	        $query->set( 'meta_query', $meta_query );
	        $query->set( 'orderby', 'meta_value' );
	    }
	}
	add_action( 'restrict_manage_posts', 'wpse45436_admin_posts_filter_restrict_manage_posts' );
	/**
	 * First create the dropdown
	 * make sure to change POST_TYPE to the name of your custom post type
	 * 
	 * 
	 */
	function wpse45436_admin_posts_filter_restrict_manage_posts(){
	    $type = 'mphb_room_type';
	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }

	    //only add filter to post type you want
	    if ('mphb_room_type' == $type){
	        //change this to the list of values you want to show
	        //in 'label' => 'value' format
	        $values = array(
	            'No online availability' => 'manual', 
	            'WebHotelier villas' => 'api',
	            'Other Channels' => 'api_manu',
	            'Private' => 'private',
	        );
	        ?>
	        <select name="ADMIN_FILTER_FIELD_VALUE">
	        <option value=""><?php _e('Filter By ', 'wose45436'); ?></option>
	        <?php
	            $current_v = isset($_GET['ADMIN_FILTER_FIELD_VALUE'])? $_GET['ADMIN_FILTER_FIELD_VALUE']:'';
	            foreach ($values as $label => $value) {
	                printf
	                    (
	                        '<option value="%s"%s>%s</option>',
	                        $value,
	                        $value == $current_v? ' selected="selected"':'',
	                        $label
	                    );
	                }
	        ?>
	        </select>
	        <?php
	    }
	}

	function wpse45436_posts_filter( $query ){
	    global $pagenow;
	    $type = 'mphb_room_type';
	    if (isset($_GET['post_type'])) {
	        $type = $_GET['post_type'];
	    }
	    if ( 'POST_TYPE' == $type && is_admin() && $pagenow=='edit.php' && isset($_GET['ADMIN_FILTER_FIELD_VALUE']) && $_GET['ADMIN_FILTER_FIELD_VALUE'] != '') {
	        $query->query_vars['meta_key'] = 'villa_type_api';
	        $query->query_vars['meta_value'] = $_GET['ADMIN_FILTER_FIELD_VALUE'];
	    }
	}
	function auto_login_new_user( $user_id ) {
		wp_set_current_user($user_id);
		wp_set_auth_cookie($user_id);
		$user = get_user_by( 'id', $user_id );
		do_action( 'wp_login', $user->user_login );//`[Codex Ref.][1]
		echo '1';
	    // wp_redirect( home_url().'/my-account/' ); // You can change home_url() to the specific URL,such as "wp_redirect( 'http://www.wpcoke.com' )";
	    exit;
	}
	 add_action( 'user_register', 'auto_login_new_user' );

	add_action('show_user_profile', 'my_user_profile_edit_action');
	add_action('edit_user_profile', 'my_user_profile_edit_action');
	function my_user_profile_edit_action($user) {
		$phoneNumber = get_user_meta($user->ID, 'MobileNo', true);
		?>
		<table class="form-table">
			<tbody>
				<tr class="user-phone_number-wrap">
					<th><label for="MobileNo">Phone number</label></th>
					<td><input type="text" name="MobileNo" id="MobileNo" value="<?php echo $phoneNumber; ?>" class="regular-text code"></td>
				</tr>
			</tbody>
		</table>
		<?php 
	}
	add_action('personal_options_update', 'my_user_profile_update_action');
	add_action('edit_user_profile_update', 'my_user_profile_update_action');
	function my_user_profile_update_action($user_id) {
		update_user_meta($user_id, 'MobileNo', $_POST['phone_number']);
	}
	add_action('wp_ajax_custom_login', 'custom_login');
	add_action('wp_ajax_nopriv_custom_login', 'custom_login');	
	function custom_login() {
		if(!is_user_logged_in()) {
	    $creds = array();
	    $userEmail = get_user_by('email', $_POST['username']);
	    $creds['user_login'] = $userEmail->user_login;
	    $creds['user_password'] = $_POST['password'];
	    $creds['remember'] = true;
	    $user = wp_signon( $creds, false );
	    if ( is_wp_error($user) ) { 
			echo $user->get_error_message();
	    	//echo '<div class="error-message">'.$user->get_error_message().'</div>';
	    } else {
			echo '1~'.get_permalink($post->ID);

	    	///wp_redirect( home_url().'/my-account/' );
	    	exit;	
	    }
	}
	exit();
	}
	add_action('wp_ajax_login', 'login');
	add_action('wp_ajax_nopriv_login', 'login');
	function login(){
		if(!is_admin()) {
			$creds = array();
			$userEmail = get_user_by('email', $_POST['username']);
			$creds['user_login'] = $userEmail->user_login;
			$creds['user_password'] = $_POST['password'];
			$creds['remember'] = true;
			$user = wp_signon( $creds, false );
			if ( is_wp_error($user) ) { 
				echo 2;
			} else {
				echo 1;
			}
		}
		die();
	}
	add_action('wp_ajax_filter_loc', 'loc_sort');
	add_action('wp_ajax_nopriv_filter_loc', 'loc_sort');
	function loc_sort(){
		$apiUrl = get_option('api_endpoint');
		$username = get_option('api_username');
		$password = get_option('api_password');
		$villa_destination		=	$_POST['destination'];
		$purpose				=	$_POST['villa-purpose'];
		$checkin				=	$_POST['checkin'];
		$checkout				=	$_POST['checkout'];
		$min_price 				=	$_POST['min_amt'];
		$max_price 				=	$_POST['max_amt']; 
		$authorization = 'Basic '. base64_encode($username.":".$password);
		$feedUrl = $apiUrl.'/availability/LUXURYKEYS?checkin='.$checkin.'&checkout='.$checkout;
		$checkin1=date_create($checkin);
		$checkout1 = date_create($checkout);
		$diff=date_diff($checkin1,$checkout1);
		$totaldays=$diff->days;
		$ch = curl_init();
		$headers = array(
			'Content-Type: application/x-www-form-urlencoded',
			'charset: utf-8',
			'Authorization:'. $authorization,
		);
		curl_setopt($ch, CURLOPT_URL, $feedUrl);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		$output = curl_exec($ch);
		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($output, $header_size);
		$villaXmlData = new SimpleXMLElement($body);

		curl_close($ch);
		$villaData = $villaXmlData->rates;
		$villaXmlData=xml2array($villaXmlData);
		$villa_rates=$villaXmlData['data']['rates']['rate'];

		foreach ($villa_rates as  $villa_rate) {
			$villa_rate=xml2array($villa_rate);

			$title = $villa_rate['room']; 
			$tarr= explode('|', $title);
			$place = $tarr[2];
			$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);

			$villapricerange[$villa_rate['type']][]=$villa_rate['pricing']['stay'];

			if($villa_rate['pricing']['stay']/$totaldays >= $min_price && $villa_rate['pricing']['stay']/$totaldays <= $max_price ){
				if($place==$villa_destination){	
					if($int>=$numbeds)
					{
						$vr[]=$villa_rate['type'];
					}
				}	

			}

		}
		if (!empty($vr)) {
			$villargs = array(
				'meta_key' => 'villa_code',
				'meta_value' => $vr,
				'post_type' => 'mphb_room_type',
				'post_status' => 'published',
				'posts_per_page' => -1
			);
		}else {
			$villargs = array(
				'post_type' => 'mphb_room_type',
				'post_status' => 'published',
				'posts_per_page' => -1
			);
		}

		$villapages = get_posts($villargs);

		foreach($villapages as $villapage) { 

			$title = $villapage->post_title; 
			$tarr=array();
			$tarr= explode('|', $title);
			if($tarr[2]!=$villa_destination){
				continue;
			}
			$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
									// if($tarr[2]==$villa_destination){
			$villa_bath   = get_post_meta( $villapage->ID, '_villa_bath', true );
			$villa_pool  = get_post_meta( $villapage->ID, 'villa_pool', true );
			$villa_guest  = get_post_meta( $villapage->ID, 'mphb_adults_capacity', true );
			$villa_address  = get_post_meta( $villapage->ID, 'villa_address', true );
			$villa_code  = get_post_meta( $villapage->ID, 'villa_code', true );
			$villa_bed  = explode('|', $villapage->post_title);
			$destination  = get_post_meta( $villapage->ID, 'villa_location', true );
			$purpose = get_post_meta( $villapage->ID, 'villa_purpose', true );
			$featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID));
			$amn_villa = wp_get_object_terms( $villapage->ID, "mphb_room_type_facility");
			?>

			<div class="col-12 listItem">
				<div class="xlist-item">
					<img src="<?php echo get_the_post_thumbnail_url($villapage->ID,'full'); ?>" alt="">
					<div class="xlist-item-feature">
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bed.svg" alt="images">
							<p><?php echo $int; ?></p>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/img/person.svg" alt="images">
							<p><?php echo $villa_guest; ?></p>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/bath.svg" alt="images">
							<p><?php echo $villa_bath; ?></p>
						</div>
						<div>
							<img src="<?php echo get_bloginfo('template_url'); ?>/img/svg/pool.svg" alt="images">
							<p><?php echo $villa_pool; ?></p>
						</div>
					</div>
					<div class="xlist-item-desc">
						<div>
							<!-- <h4>Villa <strong> Cottage </strong></h4> -->
							<h4><?php echo $tarr[1]; ?>, <?php echo $tarr[2]; ?></h4>
							<?php if(!empty($villa_rates) && min($villapricerange[$villa_code])/$totaldays!=0){ ?>
								<p>From: €<?php echo min($villapricerange[$villa_code])/$totaldays; ?> / per night</p>
							<?php } ?>
						</div>
						<div class="button button-primary button-small">
							<a href="<?php echo get_permalink($villapage->ID); ?>" tabindex="0">view villa</a>
						</div>
					</div>
				</div>
			</div>

			<?php 

		}
		exit();
	}
function get_post_by_title($page_title, $output = OBJECT) {
    global $wpdb;
        $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type='destination'", $page_title ));
        if ( $post )
            return get_post($post, $output);

    return null;
}

	function getAvailableRoomTypes_custom($roomsAtts){
		global $wpdb;

		$lockedRooms	 = MPHB()->getRoomPersistence()->searchRooms( $roomsAtts );
		$lockedRoomsStr	 = join( ',', $lockedRooms );

		$query = "SELECT DISTINCT room_types.ID AS id, COUNT(rooms.ID) AS count "
		. "FROM $wpdb->posts AS rooms ";

		$join = "INNER JOIN $wpdb->postmeta AS room_meta_room_type_id "
		. "		ON ( rooms.ID = room_meta_room_type_id.post_id  ) "
		. "INNER JOIN $wpdb->posts AS room_types "
		. "		ON ( room_meta_room_type_id.meta_value = room_types.ID ) ";

		$where = "WHERE 1=1 "
		. "AND rooms.post_type = '" . MPHB()->postTypes()->room()->getPostType() . "' "
		. (!empty( $lockedRoomsStr ) ? "AND rooms.ID NOT IN ( $lockedRoomsStr ) " : "" )
		. "AND rooms.post_status = 'publish' "
		. "AND room_meta_room_type_id.meta_key = 'mphb_room_type_id' "
		. "AND room_meta_room_type_id.meta_value IS NOT NULL "
		. "AND room_meta_room_type_id.meta_value <> '' "
		. "AND room_types.post_status = 'publish' "
		. "AND room_types.post_type = '" . MPHB()->postTypes()->roomType()->getPostType() . "' ";

		$order = "GROUP BY room_meta_room_type_id.meta_value "
		. "DESC";



		$roomTypeDetails = $wpdb->get_results( $query . $join . $where . $order, ARRAY_A );
// print_r($roomTypeDetails);
// exit();
		return $roomTypeDetails;
	}

	add_action('wp_ajax_filter_villa', 'filter_ajax');
	add_action('wp_ajax_nopriv_filter_villa', 'filter_ajax');
	function filter_ajax(){
		/*
		$package,$villatype='',$eventtype='',$mykanoslocation=''
		$route,$location,$checkin,$checkout,$numerofbeds,$minprice,$maxprice,$flexible,$package,$villatype='',$eventtype='',$villalocality=''	
		*/
		$apiUrl = get_option('api_endpoint');
		$username = get_option('api_username');
		$password = get_option('api_password');
		$route = '';
		$location = $_POST['vill_dest'];
		$checkin = $_POST['vill_chk_in'];
		$checkout = $_POST['vill_chk_out'];
		$numerofbeds = $_POST['vill_bed'];
		$minprice = $_POST['vill_prc_min'];
		$maxprice = $_POST['vill_prc_max'];
		$prc_typ = $_POST['vill_prc_typ'];
		// $flexible = $_POST['destination'];
		$flexible = $_POST['villaXmlData_flex'];
		$package = '';
		$villastyle=$_POST['villa_style'];
		$eventtype=$_POST['villa_occasion']; 
		$villalocality=$_POST['villa_locality'];
		$checkin1=date_create($checkin);
		
		$checkin_flex=date_sub($checkin1,date_interval_create_from_date_string( $flexible."days"));
		$checkin_flex=	date_format($checkin_flex,"Y-m-d");
		
		/*	echo $checkin;
		echo "<br>";
		echo date_format($cheee,"Y-m-d");
		echo "<br>";*/
		$checkout1 = date_create($checkout);
		$checkout_flex=date_add($checkout1,date_interval_create_from_date_string( $flexible."days"));
		$checkout_flex=	date_format($checkout_flex,"Y-m-d");
/*echo $checkin.'--->	'.$checkin_flex;
echo $checkout.'--->	'.$checkout_flex.'<br>';*/
$diff=date_diff(date_create($checkin),date_create($checkout));
$totaldays=$diff->days;

$roomsAtts = array(
	'availability'	 => 'locked',
	'from_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkin ),
	'to_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkout )
);
$result =  getAvailableRoomTypes_custom($roomsAtts);

foreach ($result as $value) {
		// $available[]=$lcg_value(oid)['id'];
 	// var_dump(MPHB()->getRoomTypeRepository()->findById($value['id'])->getDefaultPrice());
	$manual_price=MPHB()->getRoomTypeRepository()->findById($value['id'])->getDefaultPrice();
	if ($manual_price>=$minprice && $manual_price <= $maxprice ) {
		$available[$value['id']]=$manual_price;
	}
}

$authorization = 'Basic '. base64_encode($username.":".$password);
$feedUrl = $apiUrl.'/availability/LUXURYKEYS?checkin='.$checkin.'&checkout='.$checkout;
$ch = curl_init();
$headers = array(
	'Content-Type: application/x-www-form-urlencoded',
	'charset: utf-8',
	'Authorization:'. $authorization,
);
curl_setopt($ch, CURLOPT_URL, $feedUrl);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
$output = curl_exec($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$body = substr($output, $header_size);
$villaXmlData = new SimpleXMLElement($body);

/*echo "<pre>";
print_r($villaXmlData);
echo "<br>";
echo "<br>";
echo "<br>";
echo "<***************************************************************************************************>";
exit;*/
curl_close($ch);




$villaData = $villaXmlData->rates;
$villaXmlData=xml2array($villaXmlData);

$villa_rates=$villaXmlData['data']['rates']['rate'];

if($minprice!='' && $maxprice!=''){
	foreach ($villa_rates as  $villa_rate) {

	$villargs = array(
		'meta_key' => 'villa_code',
		'meta_value' => (string)$villa_rate->type,
		'post_type' => 'mphb_room_type',
		'post_status' => 'published',
		'posts_per_page' => -1
	);
	$villapages = get_posts($villargs);	
		/*echo $villapages[0]->ID;
		echo "<br>";*/
		

		if($minprice <= $villa_rate->pricing->stay/$totaldays  && $villa_rate->pricing->stay/$totaldays <=$maxprice  ){
		// if(1){

			$villapricerange[$villapages[0]->ID][]=(float)$villa_rate->pricing->stay/$totaldays;

		}
	}	
}else{
	foreach ($villa_rates as  $villa_rate) {

	$villargs = array(
		'meta_key' => 'villa_code',
		'meta_value' => (string)$villa_rate->type,
		'post_type' => 'mphb_room_type',
		'post_status' => 'published',
		'posts_per_page' => -1
	);
	$villapages = get_posts($villargs);	
		/*echo $villapages[0]->ID;
		echo "<br>";*/
		

		// if($minprice <= $villa_rate->pricing->stay/$totaldays  && $villa_rate->pricing->stay/$totaldays <=$maxprice  ){
		// if(1){

			$villapricerange[$villapages[0]->ID][]=(float)$villa_rate->pricing->stay/$totaldays;

		// }
	}
}


/*       min($villapricerange[$villapages[0]->ID])   */


/*flexible date start */
if ($flexible!=0) {

	$feedUrl_flex = $apiUrl.'/availability/LUXURYKEYS/flexible-calendar?startDate='.$checkin_flex.'&endDate='.$checkout_flex.'&nights='.$totaldays;
	// echo $feedUrl_flex;
	$ch_flex = curl_init();
	$headers = array(
		'Content-Type: application/x-www-form-urlencoded',
		'charset: utf-8',
		'Authorization:'. $authorization,
	);
	curl_setopt($ch_flex, CURLOPT_URL, $feedUrl_flex);
	curl_setopt($ch_flex, CURLOPT_HEADER, true);
	curl_setopt($ch_flex, CURLOPT_VERBOSE, 1);
	curl_setopt($ch_flex, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch_flex, CURLOPT_POST, 1);
	curl_setopt($ch_flex, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch_flex, CURLOPT_POSTFIELDS, $params);
	$output_flex = curl_exec($ch_flex);
	$header_size_flex = curl_getinfo($ch_flex, CURLINFO_HEADER_SIZE);
	$body_flex = substr($output_flex, $header_size_flex);
	$villaXmlData_flex = new SimpleXMLElement($body_flex);

	curl_close($ch_flex);
	$villaXmlData_flex=xml2array($villaXmlData_flex);
	$flex_avail=$villaXmlData_flex['data']['days']['day'];
	/*
	echo "<pre>";
	print_r($flex_avail);
	exit();*/
	foreach ($flex_avail  as  $flex_avai) {
		$villargs = array(
			'meta_key' => 'villa_code',
			'meta_value' => (string)$flex_avai['rm_type'],
			'post_type' => 'mphb_room_type',
			'post_status' => 'published',
			'posts_per_page' => -1
		);
		$villapages = get_posts($villargs);	
		if((float)$flex_avai->price/$totaldays >=$minprice && (float)$flex_avai->price/$totaldays <=$maxprice  ){
			// if(1){
			$villapricerange[$villapages[0]->ID][]=(float)$flex_avai->price/$totaldays;
		}

	}

}

/*flexible date ends*/

$available=array_filter(array_unique($available));

foreach ($villapricerange as $key => $villapricerang) {
	// echo $key;	
	$available[$key]= min($villapricerange[$key]);
}


foreach ($available as $id => $availabl) {
/*echo get_post_meta($id,'villa_locations')[0];
echo "<br>";*/
	// if(get_post_meta($id,'bedrrommphb_room_type')[0]>=$numerofbeds && get_post_meta($id,'villa_locations')[0]==$location ){
	if(get_post_meta($id,'bedrrommphb_room_type')[0]>=$numerofbeds){
		$available1[$id]=$availabl;
	}
}
$avble = $numerofbeds==''?$available:$available1;
$villa_cnt = 0;

if(($checkin!='' && $checkout!='') ){
			foreach ($avble as $avail_key => $avail_villa) {
	$title = get_the_title($avail_key); 
	$tarr=array();
	$tarr= explode('|', $title);
	$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
	$villastyle =$_POST['villa_style'];
	$eventtype = $_POST['villa_occasion']; 
	$villalocality = $_POST['villa_locality'];
	  $pstatus = get_page( $avail_key );
	  if ($pstatus->post_status != 'publish') {
	    // page is published
	    continue;

	  }
	if($villastyle!='' || $eventtype!='' || $villalocality!=''){
		$villa_locs = get_the_terms($avail_key, 'mphb_room_type_loc');
		$villa_style = get_the_terms($avail_key, 'mphb_room_type_style');
		$villa_occs = get_the_terms($avail_key, 'mphb_room_type_occ');
		if(!$villa_locs){
			continue;
		}else{
			foreach ($villa_locs as $key => $villa_loc) {
				if(in_array($villa_loc->slug, $villalocality)){
					continue;
				}
			}
		}
		if(!$villa_style){
			continue;
		}else{
			foreach ($villa_style as $key => $villa_style) {
				if(in_array($villa_style->slug, $villalocality)){
					continue;
				}
			}
		}
		if(!$villa_occs){
			continue;
		}else{
			foreach ($villa_occs as $key => $villa_occ) {
				if(in_array($villa_occ->slug, $villalocality)){
					continue;
				}
			}
		}
		/*if(!$villa_style){
			continue;
		}
		if(!$villa_occs){
			continue;
		}*/
		// var_dump(array_intersect($villalocality, $villa_locs));
		/*if(count(array_intersect($villalocality, $villa_locs))==0){
			continue;
		}*/

	}


	

	$villa_bath   = get_post_meta( $avail_key, 'bathroommphb_room_type', true );
	$villa_pool  = get_post_meta( $avail_key, 'villa_pool', true );
	$villa_guest  = get_post_meta( $avail_key, 'mphb_adults_capacity', true );
	$villa_address  = get_post_meta( $avail_key, 'villa_address', true );
	$villa_code  = get_post_meta( $avail_key, 'villa_code', true );
	$villa_bed  = get_post_meta( $avail_key, 'bedrrommphb_room_type', true );
	$destination  = get_post_meta( $avail_key, 'villa_locations', true );
	$purpose = get_post_meta( $avail_key, 'villa_purpose', true );
	$featImage = wp_get_attachment_url(get_post_thumbnail_id($avail_key));
	$villa_typeapi=get_post_meta( $avail_key, 'villa_type_api', true );
	$amn_villa = wp_get_object_terms( $avail_key, "mphb_room_type_facility");
	$gallerymeta  = get_post_meta( $avail_key, 'mphb_gallery', true );
	$galleryimgs = explode(',', $gallerymeta);
	$galleryimgs = array_filter($galleryimgs);
	if($villa_typeapi=='api' || $villa_typeapi=='api_manu'){

	$villa_prc = number_format($avail_villa, 2 );
	}else{
	$roomType = MPHB()->getRoomTypeRepository()->findById($avail_key) ;
	$villa_prc = number_format($roomType->getDefaultPrice(),2);
	}
	if($villa_typeapi=='private'){
		continue;
	}
	if($_POST!=''){
	if($checkin!='' && $checkout!='')
	{
	$villa_url .= get_permalink($avail_key).'?checkin='.$checkin.'&checkout='.$checkout;
	}
	if($villa_bed != ''){
	$villa_url .= '&beds='.$villa_bed;	
	}
	if($villa_guest != ''){
	$villa_url .= '&guests='.$villa_guest;	
	}
	if($villa_guest != ''){
	$villa_url .= '&price_typ='.$prc_typ;	
	}
	if($villa_prc != ''){
	$villa_url .= '&villa_pc='.$avail_villa;	
	}
	}else{
	$villa_url = get_permalink($avail_key);
	}
	$villa_type_api = get_post_meta( $avail_key, 'villa_type_api', true );
	if($villa_type_api=='private'){
		continue;
	}

	if($destination != $_POST['vill_dest']){
		continue;
	}
	?>
	<div class="col-4 listItem villadata">
					<form method="post" class="testlll" target="_blank" enctype="multipart/form-data" tar action="<?php echo get_permalink($avail_key); ?>">
        			<input type='hidden' name="villa_prc" value="<?php echo $avail_villa; ?>" id="villa_prc" >
					<input type='hidden' name="beds" value="<?php echo $villa_bed; ?>" id="beds" >
					<input type='hidden' name="guests" value="<?php echo $villa_guest; ?>" id="guests" >
					<input type='hidden' name="prc_typ" value="<?php echo $prc_typ; ?>" id="prc_typ" >
					<input type='hidden' name="checkin" value="<?php echo $checkin; ?>" id="checkin" >
					<input type='hidden' name="checkout" value="<?php echo $checkout; ?>" id="checkout" >
			        <div class="feature-item">
						<div class="feature-item-slider">
							<?php 
							$i == 0;
							foreach ($galleryimgs as $key => $galleryimg) {
								
								?>

							<div class="<?php echo $i; ?>">
			        			<img src="<?php echo wp_get_attachment_image_url($galleryimg,'full'); ?>" alt="" />
			        		</div>
			        		<?php 
							}
							
							?>
						</div>
						<div class="feature-item-content">
							<h5>
								<?php
									if($villa_bed != ''){
										echo $villa_bed.' BEDROOMS ';
									}
									if($villa_guest != ''){
										echo $villa_guest.' GUESTS ';
									}
									if($villa_bath != ''){
										echo $villa_bath.' BATHS ';
									}
								  ?>
							</h5>
							<h3><?php echo $title; ?></h3>
										<p>
											From: €<?php echo $villa_prc; ?> / per night
										</p>
								
							<p><a href="#!"><?php echo $villa_address; ?></a></p>
						</div>
						
						<div class="feature-item-top">
							<span class="fav-icon"></span>
						</div>
					</div>
				</form>
				</div>
<?php 	
$villa_url = '';
$villa_cnt++;
	# code...
}
}
if(($checkin=='' && $checkout == '') && ($location!='') )
		{
			$villargs = array(
				'meta_key' => 'villa_locations',
				'meta_value' => $location,
				'post_type' => 'mphb_room_type',
				'post_status' => 'published',
				'posts_per_page' => -1
			);
			$villapages_loc = get_posts($villargs);	

			foreach ($villapages_loc as $key => $villapage) {
				$upload_dir = wp_upload_dir(); 
						$gall_villa_dir = ( $upload_dir['basedir'] .'/'. $villapage->post_name);
						$gall_villa_url = ( $upload_dir['baseurl'] .'/'. $villapage->post_name);
						$files = scandir($gall_villa_dir); 
						$total = count($files); 
						$galleryimgs = explode(',', $gallerymeta);
						$galleryimgs = array_filter($galleryimgs);
						if($villa_typeapi=='private'){
							continue;
						}
						$title = $villapage->post_title; 
						$tarr=array();
						$tarr= explode('|', $title);
						$int = (int) filter_var($tarr[0], FILTER_SANITIZE_NUMBER_INT);
						$villa_bath   = get_post_meta( $villapage->ID, 'bathroommphb_room_type', true );
						$villa_pool  = get_post_meta( $villapage->ID, 'villa_pool', true );
						$villa_guest  = get_post_meta( $villapage->ID, 'mphb_adults_capacity', true );
						$villa_address  = get_post_meta( $villapage->ID, 'villa_address', true );
						$villa_code  = get_post_meta( $villapage->ID, 'villa_code', true );
						$villa_bed  = get_post_meta( $villapage->ID, 'bedrrommphb_room_type', true );
						$destination  = get_post_meta( $villapage->ID, 'villa_locations', true );
						$purpose = get_post_meta( $villapage->ID, 'villa_purpose', true );
						$featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID));
						$villa_typeapi=get_post_meta( $villapage->ID, 'villa_type_api', true );
						$amn_villa = wp_get_object_terms( $villapage->ID, "mphb_room_type_facility");
						$gallerymeta  = get_post_meta( $villapage->ID, 'mphb_gallery', true );
						$galleryimgs = explode(',', $gallerymeta);
						$galleryimgs = array_filter($galleryimgs);
						$Address  = get_post_meta( $villapage->ID, 'villa_address', true );
						/*if($villa_destination != $destination ){
							continue;
						}
						if($villa_typeapi=='api'){
							$villa_prc = number_format((float)min($villapricerange[$villa_code])/$totaldays, 2 );
							if($villa_prc == 0 && !count($_POST)>1){
								continue;
							}
						}else{
							$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID) ;
							$villa_prc = number_format($roomType->getDefaultPrice(),2);
						}*/


						if($_POST!=''){
							if($checkin!='' && $checkout!='')
							{
								$villa_url .= get_permalink($villapage->ID).'?checkin='.$checkin.'&checkout='.$checkout;
							}
							if($villa_bed != ''){
								$villa_url .= '&beds='.$villa_bed;	
							}
							if($villa_guest != ''){
								$villa_url .= '&guests='.$villa_guest;	
							}
							if($villa_guest != ''){
								$villa_url .= '&price_typ='.$prc_typ;	
							}
							if($villa_prc != ''){
								$villa_url .= '&villa_pc='.$villa_prc;	
							}
						}else{
							$villa_url = get_permalink($villapage->ID);
						}
						?>
						<div class="col-4 listItem" id="villa-<?php echo $villapage->ID; ?>">

							<div class="feature-item">
								<!-- <a href="<?php echo $villa_url; ?>"> -->


									<a href='javascript:void(0)' class='villadata'  data-url="<?php echo get_permalink($villapage->ID); ?>">
										<form method="post" enctype="multipart/form-data" action="<?php echo get_permalink($villapage->ID); ?>" target="_blank">
											<input type='hidden' name="villa_prc" value="<?php echo $villa_prc; ?>" id="villa_prc" >
											<input type='hidden' name="beds" value="<?php echo $villa_bed; ?>" id="beds" >
											<input type='hidden' name="guests" value="<?php echo $villa_guest; ?>" id="guests" >
											<input type='hidden' name="prc_typ" value="<?php echo $prc_typ; ?>" id="prc_typ" >
											<input type='hidden' name="checkin" value="<?php echo $checkin; ?>" id="checkin" >
											<input type='hidden' name="checkout" value="<?php echo $checkout; ?>" id="checkout" >

											<div class="feature-item-slider">
												<?php 
												if(!empty($galleryimgs)){
													foreach ($galleryimgs as $key => $galleryimg) {
														?>

														<div>
															<img src="<?php echo wp_get_attachment_image_url($galleryimg,'medium'); ?>" alt="">
														</div>
														<?php 
													}
												}else{

													?>
													<div>
														<img src="https://luxurykey.madebyfire.com/wp-content/uploads/2019/02/banner2.jpeg" alt="">
													</div>
												<?php } ?>
											</div>
											<div class="slider-nav-wrapper">
												<ul class="feature-item-slider-nav slider-nav">
													<?php 
													if(!empty($galleryimgs))
													{
														$qwe=0;
														foreach ($galleryimgs as $galleryimg) {
															if($qwe == 3){
																break;
															}

															if($galleryimg!=''){ ?>
																<li>
																	<span></span>
																</li>
																<?php
															}
															$qwe++;
														}
													}
													?>
												</ul>
											</div>

										</form>
									</a>
									<div class="feature-item-content">
										<h5>
											<?php
											if($villa_bed != ''){
												echo '<span class="villa-cont">'.$villa_bed." BEDROOMS</span>";
											}
											if($villa_guest != ''){
												echo '<span class="villa-cont">'.$villa_guest." GUESTS</span>";
											}
											if($villa_bath != ''){
												echo '<span class="villa-cont">'.$villa_bath." BATHROOMS</span>";
											}
											?>
										</h5>
										<h3><?php echo $title; ?></h3>
										<?php
										if($villa_typeapi=='api' || $villa_typeapi=='api_manu'){
											if(!empty($villa_rates) && min($villapricerange[$villa_code])/$totaldays!=0){ 
												?>
												<p>
													From: €<?php echo number_format((float)min($villapricerange[$villa_code])/$totaldays, 2, '.', ''); ?> / per night
												</p>
												<?php 
											}
										}else{
											?>
											<p>
												From: €
												<?php  
												$roomType = MPHB()->getRoomTypeRepository()->findById($villapage->ID) ;
												echo $roomType->getDefaultPrice();
												?> 
												/ per-night
											</p>	
											<?php
										} 
										if($destination!=''){
											?>
											<p>
												<a href="#!">
													<?php echo ($Address); ?>
												</a> 

											</p>
										<?php } ?>
									</div>

									<!--Favorites Button start-->
									<div class="feature-item-top">
										<?php echo do_shortcode('[favorite_button post_id="' . $villapage->ID . '"]'); ?> 		
									</div>
									<!--Favorites Button End-->

								</div>

							</div>
				<?php
				# code...
			}
			exit();


}
$locs_map = array();
$villa_ids = array();
foreach($available1 as $avail_key => $villapage){
	$latitude = get_post_meta( $avail_key, 'villa_latitude', true );
	$longtitude = get_post_meta( $avail_key, 'villa_longtitude', true );
	$villa_typeapi=get_post_meta( $villapage->ID, 'villa_type_api', true );
	if($latitude=='' || $longtitude=='' ){
		continue;
	}
	if($villa_typeapi=='private'){
							continue;
						}
	$destination =  get_post_meta( $avail_key, 'villa_locations', true );
	$title = get_the_title($avail_key); 
	$locs_map[] = '{"Latitude":'.$latitude.',"Longitude":'.$longtitude.',"DisplayName":"'.$title.'","MapId":"'.$avail_key.'"}';
}
$villa_map=implode('|',$locs_map);
echo '~'.$villa_cnt.'~'.$villa_map;
exit();
}
	function searchmodule($route,$location,$checkin,$checkout,$numerofbeds,$minprice,$maxprice,$flexible,$package,$villatype='',$eventtype='',$villalocality=''){
		/*$package,$villatype='',$eventtype='',$mykanoslocation=''*/
		$apiUrl = get_option('api_endpoint');
		$username = get_option('api_username');
		$password = get_option('api_password');
		$checkin1=date_create($checkin);
		$checkin_flex=date_sub($checkin1,date_interval_create_from_date_string( $flexible."days"));
		$checkin_flex=	date_format($checkin_flex,"Y-m-d");
		/*	echo $checkin;
		echo "<br>";
		echo date_format($cheee,"Y-m-d");
		echo "<br>";*/
		$checkout1 = date_create($checkout);
		$checkout_flex=date_add($checkout1,date_interval_create_from_date_string( $flexible."days"));
		$checkout_flex=	date_format($checkout_flex,"Y-m-d");
/*echo $checkin.'--->	'.$checkin_flex;
echo $checkout.'--->	'.$checkout_flex.'<br>';*/
$diff=date_diff(date_create($checkin),date_create($checkout));

$totaldays=$diff->days;

$roomsAtts = array(
	'availability'	 => 'locked',
	'from_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkin ),
	'to_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $checkout )
);
$result =  getAvailableRoomTypes_custom($roomsAtts);
foreach ($result as $value) {
		// $available[]=$lcg_value(oid)['id'];
 	// var_dump(MPHB()->getRoomTypeRepository()->findById($value['id'])->getDefaultPrice());
	$manual_price=MPHB()->getRoomTypeRepository()->findById($value['id'])->getDefaultPrice();
	if ($manual_price>=$minprice && $manual_price <= $maxprice ) {
		$available[$value['id']]=$manual_price;
	}
}

$authorization = 'Basic '. base64_encode($username.":".$password);
$feedUrl = $apiUrl.'/availability/LUXURYKEYS?checkin='.$checkin.'&checkout='.$checkout;
$ch = curl_init();
$headers = array(
	'Content-Type: application/x-www-form-urlencoded',
	'charset: utf-8',
	'Authorization:'. $authorization,
);
curl_setopt($ch, CURLOPT_URL, $feedUrl);
curl_setopt($ch, CURLOPT_HEADER, true);
curl_setopt($ch, CURLOPT_VERBOSE, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
$output = curl_exec($ch);
$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
$body = substr($output, $header_size);
$villaXmlData = new SimpleXMLElement($body);
/*echo "<pre>";
print_r($villaXmlData);
echo "<br>";
echo "<br>";
echo "<br>";
echo "<***************************************************************************************************>";*/
curl_close($ch);




$villaData = $villaXmlData->rates;
$villaXmlData=xml2array($villaXmlData);

$villa_rates=$villaXmlData['data']['rates']['rate'];

foreach ($villa_rates as  $villa_rate) {

	$villargs = array(
		'meta_key' => 'villa_code',
		'meta_value' => (string)$villa_rate->type,
		'post_type' => 'mphb_room_type',
		'post_status' => 'published',
		'posts_per_page' => -1
	);
	$villapages = get_posts($villargs);	
		/*echo $villapages[0]->ID;
		echo "<br>";*/

		if($villa_rate->pricing->stay/$totaldays >=$minprice && $villa_rate->pricing->stay/$totaldays <=$maxprice  ){
		// if(1){
			$villapricerange[$villapages[0]->ID][]=(float)$villa_rate->pricing->stay/$totaldays;
		}
	}
/*echo "<pre>";
print_r($villapricerange);
echo "<br>";*/

/*       min($villapricerange[$villapages[0]->ID])   */


/*flexible date start */
if ($flexible!=0) {

	$feedUrl_flex = $apiUrl.'/availability/LUXURYKEYS/flexible-calendar?startDate='.$checkin_flex.'&endDate='.$checkout_flex.'&nights='.$totaldays;
	// echo $feedUrl_flex;
	$ch_flex = curl_init();
	$headers = array(
		'Content-Type: application/x-www-form-urlencoded',
		'charset: utf-8',
		'Authorization:'. $authorization,
	);
	curl_setopt($ch_flex, CURLOPT_URL, $feedUrl_flex);
	curl_setopt($ch_flex, CURLOPT_HEADER, true);
	curl_setopt($ch_flex, CURLOPT_VERBOSE, 1);
	curl_setopt($ch_flex, CURLOPT_HTTPHEADER, $headers);
	curl_setopt($ch_flex, CURLOPT_POST, 1);
	curl_setopt($ch_flex, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch_flex, CURLOPT_POSTFIELDS, $params);
	$output_flex = curl_exec($ch_flex);
	$header_size_flex = curl_getinfo($ch_flex, CURLINFO_HEADER_SIZE);
	$body_flex = substr($output_flex, $header_size_flex);
	$villaXmlData_flex = new SimpleXMLElement($body_flex);

	curl_close($ch_flex);
	$villaXmlData_flex=xml2array($villaXmlData_flex);

	$flex_avail=$villaXmlData_flex['data']['days']['day'];
	/*
	echo "<pre>";
	print_r($flex_avail);
	exit();*/
	foreach ($flex_avail  as  $flex_avai) {
		$villargs = array(
			'meta_key' => 'villa_code',
			'meta_value' => (string)$flex_avai['rm_type'],
			'post_type' => 'mphb_room_type',
			'post_status' => 'published',
			'posts_per_page' => -1
		);
		$villapages = get_posts($villargs);	
		if((float)$flex_avai->price/$totaldays >=$minprice && (float)$flex_avai->price/$totaldays <=$maxprice  ){
			// if(1){
			$villapricerange[$villapages[0]->ID][]=(float)$flex_avai->price/$totaldays;
		}

	}

}
/*flexible date ends*/
$available=array_filter(array_unique($available));
foreach ($villapricerange as $key => $villapricerang) {
	// echo $key;	
	$available[$key]= min($villapricerange[$key]);
}

/*echo "<pre>";
print_r($available);
echo "<br>";*/

foreach ($available as $id => $availabl) {
/*echo get_post_meta($id,'villa_locations')[0];
echo "<br>";*/
	// if(get_post_meta($id,'bedrrommphb_room_type')[0]>=$numerofbeds && get_post_meta($id,'villa_locations')[0]==$location ){
if(get_post_meta($id,'bedrrommphb_room_type')[0]>=$numerofbeds && get_post_meta($id,'villa_locations')[0]==$location ){
	$available1[$id]=$availabl;
}
}
/*filter start------Additional for search*/

/*
foreach ($available1 as $id => $availab) {
	// if(get_post_meta($id,'bedrrommphb_room_type')[0]>=$numerofbeds && get_post_meta($id,'villa_locations')[0]==$location ){
if(get_post_meta($id,'villa_locality')[0]==$villalocality && get_post_meta($id,'villa_occation')[0]==$eventtype && get_post_meta($id,'villa_occation')[0] ){
	$available2[$id]=$availab;
	}
}

*/

/*filter ends ------Additional for search*/
/*echo "<pre>";
print_r($available2);
echo "<br>";*/
return $available1;

}
function admin_style() {
	wp_enqueue_style('admin-styles', get_template_directory_uri().'/admin.css');
}
add_action('admin_enqueue_scripts', 'admin_style');
/*
* 
*/




check_in_range($start_date, $end_date, $date_from_user);


function check_in_range($postID)
{
  // Convert to timestamp
	$villa_rate   = get_post_meta( $postID, 'villa_rate', true );
	  $villa_rate_def   = get_post_meta( $postID, 'villa_rate_def', true );
      $villa_start_date   = get_post_meta( $postID, 'villa_start_date', true );
      $villa_end_date   = get_post_meta( $postID, 'villa_end_date', true );
	  $start_ts = strtotime($villa_start_date);
	  $end_ts = strtotime($villa_end_date);
	  $user_ts = time();
	if(($user_ts >= $start_ts) && ($user_ts <= $end_ts)){
		echo $villa_rate;
	}else{
		echo $villa_rate_def;
	}
  // Check that user date is between start & end
  
}

//Offline functiontionality villa 
function offlineVillaRule($check_in,$check_out,$uname ='',$offl_email='',$min_prc='',$max_prc='')
{
		$villa_code = $villa_code;
		$check_in = $check_in;
		$check_out = $check_out;
		$offl_email = $offl_email;
		$uname = $uname;
		$villargs = array(
			'numberposts' => -1,
			'post_type' => 'mphb_room_type',
			'orderby' => 'menu_order',
			'order' => 'ASC',
			'post_status'=>"publish"
		);
		$offl_villas = get_posts($villargs);	
		$off_villa_arr = array();
	// foreach ($offl_villas as $key => $offl_villa) {`
		$apiUrl = get_option('api_endpoint');
		$username = get_option('api_username');
		$password = get_option('api_password');
		$diff=date_diff(date_create($check_in),date_create($check_out));
		$totaldays=$diff->days;
		$roomsAtts = array(
			'availability'	 => 'locked',
			'from_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $check_in ),
			'to_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), $check_out )
		);
		$result =  getAvailableRoomTypes_custom($roomsAtts);
		$val_id = array();

		foreach ($result as $value) {
					// $available[]=$lcg_value(oid)['id'];
			 	// var_dump(MPHB()->getRoomTypeRepository()->findById($value['id'])->getDefaultPrice());
			$manual_price=MPHB()->getRoomTypeRepository()->findById($value['id'])->getDefaultPrice();
			if($min_price != '' && $max_price != ''){
				if ($manual_price>=$min_price && $manual_price <= $max_price ) {
					$off_villa_arr[$value['id']] = $manual_price;
				}
			}else{

				$terms_ids = wp_get_post_terms($value['id'], 'mphb_room_type_prc_typ');
				foreach( $terms_ids as $terms_id ) {
					if($terms_id->slug == 'diamond'){
						$off_villa_arr[$value['id']] = $manual_price;		
					}
				}
				
			}
		}

		$authorization = 'Basic '. base64_encode($username.":".$password);
		$feedUrl = $apiUrl.'/availability/LUXURYKEYS?checkin='.$check_in.'&checkout='.$check_out;
		$ch = curl_init();
		$headers = array(
			'Content-Type: application/x-www-form-urlencoded',
			'charset: utf-8',
			'Authorization:'. $authorization,
		);
		curl_setopt($ch, CURLOPT_URL, $feedUrl);
		curl_setopt($ch, CURLOPT_HEADER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
		$output = curl_exec($ch);

		$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
		$body = substr($output, $header_size);
		$villaXmlData = new SimpleXMLElement($body);
		curl_close($ch);
		$villaData = $villaXmlData->rates;
		// var_dump($villaData);
		$villaXmlData=xml2array($villaXmlData);
		
		$villa_rates=$villaXmlData['data']['rates']['rate'];

		foreach ($villa_rates as  $villa_rate) {

			$villargs = array(
				'meta_key' => 'villa_code',
				'meta_value' => (string)$villa_rate->type,
				'post_type' => 'mphb_room_type',
				'post_status' => 'published',
				'posts_per_page' => -1
			);
			$villapages = get_posts($villargs);	
			// var_dump($villapages);
				/*echo $villapages[0]->ID;
				echo "<br>";*/
				if($villa_rate->pricing->stay/$totaldays >= $min_price && $villa_rate->pricing->stay/$totaldays <=$max_price  ){
				// if(1){
					$off_villa_arr[$villapages[0]->ID][]= number_format((float)$villa_rate->pricing->stay/$totaldays,2);
				}else{
					$terms_ids = wp_get_post_terms($villapages[0]->ID, 'mphb_room_type_prc_typ');
					var_dump($terms_ids);
					foreach( $terms_ids as $terms_id ) {
						if($terms_id->slug == 'diamond'){
							$off_villa_arr[$villapages[0]->ID] = number_format((float)$villa_rate->pricing->stay/$totaldays,2);		
						}
					}	
				}
		}
		// var_dump($off_villa_arr);	
		// exit();

	}