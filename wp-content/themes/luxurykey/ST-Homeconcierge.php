<?php
/***********************
Template Name: Home Concierge
************************/
global $post;
			$currentpage = get_post($postId);
          $concArgs = array(
              'post_type'     => 'concierge',
              'numberposts' => -1,
              'order'         => 'ASC',
              'orderby'       => 'post_date',
              'post_status'   => 'publish',
          );
          $concPages = get_posts($concArgs);
          list($conPages1,$conPages2) = array_chunk($concPages, ceil(count($concPages)/2))
?>

<!--  concirge section slider cards -->
       	<section class="section-common" id="scrollnext">
        	<div class="container container-type1">
        		
		        <div class="card-box type-1 services-slider-right">
              <?php
              		$i = 1;
              		foreach($conPages1 as $concPage1){
                          $featImage = wp_get_attachment_url(get_post_thumbnail_id($concPage1->ID) );
              						$post_count=$wp_query->found_posts;
                          $char_limit = 15;
				                  $content = apply_filters('the_content',$concPage1->post_content);
                          $excerpt = $concPage1->post_excerpt;
                          $linkUrl = get_post_meta( $concPage1->ID, 'rd_link', true );
                        	$buttonText = get_post_meta( $concPage1->ID, 'rd_btn', true );
                          if($linkUrl!='') {
                              $link = $linkUrl;
                          } else {
                              $link = 'javascript:void(0);';
                          }
              ?>
		        	<div class="col-4">
    						<a href="<?php echo $link; ?>">
    							<figure>
    								<img src="<?php echo $featImage; ?>" alt="services_1" />
    								<figcaption>
    									<h4><?php echo $concPage1->post_title; ?></h4>
    									<p><?php echo $excerpt; ?></p>
                      <?php  if($buttonText !="" && $linkUrl!='')
                        { ?>
        									<div class="button button-primary">
        										<button><?php echo $buttonText; ?></button>
        									</div>
                      <?php } ?>
    								</figcaption>
    							</figure>
    						</a>
		        	</div>
              <?php

						  }
            ?>
		        </div>
            <div class="card-box type-1 services-slider-left">
              <?php
                  $i = 1;
                  foreach($conPages2 as $conPage2){
                          $featImage = wp_get_attachment_url(get_post_thumbnail_id($conPage2->ID) );
                          $post_count=$wp_query->found_posts;
                          $char_limit = 15;
                          $content = apply_filters('the_content',$conPage2->post_content);
                          $excerpt = $conPage2->post_excerpt;
                          $linkUrl = get_post_meta( $conPage2->ID, 'rd_link', true );
                          $buttonText = get_post_meta( $conPage2->ID, 'rd_btn', true );
                          if($linkUrl!='') {
                              $link = $linkUrl;
                          } else {
                              $link = 'javascript:void(0);';
                          }
              ?>
              <div class="col-4">
                <a href="<?php echo $link; ?>">
                  <figure>
                    <img src="<?php echo $featImage; ?>" alt="services_1" />
                    <figcaption>
                      <h4><?php echo $conPage2->post_title; ?></h4>
                      <p><?php echo $excerpt; ?></p>
                      <?php  if($buttonText !="" && $linkUrl!='')
                        { ?>
                          <div class="button button-primary">
                            <button><?php echo $buttonText; ?></button>
                          </div>
                      <?php } ?>
                    </figcaption>
                  </figure>
                </a>
              </div>
              <?php
              }
            ?>
            </div>
		    </div>
        </section>
		<!-- End of concirge section slider cards -->
