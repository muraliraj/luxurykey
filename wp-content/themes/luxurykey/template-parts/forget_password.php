<div id="forget" class="loginpopup">
  <div class="fullbanner">
    <img src="<?php echo get_bloginfo('template_url'); ?>/img/banner5.jpeg">
  </div>
  <div class="white-card">
    <div class="signinhead">
      <h2>Forgot password</h2>
    </div>
    <div class="registration-form">
      <form name="lostpasswordform" id="lostpasswordform" action="<?php echo wp_lostpassword_url(); ?>" method="post">
      <div class="form-row">
        <label class="floating-item" data-error="Please enter your Email Id">
          <input type="text" id="user_login" class="floating-item-input input-item" name="user_login" value="" required />
          <span class="floating-item-label">Enter your email address</span>
        </label>
      </div>
    
    </div>
    <div class="clearfix">
      <div class="button button-primary fR">
        <button  type="submit" id="wp-submit" name="wp-submit" >Submit</button>
      </div>
    </div>
    </form>
  </div>
</div>
<!-- popup end -->
