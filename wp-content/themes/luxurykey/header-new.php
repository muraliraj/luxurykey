<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<title>Luxury Key | Villa Detail</title>
		<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
		<!-- modernizr included -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
		<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOzhN_oBfDzghug_jY9wvzGwPJI5qWs3I&callback=initMap"></script>
		<style type="text/css">
			body{
				background-color: #fff;
			}  
			.render-blk{ opacity:1; }
		</style>
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/app.css">
        <link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/style.css">



		<script type="text/javascript">
			var cssArr = ['<?php echo get_bloginfo('template_url'); ?>/css/app.css'];
			for(var i = 0; i < cssArr.length; i++) {
				var link = document.createElement('link');
				link.setAttribute('rel', 'stylesheet');
				link.setAttribute('type', 'text/css');
				link.setAttribute('href', cssArr[i]);
				document.getElementsByTagName('head')[0].appendChild(link);
			}
		</script>
		<noscript>
			<script async defer
             src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBOzhN_oBfDzghug_jY9wvzGwPJI5qWs3I&callback=initMap"></script>
			<style type="text/css" media="screen">
				.render-blk{ opacity: 1; }	
			</style>
		</noscript>
<?php wp_head(); ?>
	</head>
	<body  class="date-js internal-js">
		<div class="render-blk"> 
			<!-- header start -->
			<header class="nonav" id="scrolledTop">
	            <div class="container container-type1">
	                <a href="index.html" class="logo">
						<img src="<?php echo get_bloginfo('template_url'); ?>/img/logo.svg" alt="images">
					</a>
					<a class="popup-with-zoom-anim list-filter" href="#small-dialog">
		    			<i class="fa fa-list-ul" aria-hidden="true"></i>
		  			</a>
		  			<a class="mag-popup-close">
		  				<i class="la la-close" aria-hidden="true"></i>
		  			</a>
	                <div id="small-dialog" class="zoom-anim-dialog search-bar-container  mfp-hide">
						<div class="row">
							<div class="col">
								<div class="form-row select-list" id="mapicon">
									<div class="drop-links">
										<span class="filter">Choose a destination</span>
										<ul class="select">
											<li class="select-list-item" data-value="Mykonos">Mykonos</li>
										    <li class="select-list-item" data-value="Tulum">Tulum</li>
										    <li class="select-list-item" data-value="Ibiza">Ibiza</li>
									      	<li class="select-list-item" data-value="Santorini">Santorini</li>
											<li class="select-list-item" data-value="Other Greek Islands">Other Greek Islands</li>
											<li class="select-list-item" data-value="Private Islands">Private Islands</li>
											<li class="select-list-item" data-value="Other Global Destinations">Other Global Destinations</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="form-row select-list">
									<div class="input-item"><span>Check in</span> <i class="la la-arrow-right"></i> <span>Check out</span></div>
									<div class="daterangepicker-options">
										<h4>Flexible Days</h4>  
										<div class="checkbox-group">
											<div class="checkboxradio">
			            						<div class="checkboxradio-row">
			            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" />
			            							<label class="checkboxradio-label radio-label" for="threedays"> +3/-3 Days
			            							</label>
			            						</div>
		            						</div>
		            						<div class="checkboxradio">
			            						<div class="checkboxradio-row">
			            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" />
			            							<label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
			            							</label>
			            						</div>
		            						</div>
		            						<div class="checkboxradio">
			            						<div class="checkboxradio-row">
			            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" />
			            							<label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
			            							</label>
			            						</div>
		            						</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="form-row select-list">
									<div class="drop-links">
										<span class="filter">No of Bedroom</span>
										<ul class="select">
											<li class="select-list-item" data-value="1 Bedroom">1 Bedroom</li>
										    <li class="select-list-item" data-value="2 Bedrooms">2 Bedrooms</li>
										    <li class="select-list-item" data-value="3 Bedrooms">3 Bedrooms</li>
									      	<li class="select-list-item" data-value="4 Bedrooms">4 Bedrooms</li>
											<li class="select-list-item" data-value="8 Bedrooms">8 Bedrooms</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col-4">
								<div class="combine-dropdown">
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Price</span>
											<ul class="select">
												<li class="select-list-item" data-value="$500 – 2,999/day">$500 – 2,999/day</li>
											    <li class="select-list-item" data-value="$3,000 – 5999/day">$3,000 – 5999/day</li>
											    <li class="select-list-item" data-value="$6,000 – 9.999/day"> $6,000 – 9.999/day</li>
											    <li class="select-list-item" data-value="$10.000 – 16.000/day"> $10.000 – 16.000/day</li>
											</ul>
										</div>
									</div>
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Style</span>
											<ul class="select">
												<li class="select-list-item" data-value="Diamond">Diamond</li>
											    <li class="select-list-item" data-value="Platinum">Platinum</li>
											    <li class="select-list-item" data-value="Gold">Gold</li>
										      	<li class="select-list-item" data-value="Silver">Silver</li>
										      	<!--<li><a href="#!">What is this?</a></li>-->
											</ul>
											
										</div>
									</div>
								</div>
							</div>
							<div class="col filter-btn">
								<div class="form-row select-list data-list-input">
								<div class="drop-links">
									<span class="filter">More Filters</span>
									<ul class="select data-list-input">
									    <li class="checkbox-select">
								    		<div class="checkboxradio">
			            						<div class="checkboxradio-row">
			            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" type="checkbox" id="allarea" />
			            							<label class="checkboxradio-label checkbox-label" for="allarea">All Area
			            							</label>
			            						</div>
		            						</div>
									    </li>
									    <li class="checkbox-select">
								    		<div class="checkboxradio">
			            						<div class="checkboxradio-row">
			            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" type="checkbox" id="beachfront" />
			            							<label class="checkboxradio-label checkbox-label" for="beachfront">Beach Front
			            							</label>
			            						</div>
		            						</div>
									    </li>
									    <li class="checkbox-select">
								    		<div class="checkboxradio">
			            						<div class="checkboxradio-row">
			            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup"  type="checkbox" id="for-group"/>
			            							<label class="checkboxradio-label checkbox-label" for="for-group">For Groups
			            							</label>
			            						</div>
		            						</div>
									    </li>
									    <li class="checkbox-select">
								    		<div class="checkboxradio">
			            						<div class="checkboxradio-row">
			            							<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" type="checkbox" id="for-event" />
			            							<label class="checkboxradio-label checkbox-label" for="for-event">For Events
			            							</label>
			            						</div>
		            						</div>
									    </li>
									</ul>
								</div>
							</div>
							</div>
						</div>
					</div>
	            </div>
	        </header>