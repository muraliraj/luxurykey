<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Luxury Key | Villa Detail</title>
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700" rel="stylesheet">
	<!-- modernizr included -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js"></script>
	

	<script async defer
	src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAj2VsL2Hjul-_ZhlOtYSpdhiyZSUTkTUE&callback=initMap"></script>
	<style type="text/css">
	body{
		background-color: #fff;
	}  
	.error-message{
		display: none;
	}
	.select-list.date-pick{
		height: 51px;
	}
	.slick-slide{
		height: unset!important;
	}
	.feature-item-slider{
		height: fit-content;
	}
	.render-blk{ opacity:1; }
</style>
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/css/app.css">
<link rel="stylesheet" href="<?php echo get_bloginfo('template_url'); ?>/style.css">




<noscript>
	<style type="text/css" media="screen">
	.render-blk{ opacity: 1; }	
</style>
</noscript>
<script>
	var templateUri = "<?php  echo get_bloginfo('template_url'); ?>";
	var blogUri 	= "<?php echo get_bloginfo('url'); ?>";
</script>
<?php wp_head(); ?>
</head>
<body  class="subpage">
	<div class="global-message error">
		<div class="container">
			<p>* Please choose all the details to filter </p>
			<a class="global-message-close no-effect" href=""><i class="la la-close"></i></a>
		</div>
	</div>
	<div class="render-blk">
		<!-- header start -->
		<header class="nonav" id="scrolledTop">
			<a href="<?php echo get_bloginfo('url'); ?>" class="logo desktop">
					<img src="<?php echo get_bloginfo('template_url'); ?>/img/logo.svg" alt="images">
				</a>
				<a href="<?php echo get_bloginfo('url'); ?>" class="logo mobile">
					<img src="<?php echo get_bloginfo('template_url'); ?>/img/logo-mobile.png" alt="images">
				</a>
			<div class="container">
				<a class="mob-inline my-fav-mob" href="<?php get_bloginfo('url');?>/favorites">
	    			<i class="fa fa-heart" aria-hidden="true"></i>
	  			</a>
				<a class="popup-with-zoom-anim list-filter" href="#small-dialog">
					<i class="fa fa-list-ul" aria-hidden="true"></i>
				</a>
				<a class="mag-popup-close">
					<i class="la la-close" aria-hidden="true"></i>
				</a>
				<div id="small-dialog" class="zoom-anim-dialog search-bar-container filter-villa  mfp-hide">
					<div class="row">
						<?php 
						if(is_singular( 'mphb_room_type' )){
							?>
							<div class="col">
								<div class="form-row select-list" id="mapicon">
									<div class="drop-links">
										<span class="filter">Choose a destination</span>
										<ul class="select">
											<li class="select-list-item" data-value="Mykonos" selected>Mykonos</li>
											<li class="select-list-item" data-value="Tulum">Tulum</li>
											<li class="select-list-item" data-value="Ibiza">Ibiza</li>
											<li class="select-list-item" data-value="Santorini">Santorini</li>
											<li class="select-list-item" data-value="Other Greek Islands">Other Greek Islands</li>
											<li class="select-list-item" data-value="Private Islands">Private Islands</li>
											<li class="select-list-item" data-value="Other Global Destinations">Other Global Destinations</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col check-io-column">
								<div class="form-row select-list">
									<div class="input-item"><span><?php echo $_POST['villa-check-in']; ?></span> <i class="la la-arrow-right"></i> <span><?php echo $_POST['villa-check-out']; ?></span></div>
									<div class="daterangepicker-options">
										<h4>Flexible Days</h4>  
										<div class="checkbox-group">
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" value="3" />
													<label class="checkboxradio-label radio-label" for="threedays" > +3/-3 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" value="7" />
													<label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" value="30" />
													<label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
													</label>
												</div>
											</div>
										</div>
										<button type="button" class="datepicker-clear btn btn-sm btn-default">Clear</button>
									</div>
								</div>
							</div>
							<div class="col non-field">
								<div class="form-row select-list">
									<div class="drop-links">
										<span class="filter" id="defaultText">Bedroom</span>

										<ul class="select overflow-y no_beds">
											<li data-beds="1">1</li>
											<li data-beds="2">2</li>
											<li data-beds="3">3</li>
											<li data-beds="4">4</li>
											<li data-beds="5">5</li>
											<li data-beds="6">6</li>
											<li data-beds="7">7</li>
											<li data-beds="8">8</li>
											<li data-beds="9">9</li>
											<li data-beds="10">10</li>
											<li data-beds="11">11</li>
											<li data-beds="12">12</li>
											<li data-beds="13">13</li>
											<li data-beds="14">14</li>
											<li data-beds="15">15</li>
											<li data-beds="16">16</li>
											<li data-beds="17">17</li>
											<li data-beds="18">18</li>
											<li data-beds="19">19</li>
											<li data-beds="20">20</li>
											<li data-beds="21">21</li>
											<li data-beds="22">22</li>
											<li data-beds="23">23</li>
											<li data-beds="24">24</li>
											<li data-beds="25">25</li>
										</ul> 
									</div>                    

								</div>

							</div>
							<div class="col-4">
								<div class="combine-dropdown">
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Price</span>
											<ul class="select">
												<li class="select-list-item" data-value="€500 – €2,999/day" data-prc="500-2999">€500 – €2,999/day</li>
												<li class="select-list-item" data-value="€3,000 – €5999/day" data-prc="3000-5999">€3,000 – €5999/day</li>
												<li class="select-list-item" data-value="€6,000 – €9.999/day" data-prc="6000-9999"> €6,000 – €9.999/day</li>
												<li class="select-list-item" data-value="€10.000 – €16.000/day" data-prc="10000-16000"> €10.000 – €16.000/day</li>
											</ul>
										</div>
									</div>
									<div class="form-row select-list">
										<div class="drop-links">
											<span class="filter">Style</span>
											<ul class="select">
												<li class="select-list-item" data-value="Diamond">Diamond</li>
												<li class="select-list-item" data-value="Platinum">Platinum</li>
												<li class="select-list-item" data-value="Gold">Gold</li>
												<li class="select-list-item" data-value="Silver">Silver</li>
												<!--<li><a href="#!">What is this?</a></li> -->
											</ul>

										</div>
									</div>
								</div>
							</div>
							<?php 
						}else{
							$checkin_date = date_create($_POST['villa-check-in']);
							$checkin = date_format($checkin_date,"d M y");
							$checkout_date = date_create($_POST['villa-check-out']);
							$checkout = date_format($checkout_date,"d M y");
							?>
							<input type="hidden" name="villa-destination" id="villa-destination" value="<?php echo $_POST['villa-destination'];	?>">
							<input type="hidden" name="villa-price-typ" id="villa-price-typ" value="<?php echo $_POST['villa-price-typ']; ?>">
							<input type="hidden" name="villa-check-in" id="villa-check-in" value="<?php echo $_POST['villa-check-in']; ?>">
							<input type="hidden" name="villa-check-out" id="villa-check-out" value="<?php echo $_POST['villa-check-out']; ?>">
							<input type="hidden" name="villa-price" id="villa-price" value="<?php echo $_POST['villa-price']; ?>">
							<input type="hidden" name="villa-price-min" id="villa-price-min" value="<?php echo $_POST['villa-price-min']; ?>">
							<input type="hidden" name="villa-price-max" id="villa-price-max" value="<?php echo $_POST['villa-price-max']; ?>">
							<input type="hidden" name="no-of-bedrooms" id="no-of-bedrooms" value="<?php echo $_POST['no-of-bedrooms']; ?>">
							<input type="hidden" name="villa-flexible" id="villa-flexible" value="<?php echo $_POST['villa-flexible']; ?>">
							<div class="col">
								<div class="form-row select-list" id="mapicon">
									<div class="drop-links">
										<?php 
										if($_POST['villa-destination'] != ''){
											$villa_destination = $_POST['villa-destination'];
										}elseif($_GET['locat']!='') {
											$villa_destination = $_GET['locat'];
										}else{
											$villa_destination = '';
										}
										if($villa_destination != ''){
											?>
											<span class="filter"><?php echo $villa_destination; ?></span>
										<?php }else{ ?>
											<span class="filter" >Choose a destination</span>
										<?php } ?>
										<ul class="select" id="villa_dest">
											<?php 
											$args = array(
												'post_type'=> 'destination',
												'post_status' => 'publish',
												'order' => 'DESC',
												'numberposts' => -1
											);
											$destination_posts = get_posts($args);  
											foreach ($destination_posts as $destination_post) { 
												?>

												<li class="select-list-item" data-value="<?php echo $destination_post->post_title; ?>" data-locs="<?php echo $destination_post->post_title; ?>" dest="Mykonos"<?php echo $destination_post->post_title; ?>"><?php echo $destination_post->post_title; ?></li>



											<?php }?>
										</ul>
									</div>
								</div>
							</div>
							<div class="col check-io-column">
								<div class="form-row select-list">
									<?php 
									if($_POST['villa-check-in']!='' && $_POST['villa-check-in']!=''){
										?>
										<div class="input-item sel-loc" id="serch_date"><span><?php echo $checkin; ?></span> <i class="la la-arrow-right"></i> <span><?php echo $checkout; ?></span></div>
									<?php }else{ ?>
										<div class="input-item sel-loc" id="serch_date"><span>Check in</span> <i class="la la-arrow-right"></i> <span>Check out</span></div>
									<?php } ?>
									<div class="daterangepicker-options">
										<h4>Flexible Days</h4>  
										<div class="checkbox-group">
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="threedays" type="radio" value="3" />
													<label class="checkboxradio-label radio-label" for="threedays" > +3/-3 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="sevendays" type="radio" value="7" />
													<label class="checkboxradio-label radio-label" for="sevendays"> +7/-7 Days
													</label>
												</div>
											</div>
											<div class="checkboxradio">
												<div class="checkboxradio-row">
													<input class="checkboxradio-item checkboxradio-invisible" name="checkgroup" id="wholemonth" type="radio" value="30" />
													<label class="checkboxradio-label radio-label" for="wholemonth"> Whole Month
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col non-field">
				              	<div class="form-row select-list">
			                 	 	<div class="drop-links">
					                  	<?php 
					                  		if($_POST['no-of-bedrooms']==''){
					                  	?>
					                  	<span class="filter" id="defaultText">Bedrooms</span>
						              	<?php }else{ ?>
                                                                <?php if($_POST['no-of-bedrooms'] == '1'){ ?>
						              	<span class="filter" id="defaultText"><?php echo $_POST['no-of-bedrooms']; ?> Bedroom</span>
                                                                <?php }else{ ?>
                                                                <span class="filter" id="defaultText"><?php echo $_POST['no-of-bedrooms']; ?> Bedrooms</span>
                                                                <?php } ?>
						              	<?php } ?>
					                 
					                  	<ul class="select overflow-y no_beds" id="no_beds">
											<li data-beds="1">1</li>
											<li data-beds="2">2</li>
											<li data-beds="3">3</li>
											<li data-beds="4">4</li>
											<li data-beds="5">5</li>
											<li data-beds="6">6</li>
											<li data-beds="7">7</li>
											<li data-beds="8">8</li>
											<li data-beds="9">9</li>
											<li data-beds="10">10</li>
											<li data-beds="11">11</li>
											<li data-beds="12">12</li>
											<li data-beds="13">13</li>
											<li data-beds="14">14</li>
											<li data-beds="15">15</li>
											<li data-beds="16">16</li>
											<li data-beds="17">17</li>
											<li data-beds="18">18</li>
											<li data-beds="19">19</li>
											<li data-beds="20">20</li>
											<li data-beds="21">21</li>
											<li data-beds="22">22</li>
											<li data-beds="23">23</li>
											<li data-beds="24">24</li>
											<li data-beds="25">25</li>
					                    </ul> 
				                	</div>                
				              	</div>
				            </div>

							<div class="col-4">
								<div class="combine-dropdown">
									<div class="form-row select-list">
										<div class="drop-links">
											<?php 
											if($_POST["villa-price"] != ''){
                                                                                           $vill_prc_exp =  explode('-', $_POST["villa-price"]);
												$vill_prc_min = $vill_prc_exp[0];
												$vill_prc_max = $vill_prc_exp[1];
												?>
												<span class="filter"><?php echo '€'.$vill_prc_min.' - '.'€'.$vill_prc_max; ?></span>
											<?php }else{ ?>
												<span class="filter">Price</span>
											<?php } ?>
											<ul class="select" id="villa_prc">
												<li class="select-list-item" data-value="€500 - €2999" data-prc="500-2999">€500 – €2,999/day</li>
												<li class="select-list-item" data-value="€3000 - €5999" data-prc="3000-5999">€3,000 – €5999/day</li>
												<li class="select-list-item" data-value="€6000 - €9999" data-prc="6000-9999"> €6,000 – €9.999/day</li>
												<li class="select-list-item" data-value="€10000 - €16000" data-prc="10000-16000"> €10.000 – €16.000/day</li>
											</ul>
										</div>
									</div>
									<div class="form-row select-list">
										<div class="drop-links">
											<?php 
											if($_POST["villa-price"] != ''){
												?>
												<span class="filter" id="villa_prc_typ"><?php echo $_POST["villa-price-typ"]; ?></span>
											<?php }else{ ?>
												<span class="filter" id="villa_prc">Style</span>
											<?php } ?>
											<ul class="select">
												<li class="select-list-item" data-value="Diamond">Diamond</li>
												<li class="select-list-item" data-value="Platinum">Platinum</li>
												<li class="select-list-item" data-value="Gold">Gold</li>
												<li class="select-list-item" data-value="Silver">Silver</li>
												<!--<li><a href="#!">What is this?</a></li>-->
											</ul>
											
										</div>
									</div>
								</div>
							</div>
							<div class="col filter-btn more-filter-villa">
								<div class="form-row select-list data-list-input">
									<div class="drop-links">
										<span class="filter">More Filters</span>
										<div class="select data-list-input mega-dropdown accordion-row">
											<ul>
												<li class="checkbox-select  accordion-row-blk">
													<h4>Areas Location</h4>
													<div class="accordion-content">
														<div class="checkboxradio-group">
															<?php 
															$term_locs = get_terms( array(
																'taxonomy' => 'mphb_room_type_loc',
																'hide_empty' => false,
															) );
															foreach ($term_locs as $key => $term_loc) {
															# code...

																?>
																<div class="checkboxradio">
																	<div class="checkboxradio-row">
																		<input class="checkboxradio-item checkboxradio-invisible villa_loc" name="<?php echo $term_loc->slug; ?>" type="checkbox" id="<?php echo $term_loc->slug; ?>" value="<?php echo $term_loc->slug; ?>" />
																		<label class="checkboxradio-label checkbox-label" for="<?php echo $term_loc->slug; ?>"><?php echo $term_loc->name; ?>
																	</label>
																</div>
															</div>
															<?php 
															}
															?>
														</div>
													</div>
												</li>
												<li class="checkbox-select  accordion-row-blk">
													<h4>Villa Style</h4>
													<div class="accordion-content">
														<div class="checkboxradio-group">
															<?php 
															$term_styles = get_terms( array(
																'taxonomy' => 'mphb_room_type_style',
																'hide_empty' => false,
															) );
															foreach ($term_styles as $key => $term_style) {
																# code...

																?>
																<div class="checkboxradio">
																	<div class="checkboxradio-row">
																		<input class="checkboxradio-item checkboxradio-invisible villa_style" name="<?php echo $term_style->slug; ?>" type="checkbox" id="<?php echo $term_style->slug; ?>" value="<?php echo $term_style->slug; ?>" />
																		<label class="checkboxradio-label checkbox-label" for="<?php echo $term_style->slug; ?>"><?php echo $term_style->name; ?>
																	</label>
																</div>
															</div>
															<?php 
															}
															?>
												
														</div>
													</div>
												</li>
												<li class="checkbox-select accordion-row-blk">
													<h4>Facilites</h4>
													<div class="accordion-content">
														<div class="checkboxradio-group">
																<?php 
																$term_facilites = get_terms( array(
																	'taxonomy' => 'mphb_room_type_facility',
																	'hide_empty' => false,
																) );
																foreach ($term_facilites as $key => $term_facilites) {
																		# code...
																	
																	?>
																	<div class="checkboxradio">
																		<div class="checkboxradio-row">
																			<input class="checkboxradio-item checkboxradio-invisible villa_style" name="<?php echo $term_facilites->slug; ?>" type="checkbox" id="<?php echo $term_facilites->slug; ?>" value="<?php echo $term_facilites->slug; ?>" />
															<label class="checkboxradio-label checkbox-label" for="<?php echo $term_facilites->slug; ?>"><?php echo $term_facilites->name; ?>
																		</label>
																	</div>
																</div>
																<?php 
															}
															?>
														</div>
													</div>
												</li>

												<li class="checkbox-select accordion-row-blk">
													<h4>Holiday Occasion</h4>
													<div class="accordion-content">
														<div class="checkboxradio-group">
															<?php 
															$term_occs = get_terms( array(
																'taxonomy' => 'mphb_room_type_occ',
																'hide_empty' => false,
															) );
															foreach ($term_occs as $key => $term_occ) {
																	# code...

																?>
																<div class="checkboxradio">
																	<div class="checkboxradio-row">
																		<input class="checkboxradio-item checkboxradio-invisible villa_occs" name="<?php echo $term_occ->slug; ?>" type="checkbox" id="<?php echo $term_occ->slug; ?>" value="<?php echo $term_occ->slug; ?>" />
																		<label class="checkboxradio-label checkbox-label" for="<?php echo $term_occ->slug; ?>"><?php echo $term_occ->name; ?>
																	</label>
																</div>
															</div>
															<?php 
														}
														?>
													</div>
												</div>
											</li>
										</ul>
										<div class="flex-jsb">
											<div class="button button-secondary button-small">
												<button type="button" class="button clear-filters">Clear</button>
											</div>
											<div class="button button-primary button-small">
												<button type="button " class="apply-filters button">Apply Filter</button>
											</div>
										</div>
									</div>
							</div>
						</div>
					<nav class="menu-open" style="display: none;">
					<a href="#" class="menu-close"><i class="fa fa-times" aria-hidden="true"></i></a>
					<?php
					if(is_user_logged_in()) {
						$dashboard = '<li>
						<a href="javascript:void(0);" class="no-effect"><i class="fa fa-user"></i></a>
						<a href="'.get_bloginfo('url').'/my-account/">My account</a>
						<a href="'. wp_logout_url( home_url() ).'">Logout</a>
						</li>
						';
						
					} else {
						$dashboard = '<li> 
						<a href="javascript:void(0);" data-id="signin">Sign in</a>
						</li>
						<li>
						<a href="javascript:void(0);" data-id="signup">Sign up</a>
						</li>';
						
					}
					echo ''.$heart.'<li><a href="javascript:void(0);" class="no-effect"><i class="fa fa-user"></i></a><div class="sub-nav sign-nav"><ul>'.$dashboard.'</ul></div></li></ul>';
					echo '</ul>';
							?>
					</nav>
					</div>
						
<div class="col mobile complete-search">
							<div class="button button-primary">
								<button>Search</button>
							</div>
						</div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>
</header>
<?php get_template_part('template-parts/signup'); ?>
<?php get_template_part('template-parts/forget_password'); ?>