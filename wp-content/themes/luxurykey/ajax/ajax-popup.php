<?php
$load_url = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
include $load_url[0] . 'wp-load.php';
global $wpdb;
global $_POST;
if (isset($_POST)) {
    $data['name']    = sanitize_text_field($_POST['name']);
    $data['email']   = sanitize_text_field($_POST['email']);
    $data['checkin'] = sanitize_text_field($_POST['chk_in']);
    $data['checkout'] = sanitize_text_field($_POST['chk_out']);
    $data['nobeds'] = sanitize_text_field($_POST['no_beds']);
    $data['prc_typ'] = sanitize_text_field($_POST['prc_typ']);
    $data['name'] = ucwords($data['name']);
    $table = "l2rky_pvtvilla_usr";
    $insert_booking = $wpdb->insert($table, $data);
    $lastid = $wpdb->insert_id; 
    if($lastid!=''){

      $apiKey = '7c069b2762a552bde31f085561408222-us3';
        $listID = '98d67bb782';
        
        // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
        $fname = sanitize_text_field($_POST['name']);
        // member information
        $json = json_encode([
            'email_address' => $data['email'],
            'status'        => 'subscribed',
            'merge_fields'  => [
                'FNAME'    => $fname,
                'CHKIN'    => $data['checkin'],
                'CHKOUT'    => $data['checkout'],
                'NOBEDS'    => $data['nobeds'],
                'PRCTYP'    => $data['prc_typ'],

            ]
        ]);
        
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // store the status message based on response code
        if ($httpCode == 200) {
            echo '<p style="color: #34A853">You have successfully subscribed .</p>';
        } else {
            switch ($httpCode) {
                case 214:
                    echo 'You are already subscribed.';
                break;
                default:
                    echo 'Some problem occurred, please try again.';
                    break;
            }
            
        }
    }else{
        echo '<p style="color: #EA4335">Please enter valid email address.</p>';
    }
    
}

?>  



