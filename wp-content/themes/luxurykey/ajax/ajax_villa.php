<?php
global $_POST;
$load_url = explode('wp-content', $_SERVER['SCRIPT_FILENAME']);
include $load_url[0].'wp-load.php';
$noOfPost = -1;
$categoryId = $_POST['categoryId'];
$eventPostsArgs = array(
    'post_type' => 'villas',
    'post_status' => 'publish',
    'orderby' => 'date',
    'order' => 'DESC',
    'numberposts' => $noOfPost,
          ))

);

  
    $meta_query = array('relation' => 'AND');
 
    if(isset($_GET['villa_locations'])) {
        $villa_locations = sanitize_text_field( $_GET['villa_locations'] );
        $meta_query[] = array(
            'key' => 'villa_locations',
            'value' => $villa_locations,
            'compare' => '='
        );
    }
 
    if(isset($_GET['villa_price'])) {
        $villa_price = sanitize_text_field( $_GET['villa_price'] );
        $meta_query[] = array(
            'key' => 'villa_price',
            'value' => $villa_price,
            'compare' => '>='
        );
    }
 
    if(isset($_GET['villa_bed'])) {
        $villa_bed = sanitize_text_field( $_GET['villa_bed'] );
        $meta_query[] = array(
            'key' => 'villa_bed',
            'value' => $villa_bed,
            'compare' => '='
        );
    }


     if(isset($_GET['villa_type'])) {
        $villa_type = sanitize_text_field( $_GET['villa_type'] );
        $meta_query[] = array(
            'key' => 'villa_type',
            'value' => $villa_type,
            'compare' => '='
        );
    }
     if(isset($_GET['villa_purpose'])) {
        $villa_purpose = sanitize_text_field( $_GET['villa_purpose'] );
        $meta_query[] = array(
            'key' => 'villa_purpose',
            'value' => $villa_purpose,
            'compare' => '='
        );
    }
    
     if(isset($_GET['villa_chkin'])) {
        $villa_chkin = sanitize_text_field( $_GET['villa_chkin'] );
        $meta_query[] = array(
            'key' => 'villa_chkin',
            'value' => $villa_chkin,
            'compare' => '='
        );
    }
     if(isset($_GET['villa_chkout'])) {
        $villa_chkout = sanitize_text_field( $_GET['villa_chkout'] );
        $meta_query[] = array(
            'key' => 'villa_chkout',
            'value' => $villa_chkout,
            'compare' => '='
        );
    }

   
     if(isset($_GET['villa_filter'])) {
        $villa_filter = sanitize_text_field( $_GET['villa_filter'] );
        $meta_query[] = array(
            'key' => 'villa_filter',
            'value' => $villa_filter,
            'compare' => '='
        );
    }




$eventPosts = get_posts($eventPostsArgs);

if(count($eventPosts)>0) {
foreach ($eventPosts as $artPost) {
              $villa_locations  = get_post_meta( $artPost->ID, 'villa_locations', true );
              $villa_price  = get_post_meta( $artPost->ID, 'villa_price', true );
              $villa_bed  = get_post_meta( $artPost->ID, 'villa_bed', true );
              $villa_type  = get_post_meta( $artPost->ID, 'villa_type', true );
              $villa_purpose  = get_post_meta( $artPost->ID, 'villa_purpose', true );
              $villa_chkin  = get_post_meta( $artPost->ID, 'villa_chkin', true );
              $villa_chkout  = get_post_meta( $artPost->ID, 'villa_chkout', true );
              $villa_filter  = get_post_meta( $artPost->ID, 'villa_filter', true );

    echo '<div class="col-4">
            <a href="#" class="cards content-only">
            <div class="cards-content">
            <h6>'.$artPost->post_title.'</h6>
            <h4> '.$artPost->post_content.'</h4>
            <p><strong>'.$villa_locations.'</strong></p>
            </div>
            </a>
            </div>';
}
} else {
  echo '<div class="center-block"><h4>Sorry! No results found.</h4></div>';
}
?>