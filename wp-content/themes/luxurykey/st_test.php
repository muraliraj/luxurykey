<?php
/***********************
Template Name: test
************************/
/*
$apiUrl = get_option('api_endpoint');
	$username = get_option('api_username');
	$password = get_option('api_password');
	$authorization = 'Basic '. base64_encode($username.":".$password);
	$feedUrl = $apiUrl.'/room/LUXURYKEYS';

	$ch = curl_init();
    $headers = array(
		'Content-Type: application/x-www-form-urlencoded',
		'charset: utf-8',
		'Authorization:'. $authorization,
    );
    curl_setopt($ch, CURLOPT_URL, $feedUrl);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    $output = curl_exec($ch);
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $body = substr($output, $header_size);
    $villaXmlData = new SimpleXMLElement($body);
    curl_close($ch);


    $villaData = $villaXmlData->data->rooms->room;

    foreach ($villaData as $key => $villa) {
    	echo '$villaCode  	=====>'.$villaCode = $villa->code;
    	echo "<br>";
		echo '$villaName  	=====>'.$villaName = $villa->name;
		echo "<br>";
		echo '$villaDescription  	=====>'.$villaDescription = $villa->description;
		echo "<br>";
		echo '$minPersons  	=====>'.$minPersons = $villa->capacity->min_pers;
		echo "<br>";
		echo '$maxPersons  	=====>'.$maxPersons = $villa->capacity->max_pers;
		echo "<br>";
		echo '$maxAdults  	=====>'.$maxAdults = $villa->capacity->max_adults;
		echo "<br>";
		echo '$isChildrenAllowed  	=====>'.$isChildrenAllowed = $villa->capacity->children_allowed;
		echo "<br>";
		$photos = $villa->photos->photo;
		foreach ($photos as $photo) {
			echo '$xsmallImage  	=====>'.$xsmallImage = $photo->xsmall;
			echo "<br>";
			echo '$smallImage  	=====>'.$smallImage = $photo->small;
			echo "<br>";
			echo '$mediumImage  	=====>'.$mediumImage = $photo->medium;
			echo "<br>";
			echo '$largeImage  	=====>'.$largeImage = $photo->large;
			echo "<br>";
		}
    	
    }

 get_footer();

*/
$roomsAtts = array(
'availability'	 => 'locked',
'from_date'		 => \DateTime::createFromFormat(	MPHB()->settings()->dateTime()->getDateTransferFormat(), '2019-05-15' ),
'to_date'		 => \DateTime::createFromFormat( MPHB()->settings()->dateTime()->getDateTransferFormat(), '2019-05-21' )
		);
$result =  getAvailableRoomTypes_custom($roomsAtts);
// echo "<pre>";
 // print_r($result);
 foreach ($result as $value) {
 	echo "<br>";
 	echo $value['id'];
 }

  ?>
