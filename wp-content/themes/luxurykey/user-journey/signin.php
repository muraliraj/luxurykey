<!-- popup start -->
<?php
?>

<div id="signin" class="loginpopup">
<div class="fullbanner">
<img src="<?php echo get_bloginfo('template_url'); ?>/img/banner5.jpeg">
</div>
<div class="white-card">
<div class="signinhead">
  <h2>Sign In</h2>
</div>
<form id="wp_login_form" action="" method="post"> 
<div class="registration-form">
  <div class="form-row">
    <label class="floating-item" data-error="Please enter your first name">
      <input type="text" id="user_name" class="floating-item-input input-item" name="username" value="" />
      <span class="floating-item-label">Email address</span>
    </label>
    <div class="error-message" id="err_name1">Please enter your email address</div>
  </div>
  <div class="form-row">
    <label class="floating-item" data-error="Please enter your first name">
      <input type="Password" id="pass_word" class="floating-item-input input-item" name="password" value="" />

      <span class="floating-item-label">Password</span>
    </label>
     <div class="error-message" id="err_register_password1">Please enter your password</div>
  </div>
  <div class="forget">
    <div class="form-row checkboxradio">
      <div class="checkboxradio-row">
        <input class="checkboxradio-item checkboxradio-invisible" name="rememberme"  id="checkbox1" type="checkbox" />
        <label class="checkboxradio-label checkbox-label"  for="checkbox1">Remeber me</label>
      </div>
    </div>
    <div class="forget-text"><a href="javascript:void(0);" data-id="forget">Forgot password?</a></div>
  </div>
  <div class="clearfix">
  <!-- <input type="submit" id="submitbtn" name="submit" class="button button-primary fR" value="Login">  -->
<div class="button button-primary fR">
  <button  type="submit" id="sign-in" name="submit" >Login</button>
</div>
      <!-- <div class="button button-primary fR">
      <a href="#">Submit</a>
     </div> -->
</div>
<div class="account-link">Don’t have an account?<a href="#" data-id="signup">Sign up</a></div>
</div>
</form>

<div class="login-close"><img src="<?php echo get_bloginfo('template_url'); ?>/img/close.png"></div>
</div>
</div>
