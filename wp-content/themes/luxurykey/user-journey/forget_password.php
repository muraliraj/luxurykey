<div id="forget" class="loginpopup">
  <div class="fullbanner">
    <img src="<?php echo get_bloginfo('template_url'); ?>/img/banner5.jpeg">
  </div>
  <div class="white-card">
    <div class="signinhead">
      <h2>Forget password</h2>
    </div>
    <div class="registration-form">
      <div class="form-row">
        <label class="floating-item" data-error="Please enter your Email Id">
          <input type="text" id="mail" class="floating-item-input input-item" name="mail" value="" required />
          <span class="floating-item-label">Enter your mail Id</span>
        </label>
      </div>
    </div>
    <div class="clearfix">
      <div class="button button-primary fR">
        <a href="#">Submit</a>
      </div>
    </div>
  </div>
</div>
<!-- popup end -->
