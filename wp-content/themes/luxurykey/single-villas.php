<?php
get_header();
$villa_bath   = get_post_meta( $post->ID, 'villa_bath', true );
$villa_pool  = get_post_meta( $post->ID, 'villa_pool', true );
$villa_guest  = get_post_meta( $post->ID, 'villa_guest', true );
$villa_address  = get_post_meta( $post->ID, 'villa_address', true );
$villa_bed  = get_post_meta( $post->ID, 'villa_bed', true );
$villa_type  = get_post_meta( $post->ID, 'villa_type', true );
$price = get_post_meta( $post->ID, 'villa_price', true );
$checkin = get_post_meta( $post->ID, 'villa_chkin', true );
$checkout = get_post_meta( $post->ID, 'villa_chkout', true );
$villaCode = get_post_meta( $post->ID, 'villa_code', true );
$thumbID = get_post_thumbnail_id( $post->ID );
?>

<div class="hero-detail">
<div class="detail-banner">
  <?php
  if( class_exists('Dynamic_Featured_Image') ):
    global $dynamic_featured_image;
    global $post;
    $featured_images = $dynamic_featured_image->get_featured_images( $post->ID );
    if ( $featured_images ):
        foreach( $featured_images as $images ):
          ?>
            <a data-fancybox="test-srcset"
              data-type="image"
              data-srcset="<?php echo $images['full']; ?>"
              data-width="1200"
              data-height="720"
              class="fancy-box"
              href="<?php echo $images['full']; ?>" >
              <img width="240" src="<?php echo $images['full'];?>" />
            </a>
          <?php
        endforeach;
    endif;
  endif;
    ?>
</div>
</div>
<!-- villa detail short -->
<?php if($villa_bed!='' || $villa_guest !='' || $villa_bath !='' || $villa_pool !='') { ?>
<div class="detail-intro">
  <p><?php echo $villa_address;?></p>
  <div class="amenities">
  <?php  if($villa_bed !=''){ ?>
    <div><img src="<?php echo get_bloginfo('template_url'); ?>\img\bed-black.svg" alt="bedroom"><span><?php echo $villa_bed;?></span>Bedroom</div>
  <?php } ?>
  <?php  if($villa_guest !=''){ ?>
    <div><img src="<?php echo get_bloginfo('template_url'); ?>\img\person-black.svg" alt="guest"><span><?php echo $villa_guest;?></span>Guests</div>
  <?php } ?>
  <?php  if($villa_bath !=''){ ?>
    <div><img src="<?php echo get_bloginfo('template_url'); ?>\img\tub.svg" alt="bath"><span><?php echo $villa_bath;?></span>Bath</div>
  <?php } ?>
  <?php  if($villa_pool !=''){ ?>
    <div><img src="<?php echo get_bloginfo('template_url'); ?>\img/svg/poolblack.svg" alt="pool"><span><?php echo $villa_pool;?></span>Pool</div>
  <?php } ?>
  </div>
  <div class="price">From <span><?php echo '&euro;'.$price; ?></span> per/night</div>
  <?php  if($villa_type !=''){ ?>
  <div class="review-shell d-flex">
    <div class="col-6">
      <h6><?php echo $villa_type;?></h6>
    </div>
  </div>
   <?php } ?>
</div>
<?php } ?>
<!-- villa detail tile -->
<div class="detail-tile">
  <div class="detail-tile-col">
  <?php echo apply_filters('the_content', $post->post_content); ?>    


    <!-- <div class="detail-row">
      <div class="title-line">
        <h3>What Guests are Saying</h3>
      </div>
      <div class="listFilter">
        <p>Sort by</p>
        <span class="filter-placeholder">Most Relavant</span>
        <ul>
          <li>Most Relavant</li>
          <li>Most Relavant</li>
        </ul>
      </div>
    </div>
    <div class="review-block">
      <div class="user">
        <img src="<?php echo get_bloginfo('template_url'); ?>\img\thumb-temp.png" alt="">
        <div class="user-name">
          <h6>Nicky</h6>
          <p>December 2018</p>
        </div>
      </div>
      <p class="more">We fell in love with the apartment the moment we entered. The location is amazing and the apartment is beautiful. The rooms are very spacious and provide a beautiful picturesque view. The apartment is so airy and the sunlight beams in, which I love the most. Feels lively. Sit with a cup of coffee in the evening, you can feel the calmness there. I canâ€™t describe how beautiful and cozy it was. The kitchen has a modern touch to it and the counter is so cute. He also provided us with.</p>
    </div>
    <div class="review-block">
      <div class="user">
        <img src="<?php echo get_bloginfo('template_url'); ?>\img\thumb-temp.png" alt="">
        <div class="user-name">
          <h6>Nicky</h6>
          <p>December 2018</p>
        </div>
      </div>
      <p class="more">We fell in love with the apartment the moment we entered. The location is amazing and the apartment is beautiful. The rooms are very spacious and provide a beautiful picturesque view. The apartment is so airy and the sunlight beams in, which I love the most. Feels lively. Sit with a cup of coffee in the evening, you can feel the calmness there. I canâ€™t describe how beautiful and cozy it was. The kitchen has a modern touch to it and the counter is so cute. He also provided us with.</p>
    </div> -->
  </div>
  <div class="detail-tile-col fixed">
    <div class="detail-form">
      <div class="title-line">
        <h3>Booking Details</h3>
      </div>
      <div class="form-row">
  <label class="floating-item">
    <input type="text" id="first-name" class="floating-item-input input-item" value="<?php echo $villa_bed; ?> Bedroom" readonly />
  </label>
  </div>
  <div class="form-row select-list">
    <label class="floating-item">
      <span class="input-item" id="laneconfigdisplay">Check In <i class="fa fa-arrow-right"></i> Check Out</span>
    </label>
  </div>
  <input type="hidden" name="datetonights" id="numNights" value="">
  <span class="err">There is a 7 night minimum for these dates. Extend your stay or inquire below.</span>
  <div class="detail-row price-detail">
    <?php 
$date1 = new DateTime($checkin);
$date2 = new DateTime($checkout);

// this calculates the diff between two dates, which is the number of nights
$numberOfNights= $date2->diff($date1)->format("%a");
if($numberOfNights==0) {
    $numberOfNights=1;
}
?>
<?php if($price!='') { ?>
    <h6>&euro;<span class="pricepernight"><?php echo $price; ?></span> x <span class="nights"><?php echo $numberOfNights; ?></span> Night<?php if($numberOfNights!=1) { ?>s<?php } ?></h6>
    
    <h6>&euro;<span class="total"><?php echo $price*$numberOfNights; ?></span></h6>
  <?php } ?>
  </div>
  <div class="detail-row price-detail">
    <?php if($price!='') { ?>
    <h6>Payable Amount</h6>
    <h6>&euro;<span class="total"><?php echo $price*$numberOfNights; ?></span></h6>
  <?php } ?>
  </div>

  <div class="detail-row">
    <!-- <div class="button button-secondary">
      <button>Inquire</button>
    </div> -->
    <form action="https://luxurykey.reserve-online.net/" method="get">

  <!-- Identify your business so that you can collect the payments. -->

  <!-- Specify a Buy Now button. -->
    <input type="hidden" id="payment-nights" name="nights" value="<?php echo $numberOfNights; ?>">
   <input type="hidden" id="checkin" name="checkin" value="">
   <input type="hidden" id="checkout" name="checkout" value="">
   <input type="hidden" id="infants" name="infants" value="0">
    <input type="hidden" id="property" name="property" value="<?php echo $villaCode; ?>">
    <input type="hidden" name="currency" value="EUR">
    <input type="hidden" name="email" value="test@test.com">
  

  <!-- Specify details about the item that buyers will purchase. -->

  <!-- Display the payment button. -->
  
  <!-- <img alt="" border="0" width="1" height="1"
  src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" > -->
<div class="button button-primary">
      <button>Continue to Booking</button>
    </div>
</form>

  </div>
</div>
  </div>
</div>
<!-- villas card section -->
<div class="container container-type1 flexed-listing no-padding" id="flexed-listing">
<div class="row">

<?php 
$villargs = array(
    'numberposts' =>2,
    'post_type' => 'villas',
    'orderby' => 'menu_order',
    'order' => 'ASC',
    'post_status'=>"publish",
    'exclude'=>$post->ID
  );
$villapages = get_posts($villargs);
foreach ($villapages as $villapage) {
  $featImage = wp_get_attachment_url(get_post_thumbnail_id($villapage->ID) );
  $villa_bath   = get_post_meta( $villapage->ID, '_villa_bath', true );
  $villa_pool  = get_post_meta( $villapage->ID, '_villa_pool', true );
  $villa_guest  = get_post_meta( $villapage->ID, '_villa_guest', true );
  $villa_address  = get_post_meta( $villapage->ID, '_villa_address', true );
  $villa_bed  = get_post_meta( $villapage->ID, '_villa_bed', true );
  $villa_type  = get_post_meta( $villapage->ID, '_villa_type', true );
  $price = get_post_meta( $villapage->ID, '_villa_price', true );
?>
<div class="col-6">
  <div class="xlist-item">
        <img src="<?php echo $featImage; ?>" alt="">
        <div class="xlist-item-feature">
          <?php if($villa_bed !=''){ ?>
          <div>
          <img src="<?php echo get_bloginfo('template_url'); ?>\img/svg/bed.svg" alt="images">
          <p><?php echo $villa_bed; ?></p>
        </div>
        <?php } ?>
        <?php if($villa_guest !=''){ ?>
      <div>
        <img src="<?php echo get_bloginfo('template_url'); ?>\img/person.svg" alt="images">
        <p><?php echo $villa_guest; ?></p>
      </div>
    <?php } ?>
    <?php if($villa_bath !=''){ ?>
      <div>
        <img src="<?php echo get_bloginfo('template_url'); ?>\img/svg/bath.svg" alt="images">
        <p><?php echo $villa_bath; ?></p>
      </div>
    <?php } ?>
    <?php if($villa_pool !=''){ ?>
      <div>
        <img src="<?php echo get_bloginfo('template_url'); ?>\img/svg/pool.svg" alt="images">
        <p><?php echo $villa_pool; ?></p>
      </div>
      <?php } ?>
        </div>
        <div class="xlist-item-desc">
          <div>
            <h4><?php echo $villapage->post_title; ?></h4>
          <p><?php echo $villapage->post_excerpt; ?></p>
        <p>From: <?php echo '&euro;'.$price; ?> / per night</p>
      </div>
      <div class="button button-primary button-small">
        <a href="<?php echo get_permalink($villapage->ID); ?>" tabindex="0">view villa</a>
      </div>
        </div>
      </div>
</div>
<?php } ?>


</div>
</div>
<?php get_footer(); ?>
